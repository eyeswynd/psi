/*
 * Transmitter.java
 *
 * Created on den 13 januari 2005, 11:17
 */
package se.mpab.psiinternal.adapters;

/**
 *
 * @author viktor.norberg
 */
public interface Transmitter {

    public String transmittMessage(String address, String message) throws Exception;

    public String transmitMessageBasicAuth(String address, String username, String password, String message) throws Exception;
}
