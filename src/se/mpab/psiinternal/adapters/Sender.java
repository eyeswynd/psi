/*
 * Sender.java
 *
 * Created on den 11 januari 2005, 10:47
 */

package se.mpab.psiinternal.adapters;

import de.bea.domingo.DDocument;


/**
 *
 * @author  viktor.norberg
 */
public interface Sender {
    public static boolean STATUS_OK = true;
    public static boolean STATUS_FAILED = false;
    public boolean send(se.mpab.psi.user.Alerter alerter,se.mpab.psi.bo.Deal deal, String method);
    public String getMessage();
    public String getResponse();
}


