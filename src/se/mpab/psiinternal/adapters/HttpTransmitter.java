/*
 * HttpTransmitter.java
 *
 * Created on den 13 januari 2005, 11:19
 */
package se.mpab.psiinternal.adapters;

import java.net.URL;
import java.net.URLEncoder;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.io.OutputStreamWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Properties;
import org.apache.log4j.LogManager;
import java.security.*;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Viktor Norberg, Marquardt & Partners AB
 */
public class HttpTransmitter implements Transmitter {

    private static org.apache.log4j.Logger logger = LogManager.getLogger(HttpTransmitter.class);

    public String transmittMessage(String address, String message) throws java.net.MalformedURLException, java.io.IOException {

        // Send data
        System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
        logger.debug("Transmitter: Sending message to [" + address + "]");
        byte[] postData       = message.getBytes( "utf-8" );
        int    postDataLength = postData.length;
        URL url = new URL(address);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects( false );
        connection.setRequestMethod( "POST" );
        connection.setRequestProperty( "Content-Type", "application/xml");
        connection.setRequestProperty( "Accept", "application/xml"); 
        connection.setRequestProperty( "charset", "utf-8");
        connection.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
        connection.setUseCaches( false );
        OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
        wr.write(message);
        wr.flush();

        logger.debug("Transmitter: Sending message to [" + address + "]");
        logger.debug("Transmitter: Message [" + message + "]");

        BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        StringBuffer buffer = new StringBuffer();
        while ((line = rd.readLine()) != null) {
            buffer.append(line);
        }
        wr.close();
        rd.close();
        logger.debug("REQUEST: " + message + " -----------------------\\nRESPONSE: " + buffer.toString());
        return buffer.toString();
    }

    public String transmitMessageBasicAuth(String address, String username, String password, String message) throws java.net.MalformedURLException, java.io.IOException {
        // Send data
        System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
        logger.info("Transmitter: Sending message to [" + address + "]");

        String authString = username + ":" + password;
        //logger.info("Auth string: " + authString);
        logger.info("Username: " + username);        
        String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(authString.getBytes());
        logger.info("Base64 encoded auth string: " + basicAuth);

        URL url = new URL(address);
        URLConnection connection = url.openConnection();
        connection.setRequestProperty("Authorization", basicAuth);

        connection.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
        wr.write(message);
        wr.flush();

        logger.debug("Transmitter: Sending message to [" + address + "]");
        logger.debug("Transmitter: Message [" + message + "]");

        BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        StringBuffer buffer = new StringBuffer();
        while ((line = rd.readLine()) != null) {
            buffer.append(line);
            //buffer.append('\n');
        }
        wr.close();
        rd.close();
        logger.debug("Sent : " + message + " -----------------------\\n RESULT form itfos [" + buffer.toString() + "]");
        return buffer.toString();
    }
}
