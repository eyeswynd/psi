/*
 * ItfosSender.java
 *
 * Created on den 11 januari 2005, 09:59
 */

package se.mpab.psiinternal.adapters;

import org.jdom.Element;

import se.mpab.psi.mo.ItfosMessage;
import se.mpab.psi.mo.EntreMessageBody;
import se.mpab.psi.mo.EntreMessageHeader;
import se.mpab.psi.mo.CreditApplication;
import se.mpab.psi.mo.ItfosResult;
import se.mpab.psi.db.PSIUserDAO;
import se.mpab.psi.bo.Deal;
import se.mpab.psi.user.Alerter;
import se.mpab.psi.PSIMessageSave;

import java.net.URL;
import java.net.URLEncoder;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.io.OutputStreamWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Properties;
import org.apache.log4j.LogManager;
import org.jdom.input.SAXBuilder;


/**
 *
 * @author  viktor.norberg
 */
public class ItfosSender implements Sender {
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(ItfosSender.class);
    private String response;
    private String message;
    /** Creates a new instance of ItfosSender */
    public ItfosSender()  {
        
    }
    
    public boolean send(se.mpab.psi.user.Alerter alerter,se.mpab.psi.bo.Deal deal, String method) {
        logger.info("Send ItfosMessage");
        boolean status = false;
        try{
            
            SAXBuilder builder = new SAXBuilder();
            ItfosResult itfosResult = new ItfosResult();
            org.jdom.Document jDomDoc = null;
            
            //Build message
            ItfosMessage itfosMessage = new ItfosMessage();
            EntreMessageHeader messageHeader = new EntreMessageHeader();
            messageHeader.setPassword(alerter.getPassword());
            messageHeader.setUser(alerter.getUsername());
            messageHeader.setService("changeCreditApplication");
            itfosMessage.setHeader(messageHeader);
            EntreMessageBody messageBody = new EntreMessageBody();
            CreditApplication ca = deal.checkCreditApplication();
            messageBody.setCreditApplication(ca);
            itfosMessage.setBody(messageBody);
            Element k = itfosMessage.getElement(org.jdom.Namespace.getNamespace("", ""));
            org.jdom.output.XMLOutputter outputter = new org.jdom.output.XMLOutputter(org.jdom.output.Format.getPrettyFormat());
            //outputter.setEncoding("ISO-8859-1");
            Transmitter transmitter = TransmitterFactory.getHttpTransmitter();
            //TODO hardcoded this because IT-FOS needs it
            message = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + outputter.outputString(k);
            //Saving message
               PSIMessageSave psiMessageSave = new PSIMessageSave();
               psiMessageSave.save(false,
                     "<!--" + alerter.getUrl() + "-->\n" + message,
                   deal.getStock(),
                 alerter.getUsername());
            
            setResponse(transmitter.transmittMessage(alerter.getUrl(), message));
            //  logger.debug( "RawDATA :[" + logRawData(new java.io.StringReader(response)) + "]") ;
            jDomDoc = builder.build( new java.io.StringReader(getResponse()));
            org.jdom.Element e = jDomDoc.getRootElement();
            itfosResult.initFromElement(e);
            if(itfosResult.getResultCode() == 0){
                status = true;
            } else{
                logger.debug("Status form itfos was not 0 = OK but " + itfosResult.getResultCode() + "  \n" +getResponse() );
                status = false;
            }
        } catch(org.jdom.JDOMException jdome){
            status = true;
            logger.error(jdome.getMessage(),jdome);
        } catch(Exception e){
            status = false;
            logger.error(e.getMessage() ,e);           
        }
        return status;
    }
    
    private String logRawData(java.io.StringReader sr ){
        StringBuffer buf = new StringBuffer();
        try {
            int c = sr.read();
            while ( c != -1 ) {
                //   System.out.print( (char) c );
                c = sr.read();
                buf.append((char)c);
            }
            sr.close();
            
        } catch ( java.io.IOException e ) {
            logger.error("Error in logRawData : " , e );
        }
        return buf.toString();
    }
    
    public String getResponse() {
        return response;
    }
    
    public void setResponse(String response) {
        this.response = response;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
}
