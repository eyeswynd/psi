/*
 * SenderFactory.java
 *
 * Created on den 11 januari 2005, 10:41
 */

package se.mpab.psiinternal.adapters;

/**
 *
 * @author  viktor.norberg
 */
public final class SenderFactory {
    
    /** Creates a new instance of SenderFactory */
    private SenderFactory() {
    }
    
    public static Sender getItfosSender() {
        return new ItfosSender();
    }
    
    public static Sender getAteaSender() {
        return new AteaMessageSender();
    }
    
    public static Sender getDustinMessageSender() {
        return new DustinMessageSender();
    }
    
    public static Sender getAsedoMessageSender() {
        return new AsedoMessageSender();
    }
}
