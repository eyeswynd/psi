/*
 * TransmitterFactory.java
 *
 * Created on den 13 januari 2005, 11:35
 */

package se.mpab.psiinternal.adapters;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public final class TransmitterFactory {
        
    private TransmitterFactory() {
    }
    
    public static Transmitter getHttpTransmitter() {
        return new HttpTransmitter();
    }
    
}
