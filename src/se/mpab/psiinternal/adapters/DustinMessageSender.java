/*
 * AteaMessageSender.java
 *
 * Created on den 27 januari 2015, 09:59
 */
package se.mpab.psiinternal.adapters;

import java.io.ByteArrayInputStream;
import java.util.Iterator;
import org.apache.log4j.LogManager;
import org.jdom.Element;
import org.jdom.filter.ElementFilter;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;
import se.mpab.psi.PSIMessageSave;
import se.mpab.psi.bo.Agreement;
import se.mpab.psi.bo.Calculation;
import se.mpab.psi.bo.Credit;
import se.mpab.psi.mo.AgreementApplication;
import se.mpab.psi.mo.AteaMessage;
import se.mpab.psi.mo.AteaMessageResult;
import se.mpab.psi.mo.CreditApplication;
import se.mpab.psi.mo.EntreMessageBody;
import se.mpab.psi.mo.EntreMessageHeader;

/**
 *
 * @author hans.olsberg
 */
public class DustinMessageSender implements Sender {

    private static org.apache.log4j.Logger logger = LogManager.getLogger(DustinMessageSender.class);
    private String response;
    private String message;

    /**
     * Creates a new instance of SendMessageSender
     */
    public DustinMessageSender() {

    }

    public boolean send(se.mpab.psi.user.Alerter alerter, se.mpab.psi.bo.Deal deal, String method) {
        logger.info("Send Dustin Message");
        boolean status = false;

        try {
            
            CreditApplication ca = deal.checkCreditApplication();
            
            //UGLY FIX BECAUSE CHANGES TO DOCUMENTS ARE SOMETIMES NOT UPDATED FOR PSI AT FIRST
            if(ca.getDecision().getStatus() == "PENDING") {
                logger.info("Credit status still pending. Abort and wait for next run!");
                return false;
            }

            org.jdom.Document requestDoc = new org.jdom.Document();
            String url = alerter.getUrl();
            Element root = null;
            if (method.equals("ChangeCreditApplication")) {
                url = url + "creditapplication";
                root = new Element("CreditApplicationModel");
                root.addContent(new Element("ApplicationId").setText(ca.getDealNumber().getId() + "-" + ca.getDealNumber().getVersion()));
                root.addContent(new Element("Status").setText(ca.getDecision().getStatus()));
                requestDoc.setRootElement(root);
            }
            else if (method.equals("ChangeAgreementApplication")) {
                url = url + "signagreement";
                root = new Element("SignAgreementModel");
                root.addContent(new Element("ApplicationId").setText(ca.getDealNumber().getId() + "-" + ca.getDealNumber().getVersion()));
                requestDoc.setRootElement(root);
            }
            else {
                logger.info("Unknow message type, abort!");
                return false;
            }

            //Formatting
            org.jdom.output.Format format = org.jdom.output.Format.getPrettyFormat();
            format.setEncoding("utf-8");
            org.jdom.output.XMLOutputter outputter = new org.jdom.output.XMLOutputter(format);
            Transmitter transmitter = TransmitterFactory.getHttpTransmitter();
            message = outputter.outputString(requestDoc);
            logger.info("Message: " + message);
            
            //Saving message
            PSIMessageSave psiMessageSave = new PSIMessageSave();
            psiMessageSave.save(false,
                    "<!--" + alerter.getUrl() + "-->\n" + message,
                    deal.getStock(),
                    alerter.getUsername());

            setResponse(transmitter.transmittMessage(url, message));
            SAXBuilder builder = new SAXBuilder();
            org.jdom.Document responseDoc = builder.build(new InputSource(new ByteArrayInputStream(getResponse().getBytes("utf-8"))));
            String responseStatus = "";
            Iterator<?> iter = responseDoc.getDescendants(new ElementFilter("Response"));
            if(iter.hasNext())
            {
                    Element e = (Element) iter.next();
                    responseStatus = e.getTextTrim();
            }
            
            if (responseStatus.equalsIgnoreCase("OK")) {
                status = true;
            } else {
                logger.debug("Status form Dustin was not OK but" + "\n" + getResponse());
                status = false;
            }

            org.jdom.output.XMLOutputter op = new org.jdom.output.XMLOutputter(format);
            logger.debug("Response From Dustin Server: " + op.outputString(responseDoc.getRootElement()));


        } catch (org.jdom.JDOMException jdome) {
            status = true;
            logger.error(jdome.getMessage(), jdome);
        } catch (Exception e) {
            status = false;
            logger.error(e.getMessage(), e);
        }
        return status;
    }

    private String logRawData(java.io.StringReader sr) {
        StringBuffer buf = new StringBuffer();
        try {
            int c = sr.read();
            while (c != -1) {
                //   System.out.print( (char) c );
                c = sr.read();
                buf.append((char) c);
            }
            sr.close();

        } catch (java.io.IOException e) {
            logger.error("Error in logRawData : ", e);
        }
        return buf.toString();
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
