/*
 * AteaMessageSender.java
 *
 * Created on den 27 januari 2015, 09:59
 */
package se.mpab.psiinternal.adapters;

import se.mpab.psi.mo.AteaMessage;
import se.mpab.psi.mo.EntreMessageBody;
import se.mpab.psi.mo.EntreMessageHeader;
import se.mpab.psi.mo.CreditApplication;
import se.mpab.psi.mo.AteaMessageResult;
import se.mpab.psi.PSIMessageSave;

import org.apache.log4j.LogManager;
import org.jdom.input.SAXBuilder;
import se.mpab.psi.mo.AgreementApplication;

/**
 *
 * @author hans.olsberg
 */
public class AteaMessageSender implements Sender {

    private static org.apache.log4j.Logger logger = LogManager.getLogger(AteaMessageSender.class);
    private String response;
    private String message;

    /**
     * Creates a new instance of SendMessageSender
     */
    public AteaMessageSender() {

    }

    public boolean send(se.mpab.psi.user.Alerter alerter, se.mpab.psi.bo.Deal deal, String method) {
        logger.info("Send Message");
        boolean status = false;
        try {

            SAXBuilder builder = new SAXBuilder();
            AteaMessage sendMessage = new AteaMessage();
            org.jdom.Document jDomDoc = null;
            org.jdom.Document jDomDocRequest = new org.jdom.Document();
            String namespaceResponse = "http://schemas.entre.delagelanden.com/psimessage/V1";

            //Build message
            AteaMessageResult sendMessageResult = new AteaMessageResult();
            EntreMessageHeader messageHeader = new EntreMessageHeader();
            //messageHeader.setPassword(alerter.getPassword());
            //messageHeader.setUser(alerter.getUsername());
            //messageHeader.setService("AteaStatusCreditChanged");
            messageHeader.setService(method);
            sendMessage.setHeader(messageHeader);
            EntreMessageBody messageBody = new EntreMessageBody();
            CreditApplication ca = deal.checkCreditApplication();
            
            //UGLY FIX BECAUSE CHANGES TO DOCUMENTS ARE SOMETIMES NOT UPDATED FOR PSI AT FIRST
            if(ca.getDecision().getStatus() == "PENDING") {
                logger.info("Credit status still pending. Abort and wait for next run!");
                return false;
            }
            
            messageBody.setCreditApplication(ca);

            if (method.equals("ChangeAgreementApplication")) {
                AgreementApplication aa = deal.checkAgreementApplication();
                messageBody.setAgreementApplication(aa);
            }

            sendMessage.setBody(messageBody);
            jDomDocRequest.setRootElement(sendMessage.getElement(org.jdom.Namespace.getNamespace(namespaceResponse)));

            //Formatting
            org.jdom.output.Format format = org.jdom.output.Format.getPrettyFormat();
            //format.setEncoding("UTF-8");
            format.setEncoding("ISO-8859-1");

            org.jdom.output.XMLOutputter outputter = new org.jdom.output.XMLOutputter(format);
            Transmitter transmitter = TransmitterFactory.getHttpTransmitter();

            message = outputter.outputString(jDomDocRequest);

            logger.info("Send, Build message " + message);
            //Saving message
            PSIMessageSave psiMessageSave = new PSIMessageSave();
            psiMessageSave.save(false,
                    "<!--" + alerter.getUrl() + "-->\n" + message,
                    deal.getStock(),
                    alerter.getUsername());

            setResponse(transmitter.transmitMessageBasicAuth(alerter.getUrl(), alerter.getUsername(), alerter.getPassword(), message));
            //  logger.debug( "RawDATA :[" + logRawData(new java.io.StringReader(response)) + "]") ;
            jDomDoc = builder.build(new java.io.StringReader(getResponse()));
            org.jdom.Element e = jDomDoc.getRootElement();
            
            sendMessageResult.initFromElement(e);
            if (sendMessageResult.getResultCode() == 0) {
                status = true;
            } else {
                logger.debug("Status form atea was not 0 = OK but " + sendMessageResult.getResultCode() + "  \n" + getResponse());
                status = false;
            }

            //logger.debug(e.);
            org.jdom.output.XMLOutputter op = new org.jdom.output.XMLOutputter(format);
            logger.debug("Response From ATEA Server: " + op.outputString(e));

        } catch (org.jdom.JDOMException jdome) {
            status = true;
            logger.error(jdome.getMessage(), jdome);
        } catch (Exception e) {
            status = false;
            logger.error(e.getMessage(), e);
        }
        return status;
    }

    private String logRawData(java.io.StringReader sr) {
        StringBuffer buf = new StringBuffer();
        try {
            int c = sr.read();
            while (c != -1) {
                //   System.out.print( (char) c );
                c = sr.read();
                buf.append((char) c);
            }
            sr.close();

        } catch (java.io.IOException e) {
            logger.error("Error in logRawData : ", e);
        }
        return buf.toString();
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
