/*
 * Delegator.java
 *
 * Created on den 5 januari 2005, 10:21
 */

package se.mpab.psiinternal;



import java.util.Vector;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.LogManager;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import de.bea.domingo.DViewEntry;
import de.bea.domingo.DAgent;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.LogManager;
import se.mpab.psi.db.DominoDataAccess;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 *
 */
public class Delegator {
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(Delegator.class);
    
    private String status;
    /** Creates a new instance of Delegator */
    public Delegator(HttpServletRequest request) {
        
        try {
            // Check service
            String service = request.getParameter("service");
            logger.info("--------------------------- New internal request ---------------------------");
            logger.info("Service to use: " + service);
            if (service.equals("processMessageQueue")) {
                MessageProcessor mp = new MessageProcessor();
                status = mp.processMessageQue();
                
            } else if(service.equals("exportDeal")){
                try{
                    String country = request.getParameter("country");
                    String database = request.getParameter("database");
                    String unid = request.getParameter("unid");
                    se.mpab.psi.db.DominoDataAccess dataAccess = new se.mpab.psi.db.DominoDataAccess(country);
                    DDatabase db = dataAccess.openDatabase( dataAccess.getSession().getServerName(),  "entre/" + country +"/" + database);
                    DDocument doc =db.getDocumentByUNID(unid);
                    //se.mpab.psi.db.EntreDXLExporter.exportDeal(doc);
                    
                    
                } catch (Exception e ){
                    logger.error(e.getMessage(),e);
                }
            } else if(service.equals("listMessages")){
                String response = "";
    /*              try{
                    String user = request.getParameter("user");
                    int posts = -1;
                    try{
                        posts = Integer.parseInt(request.getParameter("posts"));
                    }catch(Exception e){
                        System.err.println("No posts");
                    }
                    DDatabase queue = DominoDataAccess.openDatabase(DominoDataAccess.PSI_DATABASE,"common");
                    DView incoming = queue.getView("incoming"); 
                    Iterator docCollection;
                    DDocument doc;
                    //docCollection = 
                    //DDocument doc = incoming.getLastDocument();
                    response ="<html><head><title>Messagelog</title></head><body><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse\" bordercolor=\"#111111\" width=\"90%\" >";
                    response+="<b><tr><td>ID</td><td>DealNo</td><td>Requestime</td><td>Request</td><td>Responsetime</td><td>Response</td></td></b>";
                    logger.debug("user"  + user);
                    int count = 0;
                    //while (doc != null) {
                    while (docCollection.hasNext()){
                        doc = docCollection.next();
                        if( doc.getItemValueString("PSIQuery_RequestFrom").equals(user)){
                            String query= doc.getItemValueString("PSIQuery_RequestData");
                            query =oldReplace(query,"<","&lt;");
                            query =oldReplace(query,">","&gt;");
                            String responseData = doc.getItemValueString("PSIQuery_ResponseData");
                            responseData =oldReplace(responseData,"<","&lt;");
                            responseData =oldReplace(responseData,">","&gt;");
                            Vector dtvec = (Vector) doc.getItemValueDateTimeArray("PSIQuery_RequestRecieved");
                            lotus.domino.DateTime dt=(lotus.domino.DateTime) dtvec.get(0);
                            response += "<tr>";
                            response += "<td>" + doc.getNoteID() + "</td>";
                            response += "<td>" + Integer.toString(doc.getItemValueInteger("PSIQuery_DealNo").intValue()) + "</td>";
                            response += "<td>" + dt.getLocalTime() + "</td>";
                            response += "<td>" + query + "</td>";
                            dtvec = (Vector) doc.getItemValueDateTimeArray("PSIQuery_RequestRecieved");
                            dt=(lotus.domino.DateTime) dtvec.get(0);
                            response += "<td>" + dt.getLocalTime()  + "</td>";
                            response += "<td>" + responseData + "</td>";
                            response += "</tr>";
                            count++;
                        }
                          doc = incoming.getPrevDocument(doc);
                        if ( count > 100 || count == posts ){
                            doc=null;
                        }
                    }
                } catch (Exception e ){
                    logger.error(e.getMessage(),e);
                }
                
   */             
                status = response;
            }
            
            else {
                logger.error("Method: " + service + " is not supported");
                
            }
            
            logger.info("--------------------------- Done ---------------------------");
        } catch (Exception e) {
            e.printStackTrace(System.out);
            logger.error( e.getMessage() , e);
        }
        
    }
    
    
    public String doIt() {
        return status;
    }
    
    public static String oldReplace(
            final String aInput,
            final String aOldPattern,
            final String aNewPattern
            ){
        if ( aOldPattern.equals("") ) {
            throw new IllegalArgumentException("Old pattern must have content.");
        }
        
        final StringBuffer result = new StringBuffer();
        //startIdx and idxOld delimit various chunks of aInput; these
        //chunks always end where aOldPattern begins
        int startIdx = 0;
        int idxOld = 0;
        while ((idxOld = aInput.indexOf(aOldPattern, startIdx)) >= 0) {
            //grab a part of aInput which does not include aOldPattern
            result.append( aInput.substring(startIdx, idxOld) );
            //add aNewPattern to take place of aOldPattern
            result.append( aNewPattern );
            
            //reset the startIdx to just after the current match, to see
            //if there are any further matches
            startIdx = idxOld + aOldPattern.length();
        }
        //the final chunk will go to the end of aInput
        result.append( aInput.substring(startIdx) );
        return result.toString();
    }
}