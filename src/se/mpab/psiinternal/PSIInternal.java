/*
 * PSIInternal.java
 *
 * Created on den 5 januari 2005, 10:15
 */

package se.mpab.psiinternal;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.*;
import se.mpab.logging.DominoConfigurator;
import de.bea.domingo.DNotesFactory;
import java.util.Enumeration;

/**
 *
 * @author  viktor.norberg
 * @version
 */
public class PSIInternal extends HttpServlet {
    
    /** Initializes the servlet.
     */
    private static Logger logger = LogManager.getLogger(PSIInternal.class);
    
    public void init(ServletConfig config) throws ServletException {
        System.out.println("INITIALIZE PSI INTERNAL SERVICE");
        super.init(config);
    }
    
    // Destroys the servlet.
     
   public void destroy() {
       DNotesFactory.dispose(); 
    }
    
    /* Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
   protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        
        try {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            //DominoConfigurator.configure();     
            //Layout layout = new PatternLayout();            
            //logger.addAppender(new ConsoleAppender(layout));                                    
            //logger.setLevel(Level.DEBUG);
            //Create the delegator and process the request
            Delegator delegator = new Delegator(request);
            out.println(delegator.doIt());            
            out.close();
            //logger.info(this.getClass().getName() + ", session ending, close output.");
        } catch(Exception e) {
          logger.error(e.getMessage(),e);
        }
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
