/*
 * MessageProcessor.java
 *
 * Created on den 5 januari 2005, 16:30
 */

package se.mpab.psiinternal;




import se.mpab.psi.bo.Deal;
import se.mpab.psi.db.DealDAO;
import se.mpab.psi.db.PSIMessageQueDAO;
import se.mpab.psi.db.PSIUserDAO;
import se.mpab.psi.mo.DealNumber;
import se.mpab.psi.user.PSIUser;
import se.mpab.psi.user.Entre;
import se.mpab.psi.user.EntreUser;

import se.mpab.psiinternal.adapters.*;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.LogManager;
import de.bea.domingo.DDocument;
/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class MessageProcessor {
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(MessageProcessor.class);
    
    private String status = null;
    
    /** Creates a new instance of MessageProcessor */
    public MessageProcessor() { }
    
    //Todo move all handling do DB layer, no NotesDocuments in this abstraction level !
    public String processMessageQue()
    {
        PSIMessageQueDAO messageQueDAO = new PSIMessageQueDAO();
        Vector messages = messageQueDAO.getMessageQue();
        logger.info("Number of messages to send: " + messages.size());
        
        if (!messages.isEmpty())
        {
            
            int counter = 0;
            int failed = 0;
            Iterator messageIterator = messages.iterator();
            while (messageIterator.hasNext())
            {
                DDocument tmpDoc = (DDocument) messageIterator.next();
                try
                {
                    if (this.processDocument(tmpDoc) == true)
                    {
                        tmpDoc.replaceItemValue("PSIQueueEntry_Status", "OK");
                    }
                    else
                    {
                        failed++;
                        if(!tmpDoc.hasItem("PSIQueueEntry_Retries") || tmpDoc.getItemValueInteger("PSIQueueEntry_Retries") == null)
                        {
                            tmpDoc.replaceItemValue("PSIQueueEntry_Retries", new Integer(1));
                        }
                        else
                        {
                            tmpDoc.replaceItemValue("PSIQueueEntry_Retries", new Integer(tmpDoc.getItemValueInteger("PSIQueueEntry_Retries").intValue() + 1));
                        }

                        if (tmpDoc.getItemValueInteger("PSIQueueEntry_Retries").intValue() >= 5)
                        {
                            tmpDoc.replaceItemValue("PSIQueueEntry_Status", "DEAD");
                        }
                        else
                        {
                            tmpDoc.replaceItemValue("PSIQueueEntry_Status", "FAILED");
                        }
                    }
                    tmpDoc.save(true);
                }
                catch(Exception e)
                {
                    logger.error("Could not process notification for deal [" + tmpDoc.getItemValueDouble("PSIQueueEntry_DealNo").toString() + "-" + tmpDoc.getItemValueDouble("PSIQueueEntry_Version").toString() + "]", e);
                }
                counter++;
            }
            
            status = "PSI Queue processed. " + counter + " in queue, " + failed + ", failed";
            
        }
        return status;
    }
    private boolean processDocument(DDocument doc)
    {
        boolean success = false;
        try
        {
            logger.info("Processing deal: " + doc.getItemValueDouble("PSIQueueEntry_DealNo").toString() + "-" + doc.getItemValueDouble("PSIQueueEntry_Version").toString());
            DealDAO dealDAO = new DealDAO(doc.getItemValueString("PSIQueueEntry_Country"));
            DealNumber dealNumber = new DealNumber();
            dealNumber.setId(doc.getItemValueInteger("PSIQueueEntry_DealNo"));
            dealNumber.setVersion(doc.getItemValueInteger("PSIQueueEntry_Version"));
            dealNumber.setPrefix(doc.getItemValueString("PSIQueueEntry_Country"));
            Deal deal = (Deal) dealDAO.findByDealNo(dealNumber);
            if (deal != null)
            {
                logger.info("Sending message");
                success = sendMessage(deal,doc);
            } 
            else
            {
                logger.error("Deal doc not found for dealno: " + doc.getItemValueDouble("PSIQueueEntry_DealNo").toString() + "-" + doc.getItemValueDouble("PSIQueueEntry_Version").toString());
            }
        }
        catch (Exception e) 
        {
            logger.error("Could not process document for dealno: " + doc.getItemValueDouble("PSIQueueEntry_DealNo").toString() + "-" + doc.getItemValueDouble("PSIQueueEntry_Version").toString(), e);
        }
        return success;
    }
    
    /*
     * sendMessage to opposite system
     */
    private boolean sendMessage(Deal deal,DDocument psiQueueEntry)
    {
        // Check what PSIUser to determine what adaptor should be used
        String userName = deal.getPsiUserID();
        String seat =  deal.getStock();
        PSIUserDAO psiUserDAO = new PSIUserDAO();
        logger.info("Username: " + deal.getPsiUserID());
        PSIUser psiUser;
        Sender sender = null;
        boolean sent = false;
        
        try
        {
            psiUser = psiUserDAO.getPSIUserFromUserName(userName);
            Entre entre = (Entre) psiUser.getSystem().get("entre");
            EntreUser entreUser = (EntreUser) entre.getUsers().get(seat);
            
            String adapter = entreUser.getAlerter().getAdapter();
            // Create and use Sender factory
            if (adapter.equals("ItfosSender")) {
                sender = SenderFactory.getItfosSender();
            }
            else if (adapter.equals("CharlieSender")) {
                sender = SenderFactory.getItfosSender();                
            }
            //Adding sender for ATEA 20150127
            else if (adapter.equals("AteaSender")) {
                sender = SenderFactory.getAteaSender();                
            }
            //Adding sender for Dustin 20150910
            else if (adapter.equals("DustinMessageSender")) {
                sender = SenderFactory.getDustinMessageSender();                
            }
            //Adding sender for Asedo 20160602
            else if (adapter.equals("AsedoMessageSender")) {
                sender = SenderFactory.getAsedoMessageSender();                
            }
            
            logger.info("Adapter: " + entreUser.getAlerter().getAdapter() + ", username: " + entreUser.getAlerter().getUsername()+ ", password: " + entreUser.getAlerter().getPassword());
            sent = sender.send(entreUser.getAlerter(), deal, psiQueueEntry.getItemValueString("PSIQueueEntry_Method"));
            
        }
        catch(se.mpab.psi.AutenticationException  ae)
        {
            logger.warn("PSIUser " + userName + "not found", ae);
        }
        catch(Exception e)
        {
            logger.error("Could not send message for dealno: " + deal.getDealNo(), e);            
        }
        
        try
        {         
            psiQueueEntry.replaceItemValue("PSIQueueEntry_Data",sender.getMessage() );
            psiQueueEntry.replaceItemValue("PSIQueueEntry_ResponseData",sender.getResponse() );
            logger.info("Computing with form!");
            psiQueueEntry.computeWithForm(false);
            psiQueueEntry.save(true);
        }
        catch(Exception ne)
        {
            logger.error("Could not store data on document after sending message for deal: " + deal.getDealNo(), ne);    
        }
        return sent;
    }
}
