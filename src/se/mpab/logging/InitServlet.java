/*
 * Copyright 1999,2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.mpab.logging;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * A servlet for initializing Log4j. See
 * <a href="http://jakarta.apache.org/log4j/docs/documentation.html">Log4j
 * documentation</a> for how to use Log4j.
 * <p>
 * <strong>Note:</strong> This log4j initialization servlet should be used
 * *only* when running under a container which doesn't support servlet-2.3.
 * A far better choice for servlet-2.3 configuration exists in
 * {@link InitContextListener}. Use it instead of this class for
 * initialization.  If you really need to use this class, read on...</p>
 * <p>
 * This servlet is never called by a client, but should be called during
 * web application initialization, i.e. when the servlet engine starts.</p>
 * <p>
 * See {@link InitContextListener} for detailed information about these
 * parameters.
 * </p>
 *
 * @author <a href="mailto:hoju@visi.com">Jacob Kjome</a>
 * @since  1.3
 */
public class InitServlet extends HttpServlet {
    /**
     * Log4j specific initialization.  Sets up log4j for the current
     * servlet context. Installs a custom repository selector if one hasn't
     * already been installed.
     *
     * @throws ServletException indicates problem running servlet
     */
    public void init() throws ServletException {
        //se.mpab.logging.DominoConfigurator.configure();
        //org.apache.log4j.Logger logger = org.apache.log4j.Logger.getRootLogger();
        //org.apache.log4j.Layout layout = new org.apache.log4j.PatternLayout();
        //logger.addAppender(new org.apache.log4j.ConsoleAppender(layout));
        //String logLevel = getInitParameter("log4j-level");
        //logger.setLevel(org.apache.log4j.Level.toLevel(logLevel ,org.apache.log4j.Level.ALL ) );
        
    }
    
    /**
     * Throws a ServletException because this servlet is not meant to be
     * accessed from the web.
     *
     * @param req the http servlet request
     * @param res the http servlet response
     * @throws ServletException indicates problem running servlet
     * @throws IOException indicates communication problem
     */
    public void service(HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException {
        throw new ServletException("Servlet only used for Log4j initialization");
    }
}