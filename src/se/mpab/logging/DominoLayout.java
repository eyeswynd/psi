package se.mpab.logging;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Layout;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.LoggingEvent;

/**
 * A Log4j Layout for the Entr� log.
 *
 * @author Johan K�nng�rd, WM-data
 */
public class DominoLayout extends Layout {
    
    /**
     * Formats the specified LoggingEvent to a String.
     *
     * @param evt the LoggingEvent to format.
     * @return a formatted String.
     */
    private static long seqNo = 0;
    public String format(LoggingEvent evt) {
        StringBuffer buf = new StringBuffer();
        buf.toString();
        String level = evt.getLevel().toString();
        
        if (level.equals("ERROR")) {
            level = "WARNING";
        } else if (level.equals("WARN")) {
            level = "WARNING";
        }
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("kk:mm.ss.SS");
        buf.append(formatter.format(date));
        //  buf.append("." + seqNo++  + level);
        buf.append(" "+level);
        buf.append(" " + evt.getRenderedMessage());
        String[] s = evt.getThrowableStrRep();
        
        if (s != null) {
            LogLog.debug("Found throwable");
            buf.append("\n");
            int len = s.length;
            
            for (int i = 0; i < len; i++) {
                buf.append(s[i] + "\n");
            }
        }
        LogLog.debug("Formatted: " + buf.toString());
        return buf.toString();
    }
    
    /**
     * @see org.apache.log4j.Layout
     */
    public boolean ignoresThrowable() {
        return false;
    }
    
    /**
     * @see org.apache.log4j.Layout
     */
    public void activateOptions() {
    }
}
