package se.mpab.logging;

import de.bea.domingo.DDatabase;
import de.bea.domingo.DNotesException;
import java.io.IOException;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import org.apache.log4j.helpers.LogLog;
import se.mpab.psi.db.DominoDataAccess;

/**
 * Configures the logging in Entr�.
 *
 * @author Johan K�nng�rd, WM-data
 */
public class DominoConfigurator {
    
    /**
     * Configures the Entr� logging.
     *
     * @param agentContext the AgentContext to use when finding the log database.
     */
    
    
    
    public static void configure() {
        LogLog.debug("Starting CONFIGURATOR");
     /*   org.apache.log4j.FileAppender fileA = null;
                Layout lo = new org.apache.log4j.PatternLayout();
        try {
            fileA = new org.apache.log4j.FileAppender(lo,"E:/PSI/log/psilog.log",true);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
       */ 
        // Get properties
        // Init databasenames from properties
        Logger logger = LogManager.getRootLogger();
        //Attempt to stop logging to ENTRE 20150127
        logger.setLevel(Level.INFO);
                
        DominoAppender appender = new DominoAppender();
        Layout layout = new DominoLayout();
        appender.setLayout(layout);
        DDatabase db = null;        
        
        try {
            db = DominoDataAccess.openDatabase(DominoDataAccess.LOG_DATABASE,"common");
        } catch (DNotesException e) {
            e.printStackTrace(System.err); 
        }
        appender.setDatabase(db);
        logger.removeAllAppenders();
        logger.addAppender(appender);
        //logger.addAppender(fileA);     
        
    }
}

