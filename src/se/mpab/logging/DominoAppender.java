package se.mpab.logging;

import java.util.Vector;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.LoggingEvent;
import se.mpab.psi.PSIService;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DSession;
import de.bea.domingo.DNotesException;
import java.util.Calendar;
import java.text.SimpleDateFormat;

/**
 * Simple Log4j Appender for logging to the Entr� application log.
 * Only works in Java agents.
 *
 * @author Johan K�nng�rd, WM-data
 */
public class DominoAppender extends AppenderSkeleton {
    
    /**
     * The Notes Database to log to.
     */
    private DDatabase db = null;
    
    /**
     * Sets the Notes Database to log to. Must be set before any logging is done.
     *
     * @param db the log Database.
     */
    public void setDatabase(DDatabase db) {
        this.db = db;
    }
    
    /**
     * Gets the Notes Database that LoggingEvents will be appended to, or
     * null if not set.
     *
     * @return the Notes Database that is target for logging.
     */
    public DDatabase getDatabase() {
        return db;
    }
    
    /**
     * Logs the specified LoggingEvent to this Appender.
     *
     * @param evt the LoggingEvent to append to this Appender.
     */
    public void append(LoggingEvent evt) {
        try {
            if (db == null) {
                LogLog.warn("No Notes database set for appender [" + getName() + "]");
                return;
            }
            
            if (!db.isOpen()) {
                LogLog.debug("Database not open. Trying to open it...");
                db.open();
                
                if (!db.isOpen()) {
                    throw new DNotesException("Database not open");
                }
            }
            
            LogLog.debug("Creating document...");
            DDocument doc = db.createDocument();
            DSession session = db.getSession();
            LogLog.debug("Setting field values...");
            doc.replaceItemValue("AgentName", PSIService.class.getName() );
            doc.replaceItemValue("CountryCode", "SE"); // FIXME do not hardcode
            doc.replaceItemValue("DocType", "Log");
            doc.replaceItemValue("Form", "frmLog");
            Calendar dateTime = Calendar.getInstance();
            java.text.SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = formatter.format(dateTime.getTime());
            doc.replaceItemValue("LogDate", dateString);

            // dateTime.setNow();
            //doc.replaceItemValue("LogDate", dateTime.getInstance());
            String level = evt.getLevel().toString();
            
            if (level.equals("ERROR")) {
                level = "WARNING";
            } else if (level.equals("WARN")) {
                level = "WARNING";
            }
            doc.replaceItemValue("MaxLevel", level);
            
            Vector v = new Vector();
            v.addElement(layout.format(evt));
            doc.replaceItemValue("Message" + level, layout.format(evt));
            doc.replaceItemValue("Path", "PSI");
            //doc.replaceItemValue("Path", evt.getLoggerName());
            doc.replaceItemValue("UserNameAbb", session.getCommonUserName());
            LogLog.debug("Refreshing fields...");
            doc.computeWithForm(false);
            LogLog.debug("Saving log document...");
            doc.save(true);
        } catch (DNotesException e) {
            e.printStackTrace(System.err);
            LogLog.warn(e.getMessage()); 
        }
    }
    
    /**
     * Replaces a substring in a string.
     *
     * @param source the String to replace a substring in.
     * @param searchFor the string to search for.
     * @param replaceWith the string to replace all found
     * searchFor-substrings with.
     * @return the replaced String.
     * FIXME use se.telia.finans.util.StringUtils instead.
     */
    private static String replace(
    String source,
    String searchFor,
    String replaceWith) {
        
        if (source.length() < 1) {
            return "";
        }
        int p = 0;
        
        while (p < source.length() &&
        (p = source.indexOf(searchFor, p)) >= 0) {
            
            source = source.substring(0, p) + replaceWith +
            source.substring(
            p + searchFor.length(),
            source.length());
            p += replaceWith.length();
        }
        return source;
    }
    /**
     * @see AppenderSkeleton.requiresLayout()
     */
    public boolean requiresLayout() {
        return true;
    }
    
    /**
     * Closes the Appender and releases the Notes Database.
     * No logging can be done to this Appender after calling this method.
     */
    public void close() {
        db = null;
    }
}
