package se.mpab.psi;
/*
 * PSIService.java
 *
 * Created on den 26 augusti 2004, 13:38
 */

import java.io.*;
import java.net.*;
import java.util.*;


import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.*;
import se.mpab.logging.*;
import de.bea.domingo.DNotesFactory;

/**
 *
 * @author  Janne Berg
 * @version
 */
public class PSIService extends HttpServlet {

    /**
     * Initializes the servlet.
     */
    private static org.apache.log4j.Logger logger = LogManager.getLogger(PSIService.class);

    public void init(ServletConfig config) throws ServletException {
        System.out.println("INITIALIZE PSI SERVICE");
        super.init(config);
        Logger rootLogger = Logger.getRootLogger();
        rootLogger.setLevel(Level.INFO);
        rootLogger.removeAllAppenders();
        
        RollingFileAppender appender = new RollingFileAppender();
        appender.setName("ROLLING_APPENDER");
        appender.setFile("log4jlogs\\log4j.log");
        appender.setMaxBackupIndex(10);
        appender.setMaxFileSize("10MB");
        appender.setAppend(true);
        appender.setLayout(new PatternLayout("[%p] %d{MM-dd-yyyy HH:mm:ss,SSS} %c %M: %m%n"));
        appender.setThreshold(Priority.INFO);
        appender.activateOptions();
        
        rootLogger.addAppender(appender);
    }

    /**
     * Destroys the servlet.
     */
    public void destroy() {
        System.out.println("DESTROY PSI SERVLET");
        DNotesFactory.dispose();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        try {

            
            
            if (request.getRemoteAddr().equals("213.203.17.73")) { // Fix for IT-FOS
                response.setContentType("text/html");
            } else {
                response.setContentType("text/xml");
            }
            PrintWriter out = response.getWriter();
            out.println(Delegator.handleRequest(request));
            out.close();
            logger.info(this.getClass().getName() + ", session ending, close output.");
            logger.info("--------------------------- Done ---------------------------");
        //DNotesFactory.dispose();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
    
   

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }

    private void setUpLoggging() {
    }
}
