/*
 * OfferId.java
 *
 * Created on den 3 januari 2005, 15:50
 */

package se.mpab.psi.user;

/**
 *
 * @author  viktor.norberg
 */
public class OfferId {
    
    private String entreOfferId;
    
    private String psiOfferId;
    
    /** Creates a new instance of OfferId */
    public OfferId() {
    }
    
    /**
     * Getter for property docUnid.
     * @return Value of property docUnid.
     */
    public java.lang.String getDocUnid() {
        return entreOfferId;
    }
    
    /**
     * Setter for property docUnid.
     * @param docUnid New value of property docUnid.
     */
    public void setDocUnid(java.lang.String entreOfferId) {
        this.entreOfferId = entreOfferId;
    }
    
    /**
     * Getter for property psiOfferId.
     * @return Value of property psiOfferId.
     */
    public java.lang.String getPsiOfferId() {
        return psiOfferId;
    }
    
    /**
     * Setter for property psiOfferId.
     * @param psiOfferId New value of property psiOfferId.
     */
    public void setPsiOfferId(java.lang.String psiOfferId) {
        this.psiOfferId = psiOfferId;
    }
    
}
