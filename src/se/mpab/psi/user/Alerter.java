/*
 * Alerter.java
 *
 * Created on den 11 januari 2005, 12:59
 */

package se.mpab.psi.user;

/**
 *
 * @author  viktor.norberg
 */
public class Alerter {
    
    private String url;
    
    private String username;
    
    private String password;
    
    private String adapter;
    
    /** Creates a new instance of Alerter */
    public Alerter() {
    }
    
    /**
     * Getter for property adapter.
     * @return Value of property adapter.
     */
    public java.lang.String getAdapter() {
        return adapter;
    }
    
    /**
     * Setter for property adapter.
     * @param adapter New value of property adapter.
     */
    public void setAdapter(java.lang.String adapter) {
        this.adapter = adapter;
    }
    
    /**
     * Getter for property password.
     * @return Value of property password.
     */
    public java.lang.String getPassword() {
        return password;
    }
    
    /**
     * Setter for property password.
     * @param password New value of property password.
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }
    
    /**
     * Getter for property url.
     * @return Value of property url.
     */
    public java.lang.String getUrl() {
        return url;
    }
    
    /**
     * Setter for property url.
     * @param url New value of property url.
     */
    public void setUrl(java.lang.String url) {
        this.url = url;
    }
    
    /**
     * Getter for property username.
     * @return Value of property username.
     */
    public java.lang.String getUsername() {
        return username;
    }
    
    /**
     * Setter for property username.
     * @param username New value of property username.
     */
    public void setUsername(java.lang.String username) {
        this.username = username;
    }
    
}
