/*
 * Entre.java
 *
 * Created on den 11 oktober 2004, 12:09
 */

package se.mpab.psi.user;
import java.util.HashMap;

/**
 *
 * @author  vino42
 */
public class Entre {
    private HashMap users = null;    
    /** Creates a new instance of Entre */
    public Entre() {
        users = new HashMap();        
    }
    
    /**
     * Getter for property users.
     * @return Value of property users.
     */
    public java.util.HashMap getUsers() {
        return users;
    }    
  
    /**
     * Setter for property users.
     * @param users New value of property users.
     */
    public void setUsers(java.util.HashMap users) {
        this.users = users;
    }
       
}
