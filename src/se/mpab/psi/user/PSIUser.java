/*
 * PSIUser.java
 *
 * Created on den 7 oktober 2004, 16:04
 */

package se.mpab.psi.user;

import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class PSIUser {
    
    private String userIp = null;
    private String userName = null;
    private String password = null;
    
    private String freeText = null;
    
    private HashMap system = null;
    
    /** Creates a new instance of PSIUser */
    public PSIUser() {
        system = new HashMap();
    }
    
    
    public EntreUser getEntreUserBySeat(String seat) {        
        Entre e = (Entre) this.system.get("entre");
        return (EntreUser) e.getUsers().get(seat);
    }
    
        
    /**
     * Getter for property freeText.
     * @return Value of property freeText.
     */
    public java.lang.String getFreeText() {
        return freeText;
    }
    
    /**
     * Setter for property freeText.
     * @param freeText New value of property freeText.
     */
    public void setFreeText(java.lang.String freeText) {
        this.freeText = freeText;
    }
    
    /**
     * Getter for property password.
     * @return Value of property password.
     */
    public java.lang.String getPassword() {
        return password;
    }
    
    /**
     * Setter for property password.
     * @param password New value of property password.
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }
    
    /**
     * Getter for property userIp.
     * @return Value of property userIp.
     */
    public java.lang.String getUserIp() {
        return userIp;
    }
    
    /**
     * Setter for property userIp.
     * @param userIp New value of property userIp.
     */
    public void setUserIp(java.lang.String userIp) {
        this.userIp = userIp;
    }
    
    /**
     * Getter for property userName.
     * @return Value of property userName.
     */
    public java.lang.String getUserName() {
        return userName;
    }
    
    /**
     * Setter for property userName.
     * @param userName New value of property userName.
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }
    
    /**
     * Getter for property system.
     * @return Value of property system.
     */
    public java.util.HashMap getSystem() {
        return system;
    }
    
    /**
     * Setter for property system.
     * @param system New value of property system.
     */
    public void setSystem(java.util.HashMap system) {
        this.system = system;
    }
    
}
