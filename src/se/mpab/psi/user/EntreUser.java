/*
 * EntreUser.java
 *
 * Created on den 11 oktober 2004, 11:03
 */

package se.mpab.psi.user;

import java.util.HashMap;
/**
 *
 * @author  vino42
 */
public class EntreUser {
    
    private String userId = null;
    private String seat = null;    
    
    private HashMap psiOfferIdByDocUnid;
    private HashMap docUnidByPsiOfferId;
    
    private Alerter alerter;            
    
    /** Creates a new instance of EntreUser */
    public EntreUser() {
        psiOfferIdByDocUnid = new HashMap();
        docUnidByPsiOfferId = new HashMap();
    }       
    
    // Getter for offerId by docunid
    public OfferId getPsiOfferByDocUnid(String docUnid) {
        OfferId oid = (OfferId) this.psiOfferIdByDocUnid.get(docUnid);
        return oid;
    }
    
    // Getter for docunit by offerId
    public OfferId getEntreOfferByPsiOfferId(String psiOfferId) {
        OfferId oid = (OfferId) this.docUnidByPsiOfferId.get(psiOfferId);
        return oid;
    }
    
    /**
     * Getter for property seat.
     * @return Value of property seat.
     */
    public java.lang.String getSeat() {
        return seat;
    }
    
    /**
     * Setter for property seat.
     * @param seat New value of property seat.
     */
    public void setSeat(java.lang.String seat) {
        this.seat = seat;
    }
    
    /**
     * Getter for property userId.
     * @return Value of property userId.
     */
    public java.lang.String getUserId() {
        return userId;
    }
    
    /**
     * Setter for property userId.
     * @param userId New value of property userId.
     */
    public void setUserId(java.lang.String userId) {
        this.userId = userId;
    }
    
    /**
     * Getter for property docUnidByOfferId.
     * @return Value of property docUnidByOfferId.
     */
    public java.util.HashMap getDocUnidByPsiOfferId() {
        return docUnidByPsiOfferId;
    }    
   
    /**
     * Setter for property docUnidByOfferId.
     * @param docUnidByOfferId New value of property docUnidByOfferId.
     */
    public void setDocUnidByOfferId(java.util.HashMap docUnidByPsiOfferId) {
        this.docUnidByPsiOfferId = docUnidByPsiOfferId;
    }    
    
    /**
     * Getter for property offerIdByDocUnid.
     * @return Value of property offerIdByDocUnid.
     */
    public java.util.HashMap getPsiOfferIdByDocUnid() {
        return psiOfferIdByDocUnid;
    }
    
    /**
     * Setter for property offerIdByDocUnid.
     * @param offerIdByDocUnid New value of property offerIdByDocUnid.
     */
    public void setPsiOfferIdByDocUnid(java.util.HashMap psiOfferIdByDocUnid) {
        this.psiOfferIdByDocUnid = psiOfferIdByDocUnid;
    }
    
    /**
     * Setter for property docUnidByPsiOfferId.
     * @param docUnidByPsiOfferId New value of property docUnidByPsiOfferId.
     */
    public void setDocUnidByPsiOfferId(java.util.HashMap docUnidByPsiOfferId) {
        this.docUnidByPsiOfferId = docUnidByPsiOfferId;
    }
    
    /**
     * Getter for property alerter.
     * @return Value of property alerter.
     */
    public se.mpab.psi.user.Alerter getAlerter() {
        return alerter;
    }
    
    /**
     * Setter for property alerter.
     * @param alerter New value of property alerter.
     */
    public void setAlerter(se.mpab.psi.user.Alerter alerter) {
        this.alerter = alerter;
    }
    
}
