/*
 * AutenticationException.java
 *
 * Created on den 24 september 2004, 10:55
 */

package se.mpab.psi.exceptions;

import se.mpab.psi.*;

/**
 *
 * @author  vino42
 */

public class AccessException extends Exception {
    private String message;
    private int code ;
    
    /** Creates a new instance of AutenticationException */
    public AccessException(int status) {
      this(Status.getNameByStatus(status),status) ;
    }
    public AccessException(String message, int code){
        this.message = message;
        this.code = code;
    }
    public String getMessage() {
        return this.message;
    }
    
    public int getCode() {
        return this.code;        
    }
}
