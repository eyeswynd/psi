/*
 * DataException.java
 *
 * Created on den 13 januari 2005, 16:24
 */

package se.mpab.psi.exceptions;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class DataException extends Exception {
    
    private String message;
        
    /** Creates a new instance of DataException */
    public DataException() {
    }
        /** Creates a new instance of DataException */
    public DataException(String message) {
        this.message= message;
    }
    
    public String getMessage() {
        return message;
    }
    
}
