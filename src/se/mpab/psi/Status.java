// Copyright Marquardt & Partners
package se.mpab.psi;

/*
 * Status.java
 *
 * Created on den 26 augusti 2004, 12:51
 */
/**
 * Status on request STATUS_OK = 0 STATUS_OK_WITH_CONDITION = 1
 * STATUS_INTERNAL_ERROR = 2 STATUS_IP_NOT_ALLOWED = 3 STATUS_USER_NOT_ALLOWED =
 * 4 STATUS_ERROR_IN_REQUEST = 5
 *
 * @author Viktor Norberg Marquardt & Partners AB
 */
public class Status {

    // Status to partner
    public static final int STATUS_OK = 000;
    public static final int STATUS_USER_NOT_FOUND = 100;
    public static final int STATUS_USER_AUTHENTICATION_ERROR = 101;
    public static final int STATUS_USER_SEAT_NOT_ALLOWED = 102;
    public static final int STATUS_IP_NOT_ALLOWED = 103;
    public static final int STATUS_DEALNUMBER_NOT_DEFINED = 104;
    public static final int STATUS_ACCESS_DENIED = 105;
    public static final int STATUS_INTERNAL_ERROR = 999;
    public static final int STATUS_ERROR_IN_REQUEST = 525;
    public static final int STATUS_IDENTIFICATION_NUMBER_ERROR = 106;

    private static final String STATUS_OK_TXT = "OK";
    private static final String STATUS_INTERNAL_ERROR_TXT = "Internal error";
    private static final String STATUS_USER_AUTHENTICATION_ERROR_TXT = "Authentication failed";
    private static final String STATUS_ERROR_IN_REQUEST_TXT = "Error in request";
    private static final String STATUS_UNKNOWN_ERROR_TXT = "Unknown Error";
    private static final String STATUS_ACCESS_DENIED_TXT = "Access denied";
    private static final String STATUS_DEALNUMBER_NOT_DEFINED_TXT = "Deal number not defined in request";
    private static final String STATUS_IDENTIFICATION_NUMBER_ERROR_TXT = "Identification number not defined in request or contains illegal characters";
    private static final String STATUS_USER_SEAT_NOT_ALLOWED_TXT = "User not found in seat";

    public static String getNameByStatus(int status) {
        // Loop through error codes and return text
        String statusString = "";
        switch (status) {
            case STATUS_INTERNAL_ERROR:
                statusString = STATUS_INTERNAL_ERROR_TXT;
                break;
            case STATUS_USER_SEAT_NOT_ALLOWED:
                statusString = STATUS_USER_SEAT_NOT_ALLOWED_TXT;
                break;
            case STATUS_USER_NOT_FOUND:
            case STATUS_USER_AUTHENTICATION_ERROR:
            case STATUS_IP_NOT_ALLOWED:
                statusString = STATUS_USER_AUTHENTICATION_ERROR_TXT;
                break;
            case STATUS_DEALNUMBER_NOT_DEFINED:
                statusString = STATUS_DEALNUMBER_NOT_DEFINED_TXT;
                break;
            case STATUS_ACCESS_DENIED:
                statusString = STATUS_ACCESS_DENIED_TXT;
                break;
            case STATUS_IDENTIFICATION_NUMBER_ERROR:
                statusString = STATUS_IDENTIFICATION_NUMBER_ERROR_TXT;
                break;
            default:
                statusString = STATUS_UNKNOWN_ERROR_TXT;
                break;

        }

        // Default return
        return statusString;
    }
}
