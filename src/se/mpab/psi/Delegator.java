/*
 * Delegator.java
 *
 * Created on den 2 september 2004, 13:18
 *
 * TODO -  We should make the exception code much smoother, do not write the same code over and over again. Start with adding a method 
 *for the messagehandling
 */
package se.mpab.psi;

import javax.servlet.http.HttpServletRequest;
import org.jdom.Element;
import se.mpab.psi.mo.*;
import se.mpab.psi.bo.*;
import se.mpab.psi.db.*;
import org.apache.log4j.LogManager;
import java.util.*;
import se.mpab.psi.bo.PSIRequest;
import se.mpab.psi.exceptions.AccessException;

/**
 *
 * @author  vino42
 */
public class Delegator {

    private int status = Status.STATUS_OK;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(Delegator.class);

    /**
     * 
     * @param request 
     * @return 
     */
    public static String handleRequest(HttpServletRequest request) {
        PSIRequest psiRequest = null;
        EntreMessage em = new EntreMessage();
        PSIMessageDAO msDAO = new PSIMessageDAO();
        PSIValidator validator = new PSIValidator();
        String responseXML;

        se.mpab.util.common.StopWatch timer = new se.mpab.util.common.StopWatch("Delegator");
        timer.start();

        try {
            logger.info("--------------------------- New request ---------------------------");
            //Todo this should be refactored. PSIRequest should have a parsemethod or somesuch that
            //respons with true or false or a messagenumber what wen wrong
            psiRequest = new PSIRequest(request);
            msDAO.logRequest(psiRequest);
            msDAO.saveRequestToFile(psiRequest);

            logger.info("Request data OK");
            em = psiRequest.getPsiMessage();
            String service = em.getHeader().getService();
            logger.info("Method to use: <" + service + ">");
            DealDAO dealDAO = new DealDAO(psiRequest.getCountry());
            if (service.equals("makeCreditApplication"))
            {
                validator.validateIdentificationNumber(psiRequest.getCountry(), em.getBody().getCreditApplication().getClient().getIdentificationNumber());
                Deal deal = (Deal) dealDAO.insert(psiRequest);
                em.getBody().setCreditApplication(deal.makeCreditApplication());
                msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "makeCreditApplication");
            }
            else if (service.equals("makeCreditApplicationHP"))
            {
                validator.validateIdentificationNumber(psiRequest.getCountry(), em.getBody().getCreditApplication().getClient().getIdentificationNumber());
                Deal deal = (Deal) dealDAO.insert(psiRequest);
                em.getBody().setCreditApplication(deal.makeCreditApplicationHP());
                msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "makeCreditApplicationHP");
                
            }
            else if (service.equals("checkCreditApplication"))
            {
                if (em.getBody().getCreditApplications() == null)
                {
                    Deal deal = (Deal) dealDAO.findByDealNo(em.getBody().getCreditApplication().getDealNumber());
                    if (!deal.getPsiUserID().equals(em.getHeader().getUser()))
                    {
                        throw new AccessException(Status.STATUS_ACCESS_DENIED);
                    }
                    em.getBody().setCreditApplication(deal.checkCreditApplication());
                    msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "checkCreditApplication");     
                }
                else
                {
                    Iterator it = em.getBody().getCreditApplications().iterator();
                    Vector creditApplications = new Vector();
                    while (it.hasNext())
                    {
                        try
                        {
                            CreditApplication creditApplication = (CreditApplication) it.next();
                            Deal deal = (Deal) dealDAO.findByDealNo(creditApplication.getDealNumber());
                            if (!deal.getPsiUserID().equals(em.getHeader().getUser()))
                            {
                                throw new AccessException(Status.STATUS_ACCESS_DENIED);
                            }
                            creditApplications.add(deal.checkCreditApplication());
                            msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "checkCreditApplication");
                        }
                        catch (DataException dx)
                        {
                            logger.error(dx.getMessage(), dx);
                        }
                    }
                    em.getBody().setCreditApplications(creditApplications);
                              
                }

            }
            else if (service.equals("changeCreditApplication"))
            {
                Deal deal = (Deal) dealDAO.findByDealNo(em.getBody().getCreditApplication().getDealNumber());
                deal.changeCreditApplication(psiRequest);
                em.getBody().setCreditApplication(deal.checkCreditApplication());
                msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "changeCreditApplication");
            }
            else if (service.equals("changeCreditApplicationHP"))
            {
                dealDAO.setVersionIsHP();
                Deal deal = (Deal) dealDAO.findByDealNo(em.getBody().getCreditApplication().getDealNumber());
                deal.changeCreditApplicationHP(psiRequest);
                em.getBody().setCreditApplication(deal.checkCreditApplication());
                msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "changeCreditApplicationHP");

            }
            else if (service.equals("makeAgreementApplication"))
            {
                Deal deal = (Deal) dealDAO.findByDealNo(em.getBody().getAgreementApplication().getDealNumber());
                em.getBody().setCreditApplication(deal.makeAgreementApplication(psiRequest, em.getBody().getAgreementApplication()));
                msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "makeAgreementApplication");
            }
            else if (service.equals("makeAgreementApplicationHP"))
            {
                // Make Agreement Hire Purchase
                dealDAO.setVersionIsHP();
                Deal deal = (Deal) dealDAO.findByDealNo(em.getBody().getAgreementApplication().getDealNumber());
                em.getBody().setCreditApplication(deal.makeAgreementApplicationHP(psiRequest, em.getBody().getAgreementApplication()));
                msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "makeAgreementApplicationHP");
            }
            else if (service.equals("checkStatus"))
            {
                if (em.getBody().getCreditApplications() == null)
                {
                    Deal deal = (Deal) dealDAO.findByDealNo(em.getBody().getCreditApplication().getDealNumber());
                    if (!deal.getPsiUserID().equals(em.getHeader().getUser()))
                    {
                        throw new AccessException(Status.STATUS_ACCESS_DENIED);
                    }
                    em.getBody().setCreditApplication(deal.checkStatus(psiRequest));
                    msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "checkStatus");
                } else {
                    Iterator it = em.getBody().getCreditApplications().iterator();
                    Vector creditApplications = new Vector();
                    while (it.hasNext())
                    {
                        try
                        {
                            CreditApplication creditApplication = (CreditApplication) it.next();
                            Deal deal = (Deal) dealDAO.findByDealNo(creditApplication.getDealNumber());
                            if (!deal.getPsiUserID().equals(em.getHeader().getUser()))
                            {
                                throw new AccessException(Status.STATUS_ACCESS_DENIED);
                            }
                            creditApplications.add(deal.checkStatus(psiRequest));
                            msDAO.updateLogRequestWithDealNoAndMethod(deal.getDealNo(), "checkStatus");
                        }
                        catch (DataException dx)
                        {
                            logger.error(dx.getMessage(), dx);
                        }
                    }
                    em.getBody().setCreditApplications(creditApplications);
                }

            }
            else if (service.equals("getPriceMatrix"))
            {
                logger.debug("getPriceMatrix!");
                EntreMessageBody body = new EntreMessageBody();
                body.setCustomerOfferMatrices(PriceMatrixHelper.getPriceMatrices(psiRequest.getCountry(), em.getBody().getCustomerOffers()));
                em.setBody(body);
            }
            else
            {
                logger.warn("No such method: <" + em.getHeader().getService() + ">");
                se.mpab.psi.mo.Error errorMessage = new se.mpab.psi.mo.Error();
                se.mpab.psi.mo.EntreMessageBody errorBody = new se.mpab.psi.mo.EntreMessageBody();
                errorMessage.setErrorString("No such method: (" + em.getHeader().getService() + ")");
                errorMessage.setErrorCode(Status.STATUS_ERROR_IN_REQUEST);
                errorBody.setError(errorMessage);
                em.setBody(errorBody);
                em.setHeader(null);
                msDAO.updateLogRequestWithError("No such method");
            }
        }
        catch (AutenticationException ae)
        {
            logger.error(ae.getMessage(), ae);
            se.mpab.psi.mo.EntreMessageBody errorBody = new se.mpab.psi.mo.EntreMessageBody();
            se.mpab.psi.mo.Error errorMessage = new se.mpab.psi.mo.Error();
            errorMessage.setErrorString(ae.getMessage());
            errorMessage.setErrorCode(Status.STATUS_ERROR_IN_REQUEST);
            errorBody.setError(errorMessage);
            em.setBody(errorBody);
            em.setHeader(null);
            msDAO.updateLogRequestWithError("AutenticationException");
        }
        catch (AccessException ae)
        {
            logger.error(ae.getMessage(), ae);
            se.mpab.psi.mo.EntreMessageBody errorBody = new se.mpab.psi.mo.EntreMessageBody();
            se.mpab.psi.mo.Error errorMessage = new se.mpab.psi.mo.Error();
            errorMessage.setErrorString(ae.getMessage());
            errorMessage.setErrorCode(Status.STATUS_ACCESS_DENIED);
            errorBody.setError(errorMessage);
            em.setBody(errorBody);
            //em.setHeader(null);
            msDAO.updateLogRequestWithError("AccessException");
        }
        catch (DataException dx)
        {
            logger.error(dx.getMessage(), dx);
            EntreMessageBody errorBody = new EntreMessageBody();
            se.mpab.psi.mo.Error errorMessage = new se.mpab.psi.mo.Error();
            errorMessage.setErrorString(dx.getMessage());
            errorMessage.setErrorCode(Status.STATUS_ERROR_IN_REQUEST);
            errorBody.setError(errorMessage);
            em.setBody(errorBody);
            em.setHeader(null);
            msDAO.updateLogRequestWithError("DataException");

        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
            se.mpab.psi.mo.EntreMessageBody errorBody = new se.mpab.psi.mo.EntreMessageBody();
            se.mpab.psi.mo.Error errorMessage = new se.mpab.psi.mo.Error();
            errorMessage.setErrorString(ex.getMessage());
            errorMessage.setErrorCode(Status.STATUS_ERROR_IN_REQUEST);
            errorBody.setError(errorMessage);
            em.setBody(errorBody);
            em.setHeader(null);
            msDAO.updateLogRequestWithError("Exception");
        }

        Element k = em.getElement(org.jdom.Namespace.getNamespace("", ""));
        org.jdom.output.XMLOutputter outputter = new org.jdom.output.XMLOutputter(org.jdom.output.Format.getPrettyFormat());
        responseXML = outputter.outputString(k);
        timer.stop();
        msDAO.updateLogRequest(responseXML, timer.get());
        logger.debug("WHOLE request took " + timer.get() + " ms");

        //logger.info("Response sent: <?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n" + responseXML);

        if (psiRequest != null && !psiRequest.getCountry().equals("NO"))
        {
            return "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n" + responseXML;
        }
        else
        {
            return responseXML;
        }
    }

    public int getStatus() {
        return this.status;
    }
}//end class
