package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;
import org.jdom.output.*;
import org.apache.log4j.LogManager;

/*
 * EntreMessage.java
 *
 * Created on den 30 juli 2004, 16:02
 */

/**
 *
 * @author  janne.berg
 */
public class EntreMessage implements XMLPackUnpack {
    
    
    private EntreMessageHeader header;
    
    private EntreMessageBody body;
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(EntreMessage.class);
    /** Creates a new instance of EntreMessage */
    public EntreMessage() {
        logger.debug("Creating Message Object");
        
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        if(header != null){
            e.addContent(this.header.getElement(namespace));
        }
        if (body != null){
            e.addContent(this.body.getElement(namespace));
        }
        return e;
    }
    
    public String getElementName() {
        return "psimessage";
    }
    
    public void initFromElement(org.jdom.Element element) {
        header = new EntreMessageHeader();
        body = new EntreMessageBody();
        header.initFromElement(element.getChild("header"));
        body.initFromElement(element.getChild("body"));
    }

    /**
     * Getter for property body.
     * @return Value of property body.
     */
    public se.mpab.psi.mo.EntreMessageBody getBody() {
        return body;
    }    
 
    /**
     * Setter for property body.
     * @param body New value of property body.
     */
    public void setBody(se.mpab.psi.mo.EntreMessageBody body) {
        this.body = body;
    }
    
    /**
     * Getter for property header.
     * @return Value of property header.
     */
    public se.mpab.psi.mo.EntreMessageHeader getHeader() {
        return header;
    }
    
    /**
     * Setter for property header.
     * @param header New value of property header.
     */
    public void setHeader(se.mpab.psi.mo.EntreMessageHeader header) {
        this.header = header;
    }
    
}
