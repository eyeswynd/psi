/*
 * Equipment.java
 *
 * Created on den 8 oktober 2004, 09:20
 */
package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;

/**
 *
 * @author  janne.berg
 */
public class Equipment implements XMLPackUnpack {

    private String type;
    private String description;
    private long quantity;
    private Amount amount;
    private String model;
    private String newOrUsed;
    private String serialNo;
    private String usage;
    private String registrationNo;
    private String vatFinance;

    /**
     * Get the value of registrationNo
     *
     * @return the value of registrationNo
     */
    public String getRegistrationNo() {
        if (registrationNo == null) {
            return "";
        } else {
            return registrationNo;
        }
    }

    /**
     * Set the value of registrationNo
     *
     * @param registrationNo new value of registrationNo
     */
    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    /**
     * Get the value of usage
     *
     * @return the value of usage
     */
    public String getUsage() {
        if (usage == null) {
            return "";
        } else {
            return usage;
        }
    }

    /**
     * Set the value of usage
     *
     * @param usage new value of usage
     */
    public void setUsage(String usage) {
        this.usage = usage;
    }

    /**
     * Get the value of serialNo
     *
     * @return the value of serialNo
     */
    public String getSerialNo() {
        if (serialNo == null) {
            return "";
        } else {
            return serialNo;
        }
    }

    /**
     * Set the value of serialNo
     *
     * @param serialNo new value of serialNo
     */
    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    /**
     * Get the value of newOrUsed
     *
     * @return the value of newOrUsed
     */
    public String getNewOrUsed() {
        if (newOrUsed == null) {
            return "";
        } else {
            return newOrUsed;
        }
    }
    /**
     * Set the value of newOrUsed
     *
     * @param newOrUsed new value of newOrUsed
     */
    public void setNewOrUsed(String newOrUsed) {
        this.newOrUsed = newOrUsed;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        if (model == null) {
            return "";
        } else {
            return model;
        }
    }
    
    public void setVatFinance(String vatFinance) {
        this.vatFinance = vatFinance;
    }

    public String getVatFinance() {
        if (vatFinance == null) {
            return "";
        } else {
            return vatFinance;
        }
    }

    /** Creates a new instance of Equipment */
    public Equipment() {
    }

    public org.jdom.Element getElement(org.jdom.Namespace namespace) {

        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("type", namespace).addContent(this.type));
        e.addContent((Element) new Element("description", namespace).addContent(this.description));
        e.addContent((Element) new Element("quantity", namespace).addContent(Long.toString(this.quantity)));
        e.addContent(amount.getElement(namespace));
        e.addContent((Element) new Element("model", namespace).addContent(this.model));
        e.addContent((Element) new Element("newOrUsed", namespace).addContent(this.newOrUsed));

        e.addContent((Element) new Element("serialNo", namespace).addContent(this.serialNo));
        e.addContent((Element) new Element("usage", namespace).addContent(this.usage));
        e.addContent((Element) new Element("registrationNo", namespace).addContent(this.registrationNo));
        e.addContent((Element) new Element("vatFinance", namespace).addContent(this.vatFinance));


        return e;
    }

    public void initFromElement(org.jdom.Element element) {

        if (element.getChild("type") != null) {
            this.type = element.getChild("type").getTextTrim();
        }
        if (element.getChild("description") != null) {
            this.description = element.getChild("description").getTextTrim();
        }
        if (element.getChild("quantity") != null) {
            this.quantity = Long.parseLong(element.getChild("quantity").getTextTrim());
        }
        if (element.getChild("amount") != null) {
            this.amount = new Amount();
            this.amount.initFromElement(element.getChild("amount"));
        }
        if (element.getChild("model") != null) {
            this.model = element.getChild("model").getTextTrim();
        }
        if (element.getChild("newOrUsed") != null) {
            this.newOrUsed = element.getChild("newOrUsed").getTextTrim();
        }
        if (element.getChild("serialNo") != null) {
            this.serialNo = element.getChild("serialNo").getTextTrim();
        }
        if (element.getChild("usage") != null) {
            this.usage = element.getChild("usage").getTextTrim();
        }
        if (element.getChild("registrationNo") != null) {
            this.registrationNo = element.getChild("registrationNo").getTextTrim();
        }
        if (element.getChild("vatFinance") != null) {
            this.vatFinance = element.getChild("vatFinance").getTextTrim();
        }
    }

    public String getElementName() {
        return "equipment";
    }

    public String toString() {
        return type;
    }

    /**
     * Getter for property description.
     * @return Value of property description.
     */
    public java.lang.String getDescription() {
        return description;
    }

    /**
     * Setter for property description.
     * @param description New value of property description.
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    /**
     * Getter for property price.
     * @return Value of property price.
     */
    public double getTotalAmount() {

        return (this.getAmount().getValue() * this.getQuantity());
    }

    /**
     * Getter for property amount.
     * @return Value of property amount.
     */
    public se.mpab.psi.mo.Amount getAmount() {
        return amount;
    }

    /**
     * Setter for property amount.
     * @param amount New value of property amount.
     */
    public void setAmount(se.mpab.psi.mo.Amount amount) {
        this.amount = amount;
    }

    /**
     * Getter for property quantity.
     * @return Value of property quantity.
     */
    public long getQuantity() {
        return quantity;
    }

    /**
     * Setter for property quantity.
     * @param quantity New value of property quantity.
     */
    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public java.lang.String getType() {
        return type;
    }

    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }
}
