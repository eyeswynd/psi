// Copyright Applicon
/*
 * InstallmentPayments .java
 *
 * Created on den 18 april 2013, 09:19
 */
package se.mpab.psi.mo;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.jdom.*;
import se.mpab.util.xml.*;
import org.apache.log4j.LogManager;
import se.mpab.psi.Delegator;

/**
 *
 * @author niklas.schold
 */
public class InstallmentPlanPayments implements XMLPackUnpack {
    private String currency;
    private Vector payments;
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(Delegator.class);

    public InstallmentPlanPayments(){
        this.payments = new Vector();
    }
           
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("currency",namespace).addContent(this.currency));
        e.addContent((Element) new Element("values",namespace).addContent(this.payments));
        
        return e;
    }
 
  
    // Getters
    public String getElementName() {
        return "installmentPlanPayments";
    }
    public String getCurrency() {
        return this.currency;
    }
    public Vector getPayments() {
        return this.payments;
    }
    
    // Setters
    public void setCurrency(String currency) {
        this.currency = currency;        
    }
    public void setPayments(Vector payments) {
        this.payments = payments;
    }
    
    
    public void initFromElement(org.jdom.Element element) {
        if (element.getChild("currency") != null) {
            this.currency = element.getChild("currency").getTextTrim();
        }
        
        if (element.getChild("values") != null) {
            Element valuesElem = (Element) element.getChild("values");
            List tmpPayments = valuesElem.getChildren("value");
             for (int i = 0; i<tmpPayments.size(); i++){
                Element singlePaymentElem = (Element) tmpPayments.get(i);
                this.payments.add(new Double(Double.parseDouble(singlePaymentElem.getTextTrim())));
            }
        }
    }
}
