// Copyright Marquardt & Partners
/*
 * Duration.java
 *
 * Created on den 31 augusti 2004, 14:26
 */

package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;
/**
 *
 * @author  vino42
 */
public class Duration implements XMLPackUnpack {
        
    private String unit;
   
    private int value;
    
    public org.jdom.Element getElement (org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("unit",namespace).addContent(this.unit));        
        e.addContent((Element) new Element("value",namespace).addContent(Integer.toString(this.value)));
        return e;
    }
    public String getElementName() {
        return "duration";
    }
    public void setUnit(String unit) {
        this.unit = unit;        
    }
    public void setValue(int value) {
        this.value = value;
    }

    public void initFromElement(org.jdom.Element element) {
        this.unit = element.getChild("unit").getTextTrim();
        this.value = Integer.parseInt(element.getChild("value").getTextTrim());
    }
    
    /**
     * Getter for property value.
     * @return Value of property value.
     */
    public int getValue() {
        return value;
    }
    
    /**
     * Getter for property unit.
     * @return Value of property unit.
     */
    public java.lang.String getUnit() {
        return unit;
    }
    
}
