/*
 * Condition.java
 *
 * Created on den 29 december 2004, 13:47
 */

package se.mpab.psi.mo;

import java.util.Vector;
import java.util.List;
import java.util.Iterator;
import org.jdom.*;
import se.mpab.util.xml.*;
import se.mpab.psi.bo.*;
/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class Condition implements XMLPackUnpack {
    
    private String description;
    
    private int code;
    
    private Vector guarantors;
    
    private Amount amount;
    
    /** Creates a new instance of Condition */
    public Condition() {
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        
        if (this.description != null){
            e.addContent((Element) new Element("description",namespace).addContent(this.description));
        }
        
        e.addContent((Element) new Element("code",namespace).addContent(Integer.toString(this.code)));
        
        if (this.guarantors != null) {
            // For a set or list
            for (Iterator it=guarantors.iterator(); it.hasNext(); ) {
                Guarantor g =(se.mpab.psi.mo.Guarantor) it.next();
                e.addContent(g.getElement(namespace));
            }
        }
        
        if (this.amount != null) {
            e.addContent(amount.getElement(namespace));
        }
        
        return e;
    }
    
    public String getElementName() {
        return "condition";
    }
    
    public void initFromElement(org.jdom.Element element) {
        this.description = element.getChild("description").getTextTrim();
        this.code = Integer.parseInt(element.getChild("code").getTextTrim());
        // Init Addresses
        if (element.getChildren("guarantor") != null) {
            this.guarantors = new Vector();
            List guarantorElements = (List) element.getChildren("guarantor");
            for (Iterator it=guarantorElements.iterator(); it.hasNext(); ) {
                se.mpab.psi.mo.Guarantor guarantorMO = new se.mpab.psi.mo.Guarantor();
                guarantorMO.initFromElement((Element) it.next());
                guarantors.add(guarantorMO);
            }
        }
        //this.guarantor.initFromElement(element.getChild("guarantor"));
        this.amount = new Amount();
        this.amount.initFromElement(element.getChild("amount"));
    }
    
    /**
     * Getter for property amount.
     * @return Value of property amount.
     */
    public se.mpab.psi.mo.Amount getAmount() {
        return amount;
    }
    
    /**
     * Setter for property amount.
     * @param amount New value of property amount.
     */
    public void setAmount(se.mpab.psi.mo.Amount amount) {
        this.amount = amount;
    }
    
    /**
     * Getter for property code.
     * @return Value of property code.
     */
    public int getCode() {
        return code;
    }
    
    /**
     * Setter for property code.
     * @param code New value of property code.
     */
    public void setCode(int code) {
        this.code = code;
    }
    
    /**
     * Getter for property description.
     * @return Value of property description.
     */
    public java.lang.String getDescription() {
        return description;
    }
    
    /**
     * Setter for property description.
     * @param description New value of property description.
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public Vector getGuarantors() {
        return guarantors;
    }

    public void setGuarantors(Vector guarantors) {
        this.guarantors = guarantors;
    }
    

    
}
