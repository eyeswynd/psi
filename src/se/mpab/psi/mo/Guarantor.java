/*
 * Guarantor.java
 *
 * Created on den 29 december 2004, 13:49
 */

package se.mpab.psi.mo;

import org.jdom.*;
import se.mpab.util.xml.*;
import se.mpab.psi.bo.*;
/**
 *
 * @author  viktor.norberg
 */
public class Guarantor implements XMLPackUnpack {
    
    private String identificationNumber;
    
    private String name;
    
    private String seat;
    
    private Amount amount;
    
    private String type;
    
    private String phoneNumber;
    
    private Address address;
    
    /** Creates a new instance of Guarantor */
    public Guarantor() {
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        if (this.identificationNumber != null) {
            e.addContent((Element) new Element("identificationNumber",namespace).addContent(this.identificationNumber));
        }
        if (this.name != null) {
            e.addContent((Element) new Element("name",namespace).addContent(this.name));
        }
        if (this.seat != null) {
            e.addContent((Element) new Element("seat",namespace).addContent(this.seat));
        }
        if (this.amount != null) {
            e.addContent(amount.getElement(namespace));
        }
        if (this.type != null) {
            e.addContent((Element) new Element("type",namespace).addContent(this.type));
        }
        if (this.phoneNumber != null) {
            e.addContent((Element) new Element("phoneNumber",namespace).addContent(this.phoneNumber));
        }
        if (this.address != null) {
            e.addContent(address.getElement(namespace));
        }
        
        return e;
    }
    
    public String getElementName() {
        return "guarantor";
    }
    
    public void initFromElement(org.jdom.Element element) {
        if (element.getChild("identificationNumber") != null) {
            identificationNumber = element.getChild("identificationNumber").getTextTrim();
        }
        if (element.getChild("name") != null) {
            name = element.getChild("name").getTextTrim();
        }
        if (element.getChild("seat") != null) {
            seat = element.getChild("seat").getTextTrim();
        }
        if (element.getChild("amount") != null) {
            amount = new Amount();
            amount.initFromElement(element.getChild("amount"));
        }
        if (element.getChild("type") != null) {
            type = element.getChild("type").getTextTrim();
        }
        if (element.getChild("phoneNumber") != null) {
            phoneNumber = element.getChild("phoneNumber").getTextTrim();
        }
        if (element.getChild("address") != null) {
            address = new Address();
            address.initFromElement(element.getChild("address"));
        }
    }
    
    /**
     * Getter for property address.
     * @return Value of property address.
     */
    public se.mpab.psi.mo.Address getAddress() {
        return address;
    }
    
    /**
     * Setter for property address.
     * @param address New value of property address.
     */
    public void setAddress(se.mpab.psi.mo.Address address) {
        this.address = address;
    }
    
    /**
     * Getter for property amount.
     * @return Value of property amount.
     */
    public se.mpab.psi.mo.Amount getAmount() {
        return amount;
    }
    
    /**
     * Setter for property amount.
     * @param amount New value of property amount.
     */
    public void setAmount(se.mpab.psi.mo.Amount amount) {
        this.amount = amount;
    }
    
    /**
     * Getter for property identificationNumber.
     * @return Value of property identificationNumber.
     */
    public java.lang.String getIdentificationNumber() {
        return identificationNumber;
    }
    
    /**
     * Setter for property identificationNumber.
     * @param identificationNumber New value of property identificationNumber.
     */
    public void setIdentificationNumber(java.lang.String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }
    
    /**
     * Getter for property phoneNumber.
     * @return Value of property phoneNumber.
     */
    public java.lang.String getPhoneNumber() {
        return phoneNumber;
    }
    
    /**
     * Setter for property phoneNumber.
     * @param phoneNumber New value of property phoneNumber.
     */
    public void setPhoneNumber(java.lang.String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    /**
     * Getter for property seat.
     * @return Value of property seat.
     */
    public java.lang.String getSeat() {
        return seat;
    }
    
    /**
     * Setter for property seat.
     * @param seat New value of property seat.
     */
    public void setSeat(java.lang.String seat) {
        this.seat = seat;
    }
    
    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public java.lang.String getType() {
        return type;
    }
    
    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
