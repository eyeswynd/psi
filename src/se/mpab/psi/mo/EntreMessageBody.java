/*
 * EntreMessageBody.java
 *
 * Created on den 31 augusti 2004, 16:25
 */

package se.mpab.psi.mo;

import org.jdom.*;
import se.mpab.util.xml.*;
import java.util.List;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.LogManager;

/**
 *
 * @author  vino42
 */
public class EntreMessageBody implements XMLPackUnpack
{
    private Vector customerOffers;
    private Vector customerOfferMatrices;
    private CreditApplication creditApplication;
    private AgreementApplication agreementApplication;
    private Vector creditApplications;
    private Vector agreementApplications;
    private se.mpab.psi.mo.Error error;
    
    
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        if (creditApplication != null) {
            e.addContent(creditApplication.getElement(namespace));
        } 
        
        if (agreementApplication != null) {
            e.addContent(agreementApplication.getElement(namespace));
        } 
        
        if (this.getCreditApplications() != null) { // Multiple DEALNUMBERS in checkCreditApplication
            Iterator it = this.getCreditApplications().iterator();
            while (it.hasNext()) {
                CreditApplication creditApplication = (CreditApplication) it.next();
                e.addContent(creditApplication.getElement(namespace));
            }
        }
        
        if (this.customerOfferMatrices != null)
        {
            Element matrices = new Element("matrices", namespace);
            Iterator it = this.customerOfferMatrices.iterator();
            while (it.hasNext())
            {
                PriceMatrix matrix = (PriceMatrix) it.next();
                matrices.addContent(matrix.getElement(namespace));
            }
            e.addContent(matrices);
        }
            
        if (error != null) {
            e.addContent(error.getElement(namespace));
        }
        return e;
    }
    
    public String getElementName() {
        return "body";
    }
    
    public CreditApplication getCreditApplication() {
        return this.creditApplication;
    }
    public void setCreditApplication(CreditApplication creditApplication) {
        this.creditApplication = creditApplication;
    }
    
  // Agreement
    public AgreementApplication getAgreementApplication() {
        return this.agreementApplication;
    }
    
    public void setAgreementApplication(AgreementApplication agreementApplication) {
        this.agreementApplication = agreementApplication;
    }
    public void initFromElement(org.jdom.Element element) {
        
        // Check what service is asked for
        List tmpCreditApplicationList = element.getChildren("CreditApplication");                        

        // Fix to get Normal flow working
        if (tmpCreditApplicationList.size() == 1) {            
            this.creditApplication = new CreditApplication();
            this.creditApplication.initFromElement(element.getChild("CreditApplication"));
        } else {
            this.setCreditApplications(new Vector());
            Iterator it = tmpCreditApplicationList.iterator();
            int i=0;            
            while (it.hasNext()) {
                i++;
                CreditApplication tmpCreditAppl = new CreditApplication();
                org.jdom.Element tmpEl = (org.jdom.Element) it.next();
                tmpCreditAppl.initFromElement(tmpEl);
                this.getCreditApplications().add(tmpCreditAppl);                                                
            }
        }
        
        List tmpAgreementApplicationList = element.getChildren("AgreementApplication");                          
        if (tmpAgreementApplicationList.size() == 1) {            
            this.agreementApplication = new AgreementApplication();
            this.agreementApplication.initFromElement(element.getChild("AgreementApplication"));
        } else {
            this.setAgreementApplications(new Vector());
            Iterator it = tmpAgreementApplicationList.iterator();
            int i=0;            
            while (it.hasNext()) {
                i++;
                AgreementApplication tmpAgreementAppl = new AgreementApplication();
                org.jdom.Element tmpEl = (org.jdom.Element) it.next();
                tmpAgreementAppl.initFromElement(tmpEl);
                this.getAgreementApplications().add(tmpAgreementAppl);                                                
            }
        }
        
        List customerOfferElementList = element.getChildren("CustomerOffer");
        if(customerOfferElementList.size() > 0)
        {
            this.customerOffers = new Vector();
            Iterator it = customerOfferElementList.iterator();
            while (it.hasNext())
            {
                Element el = (Element) it.next();
                this.customerOffers.add(el.getTextTrim());
            }
        }
        
        if (element.getChild("error") != null) {
            this.error = new se.mpab.psi.mo.Error();
            this.error.initFromElement(element.getChild("error"));
        }
    }
    
    /**
     * Getter for property error.
     * @return Value of property error.
     */
    public Error getError() {
        return error;
    }
    
    /**
     * Setter for property error.
     * @param error New value of property error.
     */
    public void setError(Error error) {
        this.creditApplication = null;
        this.error = error;
    }

    public Vector getCreditApplications() {
        return creditApplications;
    }

    public void setCreditApplications(Vector creditApplications) {
        this.creditApplications = creditApplications;
    }
    
    public Vector getAgreementApplications() {
        return agreementApplications;
    }

    public void setAgreementApplications(Vector agreementApplications) {
        this.agreementApplications = agreementApplications;
    }
    
    public Vector getCustomerOffers() {
        return customerOffers;
    }

    public void setCustomerOffers(Vector customerOffers) {
        this.customerOffers = customerOffers;
    }
    
    public Vector getCustomerOfferMatrices() {
        return this.customerOfferMatrices;
    }

    public void setCustomerOfferMatrices(Vector matrices) {
        this.customerOfferMatrices = matrices;
    }
}
