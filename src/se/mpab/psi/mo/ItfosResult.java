/*
 * ItfosResultBody.java
 *
 * Created on den 11 januari 2005, 13:30
 */

package se.mpab.psi.mo;

import org.jdom.Element;
import org.jdom.Namespace;
import se.mpab.util.xml.XMLPackUnpack;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class ItfosResult implements XMLPackUnpack {
    
    private String resultDescription;
    
    private int resultCode;
    
    /** Creates a new instance of ItfosResultBody */
    public ItfosResult() {
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        return null;
    }
    
    public String getElementName() {
        return null;
    }
    
    public void initFromElement(org.jdom.Element element) {
        Element body = element.getChild("body");
        Element result = body.getChild("result") ;
        this.resultCode = Integer.parseInt(result.getChild("resultcode").getTextTrim());
        this.resultDescription = result.getChild("resultdescription").getTextTrim();
        
    }
    

    /**
     * Getter for property resultDescription.
     * @return Value of property resultDescription.
     */
    public java.lang.String getResultDescription() {
        return resultDescription;
    }
    
    /**
     * Setter for property resultDescription.
     * @param resultDescription New value of property resultDescription.
     */
    public void setResultDescription(java.lang.String resultDescription) {
        this.resultDescription = resultDescription;
    }
    
    /**
     * Getter for property resultCode.
     * @return Value of property resultCode.
     */
    public int getResultCode() {
        return resultCode;
    }
    
    /**
     * Setter for property resultCode.
     * @param resultCode New value of property resultCode.
     */
    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
    
}
