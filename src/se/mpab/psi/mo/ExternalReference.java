/*
 * ExternalReference.java
 *
 * Created on den 8 oktober 2004, 09:06
 */

package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;
/**
 *
 * @author  janne.berg
 */
public class ExternalReference  implements XMLPackUnpack {
    
    private String externalReference;
    
    //private int id;
        
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        
        Element e = new Element(getElementName(), namespace);
        
        if(this.externalReference != null){
        e.addContent((Element) new Element("externalReference",namespace).addContent(this.externalReference));
        }
        //e.addContent((Element) new Element("id",namespace).addContent(Integer.toString(this.id)));
        return e;
    }
    public void initFromElement(org.jdom.Element element) {
        if(element.getChild("externalReference") != null) {
        externalReference = element.getChild("externalReference").getTextTrim();
        
    }
        //this.id = Integer.parseInt(element.getChild("id").getTextTrim());
    }
    public String getElementName() {
        return "externalReference";
    }
    
    /**
     * Getter for property ExternalReference.
     * @return Value of property ExternalReference.
     */
    public java.lang.String getExternalReference() {
        return externalReference;
    }
    
    /**
     * Setter for property externalReference.
     * @param externalReference New value of property externalReference.
     */
    public void setExternalReference(java.lang.String externalReference) {
        this.externalReference = externalReference;
    }
    
    
}
