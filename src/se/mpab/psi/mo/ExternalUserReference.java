/*
 * ExternalUserReference.java
 *
 * Created on den 8 oktober 2004, 09:06
 */

package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;
/**
 *
 * @author  janne.berg
 */
public class ExternalUserReference  implements XMLPackUnpack {
    
    private String fullName;
    private String type;
    private String id;
    
    /** Creates a new instance of ExternalUserReference */
    public ExternalUserReference() {
    }
    
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("type",namespace).addContent(this.type));
        e.addContent((Element) new Element("fullName",namespace).addContent(this.fullName));
        e.addContent((Element) new Element("id",namespace).addContent(this.id) );        
        return e;
    }
    public void initFromElement(org.jdom.Element element) {
        this.type = element.getChild("type").getTextTrim();
        this.fullName = element.getChild("fullName").getTextTrim();
        this.id = element.getChild("id").getTextTrim();
    }
    public String getElementName() {
        return "externalUserReference";
    }
    
    /**
     * Getter for property fullName.
     * @return Value of property fullName.
     */
    public java.lang.String getFullName() {
        return fullName;
    }
    
    /**
     * Setter for property fullName.
     * @param fullName New value of property fullName.
     */
    public void setFullName(java.lang.String fullName) {
        this.fullName = fullName;
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public String getId() {
        return id;
    }
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public java.lang.String getType() {
        return type;
    }    
    
    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }
    
}
