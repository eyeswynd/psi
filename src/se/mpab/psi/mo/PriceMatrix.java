package se.mpab.psi.mo;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import org.jdom.Element;
import org.jdom.Namespace;
import se.mpab.util.xml.XMLPackUnpack;

public class PriceMatrix implements XMLPackUnpack
{
    private String id;
    private Vector<PriceMatrixItem> items;
    
    public PriceMatrix(){}
    
    public String getElementName() {
        return "matrix";
    }
    
    public org.jdom.Element getElement(Namespace namespace)
    {
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("id",namespace).addContent(this.id));
        for(PriceMatrixItem item : this.items)
        {
            e.addContent(item.getElement(namespace));
        }
        return e;
    }

    public void initFromElement(Element element) 
    {
        this.id = element.getChild("id").getTextTrim();
        this.items = new Vector<PriceMatrixItem>();
        List<Element> newItems = element.getChildren("item");
        for(Element e : newItems)
        {
            PriceMatrixItem item = new PriceMatrixItem();
            item.initFromElement(e);
            this.items.add(item);
        }
    }
    
}
