
// Copyright Marquardt & Partners
/*
 * Address.java
 *
 * Created on den 29 december 2004, 13:01
 */

package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;
/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */

public class Address implements XMLPackUnpack {
    
    private String type;
    
    private String postal;
    
    private String zip;
    
    private String city;
    
    private String country;
    
    /** Creates a new instance of Address */
    public Address() {
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("type",namespace).addContent(this.type));
        e.addContent((Element) new Element("postal",namespace).addContent(this.postal));
        e.addContent((Element) new Element("zip",namespace).addContent(this.zip));
        e.addContent((Element) new Element("city",namespace).addContent(this.city));
        e.addContent((Element) new Element("country",namespace).addContent(this.country));
        return e;
    }
    
    public String getElementName() {
        return "address";
    }
    
    public void initFromElement(org.jdom.Element element) {
        this.type = element.getChild("type").getTextTrim();
        this.postal = element.getChild("postal").getTextTrim();
        this.zip = element.getChild("zip").getTextTrim();
        this.city = element.getChild("city").getTextTrim();
        this.country = element.getChild("country").getTextTrim();
    }
    
    /**
     * Getter for property city.
     * @return Value of property city.
     */
    public java.lang.String getCity() {
        return city;
    }
    
    /**
     * Setter for property city.
     * @param city New value of property city.
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }
    
    /**
     * Getter for property country.
     * @return Value of property country.
     */
    public java.lang.String getCountry() {
        return country;
    }
    
    /**
     * Setter for property country.
     * @param country New value of property country.
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }
    
  
    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public java.lang.String getType() {
        return type;
    }
    
    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }
    
    /**
     * Getter for property zip.
     * @return Value of property zip.
     */
    public java.lang.String getZip() {
        return zip;
    }
    
    /**
     * Setter for property zip.
     * @param zip New value of property zip.
     */
    public void setZip(java.lang.String zip) {
        this.zip = zip;
    }
    
    /**
     * Getter for property postal.
     * @return Value of property postal.
     */
    public java.lang.String getPostal() {
        return postal;
    }
    
    /**
     * Setter for property postal.
     * @param postal New value of property postal.
     */
    public void setPostal(java.lang.String postal) {
        this.postal = postal;
    }
    
    public String toString(){
    return "Type: " + type + " postal:" + postal + " zip:" + zip + " city:" + city;
    }
    
}
