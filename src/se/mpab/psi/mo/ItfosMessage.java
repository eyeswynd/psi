/*
 * ItfosMessage.java
 *
 * Created on den 10 januari 2005, 15:22
 */

package se.mpab.psi.mo;

import org.jdom.Element;
import org.jdom.Namespace;
import se.mpab.util.xml.XMLPackUnpack;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class ItfosMessage implements XMLPackUnpack {
    
    EntreMessageHeader header;
    EntreMessageBody body;
    ItfosResult result;
    
    
    /** Creates a new instance of ItfosMessage */
    public ItfosMessage() {
    }
    
    public Element getElement(Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        if(header != null){
            e.addContent(this.header.getElement(namespace));
        }
        if (body != null){
            e.addContent(this.body.getElement(namespace));
        }
        if (result != null) {
            e.addContent(this.result.getElement(namespace));
        }
        return e;
    }
    
    public String getElementName() {
        return "itfosmessage";
    }
    
    public void initFromElement(Element element) {
        
        
        if (element.getChild("header") != null) {
            header = new EntreMessageHeader();
            header.initFromElement(element.getChild("header"));
        }
        if (element.getChild("body") != null) {
            body = new EntreMessageBody();
            body.initFromElement(element.getChild("body"));
        }
        if (element.getChild("result") != null) {
            result = new ItfosResult();
            result.initFromElement(element.getChild("result"));
        }
    }
    
    /**
     * Getter for property body.
     * @return Value of property body.
     */
    public se.mpab.psi.mo.EntreMessageBody getBody() {
        return body;
    }
    
    /**
     * Setter for property body.
     * @param body New value of property body.
     */
    public void setBody(se.mpab.psi.mo.EntreMessageBody body) {
        this.body = body;
    }
    
    /**
     * Getter for property header.
     * @return Value of property header.
     */
    public se.mpab.psi.mo.EntreMessageHeader getHeader() {
        return header;
    }
    
    /**
     * Setter for property header.
     * @param header New value of property header.
     */
    public void setHeader(se.mpab.psi.mo.EntreMessageHeader header) {
        this.header = header;
    }
    
}
