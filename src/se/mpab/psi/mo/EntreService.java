/*
 * EntreService.java
 *
 * Created on den 31 augusti 2004, 16:22
 */

package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;
/**
 *
 * @author  vino42
 */
public class EntreService implements XMLPackUnpack {
    
    private String object;
    
    private String method;
    // Getters
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("object",namespace).addContent(this.object));
        e.addContent((Element) new Element("method",namespace).addContent(this.method));
        return e;
    }
        public void initFromElement(org.jdom.Element element) {        
        this.object = element.getChild("object").getTextTrim();                
        this.method = element.getChild("method").getTextTrim();                
    }
    public String getElementName() {
        return "service";
    }
    public String getMethod() {
        return this.method;
    }
    public String getObject() {
        return this.object;
    }
    // Setters
    public void setMethod(String method) {
        this.method = method;
    }
    public void setObject (String object) {
        this.object = object;
    }
    

    
}


