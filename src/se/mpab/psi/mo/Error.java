/*
 * Error.java
 *
 * Created on den 7 september 2004, 10:45
 */

package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;
/**
 *
 * @author  Viktor Norbergm, Marquards & Partners AB
 */
public class Error implements XMLPackUnpack {
    
    private int errorCode;
    private String errorString  = null;
    
    /** Creates a new instance of Error */
    public Error() {
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("errorcode",namespace).addContent(Integer.toString(this.errorCode)));
        e.addContent((Element) new Element("errorstring",namespace).addContent(this.errorString));
        return e;
    }
    
    public String getElementName() {
        return "error";
    }
    
    public void initFromElement(org.jdom.Element element) {
        this.errorCode      = Integer.parseInt(element.getChild("errorcode").getTextTrim());
        this.errorString    = element.getChild("errorstring").getTextTrim();
    }
    
    /**
     * Getter for property errorCode.
     * @return Value of property errorCode.
     */
    public int getErrorCode() {
        return errorCode;
    }    
    
    /**
     * Setter for property errorCode.
     * @param errorCode New value of property errorCode.
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }    
    
    /**
     * Getter for property errorString.
     * @return Value of property errorString.
     */
    public java.lang.String getErrorString() {
        return errorString;
    }
    
    /**
     * Setter for property errorString.
     * @param errorString New value of property errorString.
     */
    public void setErrorString(java.lang.String errorString) {
        this.errorString = errorString;
    }
    
}
