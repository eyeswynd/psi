/*
 * Decision.java
 *
 * Created on den 8 oktober 2004, 17:03
 */

package se.mpab.psi.mo;
import org.jdom.*;
import se.mpab.util.xml.*;
import se.mpab.psi.bo.*;
/**
 *
 * @author  janne.berg
 */
public class Decision  implements XMLPackUnpack{
    
    private String status;
    
    private String validUntil;
    
    private String messageFromCreditApprover;
    
    private Duration duration;
    
    private Amount amount;    
    
    private Condition condition;
    
    /** Creates a new instance of Decision */
    public Decision() {
    }
    
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        
        
        if (this.status != null){
            e.addContent((Element) new Element("status",namespace).addContent(this.status));
        }
        if (this.validUntil != null){
            e.addContent((Element) new Element("validUntil",namespace).addContent(this.validUntil));
        }
        if (this.messageFromCreditApprover != null){
            e.addContent((Element) new Element("messageFromCreditApprover",namespace).addContent(this.messageFromCreditApprover));
        }
        if (this.duration != null) {
            e.addContent(duration.getElement(namespace));
        }
        if (this.amount != null) {
            e.addContent(amount.getElement(namespace));
        }
        if (this.condition != null) {
            e.addContent(condition.getElement(namespace));
        }
        return e;
    }
    
    // getters
    public String getElementName() {
        return "decision";
    }
    
    public void initFromElement(org.jdom.Element element) {
        
        if (element.getChild("status") != null) {
            status = element.getChild("status").getTextTrim();
        }
        if (element.getChild("validUntil") != null) {
            validUntil = element.getChild("validUntil").getTextTrim();
        }
        if (element.getChild("messageFromCreditApprover") != null) {
            messageFromCreditApprover = element.getChild("messageFromCreditApprover").getTextTrim();
        }
        if (element.getChild("duration") != null) {
            duration = new Duration();
            duration.initFromElement(element.getChild("duration"));
        }
        if (element.getChild("amount") != null) {
            duration = new Duration();
            duration.initFromElement(element.getChild("amount"));
        }        
        if (element.getChild("condition") != null) {
            condition = new Condition();
            condition.initFromElement(element.getChild("condition"));
        }
        
    }
    
    
    /**
     * Getter for property status.
     * @return Value of property status.
     */
    public java.lang.String getStatus() {
        return status;
    }
    
    /**
     * Setter for property status.
     * @param status New value of property status.
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }
    
    /**
     * Getter for property validUntil.
     * @return Value of property validUntil.
     */
    public java.lang.String getValidUntil() {
        return validUntil;
    }
    
    /**
     * Setter for property validUntil.
     * @param validUntil New value of property validUntil.
     */
    public void setValidUntil(java.lang.String validUntil) {
        this.validUntil = validUntil;
    }
    
    /**
     * Getter for property messageFromCreditApprover.
     * @return Value of property messageFromCreditApprover.
     */
    public java.lang.String getMessageFromCreditApprover() {
        return messageFromCreditApprover;
    }
    
    /**
     * Setter for property messageFromCreditApprover.
     * @param messageFromCreditApprover New value of property messageFromCreditApprover.
     */
    public void setMessageFromCreditApprover(java.lang.String messageFromCreditApprover) {
        this.messageFromCreditApprover = messageFromCreditApprover;
    }
    
    /**
     * Getter for property duration.
     * @return Value of property duration.
     */
    public se.mpab.psi.mo.Duration getDuration() {
        return duration;
    }
    
    /**
     * Setter for property duration.
     * @param duration New value of property duration.
     */
    public void setDuration(se.mpab.psi.mo.Duration duration) {
        this.duration = duration;
    }
    
    /**
     * Getter for property amount.
     * @return Value of property amount.
     */
    public se.mpab.psi.mo.Amount getAmount() {
        return amount;
    }
    
    /**
     * Setter for property amount.
     * @param amount New value of property amount.
     */
    public void setAmount(se.mpab.psi.mo.Amount amount) {
        this.amount = amount;
    }
    
    /**
     * Getter for property condition.
     * @return Value of property condition.
     */
    public se.mpab.psi.mo.Condition getCondition() {
        return condition;
    }
    
    /**
     * Setter for property condition.
     * @param condition New value of property condition.
     */
    public void setCondition(se.mpab.psi.mo.Condition condition) {
        this.condition = condition;
    }
    
}
