package se.mpab.psi.mo;

import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;
import org.jdom.Namespace;
import se.mpab.util.xml.XMLPackUnpack;

public class PriceMatrixItem implements XMLPackUnpack
{
    private String amount;
    private String duration;
    private String periodLength;
    private String value;
    
    public PriceMatrixItem() {}
    
    public PriceMatrixItem(String amount, String duration, String periodLength, String value)
    {
        this.amount = amount;
        this.duration = duration;
        this.periodLength = periodLength;
        this.value = value;
    }
    
    public String getElementName() {
        return "item";
    }
    
    public org.jdom.Element getElement(Namespace namespace)
    {
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("amount",namespace).addContent(this.amount));
        e.addContent((Element) new Element("duration",namespace).addContent(this.duration));
        e.addContent((Element) new Element("periodLength",namespace).addContent(this.periodLength));
        e.addContent((Element) new Element("value",namespace).addContent(this.value));
        return e;
    }

    public void initFromElement(Element element) 
    {
        this.amount = element.getChild("row").getTextTrim();
        this.duration = element.getChild("column").getTextTrim();
        this.periodLength = element.getChild("depth").getTextTrim();
        this.value = element.getChild("value").getTextTrim();
    }
}
