/*
 * DealNumber.java
 *
 * Created on den 11 oktober 2004, 17:14
 */

package se.mpab.psi.mo;

import org.jdom.*;
import se.mpab.util.xml.*;
import se.mpab.psi.bo.*;
/**
 *
 * @author  Jonas Norrlander
 */
public class DealStatus implements XMLPackUnpack {
    

    private String dealStatus = null;
    private String calculationStatus = null;
    private String creditStatus = null;
    

    /** Creates a new instance of DealNumber */
    public DealStatus() {
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        
        e.addContent((Element) new Element("dealStatus",namespace).addContent(this.getDealStatus()));
        e.addContent((Element) new Element("calculationStatus",namespace).addContent(this.getCalculationStatus()));
        //e.addContent((Element) new Element("creditStatus",namespace).addContent(this.getCreditStatus()));
        
        return e;
    }
    
    public String getElementName() {
        return "status";
    }
    
    public void initFromElement(org.jdom.Element element) {
        this.dealStatus     = element.getChild("dealStatus").getTextTrim();
        this.calculationStatus    = element.getChild("calculationStatus").getTextTrim();        
        //this.creditStatus    = element.getChild("creditStatus").getTextTrim();
    }
    

    /**
     * Get the value of dealStatus
     *
     * @return the value of dealStatus
     */
    public String getDealStatus() {
        return dealStatus;
    }

    /**
     * Set the value of dealStatus
     *
     * @param dealStatus new value of dealStatus
     */
    public void setDealStatus(String dealStatus) {
        this.dealStatus = dealStatus;
    }


    /**
     * Get the value of calculationStatus
     *
     * @return the value of calculationStatus
     */
    public String getCalculationStatus() {
        return calculationStatus;
    }

    /**
     * Set the value of calculationStatus
     *
     * @param calculationStatus new value of calculationStatus
     */
    public void setCalculationStatus(String calculationStatus) {
        this.calculationStatus = calculationStatus;
    }
    

    /**
     * Get the value of creditStatus
     *
     * @return the value of creditStatus
     */
    public String getCreditStatus() {
        return creditStatus;
    }

    /**
     * Set the value of creditStatus
     *
     * @param creditStatus new value of creditStatus
     */
    public void setCreditStatus(String creditStatus) {
        this.creditStatus = creditStatus;
    }

    
}
