// Copyright Applicon
/*
 * InstallmentPayments .java
 *
 * Created on den 19 april 2013, 15:29
 */
package se.mpab.psi.mo;

import org.jdom.*;
import se.mpab.util.xml.*;

/**
 *
 * @author niklas.schold
 */
public class VAT {
    private int periodLength;
    private Double interestRate;
    
    public VAT(){
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("periodLength", namespace).addContent(Integer.toString(this.periodLength)));
        e.addContent((Element) new Element("interestRate", namespace).addContent(Double.toString(this.interestRate)));
        return e;
    }
    
    public String getElementName() {
        return "VAT";
    }
    
    // Getters
    public int getPeriodLength() {
        return this.periodLength;
    }
    public Double getInterestRate() {
        return this.interestRate;
    }
    
    // Setters
    public void setPeriodLength(int periodLength) {
        this.periodLength = periodLength;
    }
    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }
    
    public void initFromElement(org.jdom.Element element) {
        if (element.getChild("periodLength") != null) {
            this.periodLength = Integer.parseInt(element.getChild("periodLength").getTextTrim());
        }
        if (element.getChild("interestRate") != null) {
            this.interestRate = Double.parseDouble(element.getChild("interestRate").getTextTrim());
        }
    }
}
