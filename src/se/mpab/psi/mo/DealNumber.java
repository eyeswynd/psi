/*
 * DealNumber.java
 *
 * Created on den 11 oktober 2004, 17:14
 */
package se.mpab.psi.mo;

import org.jdom.*;
import se.mpab.util.xml.*;
import se.mpab.psi.bo.*;

/**
 *
 * @author  vino42
 */
public class DealNumber implements XMLPackUnpack {

    private String prefix = null;
    private long id;
    private int version;

    /** Creates a new instance of DealNumber */
    public DealNumber() {
    }

    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);

        e.addContent((Element) new Element("prefix", namespace).addContent(this.getPrefix()));
        e.addContent((Element) new Element("id", namespace).addContent(Long.toString(this.getId())));
        e.addContent((Element) new Element("version", namespace).addContent(Integer.toString(this.getVersion())));

        return e;
    }

    public String getElementName() {
        return "dealNumber";
    }

    public void initFromElement(org.jdom.Element element) {
        this.prefix = element.getChild("prefix").getTextTrim();
        this.id = Long.parseLong(element.getChild("id").getTextTrim());
        if (element.getChild("version") != null) {
            this.version = Integer.parseInt(element.getChild("version").getTextTrim());
        } else {
            this.version = 0;
        }
    }

    public String getEntreDealNo() {
        return Long.toString(this.id) + "-" + Long.toString(this.version);
    }

    /**
     * Getter for property prefix.
     * @return Value of property prefix.
     */
    public java.lang.String getPrefix() {
        return prefix;
    }

    /**
     * Setter for property prefix.
     * @param prefix New value of property prefix.
     */
    public void setPrefix(java.lang.String prefix) {
        this.prefix = prefix;
    }

    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public long getId() {
        return id;
    }

    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Getter for property version.
     * @return Value of property version.
     */
    public int getVersion() {
        
        return version;
    }

    /**
     * Setter for property version.
     * @param version New value of property version.
     */
    public void setVersion(int version) {
        this.version = version;
    }
}
