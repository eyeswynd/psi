/*
 * Agreement.java
 *
 * Created on den 31 augusti 2004, 14:22
 */
package se.mpab.psi.mo;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.jdom.*;
import se.mpab.util.xml.*;

public class AgreementApplication implements XMLPackUnpack {

    private DealNumber dealNumber;
    private DealStatus status;
    private Vector<String> esignContractEmail;
    private static org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger(CreditApplication.class);
    
    public AgreementApplication() {
        this.esignContractEmail = new Vector<String>();
    }

    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);


        if (this.dealNumber != null) {
            e.addContent(dealNumber.getElement(namespace));
        }

        if (this.status != null) {
            e.addContent(status.getElement(namespace));
        }

        return e;
    }
    // getters
    public String getElementName() {
        return "AgreementApplication";
    }

    public void initFromElement(org.jdom.Element element) {

        if (element.getChild("dealNumber") != null) {
            dealNumber = new DealNumber();
            dealNumber.initFromElement(element.getChild("dealNumber"));
        }

        if (element.getChild("esignContractEmail") != null) {
            Element esign = element.getChild("esignContractEmail");
            List<Element> emails = esign.getChildren("item");
            Iterator<Element> iter = emails.iterator();
            while(iter.hasNext())
            {
                this.esignContractEmail.add(iter.next().getValue());
            }
        }
    }

    /**
     * Getter for property dealNumber.
     * @return Value of property dealNumber.
     */
    public se.mpab.psi.mo.DealNumber getDealNumber() {
        return dealNumber;
    }

    /**
     * Setter for property dealNumber.
     * @param dealNumber New value of property dealNumber.
     */
    public void setDealNumber(se.mpab.psi.mo.DealNumber dealNumber) {
        this.dealNumber = dealNumber;
    }

    public se.mpab.psi.mo.DealStatus getStatus() {
        return status;
    }

    public void setStatus(se.mpab.psi.mo.DealStatus status) {
        this.status = status;
    }
    
    public Vector<String> getEsignContractEmail() {
        return esignContractEmail;
    }

    public void setEsignContractEmail(Vector<String> emails) {
        this.esignContractEmail = emails;
    }
}