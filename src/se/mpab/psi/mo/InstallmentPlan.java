// Copyright Applicon
/*
 * InstallmentPayments .java
 *
 * Created on den 18 april 2013, 15:01
 */
package se.mpab.psi.mo;

import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.LogManager;
import org.jdom.*;
import se.mpab.psi.Delegator;
import se.mpab.util.xml.*;

/**
 *
 * @author niklas.schold
 */
public class InstallmentPlan {
    private Vector installmentPayments;
    private Vector interestPayments;
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(Delegator.class);
    
    public InstallmentPlan(){
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        
        if (this.installmentPayments != null) {
            Iterator it = this.installmentPayments.iterator();
            while (it.hasNext()) {
                Double tmpD = (Double) it.next();
                long lngVal = new Double(tmpD).longValue();
                e.addContent((Element) new Element("value",namespace).addContent(Long.toString(lngVal)));
            }
        }
        
        if (this.interestPayments != null) {
            Iterator it = this.interestPayments.iterator();
            while (it.hasNext()) {
                Double tmpD = (Double) it.next();
                long lngVal = new Double(tmpD).longValue();
                e.addContent((Element) new Element("value",namespace).addContent(Long.toString(lngVal)));
            }
        }
        
        return e;
    }
    
    // Getters
    public String getElementName() {
        return "installmentPlan";
    }
    public Vector getInstallmentPayments() {
        return this.installmentPayments;
    }
    public Vector getInterestPayments() {
        return this.interestPayments;
    }
    
    // Setters
    public void setInstallmentPayments(Vector installmentPayments) {
        this.installmentPayments = installmentPayments;
    }
    public void setInterestPayments(Vector interestPayments) {
        this.interestPayments = interestPayments;
    }
    
    public void initFromElement(org.jdom.Element element) {
        if (element.getChild("installmentPayments") != null){
            InstallmentPlanPayments instPaymts = new InstallmentPlanPayments();
            instPaymts.initFromElement(element.getChild("installmentPayments"));
            this.installmentPayments = instPaymts.getPayments();
        }
        
        if (element.getChild("interestPayments") != null){
            InstallmentPlanPayments interPaymts = new InstallmentPlanPayments();
            interPaymts.initFromElement(element.getChild("interestPayments"));
            this.interestPayments = interPaymts.getPayments();
        }
        
        
    }
}
