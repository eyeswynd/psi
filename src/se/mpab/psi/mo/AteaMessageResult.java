/*
 * AteaMessageResult.java
 *
 * Created on den 27 januari 2015, 13:30
 */

package se.mpab.psi.mo;

import org.jdom.Element;
import org.jdom.Namespace;
import se.mpab.util.xml.XMLPackUnpack;

/**
 *
 * @author  hans.olsberg
 */
public class AteaMessageResult implements XMLPackUnpack {
    
    private String resultDescription;    
    private int resultCode = -1;
    
    /** Creates a new instance of SendMessageResultBody */
    public AteaMessageResult() {
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        return null;
    }
    
    public String getElementName() {
        return null;
    }
    
    public void initFromElement(org.jdom.Element element) {        
        Element correlationToken = element.getChild("CorrelationToken");        
        this.resultDescription = correlationToken.getTextTrim();
        if(!this.resultDescription.isEmpty())
        {
            this.resultCode = 0;
        }
    }    

    /**
     * Getter for property resultDescription.
     * @return Value of property resultDescription.
     */
    public java.lang.String getResultDescription() {
        return resultDescription;
    }
    
    /**
     * Setter for property resultDescription.
     * @param resultDescription New value of property resultDescription.
     */
    public void setResultDescription(java.lang.String resultDescription) {
        this.resultDescription = resultDescription;
    }
    
    /**
     * Getter for property resultCode.
     * @return Value of property resultCode.
     */
    public int getResultCode() {
        return resultCode;
    }
    
    /**
     * Setter for property resultCode.
     * @param resultCode New value of property resultCode.
     */
    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
    
}
