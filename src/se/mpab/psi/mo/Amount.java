// Copyright Marquardt & Partners
/*
 * CreditAmount .java
 *
 * Created on den 31 augusti 2004, 14:26
 */

package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;
/**
 *
 * @author  vino42
 */
public class Amount implements XMLPackUnpack{
    private String currency;
    
    private double value;
    
     public Amount(){
    }
    public Amount(String currency, double value){
        this.currency = currency;
        this.value = roundDouble(value, 2);
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        e.addContent((Element) new Element("currency",namespace).addContent(this.currency));
        if (this.currency.equals("EUR")) {
            e.addContent((Element) new Element("value",namespace).addContent(Double.toString(this.value)));
        } else {
            long lngVal = new Double(this.value).longValue();
            e.addContent((Element) new Element("value",namespace).addContent(Long.toString(lngVal)));
        }
        return e;
    }
    
    public static final double roundDouble(double d, int places) {
        return Math.round(d * Math.pow(10, (double) places)) / Math.pow(10,(double) places);
    }

    
    // Getters
    public String getElementName() {
        return "amount";
    }    
    public String getCurrency() {
        return this.currency;
    }
    public double getValue() {
        return this.value;
    }
    
    // Setters
    public void setCurrency(String currency) {
        this.currency = currency;        
    }
    public void setValue(double value) {
        this.value = value;        
    }
    
    public void initFromElement(org.jdom.Element element) {
        this.currency = element.getChild("currency").getTextTrim();
        this.value = Double.parseDouble(element.getChild("value").getTextTrim());
        if (this.currency.equals("EUR")) {
            this.value = roundDouble(this.value, 2);
        } 
    }
    
}