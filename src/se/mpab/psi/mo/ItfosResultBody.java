/*
 * ItfosResultBody.java
 *
 * Created on den 11 januari 2005, 13:30
 */

package se.mpab.psi.mo;

import org.jdom.Element;
import org.jdom.Namespace;
import se.mpab.util.xml.XMLPackUnpack;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class ItfosResultBody implements XMLPackUnpack {
    
    
    /** Creates a new instance of ItfosResultBody */
    public ItfosResultBody() {
    }
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        return null;
    }
    
    public String getElementName() {
        return null;
    }
    
    public void initFromElement(org.jdom.Element element) {
    }
    
}
