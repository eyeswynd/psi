/*
 * CreditApplication.java
 *
 * Created on den 31 augusti 2004, 14:22
 */
package se.mpab.psi.mo;

import java.util.Date;
import org.jdom.*;
import se.mpab.util.xml.*;
import java.util.List;
import java.util.Iterator;
import java.util.Vector;
import java.text.SimpleDateFormat;
import se.mpab.psi.Status;

/**
 *
 * @author  vino42
 */
public class CreditApplication implements XMLPackUnpack {

    private Client client;
    private Duration duration;
    private Decision decision;
    private DealNumber dealNumber;
    private String offerId;
    private Vector externalUserReference = new Vector();
    private String messageToCreditApprover;
    private DealStatus status;
    private String interestType;
    private ResidualValue rv;
    private PeriodFee pmt;
    private Date deliveryDate;
    
    private String dealType;
    private VAT vat;
    private InstallmentPlan installmentPlan = new InstallmentPlan();
    private String plannedStartDate;
    private String installmentMethod;
    private Amount downPayment;
    private String paymentInAdvance;
    private String externalReference;

   
    /**
     * Get the value of deliveryDate
     *
     * @return the value of deliveryDate
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Set the value of deliveryDate
     *
     * @param deliveryDate new value of deliveryDate
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    
    /**
     * Get the value of interestType
     *
     * @return the value of interestType
     */
    public String getInterestType() {
        return interestType;
    }

    /**
     * Set the value of interestType
     *
     * @param interestType new value of interestType
     */
    public void setInterestType(String interestType) {
        this.interestType = interestType;
    }  
    
    /**
     * Get the value of externalReference
     *
     * @return the value of externalReference
     */
    public String getExternalReference() {
        return externalReference;
    }

    /**
     * Set the value of externalReference
     *
     * @param externalReference new value of externalReference
     */
    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }  

   //private Equipment equipment;
    private Vector equipment = new Vector();
    private double totalEquipmentValue;
    private static org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger(CreditApplication.class);

    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        if (this.client != null) {
            e.addContent(client.getElement(namespace));
        }
        /*if (this.amount != null) {
        e.addContent(amount.getElement(namespace));
        }*/
        if (this.duration != null) {
            e.addContent(duration.getElement(namespace));
        }
        if (this.offerId != null) {
            e.addContent((Element) new Element("offerId", namespace).addContent(this.offerId));
        }
        if (this.externalUserReference != null) {
            Iterator it = this.externalUserReference.iterator();
            while (it.hasNext()) {
                ExternalUserReference tmpUserReference = (ExternalUserReference) it.next();
                e.addContent(tmpUserReference.getElement(namespace));
            }
        }
        if (this.decision != null) {
            e.addContent(decision.getElement(namespace));
        }
        if (this.dealNumber != null) {
            e.addContent(dealNumber.getElement(namespace));
        }
        if (this.status != null) {
            e.addContent(status.getElement(namespace));
        }

        if (this.interestType != null) {
            e.addContent((Element) new Element("interest", namespace).addContent(this.interestType));
        }

        if (this.messageToCreditApprover != null) {
            e.addContent((Element) new Element("messageToCreditApprover", namespace).addContent(this.messageToCreditApprover));
        }
        if (this.equipment != null) {
            // Extract from Vector
            Iterator it = this.equipment.iterator();
            double eqValue = 0;
            while (it.hasNext()) {
                Equipment tmpEquipment = (Equipment) it.next();
                e.addContent(tmpEquipment.getElement(namespace));
                eqValue += tmpEquipment.getAmount().getValue() * tmpEquipment.getQuantity();
            }
            totalEquipmentValue = eqValue;
        }
        if (this.dealType != null) {
            e.addContent((Element) new Element("dealType",namespace).addContent(this.dealType));
        }
        
        //HO Setting reference used for ATEA 20150129
        if (this.externalReference != null) {
            e.addContent((Element) new Element("externalReference", namespace).addContent(this.externalReference));
        }
        
        if (this.pmt != null) {
            logger.info("PMT IS NOT NULL!!");
            e.addContent(this.pmt.getElement(namespace));
        } else {
            logger.info("PMT IS NULL!!");
        }
        
        return e;
    }
    // getters
    public String getElementName() {
        return "CreditApplication";
    }

    public void initFromElement(org.jdom.Element element) {

        if (element.getChild("client") != null) {
            client = new Client();
            client.initFromElement(element.getChild("client"));
        }

        if (element.getChild("duration") != null) {
            duration = new Duration();
            duration.initFromElement(element.getChild("duration"));
        }
        if (element.getChild("offerId") != null) {
            offerId = element.getChild("offerId").getTextTrim();
        }

        if (element.getChild("interest") != null) {
            interestType = element.getChild("interest").getTextTrim();
        }
        
        if (element.getChild("deliveryDate") != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                deliveryDate = new Date(sdf.parse(element.getChild("deliveryDate").getTextTrim()).getTime());
            }
            catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                se.mpab.psi.mo.EntreMessageBody errorBody = new se.mpab.psi.mo.EntreMessageBody();
                se.mpab.psi.mo.Error errorMessage = new se.mpab.psi.mo.Error();
                errorMessage.setErrorString(ex.getMessage());
                errorMessage.setErrorCode(Status.STATUS_ERROR_IN_REQUEST);
                errorBody.setError(errorMessage);
            }
        }

        if (element.getChild("residualValue") != null) {
            rv = new ResidualValue();
            rv.initFromElement(element.getChild("residualValue"));
        }

        if (element.getChild("periodFee") != null) {
            pmt = new PeriodFee();
            pmt.initFromElement(element.getChild("periodFee"));
        }

        if (element.getChild("dealNumber") != null) {
            dealNumber = new DealNumber();
            dealNumber.initFromElement(element.getChild("dealNumber"));
        }

        if (element.getChild("status") != null) {
            status = new DealStatus();
            status.initFromElement(element.getChild("status"));
        }

        if (element.getChild("messageToCreditApprover") != null) {
            messageToCreditApprover = element.getChild("messageToCreditApprover").getTextTrim();
        }
        if (element.getChildren("externalUserReference") != null) {
            List tmpUserReferences = element.getChildren("externalUserReference");
            Iterator it = tmpUserReferences.iterator();
            while (it.hasNext()) {
                ExternalUserReference tmpExternalUserReference = new ExternalUserReference();
                Element tmpEl = (Element) it.next();
                tmpExternalUserReference.initFromElement(tmpEl);
                this.externalUserReference.add(tmpExternalUserReference);
            }
        }
        if (element.getChildren("equipment") != null) {
            List tmpEquipments = element.getChildren("equipment");
            Iterator it = tmpEquipments.iterator();
            float eqValue = 0;
            while (it.hasNext()) {
                Equipment tmpEq = new Equipment();
                Element tmpEl = (Element) it.next();
                tmpEq.initFromElement(tmpEl);
                this.equipment.add(tmpEq);
                eqValue += tmpEq.getAmount().getValue() * tmpEq.getQuantity();
            }
            totalEquipmentValue = eqValue;
        }
        if (element.getChild("installmentPlan") != null) {
            InstallmentPlan tmpInstallmPlan = new InstallmentPlan();
            tmpInstallmPlan.initFromElement(element.getChild("installmentPlan"));
            this.installmentPlan = tmpInstallmPlan;
        }
        if (element.getChild("vat") != null) {
            vat = new VAT();
            vat.initFromElement(element.getChild("vat"));
        } else { vat = null; }
        
        if (element.getChild("plannedStartDate") != null) {
            this.plannedStartDate = element.getChild("plannedStartDate").getTextTrim();
        }
        if (element.getChild("installmentMethod") != null) {
            this.installmentMethod = element.getChild("installmentMethod").getTextTrim();
        }
        if (element.getChild("downPayment") != null) {
            Amount tmpDownPayment = new Amount();
            Element amountChild = element.getChild("downPayment");
            tmpDownPayment.initFromElement(amountChild.getChild("amount"));
            this.downPayment = tmpDownPayment;
        }
        if (element.getChild("paymentInAdvance") != null) {
            this.paymentInAdvance = element.getChild("paymentInAdvance").getTextTrim();
        }
        
        //HO Setting reference used for ATEA 20150129
        logger.info("Credit Application externalReference" + element.getChild("externalReference"));
        
        if (element.getChild("externalReference") != null) {
            externalReference = element.getChild("externalReference").getTextTrim();
        }
    }

    public ExternalUserReference getExternalUserReference(String type) {
        ExternalUserReference reference = null;
        Vector description = new Vector();
        for (Iterator it = externalUserReference.iterator(); it.hasNext();) {
            ExternalUserReference userReference = (ExternalUserReference) it.next();
            if (userReference.getType().equals(type)) {
                reference = userReference;
            }
        }
        return reference;
    }

    /**
     * Getter for property client.
     * @return Value of property client.
     */
    public se.mpab.psi.mo.Client getClient() {
        return client;
    }

    /**
     * Setter for property client.
     * @param client New value of property client.
     */
    public void setClient(se.mpab.psi.mo.Client client) {
        this.client = client;
    }

    /**
     * Getter for property duration.
     * @return Value of property duration.
     */
    public se.mpab.psi.mo.Duration getDuration() {
        return duration;
    }

    /**
     * Setter for property duration.
     * @param duration New value of property duration.
     */
    public void setDuration(se.mpab.psi.mo.Duration duration) {
        this.duration = duration;
    }
    
    /**
     * Getter for property offerId.
     * @return Value of property offerId.
     */
    public java.lang.String getOfferId() {
        return offerId;
    }

    /**
     * Setter for property offerId.
     * @param offerId New value of property offerId.
     */
    public void setOfferId(java.lang.String offerId) {
        this.offerId = offerId;
    }
    
    /**
     * Getter for property decision.
     * @return Value of property decision.
     */
    public se.mpab.psi.mo.Decision getDecision() {
        return decision;
    }

    /**
     * Setter for property decision.
     * @param decision New value of property decision.
     */
    public void setDecision(se.mpab.psi.mo.Decision decision) {
        this.decision = decision;
    }

    /**
     * Getter for property messageToCreditApprover.
     * @return Value of property messageToCreditApprover.
     */
    public java.lang.String getMessageToCreditApprover() {
        return messageToCreditApprover;
    }

    /**
     * Setter for property messageToCreditApprover.
     * @param messageToCreditApprover New value of property messageToCreditApprover.
     */
    public void setMessageToCreditApprover(java.lang.String messageToCreditApprover) {
        this.messageToCreditApprover = messageToCreditApprover;
    }

    /**
     * Getter for property equipment.
     * @return Value of property equipment.
     */
    public java.util.Vector getEquipment() {
        return equipment;
    }

    /**
     * Setter for property equipment.
     * @param equipment New value of property equipment.
     */
    public void setEquipment(java.util.Vector equipment) {
        this.equipment = equipment;
    }

    /**
     * Getter for property dealNumber.
     * @return Value of property dealNumber.
     */
    public se.mpab.psi.mo.DealNumber getDealNumber() {
        return dealNumber;
    }

    /**
     * Setter for property dealNumber.
     * @param dealNumber New value of property dealNumber.
     */
    public void setDealNumber(se.mpab.psi.mo.DealNumber dealNumber) {
        this.dealNumber = dealNumber;
    }

    public se.mpab.psi.mo.DealStatus getStatus() {
        return status;
    }

    public void setStatus(se.mpab.psi.mo.DealStatus status) {
        this.status = status;
    }

    public se.mpab.psi.mo.ResidualValue getResidualValue() {
        return rv;
    }

    public void setResidualValue(se.mpab.psi.mo.ResidualValue rv) {
        this.rv = rv;
    }

    public se.mpab.psi.mo.PeriodFee getPeriodFee() {
        return pmt;
    }

    public void setPeriodFee(se.mpab.psi.mo.PeriodFee pmt) {
        this.pmt = pmt;
    }

    /**
     * Getter for property totalEquipmentValue.
     * @return Value of property totalEquipmentValue.
     */
    public double getTotalEquipmentValue() {
        return totalEquipmentValue;
    }

    /**
     * Setter for property totalEquipmentValue.
     * @param totalEquipmentValue New value of property totalEquipmentValue.
     */
    public void setTotalEquipmentValue(double totalEquipmentValue) {
        this.totalEquipmentValue = totalEquipmentValue;
    }

    /**
     * Getter for property externalUserReference.
     * @return Value of property externalUserReference.
     */
    public java.util.Vector getExternalUserReference() {
        return externalUserReference;
    }

    /**
     * Setter for property externalUserReference.
     * @param externalUserReference New value of property externalUserReference.
     */
    public void setExternalUserReference(java.util.Vector externalUserReference) {
        this.externalUserReference = externalUserReference;
    }
    
    /**
     * Getter for property installmentPlan.
     * @return Value of property installmentPlan.
     */
    public InstallmentPlan getInstallmentPlan() {
        return installmentPlan;
    }

    /**
     * Setter for property installmentPlan.
     * @param installmentPlan New value of property installmentPlan.
     */
    public void setInstallmentPlan(InstallmentPlan installmentPlan) {
        this.installmentPlan = installmentPlan;
    }
    
    /**
     * Getter for property vat.
     * @return Value of property vat.
     */
    public VAT getVAT() {
        return vat;
    }

    /**
     * Setter for property vat.
     * @param vat New value of property vat.
     */
    public void setVAT(VAT vat) {
        this.vat = vat;
    }
    
    /**
     * Getter for property plannedStartDate.
     * @return Value of property plannedStartDate.
     */
    public String getPlannedStartDate() {
        return plannedStartDate;
    }

    /**
     * Setter for property plannedStartDate.
     * @param plannedStartDate New value of property plannedStartDate.
     */
    public void setPlannedStartDate(String plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }
    
    /**
     * Getter for property installmentMethod.
     * @return Value of property installmentMethod.
     */
    public String getInstallmentMethod() {
        return installmentMethod;
    }

    /**
     * Setter for property installmentMethod.
     * @param installmentMethod New value of property installmentMethod.
     */
    public void setInstallmentMethod(String installmentMethod) {
        this.installmentMethod = installmentMethod;
    }
    
    /**
     * Getter for property downPayment.
     * @return Value of property downPayment.
     */
    public Amount getDownPayment() {
        return downPayment;
    }

    /**
     * Setter for property downPayment.
     * @param downPayment New value of property installmentMethod.
     */
    public void setDownPayment(Amount downPayment) {
        this.downPayment = downPayment;
    }
    
    /**
     * Getter for property dealType.
     * @return Value of property dealType.
     */
    public String getDealType() {
        return dealType;
    }

    /**
     * Setter for property dealType.
     * @param dealType New value of property dealType.
     */
    public void setDealType(String dealType) {
        this.dealType = dealType;
    }
    
    /**
     * Getter for property paymentInAdvance.
     * @return Value of property paymentInAdvance.
     */
    public String getPaymentInAdvance() {
        return paymentInAdvance;
    }

    /**
     * Setter for property paymentInAdvance.
     * @param paymentInAdvance New value of property paymentInAdvance.
     */
    public void setPaymentInAdvance(String paymentInAdvance) {
        this.paymentInAdvance = paymentInAdvance;
    }
}