/*
 * EntreMessageHeader.java
 *
 * Created on den 31 augusti 2004, 16:18
 */

package se.mpab.psi.mo;

import org.jdom.Element;
import org.jdom.Namespace;
import se.mpab.util.xml.XMLPackUnpack;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class EntreMessageHeader implements XMLPackUnpack{
    
    
    private String user;
    
    private String password;
    
    private String service;
    
    
    // Getters
    public Element getElement(Namespace namespace) {
        namespace = null;
        Element e = new Element("header",namespace);        
        e.addContent((Element) new Element("user",namespace).addContent(this.user) );
        e.addContent((Element)new Element("password",namespace).addContent(password));
        e.addContent((Element)new Element("service",namespace).addContent(service));
        return e;
    }
    public String getElementName() {
        return "header";
    }
    public String getUser() {
        return this.user;
    }
    public String getPassword() {
        return this.password;
    }
    // Setters
    public void setUser(String user) {
        this.user = user;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getService() {
        return this.service;
    }
    
    public void initFromElement(Element element) {
        user = element.getChild("user").getTextTrim();
        password = element.getChild("password").getTextTrim();
        service= element.getChild("service").getTextTrim();
    }
    
    /**
     * Setter for property service.
     * @param service New value of property service.
     */
    public void setService(java.lang.String service) {
        this.service = service;
    }    

    
}