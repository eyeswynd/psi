/*
 * DealNumber.java
 *
 * Created on den 11 oktober 2004, 17:14
 */
package se.mpab.psi.mo;

import org.jdom.*;
import se.mpab.util.xml.*;


public class PeriodFee implements XMLPackUnpack {

    private Amount amount;

 
    public PeriodFee() {
    }

    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        e.addContent(amount.getElement(namespace));
        
        return e;
    }

    public String getElementName() {
        return "periodFee";
    }

    public void initFromElement(org.jdom.Element element) {
        if (element.getChild("amount") != null) {
            this.amount = new Amount();
            this.amount.initFromElement(element.getChild("amount"));
        }
    }

   /**
     * Getter for property amount.
     * @return Value of property amount.
     */
    public se.mpab.psi.mo.Amount getAmount() {
        return amount;
    }    
  
    /**
     * Setter for property amount.
     * @param amount New value of property amount.
     */
    public void setAmount(se.mpab.psi.mo.Amount amount) {
        this.amount = amount;
    }
}
