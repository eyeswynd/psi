// Copyright Marquardt & Partners
/*
 * Client.java
 *
 * Created on den 31 augusti 2004, 14:25
 */

package se.mpab.psi.mo;

import se.mpab.util.xml.*;
import org.jdom.*;
import java.util.List;
import java.util.Iterator;
/**
 *
 * @author  vino42
 */
public class Client implements XMLPackUnpack{
    private String identificationNumber;
    
    private String type;
    
    private String seat;
    
    private String phoneNumber;
    
    private Address installationAddress = null;
    
    private Address invoiceAddress = null;
    
    private Address companyAddress = null;
    
    private String name;
    
    private String contactEmail = null;
    
    //private String externalReference = null;
    
    
    
    public org.jdom.Element getElement(org.jdom.Namespace namespace) {
        Element e = new Element(getElementName(), namespace);
        if (this.identificationNumber != null) {
            e.addContent((Element) new Element("identificationNumber",namespace).addContent(this.identificationNumber));
        }
        if (this.seat != null) {
            e.addContent((Element) new Element("seat",namespace).addContent(this.seat));
        }
        if (this.name != null) {
            e.addContent((Element) new Element("name",namespace).addContent(this.name));
        }
        if (this.phoneNumber != null) {
            e.addContent((Element) new Element("phoneNumber",namespace).addContent(this.phoneNumber));
        }
        if (this.type != null) {
            e.addContent((Element) new Element("type",namespace).addContent(this.type));
        }
        
        if (this.contactEmail != null) {
            e.addContent((Element) new Element("contactEmail",namespace).addContent(this.contactEmail));
        }
        
        if (this.installationAddress != null) {
            e.addContent(installationAddress.getElement(namespace));
        }
        if (this.invoiceAddress != null) {
            e.addContent(invoiceAddress.getElement(namespace));
        }
        
        /*if (this.externalReference != null) {
            e.addContent((Element) new Element("externalReference",namespace).addContent(this.externalReference));
        }*/
        return e;
    }
    
    public void initFromElement(org.jdom.Element element) {
        if (element.getChild("identificationNumber") != null) {
            identificationNumber = element.getChild("identificationNumber").getTextTrim();
        }
        if (element.getChild("seat") != null) {
            seat = element.getChild("seat").getTextTrim();
        }
        if (element.getChild("name") != null) {
            name = element.getChild("name").getTextTrim();             
        }
        if (element.getChild("phoneNumber") != null) {
            phoneNumber = element.getChild("phoneNumber").getTextTrim();
        }
        if (element.getChild("type") != null) {
            type = element.getChild("type").getTextTrim();
        }
        if (element.getChild("contactEmail") != null) {
            contactEmail = element.getChild("contactEmail").getTextTrim();
        }
        
        // Init Addresses
        if (element.getChildren("address") != null) {
            List tmpAddresses = (List) element.getChildren("address");
            Iterator it = tmpAddresses.iterator();
            while (it.hasNext()) {
                Address tmpAddress = new Address();            
                tmpAddress.initFromElement((Element) it.next());            
                if (tmpAddress.getType().equals("INSTALLATION")) {
                    this.installationAddress = tmpAddress;
                }
                if (tmpAddress.getType().equals("INVOICE")) {
                    this.invoiceAddress = tmpAddress;
                }
                if (tmpAddress.getType().equals("COMPANY")) {
                    this.companyAddress = tmpAddress;
                }
                
            }
        }
        
         /*if (element.getChild("externalReference") != null) {
            externalReference = element.getChild("externalReference").getTextTrim();
        
        }*/
    }
    
    // Getters
    public String getElementName() {
        return "client";
    }    
    public String getIdentificationNumber() {
        return this.identificationNumber;
    }
    public String getSeat() {
        return this.seat;
    }
    
    public String getContactEmail() {
        return this.contactEmail;
    }
    
    /*public String getExternalReference() {
        return this.externalReference;
    }
    */
    // Setters
    public void setIdentificationNumber(String identificationNumber){        
        this.identificationNumber = identificationNumber;
    }
    public void setSeat(String seat){        
        this.seat = seat;
    }
    
    public void setContactEmail(String contactEmail){        
        this.contactEmail = contactEmail;
    }
    
    /*public void setExternalReference(String externalReference){        
        this.externalReference = externalReference;
    }
    */
    /**
     * Getter for property installationAddress.
     * @return Value of property installationAddress.
     */
    public se.mpab.psi.mo.Address getInstallationAddress() {
        return installationAddress;
    }
    
    /**
     * Setter for property installationAddress.
     * @param installationAddress New value of property installationAddress.
     */
    public void setInstallationAddress(se.mpab.psi.mo.Address installationAddress) {
        this.installationAddress = installationAddress;
    }
    
    /**
     * Getter for property invoiceAddress.
     * @return Value of property invoiceAddress.
     */
    public se.mpab.psi.mo.Address getInvoiceAddress() {
        return invoiceAddress;
    }
    
    /**
     * Setter for property invoiceAddress.
     * @param invoiceAddress New value of property invoiceAddress.
     */
    public void setInvoiceAddress(se.mpab.psi.mo.Address invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }
    
    /**
     * Getter for property phoneNumber.
     * @return Value of property phoneNumber.
     */
    public java.lang.String getPhoneNumber() {
        return phoneNumber;
    }
    
    /**
     * Setter for property phoneNumber.
     * @param phoneNumber New value of property phoneNumber.
     */
    public void setPhoneNumber(java.lang.String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public java.lang.String getType() {
        return type;
    }
    
    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }
    
    /**
     * Getter for property name.
     * @return Value of property name.
     */
    public java.lang.String getName() {
        return name;
    }
    
    /**
     * Setter for property name.
     * @param name New value of property name.
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }
    
    /**
     * Getter for property companyAddress.
     * @return Value of property companyAddress.
     */
    public se.mpab.psi.mo.Address getCompanyAddress() {
        return companyAddress;
    }
    
    /**
     * Setter for property companyAddress.
     * @param companyAddress New value of property companyAddress.
     */
    public void setCompanyAddress(se.mpab.psi.mo.Address companyAddress) {
        this.companyAddress = companyAddress;
    }
    
}

