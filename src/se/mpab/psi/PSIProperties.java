/*
 * PSIProperties.java
 *
 * Created on den 29 september 2004, 17:05
 */

package se.mpab.psi;

import java.util.Properties;
import org.apache.log4j.LogManager;
import se.mpab.util.common.PropertiesSetup;
import java.io.File;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class PSIProperties {
    
    private static PSIProperties episProperties = null;
    
    public static final String APPROVED_TXT = "APPROVED";
    public static final String APPROVED_WITH_CONDITION_TXT = "APPROVED_CONDITION";
    public static final String DENIED_TXT = "DENIED";
    public static final String PENDING_TXT = "PENDING";
    
    public static final String POSTPONED_TXT = "POSTPONED";
    public static final String PENDING_INFOREQUIRED_TXT = "PENDING_INFOREQUIRED";
    
    public static final String MANUAL_DECISION_TXT = "MANUAL_DECISION";
    public static final String NO_CONDITION_TXT = "NO_CONDITION";
    public static final String CONSIDERATION_TXT = "CONSIDERATION";
    
    public static final String PERIOD_LENGHT_MONTH_TXT = "MONTH";
    public static final String PERIOD_LENGHT_QUARTER_TXT = "QUARTER";
    public static final String PERIOD_LENGHT_YEAR_TXT = "YEAR";
    
    public static final String EXTERNAL_USER_REFERENCE_SALES_TXT = "SALES";
    public static final String EXTERNAL_USER_REFERENCE_ADMIN_TXT = "ADMIN";
    

    public static final String CALCULATION_STATUS_CALCULATION_TXT = "CALCULATION";

    public static final String CALCULATION_STATUS_TENDER_TXT = "TENDER";

    public static final String CALCULATION_STATUS_AGREEMENT_TXT = "AGREEMENT";

    public static final String CALCULATION_STATUS_APPROVED_TXT = "APPROVED";

    
    
    private static String rootDir = "E:\\PSI";
    private final static String softwareName = "PSI";
    private static Properties prop;
    private File root;
    private static org.apache.log4j.Logger logger = LogManager.getLogger("EPISProperties");
    /** Creates a new instance of PSIProperties */
    private PSIProperties() {
        root = new File(getRootDir());
        try {
            prop = PropertiesSetup.init(root, softwareName, null);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }
    public static Properties getProp() {
        if (episProperties == null) {
            episProperties = new PSIProperties();
        }
        return prop;
    }
    
    public static String getRootDir() {
        return rootDir;
    }
    
    
}
