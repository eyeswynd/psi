// Copyright Marquardt & Partners
/*
 * CalculationDAO.java
 *
 * Created on den 3 september 2004, 14:10
 */
package se.mpab.psi.db;

import se.mpab.psi.mo.*;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.apache.log4j.LogManager;
import se.mpab.psi.bo.Calculation;
import se.mpab.psi.bo.Deal;
import se.mpab.psi.bo.ProductBO;
import se.mpab.psi.mo.DealNumber;
import se.mpab.psi.mo.EntreMessage;
import se.mpab.psi.mo.Equipment;
import se.mpab.psi.bo.PSIRequest;

/**
 *
 * @author  janne.berg
 */
public class CalculationDAO implements DAO {

    private DDatabase contractDb;
    private DDatabase mainDB;
    private DDocument doc;
    private DominoDataAccess dataAccess;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(CalculationDAO.class);
    private String stock;

    /** Creates a new instance of CalculationDAO */
    public CalculationDAO(String stock) {

        this.stock = stock;
    }

    public Calculation insert(PSIRequest psiRequest, Deal deal) throws DNotesException, DataException {


        EntreMessage message = psiRequest.getPsiMessage();
        Vector products;
        DDocument doc;
        DView dealView = null;
        ProductDAO productDAO = new ProductDAO(stock);
        // This variable is used for getting field which are either leasing or hire purchase specific:
        String hpString = "";
        Boolean dealIsLeasing = true;
        String paymInAdv;
        String CO_PaymInAdv;
        String CO_PaymInAdvOpt;
        
        if (message.getHeader().getService().equals("makeCreditApplication")) {
            logger.info("in CalculationDAO, method insert service is makeCreditApplication");
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
            dealView = contractDb.getView(DominoDataAccess.CALCULATION_VIEW);
            
        } else if (message.getHeader().getService().equals("makeCreditApplicationHP")) {
            logger.info("in CalculationDAO, method insert service is makeCreditApplicationHP");
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
            dealView = contractDb.getView(DominoDataAccess.HPCALCULATION_VIEW);
            dealIsLeasing = false;
            hpString = "HP"; // Set hpString for HP service.
        }
        mainDB = DominoDataAccess.openDatabase(DominoDataAccess.MAIN_DATABASE, stock);

        products = productDAO.findProductsByPartnerAndAlliance(deal.getSalesPerson().getPartnerUNID(), deal.getSalesPerson().getAllianceUNID());
        DDocument customerOffer = mainDB.getDocumentByUNID(message.getBody().getCreditApplication().getOfferId());
        DDocument partner = mainDB.getDocumentByUNID(deal.getSalesPerson().getPartnerUNID());
        DDocument alliance = mainDB.getDocumentByUNID(deal.getSalesPerson().getAllianceUNID());
        
        if (!dealIsLeasing & message.getHeader().getService().equals("makeCreditApplicationHP")) {
            if (!customerOffer.getItemValueString("CO_DealType").equalsIgnoreCase("HIREPURCHASE")) {
                logger.info("Service makeCreditApplication, used CO is of type: " + customerOffer.getItemValueString("CO_DealType") + " - it MUST be of type HIREPURCHASE");
                throw new DataException("Service makeCreditApplication requires a customer offer of type hire purchase!");
            } else { logger.info("CO_DealType is ok!"); }
        }
        

        doc = contractDb.createDocument();
        doc.replaceItemValue("DocType", "Calculation");
        doc.replaceItemValue("Form", "frmCalculation");
        doc.replaceItemValue("CountryCode", psiRequest.getCountry());

        //this needs to be done to get the correct fieldtypes
        doc.computeWithForm();

        Vector systemEditors = new Vector();
        systemEditors.add("[SysAdm]");
        systemEditors.add("[SuperUser]");
        doc.replaceItemValue("SystemEditors", systemEditors);

        Vector systemReaders = new Vector();
        systemReaders.add("[SysAdm]");
        systemReaders.add("[ReadAll]");
        doc.replaceItemValue("SystemReaders", systemReaders);

        Vector editors = new Vector();
        editors.addElement(partner.getItemValueString("PA_ExternalManagerGroup"));
        editors.addElement(partner.getItemValueString("PA_InternalSupportGroup"));
        editors.addElement(alliance.getItemValueString("AL_ExternalAllianceManagerGroup"));
        editors.addElement("[SupportAll]");
        editors.addElement(deal.getSalesPerson().getName());
        editors.trimToSize();

        doc.replaceItemValue("Editors", editors);
        doc.replaceItemValue("Readers", editors);
        ////-----------------------------------------------------------------------
        //TODO maybe we should make a lokup of these
        doc.replaceItemValue("CA_AllianceUNID", deal.getSalesPerson().getAllianceUNID());

        if (deal.getSalesPerson().getRoles().contains("[ExternalMul]")) {
            doc.replaceItemValue("CA_PartnerUNID", customerOffer.getItemValueString("CO_PartnerUNID"));
        } else {
            doc.replaceItemValue("CA_PartnerUNID", deal.getSalesPerson().getPartnerUNID());
        }

        doc.replaceItemValue("CA_DealNo", new Double(deal.getDealNo()));
        doc.replaceItemValue("CA_Version", new Integer(1));
        doc.replaceItemValue("CA_DealType", customerOffer.getItemValueString("CO_DealType"));

        doc.replaceItemValue("CA_InterestMarginCO", new Double(customerOffer.getItemValueDouble("CO_InterestMargin").doubleValue()));
        //doc.replaceItemValue("CA_InterestMarginCalc", new Double(0));

        doc.replaceItemValue("CA_InterestMarginPADisabled", customerOffer.getItemValueString("CO_InterestMarginPartnerDisabled"));
        
        doc.replaceItemValue("CA_CustomerOfferStatus", customerOffer.getItemValueString("CO_Status"));
        
        doc.replaceItemValue("CA_Sections", partner.getItemValue("PA_" + hpString + "CalcSections"));
        doc.replaceItemValue("CA_FinancedAmount", new Double((message.getBody().getCreditApplication().getTotalEquipmentValue())));
        logger.debug("---- Financed amount -- " + new Double((message.getBody().getCreditApplication().getTotalEquipmentValue())));
        for (int i = 1; i <= 3; i++) {
            doc.replaceItemValue("CA_MarginAdjustLevel" + i + "Min", partner.getItemValue("PA_" + hpString + "MarginAdjustLevel" + i + "Min"));
            doc.replaceItemValue("CA_MarginAdjustLevel" + i + "Max", partner.getItemValue("PA_" + hpString + "MarginAdjustLevel" + i + "Max"));
            if (dealIsLeasing) {
                doc.replaceItemValue("CA_ProcurementAdjustLevel" + i + "Min", partner.getItemValue("CA_ProcurementAdjustLevel" + i + "Min"));
                doc.replaceItemValue("CA_ProcurementAdjustLevel" + i + "Max", partner.getItemValue("CA_ProcurementAdjustLevel" + i + "Max"));
            }
            
        }
        doc.replaceItemValue("CA_FinancialCalculatorVisible", partner.getItemValue("PA_FinancialCalculatorVisible"));
        /*IssueFee */
        int issuefee = 0;

        if (customerOffer.getItemValueString("CO_IssueFeeEnabled") != null) {
            if (customerOffer.getItemValueInteger("CO_IssueFee").intValue() == 0) {
                // Issue fee for hire purchase is set by agent - cslHPCalculation - function getIssueFee 
                // - we do not have to fetch from partner matrix here.
                issuefee = partner.getItemValueInteger("PA_IssueFee").intValue();
            } else {
                issuefee = customerOffer.getItemValueInteger("CO_IssueFee").intValue();
            }
        }
        doc.replaceItemValue("CA_IssueFee", new Integer(issuefee));
   
        if (dealIsLeasing) {
            doc.replaceItemValue("CA_IssueFeePartner", new Integer(issuefee));
        } else { doc.replaceItemValue("CA_IssueFeePartner", 0); }
        
        doc.replaceItemValue("CA_IssueFeeReciever", partner.getItemValue("PA_IssueFeeReciever"));
        doc.replaceItemValue("CA_IssueFeePercentDLL", new Double(partner.getItemValueDouble("PA_IssueFeePercentDLL").doubleValue()));        
                
        Vector PeriodLengthOptionsString = new Vector(customerOffer.getItemValue("CO_PeriodLengthOptions"));
        Vector PeriodLengthOptionsInteger = new Vector();
               
        //20150121 HO Validation currency in message towards currency on customer offer
        Vector equipmentVector = new Vector(message.getBody().getCreditApplication().getEquipment());
        Vector CurrencyFromOffer = new Vector(customerOffer.getItemValue("CO_CustCurrency"));
        
        for (Iterator eq = equipmentVector.iterator(); eq.hasNext();)
        {
            Equipment eqRow = (Equipment) eq.next();
            String Eqcurrency = eqRow.getAmount().getCurrency();
            if(CurrencyFromOffer.contains(Eqcurrency))
            {              
                doc.replaceItemValue("CA_Currency", this.getCurrency(message.getBody().getCreditApplication().getEquipment()));
                doc.replaceItemValue("CA_CustCurrency", this.getCurrency(message.getBody().getCreditApplication().getEquipment()));           
            }
            else
            {
               logger.debug("Currency not allowed");
               throw new DataException("[" + Eqcurrency + "] is not valid currency. Valid currency is " + "[" + CurrencyFromOffer + "]");
            }           
        }
        
        doc.replaceItemValue("CA_CustomerOfferUNID", customerOffer.getUniversalID());
        
        doc.replaceItemValue("CA_CommissionFeeEnabled", customerOffer.getItemValue("CO_CommissionFeeEnabled"));

        //this will be set in agtSaveCreditPSI
        doc.replaceItemValue("CA_CurrencyRateFactor", new Long(0).longValue());
        doc.replaceItemValue("CA_PeriodFee", new Double(0).doubleValue());
            
        if (dealIsLeasing) {
            // Following field should NOT be set for hire purchase:
            doc.replaceItemValue("CA_CommissionFeeMaxRate", partner.getItemValue("PA_CommissionFeeMaxRate"));
            doc.replaceItemValue("CA_DeliveryDate", "");
            
            doc.replaceItemValue("CA_ExtensionFeeRate", partner.getItemValue("PA_ExtensionFeeRate"));
            doc.replaceItemValue("CA_ExtensionFeeRateWhenRV", partner.getItemValue("PA_ExtensionFeeRateWhenRV"));
            doc.replaceItemValue("CA_ExtensionFeeOnLumpFees", partner.getItemValue("PA_ExtensionFeeOnLumpFees"));
            
            doc.replaceItemValue("CA_InternalDiscount_ID", customerOffer.getItemValueString("CO_InternalDiscount_ID"));
            doc.replaceItemValue("CA_InternalDiscountVisibility", customerOffer.getItemValueString("CO_InternalDiscountVisibility"));
            doc.replaceItemValue("CA_InternalDiscountRate1", (customerOffer.getItemValueDouble("CO_InternalDiscountRate1").doubleValue()));
            doc.replaceItemValue("CA_InternalDiscountRate2", (customerOffer.getItemValueDouble("CO_InternalDiscountRate2").doubleValue()));
            doc.replaceItemValue("CA_InternalDiscountRate3", (customerOffer.getItemValueDouble("CO_InternalDiscountRate3").doubleValue()));
            doc.replaceItemValue("CA_InternalDiscountRate3_AdjMin", (customerOffer.getItemValueDouble("CO_InternalDiscountRate3_AdjMin").doubleValue()));
            doc.replaceItemValue("CA_InternalDiscountRate3_AdjMax", (customerOffer.getItemValueDouble("CO_InternalDiscountRate3_AdjMax").doubleValue()));
            
            doc.replaceItemValue("CA_FINALCodeForTermination", customerOffer.getItemValueString("CO_FINALCodeForTermination"));
            doc.replaceItemValue("CA_FINALAdministrativeCost", (customerOffer.getItemValueDouble("CO_FINALAdministrativeCost").doubleValue()));
            
            // Get the payment in advance set on the CO (variable CO_PaymInAdv) 
            // - NOTE! If CO has payment in arrears, CO_PaymentInAdvance = "" in Entre!)
            if (customerOffer.getItemValueString("CO_PaymentInAdvance").equalsIgnoreCase("")) {
                CO_PaymInAdv = "0";
            } else { CO_PaymInAdv = customerOffer.getItemValueString("CO_PaymentInAdvance"); }
            // Get the payment in advance option from CO (used for both leasing and HP deals):
            CO_PaymInAdvOpt = customerOffer.getItemValueString("CO_PaymentInAdvanceOptions");
            if (message.getBody().getCreditApplication().getPaymentInAdvance() != null) {
                paymInAdv = message.getBody().getCreditApplication().getPaymentInAdvance();
                if ((CO_PaymInAdvOpt.equalsIgnoreCase("1") & (paymInAdv.equalsIgnoreCase("1")|paymInAdv.equalsIgnoreCase("0"))) | paymInAdv.equalsIgnoreCase(CO_PaymInAdv)) {
                    // Payment in advance options set on CO - both values "0" and "1" are valid choices OR the paym in adv from request is the same as on the CO:
                    doc.replaceItemValue("CA_PaymentInAdvance", paymInAdv);
                } else {
                    throw new DataException("Payment in advance/arrears choice from request is not allowed according tho the customer offer in Entr�.");
                }
            } else {
                // No payment in advance is set by request - set value from CO:
                doc.replaceItemValue("CA_PaymentInAdvance", CO_PaymInAdv);
            }
            
            doc.replaceItemValue("CA_PaymentInAdvanceOptions", CO_PaymInAdvOpt);
                        
            
            
        } else {
            // Set payment in advance/arrears for HP deaals:
            if (customerOffer.getItemValueString("CO_PaymentInAdvance").equalsIgnoreCase("")) {
                doc.replaceItemValue("CA_PaymentInAdvance", "0");
            } else { doc.replaceItemValue("CA_PaymentInAdvance", customerOffer.getItemValue("CO_PaymentInAdvance")); }
            
            doc.replaceItemValue("CA_PaymentInAdvanceOptions", ""); // Payment in advance options is not available for HP deals!
        }
        

        doc.replaceItemValue("CA_InterestMarginsPADurations", partner.getItemValue("PA_" + hpString + "InterestMarginsDurations"));
        doc.replaceItemValue("CA_InterestMarginsPALevel1", partner.getItemValue("PA_" + hpString + "InterestMarginsLevel1"));
        doc.replaceItemValue("CA_InterestMarginsPALevel2", partner.getItemValue("PA_" + hpString + "InterestMarginsLevel2"));
        doc.replaceItemValue("CA_InterestMarginsPALevel3", partner.getItemValue("PA_" + hpString + "InterestMarginsLevel3"));
        doc.replaceItemValue("CA_InterestMarginsPALevel4", partner.getItemValue("PA_" + hpString + "InterestMarginsLevel4"));
        doc.replaceItemValue("CA_InterestMarginsPALevel5", partner.getItemValue("PA_" + hpString + "InterestMarginsLevel5"));
        doc.replaceItemValue("CA_InterestMarginsPALevel6", partner.getItemValue("PA_" + hpString + "InterestMarginsLevel6"));
        doc.replaceItemValue("CA_InterestMarginsPALevels", partner.getItemValue("PA_" + hpString + "InterestMarginsLevels"));
        
        if (customerOffer.getItemValueString("CO_IssueFeeEnabled") == "0") {
            doc.replaceItemValue("CA_IssueFeeEnabled", "0");
        } else { doc.replaceItemValue("CA_IssueFeeEnabled", "1"); }
        
        //TODO validate if the duration allowed on this customer offer

        Vector DurationOptionsString = new Vector(customerOffer.getItemValue("CO_DurationOptions"));
        Vector DurationOptionsInteger = new Vector();
        //Convert to numbers
        for (Iterator it = DurationOptionsString.iterator(); it.hasNext();) {
            DurationOptionsInteger.add(new Integer(Integer.parseInt(it.next().toString())));
        }
        
        Integer durationInput = getDurationInMonths(message.getBody().getCreditApplication().getDuration());
        logger.debug("DurationOptions:" + DurationOptionsString);
        logger.debug("DURATION PSI :" + durationInput);
        if (DurationOptionsInteger.contains(durationInput)) {
            logger.debug("DURATION OK");
            doc.replaceItemValue("CA_DurationOptions", DurationOptionsInteger);
            doc.replaceItemValue("CA_Duration", durationInput);
        } else {
            logger.debug("DURATION NOT OK");
            throw new DataException("[" + durationInput + "] is not valid duration. Valid duration is " + DurationOptionsString + "");

        }

        //Convert to numbers
        for (Iterator it = PeriodLengthOptionsString.iterator(); it.hasNext();) {
            PeriodLengthOptionsInteger.add(new Integer(Integer.parseInt(it.next().toString())));
        }

        doc.replaceItemValue("CA_PeriodLengthOptions", PeriodLengthOptionsInteger);
        doc.replaceItemValue("CA_PeriodLength", getPeriodLength(message.getBody().getCreditApplication().getDuration()));

        doc.replaceItemValue("CA_InsuranceMode", customerOffer.getItemValue("CO_InsuranceMode"));
        doc.replaceItemValue("CA_InsuranceTypeOptions", customerOffer.getItemValue("CO_InsuranceTypeOptions"));
        doc.replaceItemValue("CA_InsuranceType", customerOffer.getItemValue("CO_InsuranceType"));
        if (dealIsLeasing) {
            if (!doc.getItemValueString("CA_DealType").equals("ADDITION")) {
                doc.replaceItemValue("CA_ExtensionPeriodLength", doc.getItemValue("CA_PeriodLength"));
            }
            doc.replaceItemValue("CA_ResidualValueTypeOptions", customerOffer.getItemValue("CO_ResidualValueTypeOptions"));
            doc.replaceItemValue("CA_ResidualValueType", customerOffer.getItemValue("CO_ResidualValueType"));
            doc.replaceItemValue("CA_ResidualValueClauseOptions", customerOffer.getItemValue("CO_ResidualValueClauseOptions"));
            doc.replaceItemValue("CA_ResidualValueClause", customerOffer.getItemValue("CO_ResidualValueClause"));
            doc.replaceItemValue("CA_ResidualValueAdjMin", customerOffer.getItemValue("CO_ResidualValueAdjMin"));
            doc.replaceItemValue("CA_ResidualValueAdjMax", customerOffer.getItemValue("CO_ResidualValueAdjMax"));
            
            /*
                Add functionality for supplying alternative residual value / period fee
            */
            if (message.getBody().getCreditApplication().getResidualValue() != null) {
                doc.replaceItemValue("CA_ResidualValueAdjusted", new Double(message.getBody().getCreditApplication().getResidualValue().getAmount().getValue()));
            } else { doc.replaceItemValue("CA_ResidualValueAdjusted", new Double(0)); }
        
        }

        if (message.getBody().getCreditApplication().getPeriodFee() != null) {
            doc.replaceItemValue("CA_PeriodFeeAdjusted", new Double(message.getBody().getCreditApplication().getPeriodFee().getAmount().getValue()));
        } else {
            doc.replaceItemValue("CA_PeriodFeeAdjusted", new Double(0));
        }


        // If available in request, "INTEREST" should be taken care of (validated to CO_InterestBaseTypeOptions)
        // FIXME, maybe we should return some kind of error if interest type is not allowed on specific customer offer
        doc.replaceItemValue("CA_IBTypeOptions", customerOffer.getItemValue("CO_InterestBaseTypeOptions"));

        if (message.getBody().getCreditApplication().getInterestType() != null) {
            Vector IBTypeOptions = new Vector(customerOffer.getItemValue("CO_InterestBaseTypeOptions"));
            if (IBTypeOptions.contains(message.getBody().getCreditApplication().getInterestType())) {
                doc.replaceItemValue("CA_IBType", message.getBody().getCreditApplication().getInterestType());
            } else {
                doc.replaceItemValue("CA_IBType", customerOffer.getItemValue("CO_InterestBaseType"));
            }
        } else {
            doc.replaceItemValue("CA_IBType", customerOffer.getItemValue("CO_InterestBaseType"));
        }

        doc.replaceItemValue("CA_Comments", "");
        doc.replaceItemValue("CA_InternalComments", "");
        
        if (dealIsLeasing) {
            doc.replaceItemValue("CA_ResidualValueGuarantorName", customerOffer.getItemValueString("CO_ResidualValueGuarantorName"));
            doc.replaceItemValue("CA_ResidualValueGuarantorOrgNo", customerOffer.getItemValueString("CO_ResidualValueGuarantorOrgNo"));
            doc.replaceItemValue("CA_ResidualValueGuarantorFINALNo", customerOffer.getItemValueString("CO_ResidualValueGuarantorFINALNo"));
        }
        
        if (customerOffer.getItemValueString("CO_InvoiceFeeEnabled") != null) {
            if (customerOffer.getItemValueInteger("CO_InvoiceFee").intValue() == 0) {
                doc.replaceItemValue("CA_InvoiceFee", (partner.getItemValueInteger("PA_" + hpString + "InvoiceFee")));
            } else {
                doc.replaceItemValue("CA_InvoiceFee", (customerOffer.getItemValueInteger("CO_InvoiceFee")));
            }
        }
        
        // These fields should be set for both leasing and HP new release 5.1.11:
        // -----------------  START -------------------- //
        
        //Collect information about the insurance adjustment from the CO
        doc.replaceItemValue("CA_InsuranceAdjustmentMin", (partner.getItemValueInteger("PA_InsuranceAdjustmentMin")));
        doc.replaceItemValue("CA_InsuranceAdjustmentMax", (partner.getItemValueInteger("PA_InsuranceAdjustmentMax")));
        if (customerOffer.getItemValueString("CO_InsurancePartnerDisabled") != null) {
            if (customerOffer.getItemValueString("CO_InsurancePartnerDisabled") == "1") {
                doc.replaceItemValue("CA_InsuranceRateCO", (customerOffer.getItemValueInteger("CO_InsuranceAdjustment")));
            } else {
                doc.replaceItemValue("CA_InsuranceAdjustmentCO", (customerOffer.getItemValueInteger("CO_InsuranceAdjustment")));
            }
        }
	doc.replaceItemValue("CA_InsurancePartnerDisabled", (customerOffer.getItemValueInteger("CO_InsurancePartnerDisabled")));
	
        // Collect EIB data from the partner
        if (partner.getItemValueString("PA_EIBFinancingEnabled") != null) {
            if (partner.getItemValueString("PA_EIBFinancingEnabled").equalsIgnoreCase("1")) {
                doc.replaceItemValue("CA_EIBFinancingEnabled", "1");
                doc.replaceItemValue("CA_EIBDiscountLimit", partner.getItemValueDouble("PA_EIBDiscountLimit").doubleValue());
            } else {
                doc.replaceItemValue("CA_EIBFinancingEnabled", "0");
                doc.replaceItemValue("CA_EIBDiscountLimit", 0);
            }
        } else {
            doc.replaceItemValue("CA_EIBFinancingEnabled", "0");
            doc.replaceItemValue("CA_EIBDiscountLimit", 0);
        }
        // Since no EIB support in PSI today, always set CA_EIBFinancing to empty string:
        doc.replaceItemValue("CA_EIBFinancing", "");
        
        // -----------------  END -------------------- //
        
        // "Down payment" for HWM leasing is disbursement.
        String coDealType;
        coDealType = customerOffer.getItemValueString("CO_DealType");
        if (coDealType.equalsIgnoreCase("HWMLEASING")) {
            if (message.getBody().getCreditApplication().getDownPayment() != null) {
                doc.replaceItemValue("CA_DisbursementSum", message.getBody().getCreditApplication().getDownPayment().getValue());
                doc.replaceItemValue("CA_DisbursementRateMin", customerOffer.getItemValue("CO_DisbursementRateMin"));
                doc.replaceItemValue("CA_DisbursementRateMax", customerOffer.getItemValue("CO_DisbursementRateMax"));
            } else {
                doc.replaceItemValue("CA_DisbursementSum", 0);
                doc.replaceItemValue("CA_DisbursementRateMin", 0);
                doc.replaceItemValue("CA_DisbursementRateMax", 0);
            }
        } else if (! coDealType.equalsIgnoreCase("HIREPURCHASE")) {
            // For all other leasing types, set disbursement to zero:
            doc.replaceItemValue("CA_DisbursementSum", 0);
            doc.replaceItemValue("CA_DisbursementRateMin", 0);
            doc.replaceItemValue("CA_DisbursementRateMax", 0);
        }
        
        // Set fields specific for hire purchase:
        if (!dealIsLeasing) {
            doc.replaceItemValue("CA_DownPaymentCollecter", customerOffer.getItemValue("CO_DownPaymentCollecter"));
            doc.replaceItemValue("CA_DownPaymentCollecterCO", customerOffer.getItemValue("CO_DownPaymentCollecter"));
            doc.replaceItemValue("CA_DownPaymentRateMin", customerOffer.getItemValue("CO_DownPaymentRateMin"));
            if (message.getBody().getCreditApplication().getDownPayment() != null) {
                doc.replaceItemValue("CA_DownPayment", message.getBody().getCreditApplication().getDownPayment().getValue());
            } else {
                doc.replaceItemValue("CA_DownPayment", 0);
            }
            
            // Discount:
            doc.replaceItemValue("CA_HPDiscountType", customerOffer.getItemValue("CO_HPDiscountType"));
            doc.replaceItemValue("CA_MarginDiscount", customerOffer.getItemValue("CO_MarginDiscount"));
            doc.replaceItemValue("CA_MarginDiscountPeriod", customerOffer.getItemValue("CO_MarginDiscountPeriod"));
            doc.replaceItemValue("CA_MarginDiscountFirstPeriod", customerOffer.getItemValue("CO_MarginDiscountFirstPeriod"));
            doc.replaceItemValue("CA_MarginDiscountSecondPeriod", customerOffer.getItemValue("CO_MarginDiscountSecondPeriod"));
            
            // Field CO_MarginDiscountAmount does not exist on CO, set default zero:
            doc.replaceItemValue("CA_MarginDiscountAmount", 0);
            
            doc.replaceItemValue("CA_MarginDiscountFINALNo", customerOffer.getItemValue("CO_MarginDiscountFINALNo"));
            doc.replaceItemValue("CA_MarginDiscountCompany", customerOffer.getItemValue("CO_MarginDiscountCompany"));
            
            if (message.getBody().getCreditApplication().getPlannedStartDate() != null) {
                int dateYear = Integer.parseInt(message.getBody().getCreditApplication().getPlannedStartDate().substring(0, 4));
                int dateMonth = Integer.parseInt(message.getBody().getCreditApplication().getPlannedStartDate().substring(5, 7));
                int dateDay = Integer.parseInt(message.getBody().getCreditApplication().getPlannedStartDate().substring(8));
                // Long way to set Date format on the document, month argument has range 0-11 - therefore the '-1'
                Calendar reqDate = Calendar.getInstance();
                reqDate.set(dateYear, dateMonth-1, dateDay);
                reqDate.set(Calendar.HOUR_OF_DAY, 0);
                reqDate.set(Calendar.MINUTE, 0);
                reqDate.set(Calendar.SECOND, 0);
                reqDate.set(Calendar.MILLISECOND, 0);
                doc.replaceItemValue("CA_PlannedStartDate", reqDate);
            } else {
                logger.info("Hire purchase contracts requires a planned start date, which was not provided in this request.");
                throw new DataException("Hire purchase contracts requires a planned start date, which was not provided in this request.");
            }
            
            if (message.getBody().getCreditApplication().getInstallmentMethod() != null) {
                doc.replaceItemValue("CA_InstallmentMethod", message.getBody().getCreditApplication().getInstallmentMethod());
            } else {
                doc.replaceItemValue("CA_InstallmentMethod", customerOffer.getItemValue("CO_InstallmentMethod"));
            }
            doc.replaceItemValue("CA_InstallmentMethodOptions", customerOffer.getItemValue("CO_InstallmentMethodOptions"));
            
            
            // For HP deals, we need to set the installment payments and interest on tha calc aswell as the installment plan docs:
            // Note that no verification of number of payments is equal to number of periods from the request, this is done in InstallmentPlanDAO.
            doc.replaceItemValue("CA_InstallmentPayments", message.getBody().getCreditApplication().getInstallmentPlan().getInstallmentPayments());
            doc.replaceItemValue("CA_InterestPayments", message.getBody().getCreditApplication().getInstallmentPlan().getInterestPayments());
            
            // VAT:
            if (message.getBody().getCreditApplication().getVAT() != null) {
                if (customerOffer.getItemValueString("CO_HPVATFinanceEnabled").equalsIgnoreCase("")) {
                    logger.info("VAT is in the PSI request, but customer offer in Entr� do not allow short term VAT financing.");
                    throw new DataException("VAT is in the PSI request, but customer offer in Entr� do not allow short term VAT financing.");
                }
                if (message.getBody().getCreditApplication().getVAT().getPeriodLength() == customerOffer.getItemValueInteger("CO_HPVATFinancePeriodLength").intValue()) {
                    // Set CA_VATFinance - flag used in Entr� that VAT is short time financed!
                    doc.replaceItemValue("CA_VATFinance", "1");
                    doc.replaceItemValue("CA_VATPeriodLength", customerOffer.getItemValueInteger("CO_HPVATFinancePeriodLength"));
                }
                else {
                    logger.info("VAT period length from request does not match setting on customer offer in Entr�.");
                    throw new DataException("VAT period length from request does not match setting on customer offer in Entr�.");
                }
            } else {
                // Set (empty) CA_VATFinance - flag used in Entr� that VAT is short time financed!
                doc.replaceItemValue("CA_VATFinance", "");
                doc.replaceItemValue("CA_VATPeriodLength", customerOffer.getItemValueInteger("CO_HPVATFinancePeriodLength").intValue());    
            }
            // PSI/EE do not handle special case for AGCO - CA_HPVATFinanceEnabled, CA_HPVATFinanceDueMonth & CA_HPVATFinanceDueOptions just get the specified from CO:
            doc.replaceItemValue("CA_HPVATFinanceEnabled", customerOffer.getItemValue("CO_HPVATFinanceEnabled"));
            doc.replaceItemValue("CA_HPVATFinanceDueMonth", customerOffer.getItemValueInteger("CO_HPVATFinancePeriodLength").intValue());
            // CA_HPVATFinanceDueOptions is only used for AGCO - do not use PSI, set empty string:
            doc.replaceItemValue("CA_HPVATFinanceDueOptions", "");
            
            doc.replaceItemValue("CA_SeasonalPaymentEnabled", customerOffer.getItemValue("CO_SeasonalPaymentEnabled"));
            doc.replaceItemValue("CA_BalloonPaymentEnabled", customerOffer.getItemValue("CO_BalloonPaymentEnabled"));
            doc.replaceItemValue("CA_BalloonPaymentRate", 0);
            doc.replaceItemValue("CA_InitiallyNoPaymentEnabled", customerOffer.getItemValue("CO_InitiallyNoPaymentEnabled"));
            doc.replaceItemValue("CA_NoPaymentEnabled", customerOffer.getItemValue("CO_NoPaymentEnabled"));
            doc.replaceItemValue("CA_HPPartnerDiscountRateEnabled", customerOffer.getItemValue("CO_HPPartnerDiscountRateEnabled"));
            
            // Exchange object not used for PSI/EE (?):
            doc.replaceItemValue("CA_EXRowsSpec", "");
            doc.replaceItemValue("CA_EXRowsQty", 0);
            doc.replaceItemValue("CA_EXRowsPrice", 0);
            doc.replaceItemValue("CA_ExSum", 0);
            doc.replaceItemValue("CA_ExDebtSum", 0);
            doc.replaceItemValue("CA_EXVAT", 0);
            doc.replaceItemValue("CA_EXRowsDebtContractNo", "");
            doc.replaceItemValue("CA_EXRowsDebtOwner", "");
            doc.replaceItemValue("CA_EXRowsDebtRemaining", 0);
            doc.replaceItemValue("CA_EXRowsDebtSettler", "");
        }


        List sections = partner.getItemValue("PA_" + hpString + "CalcSections");
        Vector sectionsVector = new Vector();
        logger.debug("Sections used :" + sections);
        equipmentVector = new Vector(message.getBody().getCreditApplication().getEquipment());

        HashMap productMap = new HashMap();

        for (Iterator it = sections.iterator(); it.hasNext();) {
            Vector productNames = new Vector();
            String sec = (String) it.next();
            for (Iterator ite = products.iterator(); ite.hasNext();) {
                ProductBO product = (ProductBO) ite.next();
                if (product.getType().equals(sec)) {
                    productNames.add(product.getName());
                }
            }
            logger.debug("Setting RowsTypeOptions for " + sec + " " + productNames);
            doc.replaceItemValue("CA_" + sec + "RowsTypeOptions", productNames);
            productMap.put(sec, productNames);
            sectionsVector.add(sec);
        }

        //maybe get this from parameter in Entre
        Vector allSections = new Vector();
        allSections.add("HW");
        allSections.add("HWM");
        allSections.add("EX");
        
        // LF nad LI needs to be added for HP deals, since method calcLGD in cslHPCalculation assume that these fields exists.
        allSections.add("LF");
        allSections.add("LI");
        
        if (dealIsLeasing) {
            allSections.add("TLF");
            allSections.add("SE");
            allSections.add("TSE");
            allSections.add("SE");
            allSections.add("TSU");
            allSections.add("SU");
            allSections.add("RE");
            allSections.add("IC");
        }

        logger.debug("allSections:" + allSections);

        for (Iterator it = allSections.iterator(); it.hasNext();) {
            String sec = (String) it.next();
            logger.debug("initiating " + sec);
            doc.replaceItemValue("CA_" + sec + "RowsQty", new Long(0).longValue());
            doc.replaceItemValue("CA_" + sec + "RowsPrice", new Long(0).longValue());
        }
        
        if (dealIsLeasing) {
            // Following field should NOT be set for hire purchase:
            doc.replaceItemValue("CA_SERowsFactorPrimary", new Long(1).longValue());
            doc.replaceItemValue("CA_SERowsFactorExtension", new Long(1).longValue());

            doc.replaceItemValue("CA_TSERowsFactorPrimary", new Long(1).longValue());
            doc.replaceItemValue("CA_TSERowsFactorExtension", new Long(1).longValue());

            doc.replaceItemValue("CA_SURowsFactorPrimary", new Long(1).longValue());
            doc.replaceItemValue("CA_SURowsFactorExtension", new Long(1).longValue());

            doc.replaceItemValue("CA_TSURowsFactorPrimary", new Long(1).longValue());
            doc.replaceItemValue("CA_TSURowsFactorExtension", new Long(1).longValue());
        }

        //missing fields
        //doc.replaceItemValue("CA_LFSum", new Long(0));
        //doc.replaceItemValue("CA_TLFSum", new Long(0));
        //doc.replaceItemValue("CA_LISum", new Long(0));
        //doc.replaceItemValue("CA_RERowsValueLF", new Long(0));
        //doc.replaceItemValue("CA_EXDebtSum", new Long(0));
        //doc.replaceItemValue("CA_EXSum", new Long(0));
        //doc.replaceItemValue("CA_QuarterlyFees", new Long(0));
        //doc.replaceItemValue("CA_ExtensionFee", new Long(0));


        doc.replaceItemValue("CA_EXDebtSum", new Long(0).longValue());
        doc.replaceItemValue("CA_EXSum", new Long(0).longValue());
        doc.replaceItemValue("CA_EXRowsDebtRemaining", new Long(0).longValue());

        populateCalculationRows(sectionsVector, equipmentVector, productMap, doc);
        
        if (!dealIsLeasing) {
            // Verify that there is at least one HWM product inthe request for hire purchase deals:
            if (calcHasHWMproduct(doc) == false) {
                // No product in request has been matched to a HWM product - abort!
                logger.info("Hire purchase deals in Entr� demands at least one machine product, no such provided in the request.");
                throw new DataException("Hire purchase deals in Entr� demands at least one machine product, no such provided in the request.");
            }
        }
        
        if(partner.getItemValueString("PA_EsignEnabled").equalsIgnoreCase("1") && !customerOffer.getItemValueString("CO_DealType").equalsIgnoreCase("TRANSFERAL")) {
            doc.replaceItemValue("CA_EsignEnabled", "1");
        } else {
            doc.replaceItemValue("CA_EsignEnabled", "0");
        }

        doc.save(true);
        doc.replaceItemValue("CountryCode", psiRequest.getCountry());
        doc.replaceItemValue("DeleteFlag", "0");
        doc.replaceItemValue("DocUNID", doc.getUniversalID());
        doc.replaceItemValue("Created", doc.getCreated());
        doc.replaceItemValue("CreatedBy", "PSI");
        doc.replaceItemValue("Modified", "");
        doc.replaceItemValue("CA_Status", "CREDITREVIEW");

        doc.save(true);
        dealView.refresh();

        return populate(doc);
    }
    
    private boolean calcHasHWMproduct(DDocument calcDoc) {
        Vector hwmQuantities = new Vector(calcDoc.getItemValue("CA_HWMRowsQty"));
        boolean hwmProductExists = false;
        for (Iterator it = hwmQuantities.iterator(); it.hasNext();) {
            Double rowQuantity = (Double) it.next();
            if (rowQuantity.intValue() > 0){ hwmProductExists = true; }
        }

       return hwmProductExists;
    }

    public Calculation findByUNID(String UNID) throws DNotesException {
        DDocument calculationDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase("deals", stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            calculationDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("CalculationDAO.findByUNID, UNID=[" + UNID + "], getNoteID()=[" + calculationDoc.getNoteID() + "]");

        } catch (Exception e) {
            logger.warn("Error in CalculationDAO findByUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return populate(calculationDoc);
    }
    
    public Calculation findByHpUNID(String UNID) throws DNotesException {
        DDocument calculationDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase("hirepurchase", stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            calculationDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("CalculationDAO.findByHpUNID, UNID=[" + UNID + "], getNoteID()=[" + calculationDoc.getNoteID() + "]");

        } catch (Exception e) {
            logger.warn("Error in CalculationDAO findByHpUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return populate(calculationDoc);
    }
    
    public DDocument findDocumentByUNID(String UNID) throws DNotesException {
        DDocument calculationDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            calculationDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("CalculationDAO.findDocumentByUNID, UNID=[" + UNID + "], getNoteID()=[" + calculationDoc.getNoteID() + "]");

        } catch (Exception e) {
            logger.warn("Error in CalculationDAO findDocumentByUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return calculationDoc;
    }
    
    public DDocument findDocumentByHpUNID(String UNID) throws DNotesException {
        DDocument calculationDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            calculationDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("CalculationDAO.findDocumentByHpUNID, UNID=[" + UNID + "], getNoteID()=[" + calculationDoc.getNoteID() + "]");

        } catch (Exception e) {
            logger.warn("Error in CalculationDAO findDocumentByHpUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return calculationDoc;
    }

    public Calculation newVersion(PSIRequest psiRequest, Deal deal, String dealType) throws DNotesException, DataException {
        Boolean dealIsLeasing = true;
        EntreMessage message = psiRequest.getPsiMessage();
        if (dealType.equalsIgnoreCase("LEASING")) {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
        } else if (dealType.equalsIgnoreCase("HIREPURCHASE")) {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
            dealIsLeasing = false;
        }

        DView allView = contractDb.getView("vwAllByUNIDExclusive");
        allView.refresh();
        DDocument oldCalc = allView.getDocumentByKey(deal.getCalculation().getUNID(), true);
        
        // If the customer offer id is provided in the 'change request' - it MUST be the same UNID as on the 'oldCalc':
        if (message.getBody().getCreditApplication().getOfferId() != null) {
            if (! oldCalc.getItemValueString("CA_CustomerOfferUNID").equalsIgnoreCase(message.getBody().getCreditApplication().getOfferId())) {
                logger.info("Customer offer ID from the PSI request (" + message.getBody().getCreditApplication().getOfferId() + ") is not the same UNID as on the old calc (" + oldCalc.getItemValueString("CA_CustomerOfferUNID") + ")");
                throw new DataException("The offerId in the request do not match the ID on the existing application - not valid to change between versions.");
            }
        }
        DDocument newDoc = contractDb.createDocument();
        oldCalc.copyAllItems(newDoc, true);

        newDoc.replaceItemValue("CA_Version", new Double(oldCalc.getItemValueDouble("CA_Version").intValue() + 1));
        newDoc.replaceItemValue("CA_FinancedAmount", new Double((message.getBody().getCreditApplication().getTotalEquipmentValue())));
        newDoc.replaceItemValue("CA_Duration", getDurationInMonths(message.getBody().getCreditApplication().getDuration()));
        newDoc.replaceItemValue("CA_PeriodLength", getPeriodLength(message.getBody().getCreditApplication().getDuration()));

        //Change interest type if available
        if (message.getBody().getCreditApplication().getInterestType() != null) {
            Vector IBTypeOptions = new Vector(newDoc.getItemValue("CA_IBTypeOptions"));
            if (IBTypeOptions.contains(message.getBody().getCreditApplication().getInterestType())) {
                newDoc.replaceItemValue("CA_IBType", message.getBody().getCreditApplication().getInterestType());
            }
        }
 
        if (dealIsLeasing) {
            //CA_DeliveryDate
            if (message.getBody().getCreditApplication().getDeliveryDate() != null) {
                //newDoc.repreplaceItemValue("CA_DeliveryDate", DominoDataAccess.getSession().createDateTime(message.getBody().getCreditApplication().getDeliveryDate()));
                newDoc.replaceItemValue("CA_DeliveryDate", "");
            } else {
                newDoc.replaceItemValue("CA_DeliveryDate", "");
            }
            
            // Add functionality for supplying alternative residual value 
            if (message.getBody().getCreditApplication().getResidualValue() != null) {
                newDoc.replaceItemValue("CA_ResidualValueAdjusted", new Double(message.getBody().getCreditApplication().getResidualValue().getAmount().getValue()));
            } else {
                newDoc.replaceItemValue("CA_ResidualValueAdjusted", new Double(0));
            }
        }

        if (message.getBody().getCreditApplication().getPeriodFee() != null) {
            newDoc.replaceItemValue("CA_PeriodFeeAdjusted", new Double(message.getBody().getCreditApplication().getPeriodFee().getAmount().getValue()));
        } else {
            newDoc.replaceItemValue("CA_PeriodFeeAdjusted", new Double(0));
        }
        
        if (message.getBody().getCreditApplication().getDownPayment() != null) {
            newDoc.replaceItemValue("CA_DownPayment", message.getBody().getCreditApplication().getDownPayment().getValue());
        } else { newDoc.replaceItemValue("CA_DownPayment", new Double(0)); }
        
        // Set fields for hire purchase:
        if (!dealIsLeasing) {
            logger.info("CalculationDAO, function newVersion, dela is HP");
            if (message.getBody().getCreditApplication().getPlannedStartDate() != null) {
                logger.info("spara ner planerat start datum");
                int dateYear = Integer.parseInt(message.getBody().getCreditApplication().getPlannedStartDate().substring(0, 4));
                int dateMonth = Integer.parseInt(message.getBody().getCreditApplication().getPlannedStartDate().substring(5, 7));
                int dateDay = Integer.parseInt(message.getBody().getCreditApplication().getPlannedStartDate().substring(8));
                // Long way to set Date format on the document, month argument has range 0-11 - therefore the '-1'
                Calendar reqDate = Calendar.getInstance();
                reqDate.set(dateYear, dateMonth-1, dateDay);
                reqDate.set(Calendar.HOUR_OF_DAY, 0);
                reqDate.set(Calendar.MINUTE, 0);
                reqDate.set(Calendar.SECOND, 0);
                reqDate.set(Calendar.MILLISECOND, 0);
                newDoc.replaceItemValue("CA_PlannedStartDate", reqDate);
            } // Else - let the planned start date be the same as on old Doc.
           
            // VAT:
            mainDB = DominoDataAccess.openDatabase(DominoDataAccess.MAIN_DATABASE, stock);
            DDocument customerOffer = mainDB.getDocumentByUNID(oldCalc.getItemValueString("CA_CustomerOfferUNID"));
            logger.info("kolla om det finns moms i requesten");
            if (message.getBody().getCreditApplication().getVAT() != null) {
                if (message.getBody().getCreditApplication().getVAT().getPeriodLength() == customerOffer.getItemValueInteger("CO_HPVATFinancePeriodLength").intValue()) {
                    // Set CA_VATFinance - flag used in Entr� that VAT is short time financed!
                    newDoc.replaceItemValue("CA_VATFinance", "1");
                    newDoc.replaceItemValue("CA_VATPeriodLength", customerOffer.getItemValueInteger("CO_HPVATFinancePeriodLength"));
                }
                else {
                    logger.info("VAT period length from request does not match setting on customer offer in Entr�.");
                    throw new DataException("VAT period length from request does not match setting on customer offer in Entr�.");
                }
            }
        }

        logger.debug("Sections used :" + oldCalc.getItemValueString("CA_Sections"));
        Vector sectionsVector = new Vector();
        List sections = oldCalc.getItemValue("CA_Sections");
        HashMap productMap = new HashMap();

        for (Iterator iter = sections.iterator(); iter.hasNext();) {
            String sec = (String) iter.next();
            productMap.put(sec, convertListToVector(oldCalc.getItemValue("CA_" + sec + "RowsTypeOptions")));
            sectionsVector.add(sec);
        }

        Vector equipmentVector = new Vector(message.getBody().getCreditApplication().getEquipment());
        populateCalculationRows(sectionsVector, equipmentVector, productMap, newDoc);

        logger.debug("Total EQ value " + message.getBody().getCreditApplication().getTotalEquipmentValue());
        
        if (!dealIsLeasing) {
            // Verify that there is at least one HWM product in the request for hire purchase deals:
            if (calcHasHWMproduct(newDoc) == false) {
                // No product in request has been matched to a HWM product - abort!
                logger.info("Hire purchase deals in Entr� demands at least one machine product, no such provided in the request.");
                throw new DataException("Hire purchase deals in Entr� demands at least one machine product, no such provided in the request.");
            }
        }
        
        // Get the deal document - get the Editors from this doc.
        DDatabase dealsDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
        DView dealsAllView = contractDb.getView("vwAllByUNIDExclusive");
        allView.refresh();
        DDocument dealDoc = dealsAllView.getDocumentByKey(deal.getUNID(), true);
        
        // agtSaveCreditPSI empties the editors on the calc doc, but is needed for the credit doc.
        newDoc.replaceItemValue("Editors", dealDoc.getItemValue("Editors"));

        newDoc.replaceItemValue("CountryCode", psiRequest.getCountry());
        newDoc.replaceItemValue("DeleteFlag", "0");
        newDoc.replaceItemValue("DocUNID", newDoc.getUniversalID());
        newDoc.replaceItemValue("Created", newDoc.getCreated());
        newDoc.replaceItemValue("CreatedBy", "PSI");
        newDoc.replaceItemValue("CA_Status", oldCalc.getItemValue("CA_Status"));
        newDoc.save(true);
        return populate(newDoc);
    }

    private Vector convertListToVector(List list) {
        Vector v = new Vector();
        for (Iterator it = list.iterator(); it.hasNext();) {
            v.add((String) it.next());
        }
        return v;
    }

    public Calculation findByDealNo(DealNumber dealNumber) throws DNotesException {
        DDocument calculationDoc;
        DView calculationView;
        String searchString = Long.toString(dealNumber.getId()) + "-" + Long.toString(dealNumber.getVersion());

        contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
        calculationView = contractDb.getView(DominoDataAccess.CALCULATION_VIEW);
        calculationDoc = calculationView.getDocumentByKey(searchString, true);
        return populate(calculationDoc);

    }

    public Calculation findByDealNoAndLatestVersion(DealNumber dealNumber) throws DNotesException, DataException {
        Iterator docCollection;
        DDocument calculationDoc;
        DView calculationView;
        String searchString = Long.toString(dealNumber.getId());        
        calculationDoc = doc;
        int currVersion = 0;
        boolean found = false;
        //logger.info("CalculationDAO, method findByDealNoAndLatestVersion, deal no: " + dealNumber.getId());
        
        // Look for versions in both dealsDb and hirepurchaseDb:
        for (int i=0; i<2; i++) {
            if (i == 0) {
                contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
                calculationView = contractDb.getView(DominoDataAccess.CALCULATION_VIEW);
            } else {
                contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
                calculationView = contractDb.getView(DominoDataAccess.HPCALCULATION_VIEW);
            }
            docCollection = calculationView.getAllDocumentsByKey(searchString);
            // logger.info("i: " + i + " and docColl.hasnext: " + docCollection.hasNext());
            if (docCollection.hasNext()) { found = true; }

            while (docCollection.hasNext()) {
                doc = (DDocument) docCollection.next();
                logger.info("now handling deal version: " + doc.getItemValueInteger("CA_Version").intValue());
                if (doc.getItemValueInteger("CA_Version").intValue() > currVersion) {
                    calculationDoc = doc;
                    currVersion = calculationDoc.getItemValueInteger("CA_Version").intValue();
                }
            }
        }
        
        if (!found) {
            return null;
        }

        if (dealNumber.getVersion() == 0) {
            dealNumber.setVersion(calculationDoc.getItemValueInteger("CA_Version").intValue());
        }

        if (dealNumber.getVersion() != calculationDoc.getItemValueInteger("CA_Version").intValue()) {
            logger.info("[" + calculationDoc.getItemValueInteger("CA_DealNo") + "] Version number [" + dealNumber.getVersion() + "] is not valid Current version is [" + calculationDoc.getItemValueInteger("CA_Version") + "]");
            throw new DataException("[" + calculationDoc.getItemValueInteger("CA_DealNo") + "] Version number [" + dealNumber.getVersion() + "] is not valid Current version is [" + calculationDoc.getItemValueInteger("CA_Version") + "]");

        }
        return populate(calculationDoc);
    }

    /*
    public Calculation findByDealNoAndVersion(DealNumber dealNumber) throws NotesException, DataException {
    DocumentCollection docCollection;
    Document calculationDoc;
    String country = dealNumber.getPrefix();
    View calculationView;
    String searchString = Long.toString(dealNumber.getId()) + "-" + Long.toString(dealNumber.getVersion());
    dealsDb = dataAccess.getDealsDb();
    calculationView = dealsDb.getView("vwCalculations");
    docCollection = calculationView.getAllDocumentsByKey(searchString);
    Document doc = docCollection.getFirstDocument();
    calculationDoc = doc;
    int currVersion = 0;
    while (doc != null) {
    logger.debug("Current -in CALCDAO- version " + calculationDoc.getItemValueInteger("CA_Version") + " Next document " + doc.getItemValueInteger("CA_Version"));
    if (doc.getItemValueInteger("CA_Version") > currVersion) {
    calculationDoc = doc;
    currVersion = calculationDoc.getItemValueInteger("CA_Version");
    }
    doc = docCollection.getNextDocument();
    }
    if (dealNumber.getVersion() != calculationDoc.getItemValueInteger("CA_Version")) {
    logger.warn("[" + calculationDoc.getItemValueInteger("CA_DealNo") + "] Version number [" + dealNumber.getVersion() + "] is not valid Current version is [" + calculationDoc.getItemValueInteger("CA_Version") + "]");
    throw new DataException("[" + calculationDoc.getItemValueInteger("CA_DealNo") + "] Version number [" + dealNumber.getVersion() + "] is not valid Current version is [" + calculationDoc.getItemValueInteger("CA_Version") + "]");
    
    }
    return populate(calculationDoc);
    }
    
     */
    private Calculation populate(DDocument calcDoc) throws DNotesException {
        Calculation calculation = new Calculation();
        calculation.setVersion(calcDoc.getItemValueInteger("CA_Version").intValue());
        //FIXME, should we return CA_CustomerFinancedAmount or CA_FinancedAmount?
        calculation.setFinancedAmount((double) calcDoc.getItemValueDouble("CA_FinancedAmount").doubleValue());
        logger.info("PERIOD FEE: " + calcDoc.getItemValueDouble("CA_PeriodFee").toString());
        calculation.setPeriodFee((double) calcDoc.getItemValueDouble("CA_PeriodFee").doubleValue());
        calculation.setDuration(calcDoc.getItemValueInteger("CA_Duration").intValue());
        calculation.setPeriodLength(calcDoc.getItemValueInteger("CA_PeriodLength").intValue());
        calculation.setAllianceUNID(calcDoc.getItemValueString("CA_AllianceUNID"));
        calculation.setPartnerUNID(calcDoc.getItemValueString("CA_PartnerUNID"));
        calculation.setCustomerOfferUNID(calcDoc.getItemValueString("CA_CustomerOfferUNID"));
        calculation.setStatus(calcDoc.getItemValueString("CA_Status"));
        calculation.setUNID(calcDoc.getItemValueString("DocUNID"));
        calculation.setVersion((int) calcDoc.getItemValueInteger("CA_Version").intValue());
        calculation.setCurrency(calcDoc.getItemValueString("CA_Currency"));
        calculation.setEsignEnabled(calcDoc.getItemValueString("CA_EsignEnabled").equalsIgnoreCase("1"));
        
        String dealType = "LEASING";
        if (calcDoc.getItemValueString("CA_DealType").equalsIgnoreCase("HIREPURCHASE")) {
            dealType = "HIREPURCHASE";
        }
        calculation.setDealType(dealType);
        return calculation;
    }

    private void populateCalculationRows(Vector sections, Vector equipmentVector, HashMap productMap, DDocument doc) throws DNotesException, DataException {
        logger.debug("Sec : " + sections);
        Vector notFound = new Vector();
        long n = 0;
        for (Iterator it = sections.iterator(); it.hasNext();) {
            String sec = (String) it.next();
            Vector productNames = (Vector) productMap.get(sec);

            logger.debug("Sec : " + sec);
            logger.debug("productNames:" + productNames);

            //Populate all calculation columns
            Vector priceColumn = new Vector();
            Vector quantityColumn = new Vector();
            Vector typeColumn = new Vector();
            Vector specificationColumn = new Vector();
            Vector modelColumn = new Vector();
            Vector newOrUsedColumn = new Vector();
            Vector serialNoColumn = new Vector();
            Vector usageColumn = new Vector();
            Vector registrationNoColumn = new Vector();
            Vector vatFinanceColumn = new Vector();

            long sectionSum = 0;

            boolean serialNumberFound = false;
            for (Iterator iter = equipmentVector.iterator(); iter.hasNext();) {
                Equipment eqRow = (Equipment) iter.next();
                if (productNames.contains(eqRow.getType())) {
                    priceColumn.add(new Double(eqRow.getAmount().getValue()));
                    if (sec.equalsIgnoreCase("HWM") & eqRow.getQuantity() > 1) {
                        // HWM products may NOT be more than ONE - has to be specified separately - abort if quantity is > 1!!
                        logger.info("Machine products must be specified separately in Entr�, the quantity for these products may not be > 1! Request has quantity: " + eqRow.getQuantity());
                        throw new DataException("Machine products must be specified separately in Entr�, the quantity for these products may not be > 1!");
                    }
                    quantityColumn.add(new Long(eqRow.getQuantity()));
                    typeColumn.add(eqRow.getType());
                    specificationColumn.add(eqRow.getDescription());
                    sectionSum += (eqRow.getQuantity() * eqRow.getAmount().getValue());

                    //fields below not mandatory (adding empty values if missing)
                    if (!eqRow.getModel().equals("")) {
                        modelColumn.add(eqRow.getModel());
                    } else {
                        modelColumn.add("");
                    }
                    if (!eqRow.getNewOrUsed().equals("")) {
                        newOrUsedColumn.add(eqRow.getNewOrUsed());
                    } else {
                        newOrUsedColumn.add("");
                    }
                    if (!eqRow.getSerialNo().equals("")) {
                        if (!eqRow.getSerialNo().equals("")) {
                            serialNumberFound = true;
                        }
                        serialNoColumn.add(eqRow.getSerialNo());
                    } else {
                        serialNoColumn.add("");
                    }
                    if (!eqRow.getUsage().equals("")) {
                        usageColumn.add(eqRow.getUsage());
                    } else {
                        usageColumn.add("");
                    }
                    if (!eqRow.getRegistrationNo().equals("")) {
                        registrationNoColumn.add(eqRow.getRegistrationNo());
                    } else {
                        registrationNoColumn.add("");
                    }
                    if (!eqRow.getVatFinance().equals("")) {
                        vatFinanceColumn.add(eqRow.getVatFinance());
                    } else {
                        vatFinanceColumn.add("");
                    }
                } else {
                    logger.debug("not found!!");
                    //adding equipment that not matches to notFound Vector
                    notFound.add(eqRow);
                }
            }



            equipmentVector.clear();
            equipmentVector.addAll(notFound);
            notFound.clear();

            logger.debug("not found!!");
            logger.debug("typeColumn.size() " + typeColumn.size());

            int currentSize = typeColumn.size();
            int minSize = 2;

            //Add x empty rows (must be at least two items to be treated as array in domino)            
            if (currentSize < minSize) {
                for (int i = currentSize; i < minSize; i++) {
                    priceColumn.add(new Long(0));
                    quantityColumn.add(new Long(0));
                    typeColumn.add("");
                    specificationColumn.add("");
                    modelColumn.add("");
                    newOrUsedColumn.add("");
                    serialNoColumn.add("");
                    usageColumn.add("");
                    registrationNoColumn.add("");
                    vatFinanceColumn.add("");
                }
            }

            doc.replaceItemValue("CA_" + sec + "RowsPrice", priceColumn);
            doc.replaceItemValue("CA_" + sec + "RowsQty", quantityColumn);
            doc.replaceItemValue("CA_" + sec + "RowsSpec", specificationColumn);
            doc.replaceItemValue("CA_" + sec + "RowsType", typeColumn);
            if (sec.equals("HWM")) {
                doc.replaceItemValue("CA_" + sec + "RowsModel", modelColumn);
                doc.replaceItemValue("CA_" + sec + "RowsNewOrUsed", newOrUsedColumn);
                doc.replaceItemValue("CA_" + sec + "RowsUsage", usageColumn);
                doc.replaceItemValue("CA_" + sec + "RowsRegistrationNo", registrationNoColumn);
                doc.replaceItemValue("CA_" + sec + "RowsSerialNo", serialNoColumn);
                doc.replaceItemValue("CA_" + sec + "RowsVATFinance", vatFinanceColumn);
            }
            //Since serial number keeps the equipment together in objectgroups in FINAL, we have to add this information to 
            //other sections then HWM, we only want to add it if != ""
            if (serialNumberFound) {
                doc.replaceItemValue("CA_" + sec + "RowsSerialNo", serialNoColumn);
            }
            doc.replaceItemValue("CA_" + sec + "Sum", new Long(sectionSum).longValue());
        }

        if (equipmentVector.size() > 0) {
            logger.info("Following equipment was not mapped:[" + equipmentVector.toString() + "] will put [" + equipmentVector.size() + "] items in HW section ");
            Vector priceColumn = new Vector(doc.getItemValue("CA_HWRowsPrice"));
            Vector quantityColumn = new Vector(doc.getItemValue("CA_HWRowsQty"));
            Vector typeColumn = new Vector(doc.getItemValue("CA_HWRowsType"));
            Vector specificationColumn = new Vector(doc.getItemValue("CA_HWRowsSpec"));
            double sectionSum = (double) doc.getItemValueDouble("CA_HWSum").doubleValue();
            for (Iterator iter = equipmentVector.iterator(); iter.hasNext();) {
                Equipment eqRow = (Equipment) iter.next();
                priceColumn.add(new Double(eqRow.getAmount().getValue()));
                quantityColumn.add(new Long(eqRow.getQuantity()));
                typeColumn.add("");
                specificationColumn.add(eqRow.getType() + " " + eqRow.getDescription());
                sectionSum += (eqRow.getQuantity() * eqRow.getAmount().getValue());

            }
            //Default puts all remaining rows as Hardware
            String sec = "HW";
            doc.replaceItemValue("CA_" + sec + "RowsPrice", priceColumn);
            doc.replaceItemValue("CA_" + sec + "RowsQty", quantityColumn);
            doc.replaceItemValue("CA_" + sec + "RowsSpec", specificationColumn);
            doc.replaceItemValue("CA_" + sec + "RowsType", typeColumn);
            doc.replaceItemValue("CA_" + sec + "Sum", new Double(sectionSum).doubleValue());
        }

    }

    private String getCurrency(Vector equipment) {
        String currency = "";
        for (Iterator it = equipment.iterator(); it.hasNext();) {
            Equipment eqRow = (Equipment) it.next();
            //TODO must check i there is different currencies on different rows
            currency = eqRow.getAmount().getCurrency();
        }
        return currency;
    }
    /* Todo implement this if we need to make EUR deal through PSI later on
    private Vector getCurrencyOptions(String country){
    Properties prop = EPISProperties.getProp();
    Vector options = new Vector();
    String currencyOptions = "";
    if (PropertiesSetup.propertyExists(country.toUpperCase() + "CurrencyOptions", prop)) {
    templateDirectory = PropertiesSetup.getString(country.toUpperCase() + "CurrencyOptions", prop);
    } else{
    logger.warn("Cant find document template directory using default");
    templateDirectory = "e:/PSI/templates";
    }
    return options;
    
    }
     */

    private Integer getPeriodLength(Duration duration) {
        Integer i;
        if (duration.getUnit().equalsIgnoreCase("MONTH")) {
            i = new Integer(1);
        } else if (duration.getUnit().equalsIgnoreCase("QUARTER")) {
            i = new Integer(3);
        } else {
            //TODO error handling
            i = new Integer(3);
        }
        return i;
    }

    private Integer getDurationInMonths(Duration duration) throws DataException {
        Integer i;
        if (duration.getUnit().equalsIgnoreCase("MONTH")) {
            i = new Integer(1 * duration.getValue());
        } else if (duration.getUnit().equalsIgnoreCase("QUARTER")) {
            i = new Integer(3 * duration.getValue());
        } else {
            //TODO error handling
            throw new DataException("Error ,illegal duration type");
        //i = new Integer(3*duration.getValue());
        }
        return i;
    }
}
