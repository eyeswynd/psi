/*
 * PSIUserDAO.java
 *
 * Created on den 7 oktober 2004, 15:23
 */
package se.mpab.psi.db;

import org.apache.log4j.LogManager;
import se.mpab.util.common.PropertiesSetup;
import se.mpab.psi.AutenticationException;
import se.mpab.psi.PSIProperties;
import se.mpab.psi.user.*;
import java.util.Properties;
import java.util.List;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.io.File;
import se.mpab.psi.Status;


/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class PSIUserDAO {

    private static org.apache.log4j.Logger logger = LogManager.getLogger(PSIUserDAO.class);
    private String PSIUsers = "c:\\temp\\conf\\PSIUsers.xml";
    //private Document document;

    /** Creates a new instance of PSIUserDAO */
    public PSIUserDAO() {
        // Init databasenames from properties
        Properties prop = PSIProperties.getProp();
        try {
            if (PropertiesSetup.propertyExists("PSIUsers", prop)) {
                PSIUsers = PropertiesSetup.getString("PSIUsers", prop);
            } else {
                logger.info("Property PSIUsers not found setting default " + PSIUsers);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    //   logger.setLevel(   (Level)Level.INFO );
    }

    private org.jdom.Document getJdomDoc() {
        org.jdom.Document jdomDoc = null;
        org.jdom.input.SAXBuilder builder = new org.jdom.input.SAXBuilder();
        File tmpFile = new File(this.PSIUsers);
        try {
            jdomDoc = builder.build(tmpFile);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return jdomDoc;
    }

    private PSIUser populatePSIUser(org.jdom.Element user) {
        //logger.setLevel(Level.INFO);
        PSIUser psiUser = new PSIUser();
        psiUser.setUserName(user.getChildTextTrim("userName"));
        psiUser.setPassword(user.getChildTextTrim("password"));
        psiUser.setUserIp(user.getChildTextTrim("ip"));

        // Get all users. Possibly more than one.
        List systemList = user.getChildren("system");
        Iterator entreUser = systemList.iterator();
        while (entreUser.hasNext()) {
            org.jdom.Element tmpEl = (org.jdom.Element) entreUser.next();
            // Loop through entreUsers in XML
            org.jdom.Element tmpEntre = (org.jdom.Element) tmpEl.getChild("entre");
            Entre e = new Entre();
            List userList = tmpEntre.getChildren("user");
            Iterator tmpUser = userList.iterator();
            while (tmpUser.hasNext()) {
                org.jdom.Element tmpUsers = (org.jdom.Element) tmpUser.next();
                EntreUser eu = new EntreUser();
                eu.setSeat(tmpUsers.getChildTextTrim("seat").toUpperCase());
                eu.setUserId(tmpUsers.getChildTextTrim("id"));

                org.jdom.Element alerter = (org.jdom.Element) tmpUsers.getChild("alerter");
                if (alerter != null) {
                    Alerter tmpAlerter = new Alerter();
                    tmpAlerter.setAdapter(alerter.getChildTextTrim("adapter"));
                    tmpAlerter.setUrl(alerter.getChildTextTrim("url"));
                    tmpAlerter.setPassword(alerter.getChildTextTrim("password"));
                    tmpAlerter.setUsername(alerter.getChildTextTrim("username"));

                    eu.setAlerter(tmpAlerter);
                }
                logger.info("User: " + eu.getUserId());

                // Add Entr� offers to hashmap
                List tmpOfferList = tmpUsers.getChildren("offer");
                Iterator tmpOfferIt = tmpOfferList.iterator();
                while (tmpOfferIt.hasNext()) {
                    org.jdom.Element tmpOffer = (org.jdom.Element) tmpOfferIt.next();
                    OfferId oi = new OfferId();
                    oi.setDocUnid(tmpOffer.getChildText("entreOfferId"));
                    oi.setPsiOfferId(tmpOffer.getChildText("psiOfferId"));

                    eu.getDocUnidByPsiOfferId().put(tmpOffer.getChildTextTrim("psiOfferId"), oi);
                    eu.getPsiOfferIdByDocUnid().put(tmpOffer.getChildTextTrim("entreOfferId"), oi);

                }
                // Add EnterUsers to vector Entre
                e.getUsers().put(tmpUsers.getChildTextTrim("seat").toUpperCase(), eu);


            }



            // Add Entre to vector System.
            psiUser.getSystem().put("entre", e);


        // TODO: If other systems add here



        }


        return psiUser;
    }

    public PSIUser getPSIUserFromIp(String IP) throws AutenticationException {
        org.jdom.Element el = this.getJdomDoc().getRootElement();
        List userList = el.getChildren("user");
        PSIUser psiUser = null;
        Iterator user = userList.iterator();
        while (user.hasNext()) {
            org.jdom.Element tmpEl = (org.jdom.Element) user.next();
            if (IP.equals(tmpEl.getChildTextTrim("ip"))) {
                psiUser = this.populatePSIUser(tmpEl);
                break;
            }
        }

        if (psiUser == null) {
            throw new AutenticationException(Status.STATUS_IP_NOT_ALLOWED);
        }
        return psiUser;
    }

    public PSIUser getPSIUserFromUserName(String userName) throws AutenticationException {
        org.jdom.Element el = this.getJdomDoc().getRootElement();
        List userList = el.getChildren("user");
        PSIUser psiUser = null;
        Iterator user = userList.iterator();
        while (user.hasNext()) {
            org.jdom.Element tmpEl = (org.jdom.Element) user.next();
            if (userName.equals(tmpEl.getChildTextTrim("userName"))) {
                psiUser = this.populatePSIUser(tmpEl);
                logger.info("populating ? " + tmpEl.getChildTextTrim("userName"));
                break;
            }
        }

        if (psiUser == null) {
            throw new AutenticationException(Status.STATUS_USER_AUTHENTICATION_ERROR);
        }
        return psiUser;
    }

    private boolean checkNumber(String myNumber, String validNumber) {
        boolean isOk = false;

        if (validNumber.equals("*")) {
            isOk = true;
        } else if (myNumber.equals(validNumber)) {
            isOk = true;
        } else {
            isOk = false;
        }
        return isOk;
    }

    public PSIUser getPSIUserFromKey(String userName, String password, String IP) throws AutenticationException {

        org.jdom.Element el = this.getJdomDoc().getRootElement();
        //logger.debug(el.toString());
        List l = el.getChildren("user");
        PSIUser psiUser = null;
        Iterator it = l.iterator();

        while (it.hasNext()) {

            org.jdom.Element tmpEl = (org.jdom.Element) it.next();
            //logger.debug(tmpEl.getChildTextTrim("userName") + " " + userName + " " +
            //        tmpEl.getChildTextTrim("password") + "  " +
            //        tmpEl.getChildTextTrim("ip"));
            if (userName.equals(tmpEl.getChildTextTrim("userName"))) {
                logger.debug("username ok");
                if (password.equals(tmpEl.getChildTextTrim("password"))) {
                    logger.debug("password ok");

                    if (tmpEl.getChildTextTrim("ip").indexOf("*") != -1) {
                        logger.debug("Valid IP " + tmpEl.getChildTextTrim("ip"));
                        logger.debug("User IP " + IP);

                        String splitPattern = ".";
                        StringTokenizer allowedIP = new StringTokenizer(tmpEl.getChildTextTrim("ip"), splitPattern);
                        StringTokenizer currentIP = new StringTokenizer(IP, splitPattern);

                        Vector allowedIPArr = new Vector();
                        Vector currentIPArr = new Vector();

                        while (allowedIP.hasMoreElements()) {
                            String token = allowedIP.nextToken();
                            allowedIPArr.add(token);
                        }

                        while (currentIP.hasMoreElements()) {
                            String token = currentIP.nextToken();
                            currentIPArr.add(token);
                        }

                        if (checkNumber(currentIPArr.elementAt(0).toString(), allowedIPArr.elementAt(0).toString()) &
                                checkNumber(currentIPArr.elementAt(1).toString(), allowedIPArr.elementAt(1).toString()) &
                                checkNumber(currentIPArr.elementAt(2).toString(), allowedIPArr.elementAt(2).toString()) &
                                checkNumber(currentIPArr.elementAt(3).toString(), allowedIPArr.elementAt(3).toString())) {
                            logger.debug("all ok populating");
                            psiUser = this.populatePSIUser(tmpEl);
                        }

                    } //for other users the IP has to match exactly
                    else {
                        if (IP.equals(tmpEl.getChildTextTrim("ip"))) {
                            logger.debug("all ok populating");
                            psiUser = this.populatePSIUser(tmpEl);
                        }
                    }
                }
            }
        }
        if (psiUser == null) {
            logger.info("User name [" + userName + "] , password [" + password + "] or IP [" + IP + "]  not found.");
            throw new AutenticationException(Status.STATUS_USER_AUTHENTICATION_ERROR);
        }
        return psiUser;
    }
}
