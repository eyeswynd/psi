// Copyright Marquardt & Partners
/*
 * DealDAO.java
 *
 * Created on den 1 september 2004, 09:28
 */
package se.mpab.psi.db;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileWriter;

import se.mpab.psi.PSIProperties;
import se.mpab.psi.bo.*;
import se.mpab.util.common.PropertiesSetup;
import java.util.Vector;
import org.apache.log4j.LogManager;
import se.mpab.psi.mo.*;
import se.mpab.psi.user.*;
import java.util.Properties;
import se.mpab.psi.bo.PSIRequest;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DView;
import de.bea.domingo.DNotesException;

/**
 *
 * @author  janne.berg
 */
public class DealDAO implements DAO {

    private DDatabase contractDb;
    private DDocument doc;
    private DDatabase mainDB;
    private long dealNo;
    private String stock;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(DealDAO.class);
    
    // We need to know if handled request is HP or leasing
    private boolean versionIsHP;
    
    /** Creates a new instance of DealDAO */
    public DealDAO(String stock) {

        this.stock = stock;
        this.versionIsHP = false;

    }

    public Object insert(PSIRequest psiRequest) throws Exception {
        EntreMessage message = psiRequest.getPsiMessage();
        String country = message.getBody().getCreditApplication().getClient().getSeat();
        User salesPerson;
        mainDB = DominoDataAccess.openDatabase(DominoDataAccess.MAIN_DATABASE, stock);
        UserDAO userDAO = new UserDAO(country);
        Customer customer = new Customer();
        CustomerDAO customerDAO = new CustomerDAO(country);
        CalculationDAO calculationDAO = new CalculationDAO(country);
        InstallmentPlanDAO installmentPlanDAO = new InstallmentPlanDAO(country);
        CreditDAO creditDAO = new CreditDAO(country);
        Deal deal = new Deal();

        Entre e = (Entre) psiRequest.getPsiUser().getSystem().get("entre");
        EntreUser eu = (EntreUser) e.getUsers().get(psiRequest.getCountry());
        String salesPersonExternalId = message.getBody().getCreditApplication().getExternalUserReference(PSIProperties.EXTERNAL_USER_REFERENCE_SALES_TXT).getId();
        salesPerson = userDAO.findByName(salesPersonExternalId);

        User defaultSalesPerson = null;
        if (salesPerson != null) {
            // Sales person ID exists in Entr�, fine - but FOS needs to handle MUL functionality - verify that
            // the sales person has same Partner UNID set on person doc as the customer offer:
            DDocument requestCoDoc = mainDB.getDocumentByUNID(message.getBody().getCreditApplication().getOfferId());
            if (salesPerson.getPartnerUNID().equalsIgnoreCase(requestCoDoc.getItemValueString("CO_PartnerUNID"))) {
                // Sales persons partner UNID do not match the CO:s partner UNID - save the request sales persons id 
                // on the deal doc - will be handled in agtSaveCreditPSI and agtSaveCreditPSIHP
                deal.setSalesPerson(salesPerson);
            } else {
                // Get the default user for this partner from Users.xml file:
                defaultSalesPerson = userDAO.getUserByUNID(eu.getUserId());
                logger.info("Salesperson with userID: " + salesPerson.getPE_UserID() + " do not have the same partner UNID as the used CO - MUL:er? - Set default sales person on the deal and save the user id on a temp field on the deal doc");
                deal.setSalesPerson(defaultSalesPerson);
            }
            
            
        } else {
            // Get the default user for this partner from Users.xml file:
            defaultSalesPerson = userDAO.getUserByUNID(eu.getUserId());
            logger.info("Salesperson could not be resolved, using default seller" + defaultSalesPerson.getName());
            deal.setSalesPerson(defaultSalesPerson);
        }
        DDocument partner = mainDB.getDocumentByUNID(deal.getSalesPerson().getPartnerUNID());
        DDocument alliance = mainDB.getDocumentByUNID(deal.getSalesPerson().getAllianceUNID());
        DDocument customerOffer = mainDB.getDocumentByUNID(message.getBody().getCreditApplication().getOfferId());

        customer = customerDAO.populate(message);

        deal.setCustomer(customer);

        contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock); // Deal doc should always be in deals!

        doc = contractDb.createDocument();
        dealNo = getDealNo("");
        //TODO, should not set these here
        deal.setDealNo(dealNo);
        deal.setStock(psiRequest.getCountry());
        
        if (defaultSalesPerson != null) {
            logger.info("defaultSalesPerson is NOT null, the sales person in the request has not been found ");
            if (salesPerson != null) {
                // The sales person in the request exists in Entr� - set this persons UNID on temp field on the deal:
                doc.replaceItemValue("SalesPersonPSI_NotFound", salesPerson.getUNID());
            } else {
                // The sales person in the request (the Entr� user ID) do not exist in Entr�, set empty temp field in the deal.
                doc.replaceItemValue("SalesPersonPSI_NotFound", "");
                // Set salesPerson to the deafultSalesPerson (all verification of requestd sales person is now done):
                salesPerson = new User(defaultSalesPerson);
            }
        }

        doc.replaceItemValue("Form", "frmDeal");
        doc.replaceItemValue("DocType", "Deal");
        doc.replaceItemValue("CountryCode", psiRequest.getCountry());
        //TODO revise reader and editors !
        Vector systemEditors = new Vector();
        systemEditors.add("[SysAdm]");
        doc.replaceItemValue("SystemEditors", systemEditors);

        Vector systemReaders = new Vector();
        systemReaders.add("[SysAdm]");
        systemReaders.add("[ReadAll]");
        doc.replaceItemValue("SystemReaders", systemReaders);

        Vector editors = new Vector();
        editors.addElement(partner.getItemValueString("PA_ExternalManagerGroup"));
        editors.addElement(partner.getItemValueString("PA_InternalSupportGroup"));
        editors.addElement(alliance.getItemValueString("AL_ExternalAllianceManagerGroup"));


        editors.addElement("[SupportAll]");
        editors.addElement(salesPerson.getName());
        editors.trimToSize();

        doc.replaceItemValue("Editors", editors);
        doc.replaceItemValue("Readers", editors);

        doc.replaceItemValue("DE_AllianceUNID", deal.getSalesPerson().getAllianceUNID());

        //if sales person is External MUL, let offerId decide which partner to assign
        logger.debug("PE_Role" + deal.getSalesPerson().getRoles());
        if (deal.getSalesPerson().getRoles().contains("[ExternalMul]")) {
            doc.replaceItemValue("DE_PartnerUNID", customerOffer.getItemValueString("CO_PartnerUNID"));
        } else {
            doc.replaceItemValue("DE_PartnerUNID", deal.getSalesPerson().getPartnerUNID());
        }
        String comment = "";
        if (psiRequest.getPsiMessage().getBody().getCreditApplication().getExternalUserReference("ADMIN") != null) {
            comment += "Admin : " + psiRequest.getPsiMessage().getBody().getCreditApplication().getExternalUserReference("ADMIN").getFullName() + "\n";
        }
        if (psiRequest.getPsiMessage().getBody().getCreditApplication().getExternalUserReference("SALES") != null) {
            comment += "Sales : " + psiRequest.getPsiMessage().getBody().getCreditApplication().getExternalUserReference(PSIProperties.EXTERNAL_USER_REFERENCE_SALES_TXT).getFullName();
        }

        doc.replaceItemValue("DE_Comments", comment);
        doc.replaceItemValue("DE_Type", "STANDARD");
        doc.replaceItemValue("DE_Status", "ACTIVE");
        doc.replaceItemValue("DE_AdditionAgreementNo", "");
        doc.replaceItemValue("DE_DealNo", new Double(dealNo));
        doc.replaceItemValue("DE_SalesPerson", deal.getSalesPerson().getName());
        //TODO this is still pending in the XML message
        doc.replaceItemValue("DE_CustType", "COMPANY");
        //TODO this should not be hardcoded
        doc.replaceItemValue("DE_CustVerified", new Integer(2));
        doc.replaceItemValue("DE_CustOrgNo", formatOrgNo(deal.getCustomer()));
        doc.replaceItemValue("DE_CustName", deal.getCustomer().getName());
        doc.replaceItemValue("DE_CustCoName", deal.getCustomer().getCoName());
        doc.replaceItemValue("DE_CustPhone", deal.getCustomer().getPhone());
        doc.replaceItemValue("DE_CustAddress", deal.getCustomer().getAddress());
        doc.replaceItemValue("DE_CustZipCode", deal.getCustomer().getZipCode());
        doc.replaceItemValue("DE_CustCity", deal.getCustomer().getCity());
        doc.replaceItemValue("DE_CustContactName", deal.getCustomer().getContactName());
        doc.replaceItemValue("DE_CustContactPhone", deal.getCustomer().getContactPhone());
        doc.replaceItemValue("DE_CustContactFax", deal.getCustomer().getContactFax());
        doc.replaceItemValue("DE_CustContactEmail", deal.getCustomer().getContactEmail());
        doc.replaceItemValue("DE_CustBillCOName", deal.getCustomer().getBillCOName());
        doc.replaceItemValue("DE_CustBillAddress", deal.getCustomer().getBillAddress());
        doc.replaceItemValue("DE_CustBillZipCode", deal.getCustomer().getBillZipCode());
        doc.replaceItemValue("DE_CustBillCity", deal.getCustomer().getBillCity());
        doc.replaceItemValue("DE_CustInstAddress", deal.getCustomer().getInstAddress());
        doc.replaceItemValue("DE_CustInstCity", deal.getCustomer().getInstCity());
        
        logger.info("DE_CustExternalRefNo" + deal.getCustomer().getExternalReference());
        doc.replaceItemValue("DE_CustExternalRefNo", deal.getCustomer().getExternalReference());
        //TODO must set this after credit is checked
        doc.replaceItemValue("DE_CustRiskPercent", "");

        doc.computeWithForm(false);
        
        doc.save(true);
        doc.replaceItemValue("SearchCustOrgNo", formatOrgNo(deal.getCustomer()));
        doc.replaceItemValue("DeleteFlag", "0");
        doc.replaceItemValue("DocUNID", doc.getUniversalID());
        doc.replaceItemValue("Created", doc.getCreated());
        doc.replaceItemValue("CreatedBy", "PSI");
        doc.replaceItemValue("Modified", "");
        doc.replaceItemValue("PSI_User", psiRequest.getPsiUser().getUserName());
        doc.save(true);

        //if (!country.equals("NO")) {
        doc.replaceItemValue("DE_CustVerified", new Integer(2));
        doc.save(true);
        String db = "deals";
        DominoDataAccess.runLSAgent(stock, db, "agtVerifyCustomerPSI", doc.getUniversalID());
        //}

        //Update with changes from agtVerifyCustomerPSI
        doc = this.findByUNID(doc.getUniversalID());

        deal.setCustomer(customerDAO.updateFromDealDoc(customer, doc));

        deal.setCalculation(calculationDAO.insert(psiRequest, deal));
        deal.setCredit((Credit) creditDAO.insert(psiRequest, deal, 1));
        if (message.getHeader().getService().equals("makeCreditApplicationHP")) {
             deal.setInstallmentPlan(installmentPlanDAO.insert(psiRequest, deal, 1));
        }
        
        return populateDealObject(doc, deal);

    }

    /* Helper method for getting the current Entr� doc number. We need to use some lock here ! */
    private long getDealNo(String fileName) {

        long dealNo = 0;
        Properties prop = PSIProperties.getProp();
        try {
            File dealNoFile = new File(PropertiesSetup.getString("dealNoFile", prop));
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(dealNoFile)));
            String line = "";
            if ((line = br.readLine()) != null) {
                logger.info(line);
                dealNo = Integer.parseInt(line);
                br.close();
                dealNoFile.delete();
                BufferedWriter out = new BufferedWriter(new FileWriter(dealNoFile));
                out.write(Long.toString(dealNo + 1));
                out.close();
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return dealNo + 1;
    }

    public boolean update(Deal deal) {
        boolean saveStatus = false;
        try {

            doc = contractDb.createDocument();
            dealNo = getDealNo("");
            deal.setDealNo(dealNo);
            doc.replaceItemValue("form", "frmDeal");
            doc.replaceItemValue("DocType", "Deal");

            //TODO must fetch editors and readers from partner !
            Vector systemEditors = new Vector();
            systemEditors.add("[SysAdm]");
            doc.replaceItemValue("SystemEditors", systemEditors);

            Vector systemReaders = new Vector();
            systemReaders.add("[SysAdm]");
            systemReaders.add("[ReadAll]");
            doc.replaceItemValue("SystemReaders", systemReaders);

            //  doc.replaceItemValue("DE_AllianceUNID", deal.getSalesPerson().getAllianceUNID());
            //  doc.replaceItemValue("DE_PartnerUNID",deal.getSalesPerson().getPartnerUNID());
            // doc.replaceItemValue("DE_Type","STANDARD");
            //   doc.replaceItemValue("DE_Status","ACTIVE");
            // doc.replaceItemValue("DE_AdditionAgreementNo","")		;
            //doc.replaceItemValue("DE_DealNo", new Long(dealNo ));
            //            doc.replaceItemValue("DE_SalesPerson",deal.getSalesPerson().getName());
            //TODO this is still pending in the XML message
            doc.replaceItemValue("DE_CustType", "COMPANY");
            //TODO , should we really just set 1 as default .. ?
            doc.replaceItemValue("DE_CustVerified", new Integer(2));
            //          doc.replaceItemValue("DE_CustOrgNo", formatOrgNo(deal.getCustomer()) );
            doc.replaceItemValue("DE_CustName", deal.getCustomer().getName());
            doc.replaceItemValue("DE_CustCoName", deal.getCustomer().getCoName());
            doc.replaceItemValue("DE_CustPhone", deal.getCustomer().getPhone());
            doc.replaceItemValue("DE_CustAddress", deal.getCustomer().getAddress());
            doc.replaceItemValue("DE_CustZipCode", deal.getCustomer().getZipCode());
            doc.replaceItemValue("DE_CustCity", deal.getCustomer().getCity());
            doc.replaceItemValue("DE_CustContactName", deal.getCustomer().getContactName());
            doc.replaceItemValue("DE_CustContactPhone", deal.getCustomer().getContactPhone());
            doc.replaceItemValue("DE_CustContactFax", deal.getCustomer().getContactFax());
            doc.replaceItemValue("DE_CustContactEmail", deal.getCustomer().getContactEmail());
            doc.replaceItemValue("DE_CustBillCOName", deal.getCustomer().getBillCOName());
            doc.replaceItemValue("DE_CustBillAddress", deal.getCustomer().getBillAddress());
            doc.replaceItemValue("DE_CustBillZipCode", deal.getCustomer().getBillZipCode());
            doc.replaceItemValue("DE_CustBillCity", deal.getCustomer().getBillCity());
            doc.replaceItemValue("DE_CustInstAddress", deal.getCustomer().getInstAddress());
            doc.replaceItemValue("DE_CustInstCity", deal.getCustomer().getInstCity());
            doc.replaceItemValue("DE_CustExternalRefNo", deal.getCustomer().getExternalReference());
            //TODO must set this after credit is checked
            doc.replaceItemValue("DE_CustRiskPercent", deal.getCredit().getAutoRiskPrognosisCode());

            saveStatus = doc.computeWithForm();
            doc.save(true);
            doc.replaceItemValue("DeleteFlag", "0");
            doc.replaceItemValue("DocUNID", doc.getUniversalID());
            doc.replaceItemValue("Created", doc.getCreated());
            doc.replaceItemValue("CreatedBy", "PSI");


            doc.save(true);

        } catch (Exception e) {
            // todo Logger        Logger.getInstance().error("Error " + e.getMessage());
            logger.error(e.getMessage(), e);
        }

        return saveStatus;
    }

    public Deal findByDealNo(DealNumber dealNumber) throws Exception, DataException {
        // Main object to return
        Deal deal = new Deal();
        // Internal object variables
        DView dealView;
        DDocument dealDoc;

        String searchString = Long.toString(dealNumber.getId());
        //logger.info("Get deal by deal number <" + searchString + ">" + " Stock " + ();

        contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock); // TODO move this to the database class

        dealView = contractDb.getView(DominoDataAccess.DEAL_VIEW);
        dealDoc = dealView.getDocumentByKey(searchString, true);
        if (dealDoc == null) {
            logger.info("Deal number [" + dealNumber.getId() + "] is not found.");
            throw new DataException("Deal number [" + dealNumber.getId() + "] is not found.");
        }
        populate(deal, dealDoc, dealNumber);
        return deal;
    }

    public DDocument findByUNID(String UNID) throws DNotesException {
        DDocument dealDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            dealDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("DealDAO.findByUNID, UNID=[" + UNID + "], getNoteID()=[" + dealDoc.getNoteID() + "]");

        } catch (Exception e) {
            logger.warn("Error in DealDAO findByUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return dealDoc;
    }
    
    //TOD must fix this, must be a more generic way of diong this
    private Deal populateDealObject(DDocument dealDoc, Deal deal) throws Exception {


        deal.setDealNo((long) dealDoc.getItemValueDouble("DE_DealNo").longValue());
        deal.setCustomerOfferUNID(dealDoc.getItemValueString("customerOfferUNID"));
        deal.setPsiUserID(dealDoc.getItemValueString("PSI_User"));
        deal.setStatus(dealDoc.getItemValueString("DE_Status"));
        deal.setUNID(dealDoc.getItemValueString("DocUNID"));
        deal.setHistory(dealDoc.getItemValueString("DE_History"));
        deal.setComments(dealDoc.getItemValueString("DE_Comments"));
        deal.setInternalComments(dealDoc.getItemValueString("DE_InternalComments"));
        deal.setType(dealDoc.getItemValueString("DE_Type"));
        deal.setStock(dealDoc.getItemValueString("CountryCode"));

        return deal;
    }
    //TODO must populate all fields in Deal!
    //TODO Populate method should only take an Document and return the entity Deal in this case !
    //dealNo should be present in the deal object, you dont need to send in an empty Dealobject
    private void populate(Deal deal, DDocument dealDoc, DealNumber dealNumber) throws Exception {

        deal.setDealNo((long) dealDoc.getItemValueDouble("DE_DealNo").longValue());
        //TODO must fix this !
        //deal.setAllianceUNID(dealDoc.getItemValueString("DE_AllianceUNID"));
        //deal.setPartnerUNID(dealDoc.getItemValueString("DE_PartnerUNID"));

        deal.setCustomerOfferUNID(dealDoc.getItemValueString("customerOfferUNID"));
        deal.setPsiUserID(dealDoc.getItemValueString("PSI_User"));
        deal.setStatus(dealDoc.getItemValueString("DE_Status"));
        deal.setUNID(dealDoc.getItemValueString("DocUNID"));
        deal.setHistory(dealDoc.getItemValueString("DE_History"));
        deal.setComments(dealDoc.getItemValueString("DE_InternalComments"));
        deal.setStock(dealDoc.getItemValueString("CountryCode"));       
        
        // Populate objects
        CalculationDAO calculationDAO = new CalculationDAO(deal.getStock());
        CreditDAO creditDAO = new CreditDAO(deal.getStock());
        AgreementDAO agreementDAO = new AgreementDAO(deal.getStock());
        UserDAO userDAO = new UserDAO(deal.getStock());
        // DAO to be used
        CustomerDAO customerDAO = new CustomerDAO(deal.getStock());
        GuarantorDAO guarantorDAO = new GuarantorDAO(deal.getStock());
        deal.setSalesPerson(userDAO.findByName(dealDoc.getItemValueString("DE_SalesPerson")));
        deal.setCalculation(calculationDAO.findByDealNoAndLatestVersion(dealNumber));
        if (this.versionIsHP) {
            InstallmentPlanDAO installmentPlanDAO = new InstallmentPlanDAO(this.stock);
            deal.setInstallmentPlan(installmentPlanDAO.findByDealNoAndLatestVersion(dealNumber));
        }
        deal.setCredit(creditDAO.findByDealNoAndLatestVersion(dealNumber));
        deal.getCredit().setGuarantors(guarantorDAO.findByDealNo(dealNumber));
        if (dealNumber != null) {
            dealNumber.setVersion(deal.getCalculation().getVersion());
        }


        deal.setAgreement(agreementDAO.findByDealNoAndLatestVersion(dealNumber));

        Customer cust = new Customer();
        cust.setOrgNo(dealDoc.getItemValueString("DE_CustOrgNo"));
        cust.setName(dealDoc.getItemValueString("DE_CustName"));
        cust.setExternalReference(dealDoc.getItemValueString("DE_CustExternalRefNo"));

        cust.setSeat(dealDoc.getItemValueString("CountryCode"));
        deal.setCustomer(cust);


    }

    private String formatOrgNo(Customer customer) {
        String localizedOrgNo = customer.getOrgNo();
        if (customer.getSeat().equals("SE")) {
            if (localizedOrgNo.indexOf("-") < 0) {
                StringBuffer buf = new StringBuffer(localizedOrgNo);
                buf = buf.insert(6, "-");
                localizedOrgNo = buf.toString();
            }
        }
        return localizedOrgNo;
    }
    
    public void setVersionIsHP(){
        this.versionIsHP = true;
    }
}
