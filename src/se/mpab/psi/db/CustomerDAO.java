/*
 * CustomerDAO.java
 *
 * Created on den 23 september 2004, 15:53
 */
package se.mpab.psi.db;

import se.mpab.psi.bo.*;
import se.mpab.psi.PSIProperties;
import org.apache.log4j.LogManager;
import java.util.Vector;
import se.mpab.psi.mo.*;
import java.util.Properties;
import de.bea.domingo.DAgent;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;

/**
 *
 * @author  janne.berg
 */
public class CustomerDAO implements DAO {

    private DDatabase dealsDb;
    private DDatabase mainDB;
    private DominoDataAccess dataAccess;
    private String stock = "";
    private static org.apache.log4j.Logger logger = LogManager.getLogger(CreditDAO.class);

    /** Creates a new instance of CustomerDAO */
    public CustomerDAO(String stock) {
        // Init databasenames from properties
        //Properties prop = PSIProperties.getProp();
        this.stock = stock;

    }

    public Customer findByIdentificationNumber(String id) {
        Customer customer = new Customer();

        return customer;
    }

    public Customer populate(EntreMessage message) {
        Customer customer = new Customer();
        Client client = message.getBody().getCreditApplication().getClient();
        //Adding externalReference for ATEA
        String externalReference = message.getBody().getCreditApplication().getExternalReference();
        
        customer.setOrgNo(client.getIdentificationNumber());
        customer.setSeat(client.getSeat());
        if (client.getName() != null) {
            customer.setName(client.getName());
        }
        //Company address        
        if (client.getCompanyAddress() != null) {
            customer.setAddress(client.getCompanyAddress().getPostal());
            customer.setCity(client.getCompanyAddress().getCity());
            customer.setZipCode(client.getCompanyAddress().getZip());
        }
        //Invoice address
        if (client.getInvoiceAddress() != null) {
            customer.setBillAddress(client.getInvoiceAddress().getPostal());
            customer.setBillCity(client.getInvoiceAddress().getCity());
            customer.setBillZipCode(client.getInvoiceAddress().getZip());
        }
        //Installation address
        if (client.getInstallationAddress() != null) {
            customer.setInstAddress(client.getInstallationAddress().getPostal());
            customer.setInstCity(client.getInstallationAddress().getCity());
        }
        
        //Customer email address
        if (client.getContactEmail()!= null) {
            customer.setContactEmail(client.getContactEmail());
        }
        
        //External Reference       
        if (externalReference!= null) {
            customer.setExternalReference(externalReference);
        }
        
        return customer;
    }

    public Customer updateFromDealDoc(Customer customer, DDocument dealDoc) throws DNotesException {

        customer.setOrgNo(dealDoc.getItemValueString("DE_CustOrgNo"));
        customer.setName(dealDoc.getItemValueString("DE_CustName"));

        if (dealDoc.getItemValueInteger("DE_CustVerified") != null) {
            customer.setVerified(true);
        } else {
            customer.setVerified(false);
        }

        customer.setAddress(dealDoc.getItemValueString("DE_CustAddress"));
        customer.setCity(dealDoc.getItemValueString("DE_CustCity"));
        customer.setZipCode(dealDoc.getItemValueString("DE_CustZipCode"));
        customer.setContactEmail(dealDoc.getItemValueString("DE_CustContactEmail"));
        customer.setExternalReference(dealDoc.getItemValueString("DE_CustExternalRefNo"));
        return customer;
    }

    public int verifyCustomer(String parameterDocUNID) throws DNotesException {

        return DominoDataAccess.runLSAgent(stock, "deals", "agtVerifyCustomerPSI", parameterDocUNID);

    }    
}
