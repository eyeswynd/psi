/*
 * ProductDAO.java
 *
 * Created on den 1 mars 2005, 11:14
 */
package se.mpab.psi.db;

import java.io.*;
import se.mpab.psi.*;
import se.mpab.psi.bo.*;
import java.util.*;
import org.apache.log4j.LogManager;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import de.bea.domingo.DViewEntry;




/**
 *
 * @author janne.berg
 */
public class ProductDAO {
    private DDatabase mainDb;
    private DDocument doc ;
    //TABORTprivate DominoDataAccess dataAccess;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(ProductDAO.class);
    private String templateDirectory;
    /** Creates a new instance of CalculationDAO */
    public ProductDAO(String stock) throws DNotesException{
         mainDb = DominoDataAccess.openDatabase(DominoDataAccess.MAIN_DATABASE,stock);
    }
    
    public Vector findProductsByPartnerAndAlliance(String partnerUNID,String allianceUNID) {
        
        DView productView = mainDb.getView(DominoDataAccess.PRODUCTS_BY_AVAILABLE_FOR);
        Vector result = new Vector();
        //TODO stupid domino DB makes me do this 3 times!
        Iterator products = productView.getAllEntriesByKey(partnerUNID);
        //logger.debug("OBJECTS hasnext ? " + products.hasNext() );
        while (products.hasNext()) {
            Object o = products.next();
            //System.err.println(o.getClass().toString() + " " + o.getClass().getName());
            DViewEntry entry = (DViewEntry) o;
            doc = entry.getDocument();
            result.add(populate(doc));
        }
        doc = null;
        products = productView.getAllEntriesByKey(allianceUNID);
        
        while (products.hasNext()) {
            DViewEntry entry = (DViewEntry) products.next();
            doc = entry.getDocument();
            result.add(populate(doc));
        }
        doc = null;
        products = productView.getAllEntriesByKey("ALL");
        while (products.hasNext()) {
            DViewEntry entry = (DViewEntry) products.next();
            doc = entry.getDocument();
            result.add(populate(doc));
        }
        //logger.debug("Found " + result.size() + " products");
        //logger.debug(result.toString());
        result.trimToSize();
        return result;
        
    }
    
    private ProductBO populate(DDocument doc) {
        ProductBO product = new ProductBO();
        if (!doc.getItemValueString("PR_Active").equals("")){
            product.setActive(true);
        }
        product.setName(doc.getItemValueString("PR_Name"));
        product.setObjectCode(doc.getItemValueString("PR_ObjectType"));
        product.setType(doc.getItemValueString("PR_Type"));
        
        return product;
    }
}


