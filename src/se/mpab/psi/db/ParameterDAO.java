/*
 * MessageQueDAO.java
 *
 * Created on den 5 januari 2005, 14:40
 */

package se.mpab.psi.db;

import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import java.util.List;

import org.apache.log4j.LogManager;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class ParameterDAO {
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(PSIMessageQueDAO.class);
    
    private DDatabase parameterDb = null;

    
    
    /** Creates a new instance of MessageQueDAO */
    public ParameterDAO()throws DNotesException {
        // Init databasenames from properties
        parameterDb = DominoDataAccess.openDatabase("parameters","common");
        
    }
    
    public List getParameterByName(String parameterName) throws DNotesException {    
        DDocument parameterDoc;
        DView parameterView;
        parameterView = parameterDb.getView(DominoDataAccess.PARAMETERS_BY_NAME_VIEW);        
        parameterDoc = parameterView.getDocumentByKey(parameterName,true);        
        //logger.debug("ParameterName: " + parameterName + ", value: " + parameterDoc.getItemValue("PM_Value"));
        return parameterDoc.getItemValue("PM_Value");
    }
    
}


