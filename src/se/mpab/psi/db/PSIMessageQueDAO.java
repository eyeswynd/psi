/*
 * MessageQueDAO.java
 *
 * Created on den 5 januari 2005, 14:40
 */
package se.mpab.psi.db;

import se.mpab.util.common.PropertiesSetup;
import java.util.Properties;
import java.util.Vector;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DView;
import de.bea.domingo.DViewEntry;
import java.util.Iterator;

import org.apache.log4j.LogManager;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class PSIMessageQueDAO {

    private static org.apache.log4j.Logger logger = LogManager.getLogger(PSIMessageQueDAO.class);
    private String messageQueDb = null;
    private DDatabase mqDb = null;


    /** Creates a new instance of MessageQueDAO */
    public PSIMessageQueDAO() {
        // Init databasenames from properties
        Properties prop = se.mpab.psi.PSIProperties.getProp();
        try {
            if (PropertiesSetup.propertyExists("messageQueDb", prop)) {
                messageQueDb = PropertiesSetup.getString("messageQueDb", prop);
            } else {
                messageQueDb = "entre\\common\\psi.nsf";
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public Vector getMessageQue() {
        Vector returnVector = new Vector();
        try {

            DDocument doc = null;
            mqDb = DominoDataAccess.openDatabase(DominoDataAccess.PSI_DATABASE, "common");
            DView tmpView = mqDb.getView("Queue");
            Iterator docs = tmpView.getAllEntries();
            while (docs.hasNext()) {
                DViewEntry entry = (DViewEntry) docs.next();
                doc = entry.getDocument();
                returnVector.add(doc);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return returnVector;
    }
}

    