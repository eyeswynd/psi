// Copyright Marquardt & Partners
/*
 * AgreementDAO.java
 *
 * Created on den 3 september 2004, 14:10
 */
package se.mpab.psi.db;

import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.LogManager;
import se.mpab.psi.PSIProperties;
import se.mpab.psi.bo.*;
import se.mpab.psi.bo.Deal;
import se.mpab.psi.bo.PSIRequest;
import se.mpab.psi.mo.AgreementApplication;
import se.mpab.psi.mo.DealNumber;
import se.mpab.psi.mo.EntreMessage;

/**
 *
 * @author  Hans.Olsberg
 */
public class AgreementDAO implements DAO {

    private DDatabase contractDb;
    private DDatabase mainDB;
    private DDocument doc;
    private DominoDataAccess dataAccess;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(AgreementDAO.class);
    private String stock = "";
  
    /** Creates a new instance of AgreementDAO */
    public AgreementDAO(String stock) {
        // Init databasenames from properties
        this.stock = stock;
    //dataAccess = new DominoDataAccess(stock);

    }

    public Object insert(PSIRequest psiRequest, Deal deal, int version) throws DNotesException, DataException {
        EntreMessage message = psiRequest.getPsiMessage();
        DDocument doc;
        // This variable is used for getting field which are either leasing or hire purchase specific:
        Boolean dealIsLeasing = true;
        if (message.getHeader().getService().equals("makeAgreementApplication") | message.getHeader().getService().equals("changeCreditApplication")) {
            logger.info("in AgreementDAO, method insert service is makeAgreementApplication");
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
        } else if (message.getHeader().getService().equals("makeAgreementApplicationHP") | message.getHeader().getService().equals("changeCreditApplicationHP")) {
            logger.info("in AgreementDAO, method insert service is makeAgreementApplicationHP");
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
            dealIsLeasing = false;
        }

        mainDB = DominoDataAccess.openDatabase(DominoDataAccess.MAIN_DATABASE, stock);

        DDocument customerOffer = mainDB.getDocumentByUNID(deal.getCalculation().getCustomerOfferUNID());
        DDocument partner = mainDB.getDocumentByUNID(deal.getSalesPerson().getPartnerUNID());
        DDocument alliance = mainDB.getDocumentByUNID(deal.getSalesPerson().getAllianceUNID());

        doc = contractDb.createDocument();

        doc.replaceItemValue("CountryCode", psiRequest.getCountry());
        doc.replaceItemValue("DocType", "Agreement");
        doc.replaceItemValue("Form", "frmAgreement");
        

        //this needs to be done to get the correct fieldtypes

        doc.computeWithForm(false);


        Vector systemEditors = new Vector();
        systemEditors.add("[SysAdm]");
        doc.replaceItemValue("SystemEditors", systemEditors);

        Vector systemReaders = new Vector();
        systemReaders.add("[SysAdm]");
        systemReaders.add("[ReadAll]");
        doc.replaceItemValue("SystemReaders", systemReaders);

        Vector readers = new Vector();
        readers.addElement(partner.getItemValueString("PA_ExternalManagerGroup"));
        readers.addElement(partner.getItemValueString("PA_InternalSupportGroup"));
        readers.addElement(alliance.getItemValueString("AL_ExternalAllianceManagerGroup"));
        readers.addElement("[SupportAll]");
        readers.addElement(deal.getSalesPerson().getName());

        readers.trimToSize();
        doc.replaceItemValue("Editors", "");
        doc.replaceItemValue("Readers", readers);

        doc.replaceItemValue("AG_DealNo", new Double(deal.getDealNo()));
        doc.replaceItemValue("AG_Version", new Integer(version));
 
        doc.replaceItemValue("AG_PartnerName", partner.getItemValue("PA_LegalName"));
        if (dealIsLeasing) {
            //Hans till�gg 091201
            doc.replaceItemValue("AG_CalcRowsSpecTransferals", "");
            
            doc.replaceItemValue("AG_PartnerCONamePartner", partner.getItemValue("PA_COName"));
            doc.replaceItemValue("AG_PartnerAddressPartner", partner.getItemValue("PA_Address"));
            doc.replaceItemValue("AG_PartnerZipCodePartner", partner.getItemValue("PA_ZipCode"));
            doc.replaceItemValue("AG_PartnerCityPartner", partner.getItemValue("PA_City"));
            doc.replaceItemValue("AG_AgreementAddress", partner.getItemValue("PA_AgreementAddress"));
			
            doc.replaceItemValue("AG_Logo", ""); // AG_Logo is only used for Transferals - set empty string
            doc.replaceItemValue("AG_Footer", "DLL"); // AG_Footer is only used for Transferals - set default value
            doc.replaceItemValue("AG_SpecialConditionsCO", customerOffer.getItemValue("CO_SpecialConditions"));
        } else {
            doc.replaceItemValue("AG_ExchangeObject", ""); //Exchange is not valid (yet) for PSI - set empty string           
        }
        
        doc.replaceItemValue("AG_AgreementCondition", customerOffer.getItemValue("CO_AgreementCondition"));
        doc.replaceItemValue("AG_AppointSupplierEnabled", partner.getItemValue("PA_AppointSupplierEnabled"));
        doc.replaceItemValue("AG_AppointedSupplierName", partner.getItemValue("PA_LegalName"));
        doc.replaceItemValue("AG_AppointedSupplierOrgNo", partner.getItemValue("PA_OrganizationNo"));
        doc.replaceItemValue("AG_AppointedSalesPerson", partner.getItemValue("FirstName") + " " + partner.getItemValue("LastName"));

        doc.replaceItemValue("AG_SpecificationEnabled", "1");
            

        doc.save(true);

        doc.replaceItemValue("CountryCode", psiRequest.getCountry());
        doc.replaceItemValue("DeleteFlag", "0");
        doc.replaceItemValue("DocUNID", doc.getUniversalID());
        doc.replaceItemValue("Created", doc.getCreated());
        doc.replaceItemValue("Modified", "");
        doc.replaceItemValue("CreatedBy", "PSI");

        doc.save(true);

        return populate(doc);

    }

    public Agreement findByUNID(String UNID) throws DNotesException {
        DDocument agreementDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            agreementDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("AgreementDAO.findByUNID, UNID=[" + UNID + "], getNoteID()=[" + agreementDoc.getNoteID() + "]");

        } catch (Exception e) {
            logger.warn("Error in Agreement findByUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return populate(agreementDoc);
    }
    
     public Agreement findByHpUNID(String UNID) throws DNotesException {
        DDocument agreementDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            agreementDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("AgreementDAO.findByHPUNID, UNID=[" + UNID + "], getNoteID()=[" + agreementDoc.getNoteID() + "]");

        } catch (Exception e) {
            logger.warn("Error in Agreement findByUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return populate(agreementDoc);
    }

    public Agreement findByDealNoAndLatestVersion(DealNumber dealNumber) throws DNotesException, DataException {
        Iterator docCollection;
        DDocument agreementDoc = null;
        DDocument doc;
        DView agreementView;
        String searchString = Long.toString(dealNumber.getId());
        int currVersion = 0;
        boolean found = false;
        
        logger.info("AgreementDAO, method findByDealNoAndLatestVersion, deal no: " + dealNumber.getId());
        
        // Look for versions in both dealsDb and hirepurchaseDb:
        for (int i=0; i<2; i++) {
            if (i == 0) {
                contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
                agreementView = contractDb.getView(DominoDataAccess.AGREEMENT_VIEW);
            } else {
                agreementView = null;
                docCollection = null;
                contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
                agreementView = contractDb.getView(DominoDataAccess.HPAGREEMENT_VIEW);
            }
            docCollection = agreementView.getAllDocumentsByKey(searchString);
            if (docCollection.hasNext()) { found = true; }
            
            while (docCollection.hasNext()) {
                doc = (DDocument) docCollection.next();
                logger.info("now handling deal version: " + doc.getItemValueInteger("AG_Version").intValue());
                if (doc.getItemValueInteger("AG_Version").intValue() > currVersion) {
                    agreementDoc = doc;
                    currVersion = agreementDoc.getItemValueInteger("AG_Version").intValue();
                }
            }
        }
        
        if (!found) { return null; }

        if (dealNumber.getVersion() == 0) {
            dealNumber.setVersion(agreementDoc.getItemValueInteger("AG_Version").intValue());
        }
        if (dealNumber.getVersion() != agreementDoc.getItemValueInteger("AG_Version").intValue()) {
            logger.warn("[" + dealNumber.getId() + "] Version number [" + dealNumber.getVersion() + "] is not valid Agreement Current version is [" + agreementDoc.getItemValueInteger("AG_Version") + "]");
            throw new DataException("[" + dealNumber.getId() + "] Version number [" + dealNumber.getVersion() + "] is not valid Current version is [" + agreementDoc.getItemValueInteger("AG_Version") + "]");
        }

        return populate(agreementDoc);
    }

    public Agreement findByDealNoAndVersion(DealNumber dealNumber) throws DNotesException, DataException {
        Iterator docCollection;
        DDocument agreementDoc;
        DView agreementView;
        String searchString = Long.toString(dealNumber.getId()) + "-" + Long.toString(dealNumber.getVersion());
        contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
        agreementView = contractDb.getView("vwAgreements");
        docCollection = agreementView.getAllDocumentsByKey(searchString);

        agreementDoc = doc;
        int currVersion = 0;
        boolean found = false;

        if (docCollection.hasNext()) {
            found = true;
        }

        while (doc != null) {
            logger.debug("Current version Agreement Version " + agreementDoc.getItemValueInteger("AG_Version") + " Next document " + doc.getItemValueInteger("AG_Version"));
            if (doc.getItemValueInteger("AG_Version").intValue() > currVersion) {
                agreementDoc = doc;
                currVersion = agreementDoc.getItemValueInteger("AG_Version").intValue();
            }

        }

        if (!found) {
            return null;
        }

        return populate(agreementDoc);
    }

    public Agreement populate(DDocument agreementDoc) throws DNotesException {
        Agreement agreement = new Agreement();

        agreement.setPartnername(agreementDoc.getItemValueString("AG_PartnerName"));
        agreement.setPartnerCondition(agreementDoc.getItemValueString("AG_PartnerCondition"));
        agreement.setPartnerAddress(agreementDoc.getItemValueString("AG_PartnerAddress"));
        agreement.setPartnerZipCode(agreementDoc.getItemValueString("AG_PartnerZipCode"));
        agreement.setPartnerCoName(agreementDoc.getItemValueString("AG_PartnerConame"));
        agreement.setPartnerCity(agreementDoc.getItemValueString("AG_PartnerCity;"));
        agreement.setSpecificationHeadline(agreementDoc.getItemValueString("AG_SpecificationHeadline"));
        agreement.setSpecification(agreementDoc.getItemValueString("AG_Specification"));
        agreement.setSpecialConditions(agreementDoc.getItemValueString("AG_SpecialConditions"));
        agreement.setAgreementCondition(agreementDoc.getItemValueString("AG_AgreementCondition"));
        agreement.setCalcRowsSpecification(agreementDoc.getItemValueString("AG_CalcRowsSpecification"));
        agreement.setAppointSupplierEnabled(agreementDoc.getItemValueString("AG_AppointSupplierEnabled"));
        agreement.setAppointSupplierName(agreementDoc.getItemValueString("AG_AppointSupplierName"));
        logger.info("Esign enabled: " + agreementDoc.getItemValueString("AG_EsignEnabled"));
        agreement.setEsignEnabled(agreementDoc.getItemValueString("AG_EsignEnabled").equalsIgnoreCase("1"));

        agreement.setUNID(agreementDoc.getUniversalID());
        agreement.setStock(this.getStock());

        return agreement;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
    
}


