/*
 * DataException.java
 *
 * Created on den 13 januari 2005, 16:24
 */

package se.mpab.psi.db;

/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */
public class DataException extends Exception {
    
    private String message;
        
    /** Creates a new instance of DataException */
    public DataException(String message) {
        this.setMessage(message);
    }
    
    public String getMessage() {
        return message;
    }
    
    /**
     * Setter for property message.
     * @param message New value of property message.
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }
    
}
