// Copyright Marquardt & Partners
/*
 * DominoDataAccess.java
 *
 * Created on den 31 augusti 2004, 13:42
 */
package se.mpab.psi.db;

import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesFactory;
import de.bea.domingo.DSession;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DAgent;
import de.bea.domingo.DView;
import org.apache.log4j.LogManager;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import se.mpab.psi.PSIProperties;

/**
 *
 * @author  janne.berg
 */
public class DominoDataAccess {

    //private static DNotesFactory factory = null;
    private static DSession session = null;
    private boolean runFromAgent;
    private String stock;
    private static Map dataBaseConnections = new HashMap();
    private static org.apache.log4j.Logger logger = LogManager.getLogger(DominoDataAccess.class);    // DB NAMES
    public static final String DEALS_DATABASE = "deals";
    public static final String HP_DATABASE = "hirepurchase";
    public static final String MAIN_DATABASE = "main";
    public static final String PEOPLE_DATABASE = "people";
    public static final String PSI_DATABASE = "psi";
    public static final String PARAMETER_DATABASE = "parameters";
    public static final String LOG_DATABASE = "entre_log";
    //View names
    public static final String CALCULATION_VIEW = "vwCalculations";
    public static final String HPCALCULATION_VIEW = "vwHPCalculations";
    public static final String HPINSTALLMENTPLAN_VIEW = "vwHPInstallmentPlan";
    public static final String CREDIT_VIEW = "vwCredits";
    public static final String HPCREDIT_VIEW = "vwHPCredits";
    public static final String DEAL_VIEW = "vwDeals";
    public static final String GUARANTOR_VIEW = "vwGuarantors";
    public static final String PARAMETERS_BY_NAME_VIEW = "vwParametersByName";
    public static final String PRODUCTS_BY_AVAILABLE_FOR = "vwProductsByAvailableFor";
    public static final String AGREEMENT_VIEW = "vwAgreements";
    public static final String HPAGREEMENT_VIEW = "vwHPAgreements";
    /*THIS MUST BE REMOVED */
    //private static lotus.domino.Session notessession = null;
    public DominoDataAccess(String stock) {
        //blank constructor
        this.stock = stock.toLowerCase();
        runFromAgent = false;
    }
    //This constructor is for a specific and unique purpose: At one time, this
    //class could be instantiated by either a Servlet (thus no Domino session)
    //or by an Agent (thus an AgentContext and Session). Ignore this if you
    //don't need it.
    public DominoDataAccess(DSession ses) {
        session = ses;

        runFromAgent = true;
    }

    public boolean isRunFromAgent() {
        return runFromAgent;
    }

    /*==================================================================
    //Session management
     */
    public static DSession getSession() {
        if (session == null) {
            //lotus.domino.NotesThread.sinitThread();
           DNotesFactory factory = DNotesFactory.getInstance();
            session = factory.getSession();

        }
        return session;
    }

    /* This is a temporary method thats needed for the dxl importing*/
    /*   
    public static  lotus.domino.Session getNotesSession() throws DNotesException {
    logger.debug("getNotesSession");
    try {
    if ( notessession == null || !notessession.isValid()) {
    NotesThread.sinitThread();
    notessession = NotesFactory.createSession();
    logger.debug("Notessession created -" + notessession.getCommonUserName());
    } else{
    logger.debug("Notessession exists -" + notessession.getCommonUserName());
    }
    }/* catch( DNotesException ne) {
    logger.error(ne.getMessage(), ne);
    ne.printStackTrace(System.err);
    }
    return notessession;
    }
     */
    /* This is a temporary method thats needed for the dxl importing*/
    /*
    public void discardNotesSession() {
    try {
    if (notessession != null){
    notessession.recycle();
    }
    notessession = null;
    } catch (NotesException ne2) {
    logger.error(ne2.getMessage(),ne2);
    }
    NotesThread.stermThread();
    }
     */
    /*
    public void discardSession() {
    session = null;
    NotesThread.stermThread();
    }
     */
    public static DDatabase openDatabase(String dbname, String stock) throws DNotesException {
        DDatabase db = null;
        String path = "";

        String servername = getSession().getUserName();

        //Check the databaseconnection cache
        if (dataBaseConnections.containsKey(dbname + stock)) {
            db = (DDatabase) dataBaseConnections.get(dbname + stock);
            return db;
        }

        if (dbname.equals(DominoDataAccess.DEALS_DATABASE)) {
            path = "entre/" + stock + "/entre_deals";
        } else if (dbname.equals(DominoDataAccess.HP_DATABASE)) {
            path = "entre/" + stock + "/entre_hirepurchase";
        } else if (dbname.equals(DominoDataAccess.MAIN_DATABASE)) {
            path = "entre/" + stock + "/entre_main";
        } else if (dbname.equals(DominoDataAccess.PEOPLE_DATABASE)) {
            path = "entre/" + stock + "/entre_people";
        } else if (dbname.equals(DominoDataAccess.PSI_DATABASE)) {
            path = "entre/" + stock + "/psi";
        } else if (dbname.equals(DominoDataAccess.PARAMETER_DATABASE)) {
            path = "entre/" + stock + "/entre_params";
        } else if (dbname.equals(DominoDataAccess.LOG_DATABASE)) {
            path = PSIProperties.getProp().getProperty("logDb", "entre/common/entre_log");
        }
        db = getSession().getDatabase(servername, path);
        dataBaseConnections.put(dbname + stock, db);
        return db;
    }


    /*
    public static lotus.domino.Database getNotesDatabase( String database){
    lotus.domino.Database db = null;
    try {
    NotesThread.sinitThread();
    lotus.domino.Session notessession = NotesFactory.createSession();
    db = notessession.getDatabase(notessession.getServerName(),database);
    } catch( NotesException ne) {
    logger.error(ne.getMessage(), ne);
    //logger.error("getsession error: ",ne);
    }
    return db;
    }
    
    /*
    
    public DateTime getDateTime(java.util.Date  date){
    DateTime dateTime;
    try{
    dateTime =  getNotesSession().createDateTime(date);
    } catch (NotesException e){
    logger.error(e.getMessage(),e);
    dateTime = null;
    }
    //     discardNotesSession();
    return dateTime;
    }
    
     */
    /*
     *Internal method for running an agent
     */
    public static int runLSAgent(String stock,
            String database,
            String ag,
            String parameterDocUNID) {
        int agentStatus = -1;
        try {
            DDatabase db = openDatabase(database, stock);
            DAgent agent = db.getAgent(ag);
            DDocument doc = db.getDocumentByUNID(parameterDocUNID.trim());
            agentStatus = agent.runOnServer(doc.getNoteID());
            logger.info("Executing LS Agent, AgentName=[" + agent.getName() + "], Status=[" + agentStatus + "]");
        } catch (DNotesException e) {
            logger.error(e.getMessage(), e);
        }
        return agentStatus;
    }

    /*
     * Creates  a Calulation or Credit document with all requierd fields from a dxl file on disc
     * or from the database
     * @parameters
     **/
    public static DDocument getTemplateDocument(String stock, String id) {
        DDocument document = null;
        /* From database imports the template doc from database Obvious right ;) ?*/
        /*If the document should be created database the ID is a DocUNID */
        try {
            logger.debug("Fetching template from database");
            DDatabase dealsDb = openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
            document = dealsDb.createDocument();
            DDocument template = dealsDb.getDocumentByUNID(id);
            template.copyAllItems(document, true);
        } catch (DNotesException e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace(System.err);
        }
        return document;
    }

    /**
     * Getter for property stock.
     * @return Value of property stock.
     */
    public java.lang.String getStock() {
        return stock;
    }

    /**
     * Setter for property stock.
     * @param stock New value of property stock.
     */
    public void setStock(java.lang.String stock) {
        this.stock = stock;
    }
    
    public static DDocument getDocumentByKey(String stock, String database, String view, String key, boolean strictMatch)
    {
        DDocument doc = null;
        
        try
        {
            DDatabase db = openDatabase(database, stock);
            DView vw = db.getView(view);
            doc = vw.getDocumentByKey(key, strictMatch);
        } 
        catch (DNotesException e)
        {
            logger.error(e.getMessage(), e);
        }
        
        return doc;
    }
}

