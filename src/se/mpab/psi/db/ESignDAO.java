/*
 * ProductDAO.java
 *
 * Created on den 1 mars 2005, 11:14
 */
package se.mpab.psi.db;

import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import de.bea.domingo.DViewEntry;
import java.io.*;
import java.util.*;
import org.apache.log4j.LogManager;
import se.mpab.psi.*;
import se.mpab.psi.bo.*;
import se.mpab.psi.mo.AgreementApplication;




/**
 *
 * @author janne.berg
 */
public class ESignDAO {
    private DDatabase dealsDb;
    private DDocument doc ;
    private String stock;

    private static org.apache.log4j.Logger logger = LogManager.getLogger(ESignDAO.class);

    public ESignDAO(String stock) throws DNotesException{
        this.stock = stock;
        dealsDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
    }
    
    public void insert(PSIRequest psiRequest, Deal deal, int version, AgreementApplication agreementApplication) throws Exception {
        DDocument tempDoc = dealsDb.createDocument();
        tempDoc.replaceItemValue("DealNo", deal.getDealNo());
        tempDoc.replaceItemValue("Version", version);
        tempDoc.replaceItemValue("AgreementEmails", agreementApplication.getEsignContractEmail());
        if(deal.getAgreement().getEsignEnabled()) {
            tempDoc.replaceItemValue("EsignEnabled", ("1"));
        } else {
            tempDoc.replaceItemValue("EsignEnabled", ("0"));
        }
        tempDoc.replaceItemValue("DeleteFlag", "1");
        tempDoc.save(true, false);
        int status = DominoDataAccess.runLSAgent(this.stock, "deals", "agtCreateEsignOrderPSI", tempDoc.getUniversalID());
        if(status != 0)
        {
            throw new Exception("Unable to create E-Sign documents");
        }
    }
}


