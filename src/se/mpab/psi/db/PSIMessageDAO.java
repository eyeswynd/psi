/*
 * PSIMessageDAO.java
 *
 * Created on den 10 februari 2005, 15:27
 */
package se.mpab.psi.db;

/**
 *
 * @author Janne Berg
 */
import se.mpab.util.common.DateTimeHandler;
import se.mpab.util.common.PropertiesSetup;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import org.apache.log4j.*;
import se.mpab.psi.PSIProperties;
import se.mpab.psi.bo.PSIRequest;
import java.util.Properties;
import java.util.Calendar;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DRichTextItem;

public class PSIMessageDAO {

    private String incomingDir;
    private String outgoingDir;
    private DDocument currentDocument;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(PSIMessageDAO.class);
    /** Creates a new instance of PSIMessageDAO */
    private DominoDataAccess da;
    private DDatabase queue;

    public PSIMessageDAO() {
        try {
            Properties prop = PSIProperties.getProp();
            if (PropertiesSetup.propertyExists("incomingDir", prop)) {
                this.incomingDir = PropertiesSetup.getString("incomingDir", prop);
            //logger.debug("incoming dir: <" + this.incomingDir +">");
            } else {
                this.incomingDir = PSIProperties.getRootDir() + "messages/incoming";
            }
            if (PropertiesSetup.propertyExists("outgoingDir", prop)) {
                this.outgoingDir = PropertiesSetup.getString("outgoingDir", prop);
            //logger.debug("incoming dir: <" + this.incomingDir +">");
            } else {
                this.outgoingDir = PSIProperties.getRootDir() + "messages/outgoing";
            }
            da = new DominoDataAccess("common");
            //TABORTqueue = da.getPSIQueueDb();
            queue = DominoDataAccess.openDatabase(DominoDataAccess.PSI_DATABASE, "common");

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void saveRequestToFile(PSIRequest request) {
        DateTimeHandler dth = new DateTimeHandler();
        java.util.Calendar cal = new java.util.GregorianCalendar();

        String datedirPath = Integer.toString(cal.get(java.util.Calendar.YEAR)) + java.io.File.separatorChar +
                Integer.toString(cal.get(java.util.Calendar.MONTH) + 1) + java.io.File.separatorChar +
                Integer.toString(cal.get(java.util.Calendar.DATE));

        String fileName = request.getPsiUser().getUserName() + " " + dth.getTimeStamp(dth.YYYYMMDD) + "_" + dth.getTimeStamp(dth.HHMMSSCC) + ".xml";

        java.io.File incomingFileDir = new java.io.File(incomingDir + java.io.File.separatorChar +
                request.getCountry() + java.io.File.separatorChar +
                datedirPath);

        try {
            boolean exists = incomingFileDir.exists();
            if (!exists) {
                incomingFileDir.mkdirs();
            }
            BufferedWriter out = new BufferedWriter(new FileWriter(incomingFileDir.getAbsolutePath() + java.io.File.separatorChar + fileName));
            out.write(request.toPSIString());
            out.close();
        } catch (IOException e) {
            logger.warn(this.getClass().getName() + ", Error while saving request to file <" + incomingDir + fileName + ">");
            logger.error(e.getMessage(), e);
        }
    }
    //Use this to log a request
    public void logRequest(PSIRequest request) {
        try {

            DDocument doc = queue.createDocument();
            DRichTextItem rtRequestData = doc.createRichTextItem("PSIQuery_RequestData");
            doc.replaceItemValue("Form", "PSIQuery");
            doc.replaceItemValue("PSIQuery_RequestRecieved", doc.getCreated());
            doc.replaceItemValue("PSIQuery_RequestFrom", request.getPsiUser().getUserName());
            doc.replaceItemValue("PSIQuery_RequestHost", request.getPsiUser().getUserIp());
            rtRequestData.appendText(request.toString());
            doc.computeWithForm(false);
            doc.save(true);
            //saves this so we may append response data
            
            this.currentDocument = doc;
        } catch (Exception e) {

            logger.error(e.getMessage(), e);
        }

    }
    //this is used for updating a request log
    public void updateLogRequest(String responseXML, int time) {
        try {
            DRichTextItem rtResponseData = currentDocument.createRichTextItem("PSIQuery_ResponseData");
            currentDocument.replaceItemValue("PSIQuery_ResponseSent", Calendar.getInstance());
            rtResponseData.appendText(responseXML);
            currentDocument.replaceItemValue("PSIQuery_ElapsedTime", new Integer(time));
            currentDocument.save(true);
        } catch (Exception e) {
            logger.warn("Could not update request");
            logger.error(e.getMessage(), e);
        }

    }

    public void updateLogRequestWithDealNoAndMethod(double dealNo, String requestMethod) {
        try {
            currentDocument.replaceItemValue("PSIQuery_DealNo", new Double(dealNo));
            currentDocument.replaceItemValue("PSIQuery_RequestMethod", requestMethod);
            currentDocument.save(true);
        } catch (Exception e) {
            logger.warn("Could not save request");
            logger.error(e.getMessage(), e);
        }

    }

    public void updateLogRequestWithError(String errorMessage) {
        try {
            currentDocument.replaceItemValue("PSIQuery_Error", errorMessage);
            currentDocument.save(true);
        } catch (Exception e) {
            logger.warn("Could not save request");
            logger.error(e.getMessage(), e);
        }

    }
}