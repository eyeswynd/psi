// Copyright Marquardt & Partners
/*
 * EntreDXLExporter.java
 *
 * Created on den 2 september 2004, 13:57
 */

package se.mpab.psi.db;

import se.mpab.psi.*;

import org.apache.log4j.LogManager;
import java.io.*;
import de.bea.domingo.DSession;
import se.mpab.psi.bo.PSIRequest;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import de.bea.domingo.DViewEntry;
import de.bea.domingo.DAgent;
import de.bea.domingo.DDocumentCollection;
import de.bea.domingo.DDxlExporter;

 /**
 *
 * @author  janne.berg
 */

     public class EntreDXLExporter {
    
    
    
/*    private static org.apache.log4j.Logger logger = LogManager.getLogger(EntreDXLExporter.class);
    
        public  static void exportDeals(){
        try {
            DominoDataAccess dataAccess = new DominoDataAccess("common");
            DSession session = DominoDataAccess.getSession();
            
            FileOutputStream out ;
            PrintWriter pout;
            String country = "se";
            DDxlExporter exporter = session.createDxlExporter();
            logger.info("Trying to open DB");
            DDatabase db2 = dataAccess.openDatabase("Test1/DLL","entre/" + country + "/entre_deals" );
            if (db2 == null)
                logger.error(   "Database open failed");
            else {
                logger.debug("Title:\t\t" +  db2.getTitle());
                
                DView view = db2.getView("vwDeals");
                DViewEntryCollection vec = view.getAllEntries();
                //  ViewEntry entry =  vec.getNthEntry(vec.getCount() - 1);
                ViewEntry entry =  vec.getFirstEntry();
                while (entry != null ) {
                    //    log.info("\tPosition " + entry.getPosition('.') + "\t" +  Integer.toString(entry.getDocument().getItemValueInteger("DE_DealNo")));
                    //  log.info("\n------------------------------Deal---------------------------------\n" + exporter.exportDxl(entry.getDocument()) );
                    entry = vec.getNextEntry();
                    out = new FileOutputStream("log\\" + Integer.toString(entry.getDocument().getItemValueInteger("DE_DealNo")), true);
                    pout = new PrintWriter(out);
                    pout.println(exporter.exportDxl(entry.getDocument()) );
                    pout.flush();
                    pout.close();
                    out.close();
                }
                //E:\DominoData\domino\java
                
                
            }
            
        }
        
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
*/        
 /*   public  static void exportDeal(DDocument doc){
        try {
            DominoDataAccess dataAccess = new DominoDataAccess("common");
            DSession session = dataAccess.getSession();
            FileOutputStream out ;
            PrintWriter pout;
            DDxlExporter exporter = session.createDxlExporter();
            //    log.info("\tPosition " + entry.getPosition('.') + "\t" +  Integer.toString(entry.getDocument().getItemValueInteger("DE_DealNo")));
            //  log.info("\n------------------------------Deal---------------------------------\n" + exporter.exportDxl(entry.getDocument()) );
            out = new FileOutputStream("e:/PSI/" + doc.getItemValue("DocUNID"), true);
            pout = new PrintWriter(out);
            pout.println(exporter.exportDxl(doc) );
            pout.flush();
            pout.close();
            out.close();
            
        }
        
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
    
    public  static void importDeal(String fileName, String server, String dbName ){
        DominoDataAccess dataAccess;
        DSession session;
        DDatabase db;
        DxlImporter importer=null;
        Stream stream = null;
        try {
            dataAccess = new DominoDataAccess("common");
            session = DominoDataAccess.getSession();
            db = DominoDataAccess.openDatabase(server, dbName );
            importer = session.createxlImporter();
            importer.setDocumentImportOption(importer.DXLIMPORTOPTION_CREATE);
            
            // Get DXL file
            //   String filename = "d:\\Domino\\log\\import2.dxl";
            logger.debug("Filename to import " + fileName);
            stream = session.createStream();
            
            if (stream.open(fileName) & (stream.getBytes() >0)) {                
                
                importer.importDxl(stream, db);
                String noteID = importer.getFirstImportedNoteID();
                logger.debug("noteid " + noteID + " imported");
            }
            else {                
                logger.debug(fileName + " does not exist or is empty");
            }
            
        } catch(Exception e) {
            
           
            
           logger.error(e.getMessage(), e);           
            
        } finally {
            
            // Print importer log
            try { 
                
                
                logger.info(importer.getLog());
            } catch(Exception e) {
                 logger.error(e.getMessage(), e);
            }
        }
        
    }
    
*/    
    
}
