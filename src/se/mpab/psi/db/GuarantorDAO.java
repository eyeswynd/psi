/*
 * GuarantorDAO.java
 *
 * Created on den 13 januari 2005, 15:26
 */
package se.mpab.psi.db;

import se.mpab.psi.bo.GuarantorBO;
import se.mpab.psi.PSIProperties;
import org.apache.log4j.LogManager;
import java.util.Vector;
import java.util.Iterator;
import se.mpab.psi.mo.DealNumber;
import java.util.Properties;
import se.mpab.util.common.PropertiesSetup;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import de.bea.domingo.DViewEntry;
import se.mpab.psi.util.NumberUtils;

/**
 *
 * @author  janne.berg
 */
public class GuarantorDAO {

    private DDatabase dealsDb;
    private DDatabase mainDB;
    private DominoDataAccess dataAccess;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(GuarantorDAO.class);
    private String templateDirectory;
    private String stock;

    /** Creates a new instance of GuarantorDAO */
    public GuarantorDAO(String stock) {
        // Init databasenames from properties
        Properties prop = PSIProperties.getProp();
        dataAccess = new DominoDataAccess(stock);

        this.stock = stock;

        try {
            if (PropertiesSetup.propertyExists("TemplateDirectory", prop)) {
                templateDirectory = PropertiesSetup.getString("TemplateDirectory", prop);
            } else {
                logger.warn("Cant find document template directory using default");
                templateDirectory = "e:/PSI/templates";
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public Vector findByDealNo(DealNumber dealNumber) throws DNotesException {
        DView view;
        DDocument doc;
        Vector guarantors = new Vector();
        Iterator docCollection;
        String searchString = NumberUtils.getFractionFreeString(dealNumber.getId()) + "-" + NumberUtils.getFractionFreeString(dealNumber.getVersion());
        dealsDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
        view = dealsDb.getView(DominoDataAccess.GUARANTOR_VIEW);
        docCollection = view.getAllEntriesByKey(searchString, true);
        while (docCollection.hasNext()) {
            DViewEntry entry = (DViewEntry) docCollection.next();
            doc = entry.getDocument();
            guarantors.add(populate(doc));
        }
        return guarantors;

    }

    public GuarantorBO populate(DDocument doc) throws DNotesException {
        GuarantorBO guarantor = new GuarantorBO();
        guarantor.setFullName(doc.getItemValueString("GU_Name"));
        guarantor.setCity(doc.getItemValueString("GU_City"));
        guarantor.setPostal(doc.getItemValueString("GU_Address"));
        guarantor.setZip(doc.getItemValueString("GU_ZipCode"));
        guarantor.setGuaranteedAmount((long) doc.getItemValueDouble("GU_Limit").longValue());
        guarantor.setType(doc.getItemValueString("GU_Type"));
        guarantor.setPhoneNumber(doc.getItemValueString("GU_Phone"));
        guarantor.setIdentificationNumber(doc.getItemValueString("GU_OrgNo"));
        guarantor.setCountry(doc.getItemValueString("CountryCode"));
        guarantor.setSeat(doc.getItemValueString("CountryCode"));
        return guarantor;
    }
    /*   private String formatOrgNo(GuarantorBOcustomer){
    String localizedOrgNo = customer.getOrgNo();
    if (customer.getSeat().equals("SE")  ){
    if (localizedOrgNo.indexOf("-")< 0){
    StringBuffer buf = new StringBuffer(localizedOrgNo);
    buf = buf.insert(6,"-");
    localizedOrgNo = buf.toString();
    }
    }
    return localizedOrgNo;
    }*/
}
