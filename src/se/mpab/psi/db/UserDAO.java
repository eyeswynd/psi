// Copyright Marquardt & Partners
/*
 * UserDAO.java
 *
 * Created on den 3 september 2004, 16:21
 */
package se.mpab.psi.db;

import java.util.*;
import se.mpab.psi.bo.*;
import se.mpab.psi.*;
import org.apache.log4j.LogManager;
import java.util.Properties;
import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DSession;
import de.bea.domingo.DView;

/**
 *
 * @author  janne.berg
 */
public class UserDAO implements DAO {

    private static org.apache.log4j.Logger logger = LogManager.getLogger(UserDAO.class);
    private String stock;
    private DominoDataAccess dataAccess;

    /** Creates a new instance of UserDAO */
    public UserDAO(String stock) {
        // Init databasenames from properties
        this.stock = stock.toLowerCase();
        //Properties prop = PSIProperties.getProp();
        dataAccess = new DominoDataAccess(stock);
    }

    public String getAllianceUNID(String partnerUNID) {
        DDatabase db;
        DDocument doc = null;
        String allianceUNID = "";
        try {
            db = DominoDataAccess.openDatabase(DominoDataAccess.MAIN_DATABASE, stock);
            doc = db.getDocumentByUNID(partnerUNID);
            allianceUNID = doc.getItemValueString("PA_AllianceUNID");

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return allianceUNID;
    }

    public User findByName(String name) throws Exception {
        DDocument userDoc = getUserData(name);
        User user = null;
        if (userDoc != null) {
            // logAllFields(userDoc);
            user = populate(userDoc);
        } else {
            logger.info("User " + name + " not found");
        }
        return user;

    }

    public User findByExternalId(String id) throws Exception {
        DDocument userDoc = getUserData(id);
        User user = null;
        if (userDoc != null) {
            // logAllFields(userDoc);
            user = populate(userDoc);
        } else {
            logger.info("User " + id + " not found");
        }
        return user;

    }

    private DDocument getUserData(String id) throws Exception {

        logger.info("Id to search: " + id);
        DDatabase db;
        DDocument doc = null;
        String unid;
        DSession session = DominoDataAccess.getSession();
        List result = session.evaluate("@Trim(  @NameLookup ( [EXHAUSTIVE]; \"" + id + "\"; \"DocUNID\" )     )");
        logger.info("Found " + result.toString());
        db = DominoDataAccess.openDatabase(DominoDataAccess.PEOPLE_DATABASE, stock);
        unid = (String) result.get(0);
        if (!unid.equals("")) {
            doc = db.getDocumentByUNID(unid);
        }
        return doc;
    }

    public User getUserByUNID(String unid) throws Exception {
        DDatabase db;
        DDocument doc = null;
        User user = null;
        db = DominoDataAccess.openDatabase(DominoDataAccess.PEOPLE_DATABASE, stock);
        DView allView = db.getView("vwAllByUNIDExclusive");
        allView.refresh();
        doc = allView.getDocumentByKey(unid, true);
        if (doc != null) {
            //  logAllFields(doc);
            user = populate(doc);
        } else {
            logger.info("User with unid " + unid + " not found");
        }
        return user;
    }

    private User populate(DDocument userDoc) throws Exception {
        User user = new User();

        user.setName(userDoc.getItemValueString("FirstName") + " " + userDoc.getItemValueString("MiddleInitial") + " " + userDoc.getItemValueString("LastName"));
        user.setPartnerUNID(userDoc.getItemValueString("PE_PartnerUNID"));
        // TODO need to fix this . store alliance unid in user now for  convenience

        user.setAllianceUNID(this.getAllianceUNID(user.getPartnerUNID()));
        user.setUNID(userDoc.getItemValueString("DocUNID"));
        user.setRolesFromList(userDoc.getItemValue("PE_Role"));
        
        user.setPE_UserID(userDoc.getItemValueString("PE_UserID"));
        return user;
    }

    /*
    private void logAllFields(Document doc){
    try{
    java.util.Enumeration items = doc.getItems().elements();
    while (items.hasMoreElements()) {
    //   logger.info("******");
    Item item = (Item)items.nextElement();
    //  logger.info("------ [" +  item.getName() + "] -- [" +  item.getText() + "] -------");
    logger.debug("------ " +  item.getName() + " -- [" +  item.getText() + "] -------");
    }

    }
    catch(Exception e){
    logger.error(e.getMessage(),e);
    }
    }
     */
}
