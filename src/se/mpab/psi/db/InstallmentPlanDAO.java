// Copyright Applicon
/*
 * InstallmentPlanDAO.java
 *
 * Created on den 15 april 2013, 16:30
 */
package se.mpab.psi.db;

import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.LogManager;
import se.mpab.psi.bo.*;
import se.mpab.psi.exceptions.DataException;
import se.mpab.psi.mo.DealNumber;
import se.mpab.psi.mo.EntreMessage;

/**
 *
 * @author niklas.schold
 */
public class InstallmentPlanDAO implements DAO {
    
    private DDatabase contractDb;
    private DDatabase mainDB;
    private DDocument doc;
    private DominoDataAccess dataAccess;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(CalculationDAO.class);
    private String stock;
    
    /** Creates a new instance of InstallmentPlanDAO */
    public InstallmentPlanDAO(String stock) {
        
        this.stock = stock;
    }
    
    public InstallmentPlan insert(PSIRequest psiRequest, Deal deal, int version) throws DNotesException, se.mpab.psi.db.DataException {
        EntreMessage message = psiRequest.getPsiMessage();
        DDocument doc;
        
        mainDB = DominoDataAccess.openDatabase(DominoDataAccess.MAIN_DATABASE, stock);
        contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
        
        DDocument partner = mainDB.getDocumentByUNID(deal.getSalesPerson().getPartnerUNID());
        DDocument alliance = mainDB.getDocumentByUNID(deal.getSalesPerson().getAllianceUNID());
        DDocument customerOffer = mainDB.getDocumentByUNID(message.getBody().getCreditApplication().getOfferId());
                
        doc = contractDb.createDocument();
        doc.replaceItemValue("DocType", "InstallmentPlan");
        doc.replaceItemValue("Form", "frmInstallmentPlan");
        doc.replaceItemValue("CountryCode", psiRequest.getCountry());

        //this needs to be done to get the correct fieldtypes
        doc.computeWithForm();
        
        Vector systemEditors = new Vector();
        systemEditors.add("[SysAdm]");
        systemEditors.add("[SuperUser]");
        doc.replaceItemValue("SystemEditors", systemEditors);

        Vector systemReaders = new Vector();
        systemReaders.add("[SysAdm]");
        systemReaders.add("[ReadAll]");
        doc.replaceItemValue("SystemReaders", systemReaders);

        Vector editors = new Vector();
        editors.addElement(partner.getItemValueString("PA_ExternalManagerGroup"));
        editors.addElement(partner.getItemValueString("PA_InternalSupportGroup"));
        editors.addElement(alliance.getItemValueString("AL_ExternalAllianceManagerGroup"));
        editors.addElement("[SupportAll]");
        editors.addElement(deal.getSalesPerson().getName());
        editors.trimToSize();

        doc.replaceItemValue("Editors", editors);
        doc.replaceItemValue("Readers", editors);
        
        doc.replaceItemValue("IP_DealNo", new Double(deal.getDealNo()));
        doc.replaceItemValue("IP_Version", new Integer(version));
        doc.replaceItemValue("IP_SeasonalPaymentEnabled", customerOffer.getItemValueString("CO_SeasonalPaymentEnabled"));
        
        doc.replaceItemValue("Duration", new Integer(0));       //This will be set in agtSaveCreditPSI_HP
        doc.replaceItemValue("FinancedAmount", new Double(0));  //This will be set in agtSaveCreditPSI_HP
        doc.replaceItemValue("InstallmentMethod", "");          //This will be set in agtSaveCreditPSI_HP
        
        // Verify that request payment plan consists of correct number of payments:
        int numOfPayments = deal.getCalculation().getDuration() / deal.getCalculation().getPeriodLength();
        if (message.getBody().getCreditApplication().getInstallmentPlan().getInstallmentPayments().size() != message.getBody().getCreditApplication().getInstallmentPlan().getInterestPayments().size()) {
            // Request do not have the same num of payments installment plan -abort!
            logger.info("Installment plan is not correct, diffrent number of payments (interest- and installment payments)");
            throw new se.mpab.psi.db.DataException("Installment plan is not correct, diffrent number of payments (interest- and installment payments)");
        }
        if (message.getBody().getCreditApplication().getInstallmentPlan().getInstallmentPayments().size() != numOfPayments) {
            logger.info("Installment plan is not correct, num of payments from request (" + message.getBody().getCreditApplication().getInstallmentPlan().getInstallmentPayments().size() + ") are not expected (duration/period length: " + numOfPayments + ")");
            throw new se.mpab.psi.db.DataException("Installment plan is not correct, num of payments from request (" + message.getBody().getCreditApplication().getInstallmentPlan().getInstallmentPayments().size() + ") are not expected (duration/period length: " + numOfPayments + ")");
        }
        doc.replaceItemValue("InstallmentPayments", message.getBody().getCreditApplication().getInstallmentPlan().getInstallmentPayments());
        doc.replaceItemValue("InterestPayments", message.getBody().getCreditApplication().getInstallmentPlan().getInterestPayments());
        
        // Check if short termed financing of VAT (moms) is used:
        if (message.getBody().getCreditApplication().getVAT() != null) {
            if (message.getBody().getCreditApplication().getVAT().getInterestRate() != null) {
                // VAT - VAT field on installment plan is copied from CA_VAT, which is calculated in Entr� (25% of HW + HWM sums)
                doc.replaceItemValue("VATInterestRate", message.getBody().getCreditApplication().getVAT().getInterestRate());
            }
        }
        
        doc.save(true);
        doc.replaceItemValue("DeleteFlag", "0");
        doc.replaceItemValue("DocUNID", doc.getUniversalID());
        doc.replaceItemValue("Created", doc.getCreated());
        doc.replaceItemValue("CreatedBy", "PSI");
        doc.replaceItemValue("Modified", "");
        doc.save(true);
        
        return populate(doc);
    }
    
    public InstallmentPlan findByHpUNID(String UNID) throws DNotesException {
        DDocument installmentPlanDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            installmentPlanDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("InstallmentPlanDAO.findByHpUNID, UNID=[" + UNID + "], getNoteID()=[" + installmentPlanDoc.getNoteID() + "]");

        } catch (Exception e) {
            logger.warn("Error in InstallmentPlanDAO findByHpUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return populate(installmentPlanDoc);
    }
    
    public InstallmentPlan findByDealNoAndLatestVersion(DealNumber dealNumber) throws DNotesException, se.mpab.psi.db.DataException {
        Iterator docCollection;
        DDocument installmentPlanDoc = null;
        DView installmentPlanView;
        String searchString = Long.toString(dealNumber.getId());
        
        installmentPlanDoc = doc;
        int currVersion = 0;
        boolean found = false;
        
        // Look for versions in hirepurchase Db:
        contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
        installmentPlanView = contractDb.getView(DominoDataAccess.HPINSTALLMENTPLAN_VIEW);
        
        docCollection = installmentPlanView.getAllDocumentsByKey(searchString);
        if (docCollection.hasNext()) { found = true; }

        while (docCollection.hasNext()) {
            doc = (DDocument) docCollection.next();
            if (doc.getItemValueInteger("IP_Version").intValue() > currVersion) {
                installmentPlanDoc = doc;
                currVersion = installmentPlanDoc.getItemValueInteger("IP_Version").intValue();
            }
        }
        
        if (!found) {
            return null;
        }

        if (dealNumber.getVersion() == 0) {
            dealNumber.setVersion(installmentPlanDoc.getItemValueInteger("IP_Version").intValue());
        }

        if (dealNumber.getVersion() != installmentPlanDoc.getItemValueInteger("IP_Version").intValue()) {
            logger.info("[" + installmentPlanDoc.getItemValueInteger("IP_DealNo") + "] Version number [" + dealNumber.getVersion() + "] is not valid Current version is [" + installmentPlanDoc.getItemValueInteger("IP_Version") + "]");
            throw new se.mpab.psi.db.DataException("[" + installmentPlanDoc.getItemValueInteger("IP_DealNo") + "] Version number [" + dealNumber.getVersion() + "] is not valid Current version is [" + installmentPlanDoc.getItemValueInteger("IP_Version") + "]");

        }
        return populate(installmentPlanDoc);
    }
    
    public InstallmentPlan newVersion(PSIRequest psiRequest, Deal deal) throws DNotesException, DataException, se.mpab.psi.db.DataException {
        EntreMessage message = psiRequest.getPsiMessage();
        contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
        DDocument oldInstPlan = contractDb.getDocumentByUNID(deal.getInstallmentPlan().getUNID());
        
        DDocument newDoc = contractDb.createDocument();
        oldInstPlan.copyAllItems(newDoc, true);
        
        newDoc.replaceItemValue("IP_Version", new Double(oldInstPlan.getItemValueDouble("IP_Version").intValue() + 1));
        newDoc.replaceItemValue("CountryCode", psiRequest.getCountry());
        newDoc.replaceItemValue("DeleteFlag", "0");
        newDoc.replaceItemValue("DocUNID", newDoc.getUniversalID());
        newDoc.replaceItemValue("Created", newDoc.getCreated());
        newDoc.replaceItemValue("CreatedBy", "PSI");
        
        // NOTE!! 
        // PSI Hirepurcahse was developed for CBF/FOS - they will not make any changes to the installmentplan between
        // versions according to Morten Jensen - therefore we do not handle change of installment plan payments/interests
        // nor VAT. - The 'newDoc' is almost a clean copy of the previous version ('oldInstPlan').
        
        newDoc.save(true);
       
        return populate(newDoc);
    }
    
    public InstallmentPlan populate(DDocument installmentPlanDoc) {
        InstallmentPlan installmentPlan = new InstallmentPlan();
        
        Vector instPayments = new Vector(installmentPlanDoc.getItemValue("InstallmentPayments"));
        installmentPlan.setInstallmentPayments(instPayments);
        Vector interPayments = new Vector(installmentPlanDoc.getItemValue("InterestPayments"));
        installmentPlan.setInstallmentPayments(interPayments);
        
        installmentPlan.setUNID(installmentPlanDoc.getUniversalID());
        installmentPlan.setStock(this.getStock());

        return installmentPlan;
    }
    
    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}
