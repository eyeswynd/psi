// Copyright Marquardt & Partners
/*
 * CreditDAO.java
 *
 * Created on den 7 september 2004, 09:44
 */
package se.mpab.psi.db;

import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import de.bea.domingo.DView;
import de.bea.domingo.DViewEntry;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesFactory;
import lotus.domino.Session;
import lotus.domino.View;
import org.apache.log4j.LogManager;
import se.mpab.psi.*;
import se.mpab.psi.bo.*;
import se.mpab.psi.bo.PSIRequest;
import se.mpab.psi.exceptions.DataException;
import se.mpab.psi.mo.*;
import se.mpab.psi.util.PSIUtils;
import se.mpab.util.common.PropertiesSetup;

/**
 *
 * @author  janne.berg
 */
public class CreditDAO implements DAO {

    private DDatabase contractDb;
    private DDatabase mainDB;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(CreditDAO.class);
    private String stock = "";

    /** Creates a new instance of CreditDAO */
    public CreditDAO(String stock) {

        this.stock = stock;

    }

    public Object insert(PSIRequest psiRequest, Deal deal, int version) throws DNotesException, DataException {
        EntreMessage message = psiRequest.getPsiMessage();
        DDocument doc;
        
        // This variable is used for getting field which are either leasing or hire purchase specific:
        Boolean dealIsLeasing;
        dealIsLeasing = true;
        
        if (message.getHeader().getService().equals("makeCreditApplication") | message.getHeader().getService().equals("changeCreditApplication")) {
            logger.info("in CreditDAO, method insert service is makeCreditApplication");
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
        } else if (message.getHeader().getService().equals("makeCreditApplicationHP") | message.getHeader().getService().equals("changeCreditApplicationHP")) {
            logger.info("in CreditDAO, method insert service is makeCreditApplicationHP");
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
            dealIsLeasing = false;
        }
        Credit credit = new Credit();

    //    logger.info("Servername " + DominoDataAccess.getSession().getServerName());

        mainDB = DominoDataAccess.openDatabase(DominoDataAccess.MAIN_DATABASE, stock);

        DDocument partner = mainDB.getDocumentByUNID(deal.getSalesPerson().getPartnerUNID());
        DDocument alliance = mainDB.getDocumentByUNID(deal.getSalesPerson().getAllianceUNID());

        //We do not want to use the template, create document from scratch
        doc = contractDb.createDocument();

        doc.replaceItemValue("CountryCode", psiRequest.getCountry());
        doc.replaceItemValue("DocType", "Credit");
        if (dealIsLeasing) {
            doc.replaceItemValue("Form", "frmCredit");
            doc.replaceItemValue("CR_CashRegisterSpecialField", "");
            doc.replaceItemValue("CR_TransferalCreditCondition", "");
        } else { 
            doc.replaceItemValue("Form", "frmCreditHirePurchase"); 
        }
        
        try {
            doc.computeWithForm();
           
        } catch (Exception e) {
            logger.warn("Error in Insert function CreditDAO :", e);
            logger.error(e.getMessage(), e);
        }

        //TODO revise editors and readers
        Vector systemEditors = new Vector();
        systemEditors.add("[SysAdm]");
        systemEditors.add("[SuperUser]");
        doc.replaceItemValue("SystemEditors", systemEditors);

        Vector systemReaders = new Vector();
        systemReaders.add("[SysAdm]");
        systemReaders.add("[ReadAll]");
        doc.replaceItemValue("SystemReaders", systemReaders);

        Vector editors = new Vector();
        editors.addElement(partner.getItemValueString("PA_ExternalManagerGroup"));
        editors.addElement(partner.getItemValueString("PA_InternalSupportGroup"));
        editors.addElement(alliance.getItemValueString("AL_ExternalAllianceManagerGroup"));
        editors.addElement("[SupportAll]");
        editors.addElement(deal.getSalesPerson().getName());
        editors.trimToSize();

        doc.replaceItemValue("Editors", editors);
        doc.replaceItemValue("Readers", editors);

        doc.replaceItemValue("CR_Comments", message.getBody().getCreditApplication().getMessageToCreditApprover());
        doc.replaceItemValue("CR_DealNo", new Long(deal.getDealNo()).longValue());
        doc.replaceItemValue("CR_Version", new Integer(version));

        doc.replaceItemValue("CR_CreditTemplate", partner.getItemValue("PA_CreditTemplate"));
        doc.replaceItemValue("CR_CreditQueue", partner.getItemValue("PA_CreditQueue"));

        doc.replaceItemValue("CR_Currency", this.getCurrency(message.getBody().getCreditApplication().getEquipment()));
        doc.replaceItemValue("CR_CustCurrency", this.getCurrency(message.getBody().getCreditApplication().getEquipment()));

        doc.replaceItemValue("CR_CustOrgNo", formatOrgNo(deal.getCustomer()));
        doc.replaceItemValue("CR_CustName", deal.getCustomer().getName());

        doc.replaceItemValue("CR_CorrectionCustOrgNo", formatOrgNo(deal.getCustomer()));
        doc.replaceItemValue("CR_CorrectionCustName", deal.getCustomer().getName());

        if (deal.getCustomer().isVerified()) {
            doc.replaceItemValue("CR_CustVerified", new Integer(1));
        } else {
            doc.replaceItemValue("CR_CustVerified", new Integer(2));
        }

        doc.replaceItemValue("CR_CustType", deal.getCustomer().getType());
        doc.replaceItemValue("CR_Duration", getDurationInMonths(message.getBody().getCreditApplication().getDuration()));

        doc.replaceItemValue("CR_FinancedAmount", new Double(0)); //This will be set in agtSaveCreditPSI
        if (dealIsLeasing) {
            doc.replaceItemValue("CR_CustomerFinancedAmount", new Double(0)); //This will be set in agtSaveCreditPSI
        }
      
        /* BEGIN New fields since not using template anymore. */
        doc.replaceItemValue("CR_GroupExposureFINAL", new Double(0));
        doc.replaceItemValue("CR_GroupExposureEntre", new Double(0));
        doc.replaceItemValue("CR_GroupExposureTotal", new Double(0));

        /* END New fields since not using template anymore. */
        
        /* BEGIN Setting of fields, update 2013-04 CBF development Niklas */
        doc.replaceItemValue("CR_AppointedSupplierVerified", 0);
        doc.replaceItemValue("CR_PartnerPriority", partner.getItemValueDouble("PA_Priority"));
        
        /* END Setting of fields update 2013-04 */

        doc.save(true);
        doc.replaceItemValue("DeleteFlag", "0");
        doc.replaceItemValue("DocUNID", doc.getUniversalID());
        doc.replaceItemValue("Created", doc.getCreated());
        doc.replaceItemValue("CreatedBy", "PSI");
        doc.replaceItemValue("Modified", "");
        doc.save(true);
        credit.setStock(this.getStock());
        credit.setUNID(doc.getUniversalID());


        if (version > 1) {
            if (dealIsLeasing) {
                DominoDataAccess.runLSAgent(stock, "deals", "agtSaveCreditPSI", doc.getUniversalID());
            } else {
                DominoDataAccess.runLSAgent(stock, "hirepurchase", "agtSaveCreditPSI_HP", doc.getUniversalID());
            }
            
        }


        return populate(doc);
    }

    public Object newVersion(PSIRequest psiRequest, Deal deal, String dealType) throws DNotesException, DataException {
         if (dealType.equalsIgnoreCase("LEASING")) {
             contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
         } else if (dealType.equalsIgnoreCase("HIREPURCHASE")) {
             contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
         }
        DDocument oldCredit = contractDb.getDocumentByUNID(deal.getCredit().getUNID());
        
        return this.insert(psiRequest, deal, oldCredit.getItemValueInteger("CR_Version").intValue() + 1);

    }

    public Credit findByUNID(String UNID) throws DNotesException {
        DDocument creditDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            creditDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("CreditDAO.findByUNID, UNID=[" + UNID + "], getNoteID()=[" + creditDoc.getNoteID() + "]");
            
        } catch (Exception e) {
            logger.warn("Error in CreditDAO findByUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return populate(creditDoc);
    }
    
    public Credit findByHpUNID(String UNID) throws DNotesException {
        DDocument creditDoc = null;
        try {
            contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
            DView allView = contractDb.getView("vwAllByUNIDExclusive");
            allView.refresh();
            creditDoc = allView.getDocumentByKey(UNID, true);
            logger.debug("CreditDAO.findByHpUNID, UNID=[" + UNID + "], getNoteID()=[" + creditDoc.getNoteID() + "]");
            
        } catch (Exception e) {
            logger.warn("Error in CreditDAO findByHpUNID :", e);
            logger.error(e.getMessage(), e);
        }
        return populate(creditDoc);
    }

    public Credit findByDealNoAndLatestVersion(DealNumber dealNumber) throws DNotesException, DataException {
        Iterator docCollection;
        DDocument creditDoc = null;
        DDocument doc;
        DView creditView;
        String searchString = Long.toString(dealNumber.getId());
        int currVersion = 0;
        
        logger.info("CreditDAO, method findByDealNoAndLatestVersion, deal no: " + dealNumber.getId());
        
        // Look for versions in both dealsDb and hirepurchaseDb:
        for (int i=0; i<2; i++) {
            if (i == 0) {
                contractDb = DominoDataAccess.openDatabase(DominoDataAccess.DEALS_DATABASE, stock);
                creditView = contractDb.getView(DominoDataAccess.CREDIT_VIEW);
            } else {
                creditView = null;
                docCollection = null;
                contractDb = DominoDataAccess.openDatabase(DominoDataAccess.HP_DATABASE, stock);
                creditView = contractDb.getView(DominoDataAccess.HPCREDIT_VIEW);
            }
            docCollection = creditView.getAllDocumentsByKey(searchString);
            
            while (docCollection.hasNext()) {
                doc = (DDocument) docCollection.next();
                logger.info("now handling deal version: " + doc.getItemValueInteger("CR_Version").intValue());
                if (doc.getItemValueInteger("CR_Version").intValue() > currVersion) {
                    creditDoc = doc;
                    currVersion = creditDoc.getItemValueInteger("CR_Version").intValue();
                }
            }
        }
        
        if (dealNumber.getVersion() != creditDoc.getItemValueInteger("CR_Version").intValue()) {
            logger.info("[" + dealNumber.getId() + "] Version number [" + dealNumber.getVersion() + "] is not valid Current version is [" + creditDoc.getItemValueInteger("CR_Version") + "]");
            throw new DataException("[" + dealNumber.getId() + "] Version number [" + dealNumber.getVersion() + "] is not valid Current version is [" + creditDoc.getItemValueInteger("CR_Version") + "]");

        }
        return populate(creditDoc);
    }

    public Credit populate(DDocument creditDoc) {

        Credit credit = new Credit();
        credit.setAutoDecision(creditDoc.getItemValueString("CR_AutoDecision"));
        credit.setAutoDecisionSource(creditDoc.getItemValueString("CR_AutoDecisionSource"));
        Calendar cal = creditDoc.getItemValueDate("CR_AutoDecisionTime");
        if (cal != null) {
            credit.setAutoDecisionTime(cal.getTime());
        }

        credit.setAutoReason(creditDoc.getItemValueString("CR_AutoReason"));
        credit.setAutoRiskPrognosisCode(creditDoc.getItemValueString("CR_AutoRiskPrognosisCode"));
        credit.setComments(creditDoc.getItemValueString("CR_Comments"));
        logger.info("CREDIT CONDITION: " + creditDoc.getItemValueString("CR_ConditionCode"));
        credit.setConditionCode(creditDoc.getItemValueString("CR_ConditionCode"));
        credit.setConditionDescription(getConditionDescriptionFromProperties(credit.getConditionCode()));
        if (creditDoc.getItemValueDouble("CR_AddToFirstPayment") != null) {
            credit.setAddToFirstPayment((long) creditDoc.getItemValueDouble("CR_AddToFirstPayment").doubleValue());
        }
        credit.setManualDecision(creditDoc.getItemValueString("CR_ManualDecision"));
        cal = null;

        if (creditDoc.hasItem("CR_ManualDecisionTime")) {
            cal = creditDoc.getItemValueDate("CR_ManualDecisionTime");
            if (cal != null) {
                credit.setManualDecisionTime(cal.getTime());
            }
        }

        credit.setManualReason(PSIUtils.getStringFromMultivalue(creditDoc.getItemValue("CR_ManualReason")));
        if (creditDoc.getItemValueDouble("CR_OrderFee") != null) {
            credit.setAddToFirstPayment((long) creditDoc.getItemValueDouble("CR_OrderFee").doubleValue());
        }
        credit.setUNID(creditDoc.getUniversalID());
        credit.setStock(this.getStock());

        int validDaysInt = 0;
        String validDaysString = creditDoc.getItemValueString("CR_CreditValidDays");
        if (validDaysString != null) {
            if (!validDaysString.equals("")) {
                validDaysInt = Integer.parseInt(validDaysString);
            }
        }

        credit.setValidDays(validDaysInt);
        return credit;
    }

    private String formatOrgNo(Customer customer) {
        String localizedOrgNo = customer.getOrgNo();
        if (customer.getSeat().equals("SE")) {
            if (localizedOrgNo.indexOf("-") < 0) {
                StringBuffer buf = new StringBuffer(localizedOrgNo);
                buf = buf.insert(6, "-");
                localizedOrgNo = buf.toString();
            }
        }
        return localizedOrgNo;
    }

    private String getConditionDescriptionFromProperties(String conditionCode) {
        Properties prop = PSIProperties.getProp();
        String conditionDescription = "";
        String conditionKey = stock + "." + conditionCode;
        logger.debug("Trying to find condition :" + conditionKey);
        try {
            if (PropertiesSetup.propertyExists(conditionKey, prop)) {
                conditionDescription = PropertiesSetup.getString(conditionKey, prop);
            } else {
                logger.info("Cant find condition description using default");
                conditionDescription = "Description is missing";
            }
        } catch (Exception e) {

            logger.warn("Error in CreditDAO getConditionDescriptionFromProperties :", e);
            logger.error(e.getMessage(), e);
        }

        return conditionDescription;
    }

    private Integer getDurationInMonths(Duration duration) throws DNotesException {
        Integer i;
        if (duration.getUnit().equalsIgnoreCase("MONTH")) {
            i = new Integer(1 * duration.getValue());
        } else if (duration.getUnit().equalsIgnoreCase("QUARTER")) {
            i = new Integer(3 * duration.getValue());
        } else {
            //TODO error handling
            throw new DNotesException("Error ,illegal duration type");
        //i = new Integer(3*duration.getValue());
        }
        return i;
    }

    private String getCurrency(Vector equipment) {
        String currency = "";
        for (Iterator it = equipment.iterator(); it.hasNext();) {
            Equipment eqRow = (Equipment) it.next();
            //TODO must check i there is different currencies on different rows
            currency = eqRow.getAmount().getCurrency();
        }

        return currency;
    }

    private String getStringFromMultivalue(Vector textField) {
        String text = "";
        for (Iterator it = textField.iterator(); it.hasNext();) {
            String row = (String) it.next();
            text += row + "\n";
        }
        return text;

    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}
