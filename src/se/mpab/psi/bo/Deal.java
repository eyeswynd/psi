// Copyright Marquardt & Partners
/*
 * Deal.java
 *sdafsdaf
 * Created on den 3 september 2004, 13:20
 */
package se.mpab.psi.bo;

import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import se.mpab.psi.PSIProperties;
import se.mpab.psi.Status;
import se.mpab.psi.db.AgreementDAO;
import se.mpab.psi.db.CalculationDAO;
import se.mpab.psi.db.CreditDAO;
import se.mpab.psi.db.DominoDataAccess;
import se.mpab.psi.db.ESignDAO;
import se.mpab.psi.db.InstallmentPlanDAO;
import se.mpab.psi.exceptions.AccessException;
import se.mpab.psi.exceptions.DataException;
import se.mpab.psi.mo.*;
import se.mpab.util.common.DateTimeHandler;

/**
 *
 * @author janne.berg
 */
/*
 * Deal.java
 *
 * Created on den 27 juli 2004, 15:20
 */
/**
 *
 * @author janne.berg
 */
public class Deal {

    private long dealNo;
    private String customerOfferUNID;
    private String psiUserID;
    private String status;
    private String id;
    private String History;
    private String Comments;
    private String InternalComments;
    private String type;
    private Customer customer;
    private User salesPerson;
    private Calculation calculation;
    private InstallmentPlan installmentPlan;
    private Credit credit;
    private Agreement agreement;
    private String UNID;
    private String stock;
    private String allianceUNID;

    /**
     * Get the value of allianceUNID
     *
     * @return the value of allianceUNID
     */
    public String getAllianceUNID() {
        return allianceUNID;
    }

    /**
     * Set the value of allianceUNID
     *
     * @param allianceUNID new value of allianceUNID
     */
    public void setAllianceUNID(String allianceUNID) {
        this.allianceUNID = allianceUNID;
    }
    private static Logger logger = LogManager.getLogger(Deal.class);

    public CreditApplication checkCreditApplication() throws Exception {
        CreditApplication cr = getCreditApplicationMessageObj();
        return cr;
    }

    public AgreementApplication checkAgreementApplication() throws Exception {
        //AgreementApplication cr = getCreditApplicationMessageObj();
        AgreementApplication aa = getAgreementApplicationMessageObj();
        return aa;
    }

    public CreditApplication makeCreditApplication() throws Exception {
        CreditDAO creditDAO = new CreditDAO(this.getStock());
        CalculationDAO calculationDAO = new CalculationDAO(this.getStock());
        DDocument calcDoc;
        String errorFieldText;

        DominoDataAccess.runLSAgent(stock, "deals", "agtSaveCreditPSI", this.getCredit().getUNID());

        calcDoc = calculationDAO.findDocumentByUNID(this.calculation.getUNID());
        if (calcDoc != null) {
            // If the calcDoc has the filed ErrormessagePSI - something went wrong - get the error message from this field
            // and return this message to the PSI user:
            if (calcDoc.hasItem("ErrormessagePSI")) {
                errorFieldText = calcDoc.getItemValueString("ErrormessagePSI");
                logger.info("ErrormessagePSI: " + errorFieldText);
                throw new se.mpab.psi.db.DataException(errorFieldText);
            }
        }

        this.setCalculation(calculationDAO.findByUNID(this.getCalculation().getUNID()));
        this.setCredit(creditDAO.findByUNID(this.getCredit().getUNID()));

        CreditApplication creditApplication = getCreditApplicationMessageObj();
        return creditApplication;
    }

    public CreditApplication makeCreditApplicationHP() throws Exception {
        CreditDAO creditDAO = new CreditDAO(this.getStock());
        CalculationDAO calculationDAO = new CalculationDAO(this.getStock());
        DDocument calcDoc;
        String errorFieldText;

        DominoDataAccess.runLSAgent(stock, DominoDataAccess.HP_DATABASE, "agtSaveCreditPSI_HP", this.getCredit().getUNID());

        calcDoc = calculationDAO.findDocumentByHpUNID(this.calculation.getUNID());
        if (calcDoc != null) {
            // If the calcDoc has the filed ErrormessagePSI - something went wrong - get the error message from this field
            // and return this message to the PSI user:
            if (calcDoc.hasItem("ErrormessagePSI")) {
                errorFieldText = calcDoc.getItemValueString("ErrormessagePSI");
                logger.info("ErrormessagePSI: " + errorFieldText);
                throw new se.mpab.psi.db.DataException(errorFieldText);
            }
        }

        this.setCalculation(calculationDAO.findByHpUNID(this.getCalculation().getUNID()));
        this.setCredit(creditDAO.findByHpUNID(this.getCredit().getUNID()));

        CreditApplication creditApplication = getCreditApplicationMessageObj();
        return creditApplication;
    }

    public CreditApplication checkStatus(PSIRequest request) throws Exception {

        if (!this.getPsiUserID().equals(request.getPsiUser().getUserName())) {
            throw new AccessException(Status.STATUS_ACCESS_DENIED);
        }

        DealStatus dealStatus = new DealStatus();
        CreditApplication cr = getCreditApplicationMessageObj();
        dealStatus.setDealStatus(this.getStatus());
        dealStatus.setCalculationStatus(this.getCalculation().getStatus());
        cr.setStatus(dealStatus);
        return cr;
    }

    public CreditApplication makeAgreementApplication(PSIRequest request, AgreementApplication agreementApplication) throws Exception {

        if (!this.getPsiUserID().equals(request.getPsiUser().getUserName())) {
            throw new AccessException(Status.STATUS_ACCESS_DENIED);
        }

        if (this.getAgreement() != null) {
            throw new DataException("[Deal [" + this.getDealNo() + "] already have agreement]");
        }

        if (!(this.getCalculation().getStatus().equals("CREDITAPPROVED")
                || this.getCalculation().getStatus().equals("TENDER"))) {
            throw new DataException("[Deal [" + this.getDealNo() + "], Agreement can not be created before credit is approved]");
        }

        /*
         * BEGIN
         If we receive "new" CreditApplication information in the request, we first make a change...
         * 
         * 
         */
        if (request.getPsiMessage().getBody().getCreditApplication() != null) {
            this.changeCreditApplication(request);

            if (!(this.getCalculation().getStatus().equals("CREDITAPPROVED")
                    || this.getCalculation().getStatus().equals("TENDER"))) {
                throw new DataException("[Deal [" + this.getDealNo() + "], Agreement can not be created before credit is approved]");
            }
        }

        /*
        
         * END
         * 
         */
        AgreementDAO agreementDAO = new AgreementDAO(this.getStock());
        this.setAgreement((Agreement) agreementDAO.insert(request, this, this.getCalculation().getVersion()));
        DominoDataAccess.runLSAgent(stock, "deals", "agtCreatePDFPSI", this.getAgreement().getUNID());
        this.setAgreement(agreementDAO.findByUNID(this.getAgreement().getUNID()));
        
        if(agreementApplication.getEsignContractEmail().size() > 0) {
            ESignDAO esignDAO = new ESignDAO(this.stock);
            esignDAO.insert(request, this, this.getCalculation().getVersion(), agreementApplication);
        }

        this.setAgreement(agreementDAO.findByUNID(this.getAgreement().getUNID()));
        CreditApplication creditApplication = getCreditApplicationMessageObj();

        CalculationDAO calculationDAO = new CalculationDAO(this.getStock());
        this.setCalculation(calculationDAO.findByUNID(this.getCalculation().getUNID()));
        DealStatus dealStatus = new DealStatus();
        dealStatus.setDealStatus(this.getStatus());
        dealStatus.setCalculationStatus(this.getCalculation().getStatus());

        DealNumber dealNumber = new DealNumber();
        dealNumber.setPrefix(this.getCustomer().getSeat().toUpperCase());
        dealNumber.setId(this.getDealNo());
        dealNumber.setVersion(this.getCalculation().getVersion());

        creditApplication.setStatus(dealStatus);
        agreementApplication.setStatus(dealStatus);
        agreementApplication.setDealNumber(dealNumber);

        return creditApplication;
    }

    public CreditApplication makeAgreementApplicationHP(PSIRequest request, AgreementApplication agreementApplication) throws Exception {
        if (!this.getPsiUserID().equals(request.getPsiUser().getUserName())) {
            throw new AccessException(Status.STATUS_ACCESS_DENIED);
        }

        if (this.getAgreement() != null) {
            throw new DataException("[Deal [" + this.getDealNo() + "] already have agreement]");
        }

        if (!(this.getCalculation().getStatus().equals("CREDITAPPROVED")
                || this.getCalculation().getStatus().equals("TENDER"))) {
            throw new DataException("[Deal [" + this.getDealNo() + "], Agreement can not be created before credit is approved]");
        }

        /*
         * BEGIN
         * If we receive "new" CreditApplication information in the request, we first make a change...
         */
        if (request.getPsiMessage().getBody().getCreditApplication() != null) {
            this.changeCreditApplicationHP(request);
            if (!(this.getCalculation().getStatus().equals("CREDITAPPROVED")
                    || this.getCalculation().getStatus().equals("TENDER"))) {
                throw new DataException("[Deal [" + this.getDealNo() + "], Agreement can not be created before credit is approved]");
            }
        }
        /*
         * END
         */

        AgreementDAO agreementDAO = new AgreementDAO(this.getStock());

        this.setAgreement((Agreement) agreementDAO.insert(request, this, this.getCalculation().getVersion()));

        DominoDataAccess.runLSAgent(stock, DominoDataAccess.HP_DATABASE, "agtCreatePDFPSI", this.getAgreement().getUNID());

        this.setAgreement(agreementDAO.findByHpUNID(this.getAgreement().getUNID()));
        CreditApplication creditApplication = getCreditApplicationMessageObj();

        CalculationDAO calculationDAO = new CalculationDAO(this.getStock());
        this.setCalculation(calculationDAO.findByHpUNID(this.getCalculation().getUNID()));
        DealStatus dealStatus = new DealStatus();
        dealStatus.setDealStatus(this.getStatus());
        dealStatus.setCalculationStatus(this.getCalculation().getStatus());

        DealNumber dealNumber = new DealNumber();
        dealNumber.setPrefix(this.getCustomer().getSeat().toUpperCase());
        dealNumber.setId(this.getDealNo());
        dealNumber.setVersion(this.getCalculation().getVersion());

        creditApplication.setStatus(dealStatus);
        agreementApplication.setStatus(dealStatus);
        agreementApplication.setDealNumber(dealNumber);

        return creditApplication;
    }

    public CreditApplication changeCreditApplication(PSIRequest request) throws Exception {

        if (!this.getPsiUserID().equals(request.getPsiUser().getUserName())) {
            throw new AccessException(Status.STATUS_ACCESS_DENIED);
        }

        CalculationDAO calculationDAO = new CalculationDAO(this.getStock());
        Calculation calc = calculationDAO.newVersion(request, this, "LEASING");
        this.setCalculation(calc);
        CreditDAO creditDAO = new CreditDAO(this.getStock());
        Credit newCredit = (Credit) creditDAO.newVersion(request, this, "LEASING");
        this.setCredit(newCredit);
        this.setCredit(creditDAO.findByUNID(this.getCredit().getUNID()));
        this.setCalculation(calculationDAO.findByUNID(this.getCalculation().getUNID()));

        CreditApplication creditApplication = getCreditApplicationMessageObj();
        return creditApplication;
    }

    public CreditApplication changeCreditApplicationHP(PSIRequest request) throws Exception {

        if (!this.getPsiUserID().equals(request.getPsiUser().getUserName())) {
            throw new AccessException(Status.STATUS_ACCESS_DENIED);
        }

        CalculationDAO calculationDAO = new CalculationDAO(this.getStock());
        Calculation calc = calculationDAO.newVersion(request, this, "HIREPURCHASE");
        this.setCalculation(calc);

        InstallmentPlanDAO installmentPlanDAO = new InstallmentPlanDAO(this.stock);
        InstallmentPlan newInstallmentPlan = installmentPlanDAO.newVersion(request, this);
        this.setInstallmentPlan(newInstallmentPlan);

        CreditDAO creditDAO = new CreditDAO(this.getStock());
        Credit newCredit = (Credit) creditDAO.newVersion(request, this, "HIREPURCHASE");
        this.setCredit(newCredit);

        this.setCredit(creditDAO.findByHpUNID(this.getCredit().getUNID()));
        this.setCalculation(calculationDAO.findByHpUNID(this.getCalculation().getUNID()));
        this.setInstallmentPlan(installmentPlanDAO.findByHpUNID(this.getInstallmentPlan().getUNID()));

        CreditApplication creditApplication = getCreditApplicationMessageObj();
        return creditApplication;
    }

    private AgreementApplication getAgreementApplicationMessageObj() throws DNotesException {
        AgreementApplication agreementApplication = new AgreementApplication();
        
        DealStatus dealStatus = new DealStatus();
        dealStatus.setDealStatus(this.getStatus());
        dealStatus.setCalculationStatus(this.getCalculation().getStatus());

        DealNumber dealNumber = new DealNumber();
        dealNumber.setPrefix(this.getCustomer().getSeat().toUpperCase());
        dealNumber.setId(this.getDealNo());
        dealNumber.setVersion(this.getCalculation().getVersion());

        agreementApplication.setStatus(dealStatus);
        agreementApplication.setDealNumber(dealNumber);
        
        return agreementApplication;
    }
    
    private CreditApplication getCreditApplicationMessageObj() throws DNotesException {
        CreditApplication creditApplication = new CreditApplication();
        Decision decision = new Decision();
        DealNumber dealNumber = new DealNumber();

        dealNumber.setPrefix(this.getCustomer().getSeat().toUpperCase());
        dealNumber.setId(this.getDealNo());
        dealNumber.setVersion(this.getCalculation().getVersion());
        decision.setStatus(this.getCredit().getDecision());
        // Credit decision date
        int validMonths = 0;
        validMonths = this.getCredit().getValidDays() / 30;
        decision.setValidUntil(rollDate(validMonths));
        decision.setMessageFromCreditApprover(this.getCredit().getComments());
        Duration duration = new Duration();
        String unit = "";
        switch (this.getCalculation().getPeriodLength()) {
            case 1:
                unit = PSIProperties.PERIOD_LENGHT_MONTH_TXT;
                break;
            case 3:
                unit = PSIProperties.PERIOD_LENGHT_QUARTER_TXT;
                break;
            case 12:
                unit = PSIProperties.PERIOD_LENGHT_YEAR_TXT;
                break;
            default:
                logger.info("Period lenght other " + this.getCalculation().getPeriodLength());
                break;
        }
        duration.setUnit(unit);
        duration.setValue(this.getCalculation().getDuration() / this.getCalculation().getPeriodLength());
        decision.setDuration(duration);
        Amount amount = new Amount();
        amount.setCurrency(this.getCalculation().getCurrency());
        amount.setValue(this.getCalculation().getFinancedAmount());
        decision.setAmount(amount);
        if (this.getCredit().getDecision().equalsIgnoreCase(PSIProperties.APPROVED_WITH_CONDITION_TXT)) {
            Condition condition = new Condition();
            condition.setDescription(this.getCredit().getConditionText());
            condition.setCode(Integer.parseInt(this.getCredit().getConditionCode()));

            int conditionCode = Integer.parseInt(this.getCredit().getConditionCode());
            switch (conditionCode) {
                case 1:
                    condition.setGuarantors(getGuarantorMO());
                    break;
                case 2:
                    condition.setGuarantors(getGuarantorMO());
                    break;
                case 3:
                    condition.setAmount(getAddToFirstPayment());
                    break;
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                    condition.setGuarantors(getGuarantorMO());
                    break;
                case 15:
                    condition.setAmount(getDownPayment());
                    break;
                default:
                    logger.info("Not a special condition " + conditionCode);
                    break;
            }

            decision.setCondition(condition);
        }

        decision.setMessageFromCreditApprover(this.getCredit().getManualReason());
        creditApplication.setDealNumber(dealNumber);
        creditApplication.setDecision(decision);
        //IB type 
        //creditApplication.setInterestType(this.getCalculation().);

        creditApplication.setDealType(this.calculation.getDealType());
        //HO setting ExternalReference ATEA 20150129
        creditApplication.setExternalReference(this.customer.getExternalReference());
        
        PeriodFee pf = new PeriodFee();
        pf.setAmount(new Amount(this.calculation.getCurrency(), this.calculation.getPeriodFee()));
        creditApplication.setPeriodFee(pf);

        return creditApplication;
    }

    private Vector getGuarantorMO() {
        Guarantor guarantor;
        Vector guarantorsMO = new Vector();
        Vector guarantors = getCredit().getGuarantors();
        // For a set or list
        for (Iterator it = guarantors.iterator(); it.hasNext();) {
            GuarantorBO guarantorBO = (GuarantorBO) it.next();
            guarantor = new Guarantor();
            //TODO fix all these fields  .
            guarantor.setIdentificationNumber(guarantorBO.getIdentificationNumber());
            guarantor.setName(guarantorBO.getFullName());
            guarantor.setSeat(guarantorBO.getSeat());
            Amount amount = new Amount(this.getCalculation().getCurrency(), guarantorBO.getGuaranteedAmount());
            guarantor.setAmount(amount);
            guarantor.setType(guarantorBO.getType());
            guarantor.setPhoneNumber(guarantorBO.getPhoneNumber());
            Address address = new Address();
            address.setType(guarantorBO.getType());
            address.setPostal(guarantorBO.getPostal());
            address.setZip(guarantorBO.getZip());
            address.setCity(guarantorBO.getCity());
            address.setCountry(guarantorBO.getCountry());
            guarantor.setAddress(address);
            guarantorsMO.add(guarantor);
        }
        return guarantorsMO;
    }

    private Amount getDownPayment() {
        Amount amount = new Amount(this.getCalculation().getCurrency(), this.getCredit().getDownPayment());
        return amount;
    }

    private Amount getAddToFirstPayment() {
        Amount amount = new Amount(this.getCalculation().getCurrency(), this.getCredit().getAddToFirstPayment());
        return amount;
    }

    private String rollDate(int months) {

        Calendar cal = Calendar.getInstance();
        DateTimeHandler dth = new DateTimeHandler();
        cal.setTime(this.getCredit().getDecisionTime());
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + months);
        String date = dth.getTimeStamp(DateTimeHandler.YYYY_MM_DD, cal.getTime());
        return date;
    }
    /*---------------------------------= Getter and Setters =----------------------------- */

    /**
     * Getter for property credit.
     *
     * @return Value of property credit.
     */
    public se.mpab.psi.bo.Credit getCredit() {
        return credit;
    }

    /**
     * Setter for property credit.
     *
     * @param credit New value of property credit.
     */
    public void setCredit(se.mpab.psi.bo.Credit credit) {
        this.credit = credit;
    }

    /**
     * Getter for property agreement.
     *
     * @return Value of property credit.
     */
    public se.mpab.psi.bo.Agreement getAgreement() {
        return agreement;
    }

    /**
     * Setter for property credit.
     *
     * @param credit New value of property credit.
     */
    public void setAgreement(se.mpab.psi.bo.Agreement agreement) {
        this.agreement = agreement;
    }

    /**
     * Getter for property dealNo.
     *
     * @return Value of property dealNo.
     */
    public long getDealNo() {
        return dealNo;
    }

    /**
     * Setter for property dealNo.
     *
     * @param dealNo New value of property dealNo.
     */
    public void setDealNo(long dealNo) {
        this.dealNo = dealNo;
    }

    /**
     * Getter for property UNID.
     *
     * @return Value of property UNID.
     */
    public java.lang.String getUNID() {
        return UNID;
    }

    /**
     * Setter for property UNID.
     *
     * @param UNID New value of property UNID.
     */
    public void setUNID(java.lang.String UNID) {
        this.UNID = UNID;
    }

    /**
     * Getter for property Comments.
     *
     * @return Value of property Comments.
     */
    public java.lang.String getComments() {
        return Comments;
    }

    /**
     * Setter for property Comments.
     *
     * @param Comments New value of property Comments.
     */
    public void setComments(java.lang.String Comments) {
        this.Comments = Comments;
    }

    /**
     * Getter for property partnerUNID.
     *
     * @return Value of property partnerUNID.
     */
    /*  public java.lang.String getPartnerUNID() {
     return partnerUNID;
     }
     */
    /**
     * Setter for property partnerUNID.
     *
     * @param partnerUNID New value of property partnerUNID.
     */
    /*    public void setPartnerUNID(java.lang.String partnerUNID) {
     this.partnerUNID = partnerUNID;
     }
     */
    /**
     * Getter for property InternalComments.
     *
     * @return Value of property InternalComments.
     */
    public java.lang.String getInternalComments() {
        return InternalComments;
    }

    /**
     * Setter for property InternalComments.
     *
     * @param InternalComments New value of property InternalComments.
     */
    public void setInternalComments(java.lang.String InternalComments) {
        this.InternalComments = InternalComments;
    }

    /**
     * Getter for property allianceUNID.
     *
     * @return Value of property allianceUNID.
     */
    /*   public java.lang.String getAllianceUNID() {
     return allianceUNID;
     }
    
     /**
     * Setter for property allianceUNID.
     * @param allianceUNID New value of property allianceUNID.
     *
     public void setAllianceUNID(java.lang.String allianceUNID) {
     this.allianceUNID = allianceUNID;
     }
     */
    /**
     * Getter for property customer.
     *
     * @return Value of property customer.
     */
    public se.mpab.psi.bo.Customer getCustomer() {
        return customer;
    }

    /**
     * Setter for property customer.
     *
     * @param customer New value of property customer.
     */
    public void setCustomer(se.mpab.psi.bo.Customer customer) {
        this.customer = customer;
    }

    /**
     * Getter for property type.
     *
     * @return Value of property type.
     */
    public java.lang.String getType() {
        return type;
    }

    /**
     * Setter for property type.
     *
     * @param type New value of property type.
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }

    /**
     * Getter for property calculation.
     *
     * @return Value of property calculation.
     */
    public se.mpab.psi.bo.Calculation getCalculation() {
        return calculation;
    }

    /**
     * Setter for property calculation.
     *
     * @param calculation New value of property calculation.
     */
    public void setCalculation(se.mpab.psi.bo.Calculation calculation) {
        this.calculation = calculation;
    }

    /**
     * Getter for property installment plan.
     *
     * @return Value of property installment plan.
     */
    public se.mpab.psi.bo.InstallmentPlan getInstallmentPlan() {
        return installmentPlan;
    }

    /**
     * Setter for property installment plan.
     *
     * @param calculation New value of property installment plan.
     */
    public void setInstallmentPlan(se.mpab.psi.bo.InstallmentPlan installmentPlan) {
        this.installmentPlan = installmentPlan;
    }

    /**
     * Getter for property History.
     *
     * @return Value of property History.
     */
    public java.lang.String getHistory() {
        return History;
    }

    /**
     * Setter for property History.
     *
     * @param History New value of property History.
     */
    public void setHistory(java.lang.String History) {
        this.History = History;
    }

    /**
     * Getter for property id.
     *
     * @return Value of property id.
     */
    public java.lang.String getId() {
        return id;
    }

    /**
     * Setter for property id.
     *
     * @param id New value of property id.
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }

    /**
     * Getter for property status.
     *
     * @return Value of property status.
     */
    public java.lang.String getStatus() {
        return status;
    }

    /**
     * Setter for property status.
     *
     * @param status New value of property status.
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    /**
     * Getter for property customerOfferUNID.
     *
     * @return Value of property customerOfferUNID.
     */
    public java.lang.String getCustomerOfferUNID() {
        return customerOfferUNID;
    }

    /**
     * Setter for property customerOfferUNID.
     *
     * @param customerOfferUNID New value of property customerOfferUNID.
     */
    public void setCustomerOfferUNID(java.lang.String customerOfferUNID) {
        this.customerOfferUNID = customerOfferUNID;
    }

    /**
     * Getter for property psiUserID.
     *
     * @return Value of property psiUserID.
     */
    public java.lang.String getPsiUserID() {
        return psiUserID;
    }

    /**
     * Setter for property psiUserID.
     *
     * @param psiUserID New value of property psiUserID.
     */
    public void setPsiUserID(java.lang.String psiUserID) {
        if (psiUserID == null) {
            psiUserID = "";
        }
        this.psiUserID = psiUserID;
    }

    /**
     * Getter for property stock.
     *
     * @return Value of property stock.
     */
    public java.lang.String getStock() {
        return stock;
    }

    /**
     * Setter for property stock.
     *
     * @param stock New value of property stock.
     */
    public void setStock(java.lang.String stock) {
        this.stock = stock;
    }

    /**
     * Getter for property salesPerson.
     *
     * @return Value of property salesPerson.
     */
    public se.mpab.psi.bo.User getSalesPerson() {
        return salesPerson;
    }

    /**
     * Setter for property salesPerson.
     *
     * @param salesPerson New value of property salesPerson.
     */
    public void setSalesPerson(se.mpab.psi.bo.User salesPerson) {
        this.salesPerson = salesPerson;
    }
    /*---------------------------------= Getter and Setters =----------------------------- */
}
