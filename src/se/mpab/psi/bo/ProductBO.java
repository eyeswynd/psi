/*
 * ProductBO.java
 *
 * Created on den 1 mars 2005, 11:15
 */

package se.mpab.psi.bo;

/**
 *
 * @author janne.berg
 */
public class ProductBO {
    
    private boolean active;
    private String name;
    private String type;
    private String objectCode;
            
    /** Creates a new instance of ProductBO */
    public ProductBO() {
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }
    public String toString(){
        return name;
    }
    
}
