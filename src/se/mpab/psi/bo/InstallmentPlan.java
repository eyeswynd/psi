// Copyright Applicon
/*
 * InstallmentPlan.java
 *
 * Created on den 15 april 2013, 16:51
 */
package se.mpab.psi.bo;

import java.util.Vector;

/**
 *
 * @author niklas.schold
 */
public class InstallmentPlan {
    private Vector installmentPayments;
    private Vector interestPayments;
    
    private String UNID;
    private String stock;
    
    
    public Vector getInstallmentPayments() {
        return installmentPayments;
    }
    public void setInstallmentPayments(Vector installmentPayments) {
        this.installmentPayments = installmentPayments;
    }
    
    
    public Vector getInterestPayments() {
        return interestPayments;
    }
    public void setInterestPayments(Vector interestPayments) {
        this.interestPayments = interestPayments;
    }
    
    
    public java.lang.String getUNID() {
        return UNID;
    }
    public void setUNID(java.lang.String UNID) {
        this.UNID = UNID;
    }
    
    
    public String getStock() {
        return stock;
    }
    public void setStock(String stock) {
        this.stock = stock;
    }
}
