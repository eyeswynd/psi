// Copyright Marquardt & Partners
/*
 * Calculation.java
 *
 * Created on den 3 september 2004, 14:08
 */

package se.mpab.psi.bo;
import se.mpab.psi.mo.*;
import se.mpab.psi.db.*;
import org.apache.log4j.LogManager;
import se.mpab.psi.*;

/**
 *
 * @author  janne.berg
 */
public class Calculation {
    private double financedAmount;
    private double periodFee;
    private int duration;
    private int periodLength;
    // TODO this is double for now need to fix this !
    // private long dealNo;
    private String allianceUNID;
    private String partnerUNID;
    private String customerOfferUNID;
    private String status;
    private String UNID;
    private String currency;
    private int version = 1;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(Calculation.class);
    private String installmentMethod;
    private String dealType;
    private boolean esignEnabled;
    
    /** Creates a new instance of Calculation */
    public Calculation(EntreMessage message) {
        this.setCustomerOfferUNID(message.getBody().getCreditApplication().getOfferId());
        this.financedAmount = message.getBody().getCreditApplication().getTotalEquipmentValue();
        this.duration = message.getBody().getCreditApplication().getDuration().getValue();
    }
    public Calculation(){
        
    }
    /**
     * Getter for property duration.
     * @return Value of property duration.
     */
    public int getDuration() {
        return duration;
    }
    
    /**
     * Setter for property duration.
     * @param duration New value of property duration.
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }
    
    /**
     * Getter for property allianceUNID.
     * @return Value of property allianceUNID.
     */
    public java.lang.String getAllianceUNID() {
        return allianceUNID;
    }
    
    /**
     * Setter for property allianceUNID.
     * @param allianceUNID New value of property allianceUNID.
     */
    public void setAllianceUNID(java.lang.String allianceUNID) {
        this.allianceUNID = allianceUNID;
    }
    
    /**
     * Getter for property financedAmount.
     * @return Value of property financedAmount.
     */
    public double getFinancedAmount() {
        return financedAmount;
    }
    
    /**
     * Setter for property financedAmount.
     * @param financedAmount New value of property financedAmount.
     */
    public void setFinancedAmount(double financedAmount) {
        this.financedAmount = financedAmount;
    }
    
    /**
     * Getter for property financedAmount.
     * @return Value of property financedAmount.
     */
    public double getPeriodFee() {
        return periodFee;
    }
    
    /**
     * Setter for property financedAmount.
     * @param periodFee New value of property periodFee.
     */
    public void setPeriodFee(double periodFee) {
        this.periodFee = periodFee;
    }
    
    /**
     * Getter for property partnerUNID.
     * @return Value of property partnerUNID.
     */
    public java.lang.String getPartnerUNID() {
        return partnerUNID;
    }
    
    /**
     * Setter for property partnerUNID.
     * @param partnerUNID New value of property partnerUNID.
     */
    public void setPartnerUNID(java.lang.String partnerUNID) {
        this.partnerUNID = partnerUNID;
    }
    
    /**
     * Getter for property status.
     * @return Value of property status.
     */
    public java.lang.String getStatus() {
        if(status.equals(PSIProperties.CALCULATION_STATUS_CALCULATION_TXT)){
              status = PSIProperties.CALCULATION_STATUS_CALCULATION_TXT;  
        }
        else if(status.equals(PSIProperties.CALCULATION_STATUS_AGREEMENT_TXT)){
              status = PSIProperties.CALCULATION_STATUS_AGREEMENT_TXT;  
        }
        else if(status.equals(PSIProperties.CALCULATION_STATUS_APPROVED_TXT)){
              status = PSIProperties.CALCULATION_STATUS_APPROVED_TXT;  
        }
        else if(status.equals(PSIProperties.CALCULATION_STATUS_TENDER_TXT)){
              status = PSIProperties.CALCULATION_STATUS_TENDER_TXT;  
        }
        return status;
    }
    
    /**
     * Setter for property status.
     * @param status New value of property status.
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }
    
    /**
     * Getter for property version.
     * @return Value of property version.
     */
    public int getVersion() {
        return version;
    }
    
    /**
     * Setter for property version.
     * @param version New value of property version.
     */
    public void setVersion(int version) {
        this.version = version;
    }
    
    /**
     * Getter for property UNID.
     * @return Value of property UNID.
     */
    public java.lang.String getUNID() {
        return UNID;
    }
    
    /**
     * Setter for property UNID.
     * @param UNID New value of property UNID.
     */
    public void setUNID(java.lang.String UNID) {
        this.UNID = UNID;
    }
    
    /**
     * Getter for property customerOfferUNID.
     * @return Value of property customerOfferUNID.
     */
    public java.lang.String getCustomerOfferUNID() {
        return customerOfferUNID;
    }
    
    /**
     * Setter for property customerOfferUNID.
     * @param customerOfferUNID New value of property customerOfferUNID.
     */
    public void setCustomerOfferUNID(java.lang.String customerOfferUNID) {
        this.customerOfferUNID = customerOfferUNID;
    }
    
    /**
     * Getter for property periodLength.
     * @return Value of property periodLength.
     */
    public int getPeriodLength() {
        return periodLength;
    }
    
    /**
     * Setter for property periodLength.
     * @param periodLength New value of property periodLength.
     */
    public void setPeriodLength(int periodLength) {
        this.periodLength = periodLength;
    }
    
    /**
     * Getter for property currency.
     * @return Value of property currency.
     */
    public java.lang.String getCurrency() {
        return currency;
    }
    
    /**
     * Setter for property currency.
     * @param currency New value of property currency.
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }
    
    public java.lang.String getInstallmentMethod() {
        return installmentMethod;
    }
    
    public void setInstallmentMethod(java.lang.String installmentMethod) {
        this.installmentMethod = installmentMethod;
    }
    
    /**
     * Getter for property dealType.
     * @return Value of property dealType.
     */
    public String getDealType() {
        return dealType;
    }

    /**
     * Setter for property dealType.
     * @param dealType New value of property dealType.
     */
    public void setDealType(String dealType) {
        this.dealType = dealType;
    }
    
 /*   public int save(Calculation calculation){
        CalculationDAO calculationDAO = new CalculationDAO();
        calculationDAO.save(calculation);
        return 0;
    }
  */
   /* public int updateStatus(String status){
        this.status = status;
        CalculationDAO calculationDAO = new CalculationDAO();
        calculationDAO.updateStatus(this);
        return 0;
    }
    **/
    
    public boolean getEsignEnabled() {
        return esignEnabled;
    }
    
    public void setEsignEnabled(boolean enabled) {
        this.esignEnabled = enabled;
    }
}
