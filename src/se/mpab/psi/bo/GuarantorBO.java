/*
 * Guarantor.java
 *
 * Created on den 7 januari 2005, 15:20
 */

package se.mpab.psi.bo;

/**
 *
 * @author  janne.berg
 */
public class GuarantorBO extends PersonBO{
    
    private String postal;
    
    private String zip;
    
    private String city;
    
    private String country;
    
    private String seat;
    
    private double guaranteedAmount;
    
    private String type;
    
    private String phoneNumber;
    
    private String fullName;
    
    private String identificationNumber;
    
    
    /** Creates a new instance of Guarantor */
    public GuarantorBO() {
    }
    
    /**
     * Getter for property city.
     * @return Value of property city.
     */
    public java.lang.String getCity() {
        return city;
    }
    
    /**
     * Setter for property city.
     * @param city New value of property city.
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }
    
    /**
     * Getter for property country.
     * @return Value of property country.
     */
    public java.lang.String getCountry() {
        return country;
    }
    
    /**
     * Setter for property country.
     * @param country New value of property country.
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }
    
    /**
     * Getter for property guaranteedAmount.
     * @return Value of property guaranteedAmount.
     */
    public double getGuaranteedAmount() {
        return guaranteedAmount;
    }
    
    /**
     * Setter for property guaranteedAmount.
     * @param guaranteedAmount New value of property guaranteedAmount.
     */
    public void setGuaranteedAmount(double guaranteedAmount) {
        this.guaranteedAmount = guaranteedAmount;
    }
    
    /**
     * Getter for property postal.
     * @return Value of property postal.
     */
    public java.lang.String getPostal() {
        return postal;
    }
    
    /**
     * Setter for property postal.
     * @param postal New value of property postal.
     */
    public void setPostal(java.lang.String postal) {
        this.postal = postal;
    }
    
    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public java.lang.String getType() {
        return type;
    }
    
    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }
    
    /**
     * Getter for property zip.
     * @return Value of property zip.
     */
    public java.lang.String getZip() {
        return zip;
    }
    
    /**
     * Setter for property zip.
     * @param zip New value of property zip.
     */
    public void setZip(java.lang.String zip) {
        this.zip = zip;
    }
    
    /**
     * Getter for property phoneNumber.
     * @return Value of property phoneNumber.
     */
    public java.lang.String getPhoneNumber() {
        return phoneNumber;
    }
    
    /**
     * Setter for property phoneNumber.
     * @param phoneNumber New value of property phoneNumber.
     */
    public void setPhoneNumber(java.lang.String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    /**
     * Getter for property fullName.
     * @return Value of property fullName.
     */
    public java.lang.String getFullName() {
        return fullName;
    }
    
    /**
     * Setter for property fullName.
     * @param fullName New value of property fullName.
     */
    public void setFullName(java.lang.String fullName) {
        this.fullName = fullName;
    }
    
    /**
     * Getter for property identificationNumber.
     * @return Value of property identificationNumber.
     */
    public java.lang.String getIdentificationNumber() {
        return identificationNumber;
    }
    
    /**
     * Setter for property identificationNumber.
     * @param identificationNumber New value of property identificationNumber.
     */
    public void setIdentificationNumber(java.lang.String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }
    
    
    
    public String getSeat() {
        return seat;
    }
    
    public void setSeat(String seat) {
        this.seat = seat;
    }
    
}
