package se.mpab.psi.bo;

import de.bea.domingo.DDatabase;
import de.bea.domingo.DDocument;
import de.bea.domingo.DNotesException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.LogManager;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import se.mpab.psi.bo.Deal;
import se.mpab.psi.db.DominoDataAccess;
import se.mpab.psi.mo.EntreMessage;
import se.mpab.psi.mo.PriceMatrix;
import se.mpab.psi.user.OfferId;

public class PriceMatrixHelper {
    
    /** Creates a new instance of CustomerOffer */
    private static org.apache.log4j.Logger logger = LogManager.getLogger(PriceMatrixHelper.class);
    
    public static Vector<PriceMatrix> getPriceMatrices(String stock, Vector<OfferId> offerIds) throws Exception
    {   
        Vector<PriceMatrix> matrices = new Vector<PriceMatrix>();
        
        Vector<String> unids = new Vector<String>();
        for(OfferId id : offerIds)
        {
            if(id != null)
            {
                unids.add(id.getDocUnid());
            }
        }
        
        if(unids.size() > 0)
        {
            DDatabase mainDB = DominoDataAccess.openDatabase("main", stock);
            DDocument tempDoc = mainDB.createDocument();
            String docUnid = tempDoc.getUniversalID();
            tempDoc.replaceItemValue("DocUNID", docUnid);
            tempDoc.replaceItemValue("DocType", "InterAgentMessage");
            tempDoc.replaceItemValue("CustomerOfferUNIDs", unids);
            tempDoc.replaceItemValue("DeleteFlag", "1");
            tempDoc.save(true, false);

            int result = DominoDataAccess.runLSAgent(stock, "main", "agtGetPriceMatrixPSI", docUnid);
            if(result != 0)
            {
                throw new Exception("Unknown error running price matrix agent (agtGetPriceMatrixPSI)!");
            }
            tempDoc = DominoDataAccess.getDocumentByKey(stock, "main", "vwInterAgentMessage", docUnid, true);
            if(tempDoc == null)
            {
                throw new Exception("Unknown error fetching price matrix document after agent run!");
            }

            SAXBuilder builder = new SAXBuilder();
            for(String unid : unids)
            {
                String xml = tempDoc.getItemValueString(unid);
                if(xml != null && !xml.isEmpty())
                {
                    Document xmlDoc = builder.build(new StringReader(xml));
                    PriceMatrix matrix = new PriceMatrix();
                    matrix.initFromElement(xmlDoc.getRootElement());
                    matrices.add(matrix);
                }
            }
        }    

        return matrices;
    }
}
