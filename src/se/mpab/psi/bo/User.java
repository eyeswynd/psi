/*
 * User.java
 *
 * Created on den 3 september 2004, 16:16
 */
package se.mpab.psi.bo;

import se.mpab.psi.mo.*;
import java.util.Vector;
import java.util.List;
import java.util.ListIterator;
import org.apache.log4j.LogManager;

/**
 *
 * @author  janne.berg
 */
public class User {

    private String name;
    /*
    We store this data in the user object for now just to simplify things.
    Later on we'll have a partner and a Alliance object
     */
    private String PartnerUNID;
    private String AllianceUNID;
    private String UNID;
    private Vector roles;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(User.class);
    private String PE_UserID;

    /** Creates a new instance of User */
    public User() {
    }
    
    /** Creates a new instance of User - copies all values from another instance */
    public User(User fromUser){
        this.name = fromUser.getName();
        this.PartnerUNID = fromUser.getPartnerUNID();
        this.AllianceUNID = fromUser.getAllianceUNID();
        this.UNID = fromUser.getUNID();
        this.roles = fromUser.getRoles();
        this.PE_UserID = fromUser.getPE_UserID();
    }

    /**
     * Getter for property PartnerUNID.
     * @return Value of property PartnerUNID.
     */
    public java.lang.String getPartnerUNID() {
        return PartnerUNID;
    }

    /**
     * Setter for property PartnerUNID.
     * @param PartnerUNID New value of property PartnerUNID.
     */
    public void setPartnerUNID(java.lang.String PartnerUNID) {
        this.PartnerUNID = PartnerUNID;
    }

    /**
     * Getter for property UNID.
     * @return Value of property UNID.
     */
    public String getUNID() {
        return UNID;
    }

    /**
     * Setter for property UNID.
     * @param UNID New value of property UNID.
     */
    public void setUNID(String UNID) {
        this.UNID = UNID;
    }

    /**
     * Getter for property AllianceUNID.
     * @return Value of property AllianceUNID.
     */
    public java.lang.String getAllianceUNID() {
        return AllianceUNID;
    }

    /**
     * Setter for property AllianceUNID.
     * @param AllianceUNID New value of property AllianceUNID.
     */
    public void setAllianceUNID(java.lang.String AllianceUNID) {
        this.AllianceUNID = AllianceUNID;
    }

    /**
     * Getter for property name.
     * @return Value of property name.
     */
    public java.lang.String getName() {
        return name;
    }

    /**
     * Setter for property name.
     * @param name New value of property name.
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }

    /**
     * Get the value of roles
     *
     * @return the value of roles
     */
    public Vector getRoles() {
        return roles;
    }

    /**
     * Set the value of roles
     *
     * @param roles new value of roles
     */
    public void setRoles(Vector roles) {
        this.roles = roles;
    }

    public void setRolesFromList(List roles) {
        Vector v = new Vector();
        ListIterator listItr = roles.listIterator();
        while (listItr.hasNext()) {
            v.add(listItr.next());
        }
        this.roles = v;
    }
    
    /**
     * Getter for property PE_UserID.
     * @return Value of property PE_UserID.
     */
    public java.lang.String getPE_UserID() {
        return PE_UserID;
    }

    /**
     * Setter for property PE_UserID.
     * @param PE_UserID New value of property PE_UserID.
     */
    public void setPE_UserID(java.lang.String id) {
        this.PE_UserID = id;
    }
}
