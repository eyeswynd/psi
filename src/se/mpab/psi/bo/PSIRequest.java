// Copyright Marquardt & Partners
package se.mpab.psi.bo;

/*
 * Request.java
 *
 * Created on den 26 augusti 2004, 10:49
 */
import java.io.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.*;
import java.util.Iterator;
import java.util.Properties;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.LogManager;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import se.mpab.psi.*;
import se.mpab.psi.db.*;
import se.mpab.psi.mo.*;
import se.mpab.psi.user.Entre;
import se.mpab.psi.user.EntreUser;
import se.mpab.psi.user.OfferId;
import se.mpab.psi.user.PSIUser;
import se.mpab.util.common.DateTimeHandler;
import se.mpab.util.common.PropertiesSetup;

/**
 * The request object used for request data.
 * @author Janne Berg
 * @version 1
 */
public class PSIRequest implements Serializable {

    private se.mpab.psi.user.PSIUser psiUser = null;
    private EntreMessage psiMessage = null;
    private String country = "";
    private String incomingDir = null;
    private org.jdom.Document jDomDoc = null;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(PSIRequest.class);

    /** Creates a new instance of Request */
    public PSIRequest(HttpServletRequest request) throws Exception, DataException {
        // For printing to disc
        try {
            Properties prop = PSIProperties.getProp();
            if (PropertiesSetup.propertyExists("incomingDir", prop)) {
                this.incomingDir = PropertiesSetup.getString("incomingDir", prop);
            //logger.debug("incoming dir: <" + this.incomingDir +">");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }


        // Create PSIMessage
        // Todo this code should be pasted to at parseRequest method that responds with true or false
        // this should make the code kore readable
        SAXBuilder builder = new SAXBuilder();
        if (request.getParameter("xml") != null) {
            jDomDoc = builder.build(new StringReader(request.getParameter("xml")));
        } else {
            try {
                jDomDoc = builder.build(request.getInputStream());
            } catch (Exception e) {
                //logger.error(e.getMessage(), e);
                String xmlText = "";
                Enumeration enumVar = request.getParameterNames();
                while (enumVar.hasMoreElements()) {
                    String param = (String) enumVar.nextElement();
                    logger.info(" param = [" + param + "]");
                    xmlText += param + "=" + request.getParameterValues(param)[0];
                }
                /*
                int bytesin;
                String xmlText = "";
                byte[] buf = new byte[1024];
                //ServletInputStream sis = request.getInputStream();
                while ((bytesin = request.getInputStream().read(buf)) >= 0) {
                xmlText += new String(buf, 0, bytesin);
                logger.info(" Incoming XML conv to text [" + xmlText + "]");
                }

                
                BufferedReader in = r2.getReader();
                String input = null;
                String xmlText = "";
                while ((input = in.readLine()) != null) {
                xmlText += input;
                 */
                jDomDoc = builder.build(new StringReader(xmlText));
            }
        }
        //logger.info(" Incoming XML [" + this.toString() + "]");
        psiMessage = new EntreMessage();
        org.jdom.Element e = jDomDoc.getRootElement();
        psiMessage.initFromElement(e);


        // Get country
        if ((psiMessage.getHeader().getService().equals("makeCreditApplication")) | (psiMessage.getHeader().getService().equals("makeCreditApplicationHP"))) {
            this.country = psiMessage.getBody().getCreditApplication().getClient().getSeat().toUpperCase();
        }
        if (psiMessage.getHeader().getService().equals("checkCreditApplication")) {
            if (psiMessage.getBody().getCreditApplications() == null) {
                this.country = psiMessage.getBody().getCreditApplication().getDealNumber().getPrefix().toUpperCase();
            } else {
                Iterator it = psiMessage.getBody().getCreditApplications().iterator();
                if (it.hasNext()) {
                    CreditApplication creditApplication = (CreditApplication) it.next();
                    this.country = creditApplication.getDealNumber().getPrefix().toUpperCase();
                }
            }
        }
        if (psiMessage.getHeader().getService().equals("checkStatus")) {
            if (psiMessage.getBody().getCreditApplications() == null) {
                this.country = psiMessage.getBody().getCreditApplication().getDealNumber().getPrefix().toUpperCase();
            } else {
                Iterator it = psiMessage.getBody().getCreditApplications().iterator();
                if (it.hasNext()) {
                    CreditApplication creditApplication = (CreditApplication) it.next();
                    this.country = creditApplication.getDealNumber().getPrefix().toUpperCase();
                }
            }
        }
        if (psiMessage.getHeader().getService().equals("changeCreditApplication") | psiMessage.getHeader().getService().equals("changeCreditApplicationHP")) {
            System.out.println("Check if dealno is defined..");
            if (psiMessage.getBody().getCreditApplication().getDealNumber() == null) {
                throw new DataException("Deal number not defined");
            }
            this.country = psiMessage.getBody().getCreditApplication().getDealNumber().getPrefix().toUpperCase();
        }
        if (psiMessage.getHeader().getService().equals("makeAgreementApplication") | psiMessage.getHeader().getService().equals("makeAgreementApplicationHP")) {
            System.out.println("Check if dealno is defined..");
            if (psiMessage.getBody().getAgreementApplication().getDealNumber() == null) {
                throw new DataException("Deal number not defined");
            }
            this.country = psiMessage.getBody().getAgreementApplication().getDealNumber().getPrefix().toUpperCase();
        }
        // Create and check PSIUser Authenticate user IP
        PSIUserDAO psiUserDAO = new PSIUserDAO();

        this.psiUser = psiUserDAO.getPSIUserFromKey(psiMessage.getHeader().getUser(),
                psiMessage.getHeader().getPassword(),
                request.getRemoteAddr());
        
        //If country is not set get it from the first user in the PSIUsers config file for this PSIUser. Always only one at the moment.
        if(this.country == null || this.country.isEmpty()) {
            Entre entre = (Entre) this.psiUser.getSystem().get("entre");
            HashMap users = entre.getUsers();
            if(users.size() > 0) {
                String key = (String) users.keySet().toArray()[0];
                EntreUser tempUser = (EntreUser) users.get(key);
                this.country = tempUser.getSeat();
            }
        }
        if(this.country == null || this.country.isEmpty()) {
            throw new DataException("Unable to identify country code for user");
        }

        if ((psiMessage.getHeader().getService().equals("makeCreditApplication")) | (psiMessage.getHeader().getService().equals("makeCreditApplicationHP"))) {
            se.mpab.psi.user.EntreUser entreUser = psiUser.getEntreUserBySeat(this.country);
            if (entreUser == null) {
                throw new se.mpab.psi.AutenticationException(Status.STATUS_USER_SEAT_NOT_ALLOWED);
            }
            if (psiMessage.getBody().getCreditApplication().getOfferId() == null) {
                throw new DataException("Invalid Offer id");
            }
            if (entreUser.getEntreOfferByPsiOfferId(psiMessage.getBody().getCreditApplication().getOfferId()) == null) {
                throw new DataException("Invalid Offer id");
            }
            String entreOffer = entreUser.getEntreOfferByPsiOfferId(psiMessage.getBody().getCreditApplication().getOfferId()).getDocUnid();
            if (entreOffer == null) {
                throw new DataException("Invalid Offer id");
            }
            psiMessage.getBody().getCreditApplication().setOfferId(entreOffer);
        }
        if ((psiMessage.getHeader().getService().equals("changeCreditApplication")) | (psiMessage.getHeader().getService().equals("changeCreditApplicationHP"))) {
            se.mpab.psi.user.EntreUser entreUser = psiUser.getEntreUserBySeat(this.country);
            if (entreUser == null) {
                throw new se.mpab.psi.AutenticationException(Status.STATUS_USER_SEAT_NOT_ALLOWED);
            }
            if (psiMessage.getBody().getCreditApplication().getOfferId() != null) {
                // If offer id is set by the request - it has to be the same as earlier version - otherwise abort:
                if (entreUser.getEntreOfferByPsiOfferId(psiMessage.getBody().getCreditApplication().getOfferId()) == null) {
                    throw new DataException("Invalid Offer id");
                }
                String entreOffer = entreUser.getEntreOfferByPsiOfferId(psiMessage.getBody().getCreditApplication().getOfferId()).getDocUnid();
                if (entreOffer == null) {
                    throw new DataException("Invalid Offer id");
                }
                psiMessage.getBody().getCreditApplication().setOfferId(entreOffer);
            }
            
        }
        if (psiMessage.getHeader().getService().equals("checkCreditApplication")) {
            logger.info("Not applicable");
        }
        //Agreement
        if (psiMessage.getHeader().getService().equals("makeAgreementApplication")) {
            if (psiMessage.getBody().getCreditApplication() != null) {
                se.mpab.psi.user.EntreUser entreUser = psiUser.getEntreUserBySeat(this.country);
                if (entreUser == null) {
                    throw new se.mpab.psi.AutenticationException(Status.STATUS_USER_SEAT_NOT_ALLOWED);
                }
                if (psiMessage.getBody().getCreditApplication().getOfferId() == null) {
                    throw new DataException("Invalid Offer id");
                }
                if (entreUser.getEntreOfferByPsiOfferId(psiMessage.getBody().getCreditApplication().getOfferId()) == null) {
                    throw new DataException("Invalid Offer id");
                }
                String entreOffer = entreUser.getEntreOfferByPsiOfferId(psiMessage.getBody().getCreditApplication().getOfferId()).getDocUnid();
                if (entreOffer == null) {
                    throw new DataException("Invalid Offer id");
                }
                psiMessage.getBody().getCreditApplication().setOfferId(entreOffer);
            } else { logger.info("Not applicable");}
        }
        if (psiMessage.getHeader().getService().equals("getPriceMatrix"))
        {
            se.mpab.psi.user.EntreUser entreUser = psiUser.getEntreUserBySeat(this.country);
            if (entreUser == null)
            {
                throw new se.mpab.psi.AutenticationException(Status.STATUS_USER_SEAT_NOT_ALLOWED);
            }
            Vector offers = psiMessage.getBody().getCustomerOffers();
            Vector realOffers = new Vector();
            Iterator it = offers.iterator();
            while(it.hasNext())
            {
                OfferId offerId = entreUser.getEntreOfferByPsiOfferId((String)it.next());
                realOffers.add(offerId);
            }
            psiMessage.getBody().setCustomerOffers(realOffers);
        }
    }

    /*  public void writeToFile() {
    DateTimeHandler dth = new DateTimeHandler();
    java.util.Calendar cal = new java.util.GregorianCalendar();

    String datedirPath =  Integer.toString(cal.get(java.util.Calendar.YEAR)) + java.io.File.separatorChar  +
    Integer.toString(cal.get(java.util.Calendar.MONTH )+1)+java.io.File.separatorChar  +
    Integer.toString(cal.get(java.util.Calendar.DATE ));

    String fileName = psiUser.getUserName() + " " + dth.getTimeStamp(dth.YYYYMMDD) + "_"
    + dth.getTimeStamp(dth.HHMMSSCC) + ".xml";

    File incomingFileDir = new File(  incomingDir + java.io.File.separatorChar +
    country.toLowerCase() + java.io.File.separatorChar +
    datedirPath  );
    logger.debug("Filename: " + incomingFileDir.getAbsolutePath());


    try {
    boolean exists = incomingFileDir.exists();
    if (!exists) {
    incomingFileDir.mkdirs();
    }
    BufferedWriter out = new BufferedWriter(new FileWriter(incomingFileDir.getAbsolutePath() +  java.io.File.separatorChar +  fileName));
    out.write(this.toString());
    out.close();
    } catch (IOException e) {
    logger.warn(this.getClass().getName() + ", Error while saving request to file <"+ incomingDir + fileName+">",e);
    logger.error(e.getMessage(), e);
    }
    }*/
    public String toPSIString() {
        String outStr = null;
        outStr = "<!--" + this.psiUser.getUserIp() + "-->\n";
        org.jdom.output.XMLOutputter outputter = new org.jdom.output.XMLOutputter(org.jdom.output.Format.getPrettyFormat());
        outStr += outputter.outputString(psiMessage.getElement(org.jdom.Namespace.getNamespace("", "")));
        return outStr;
    }

    public String toString() {
        String outStr = null;
        org.jdom.output.XMLOutputter outputter = new org.jdom.output.XMLOutputter(org.jdom.output.Format.getPrettyFormat());
        outStr += outputter.outputString(jDomDoc);
        return outStr;
    }

    /**
     * Getter for property psiMessage.
     * @return Value of property psiMessage.
     */
    public se.mpab.psi.mo.EntreMessage getPsiMessage() {
        return psiMessage;
    }

    /**
     * Setter for property psiMessage.
     * @param psiMessage New value of property psiMessage.
     */
    public void setPsiMessage(se.mpab.psi.mo.EntreMessage psiMessage) {
        this.psiMessage = psiMessage;
    }

    /**
     * Getter for property psiUser.
     * @return Value of property psiUser.
     */
    public se.mpab.psi.user.PSIUser getPsiUser() {
        return psiUser;
    }

    /**
     * Setter for property psiUser.
     * @param psiUser New value of property psiUser.
     */
    public void setPsiUser(se.mpab.psi.user.PSIUser psiUser) {
        this.psiUser = psiUser;
    }

    /**
     * Getter for property country.
     * @return Value of property country.
     */
    public java.lang.String getCountry() {
        return country;
    }

    /**
     * Setter for property country.
     * @param country New value of property country.
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }
}