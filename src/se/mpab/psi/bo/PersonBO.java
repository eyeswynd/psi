/*
 * Person.java
 *
 * Created on den 7 januari 2005, 15:14
 */

package se.mpab.psi.bo;

/**
 *
 * @author  janne.berg
 */
public class PersonBO {
    
    private String id;
    
    private String firstName;
    
    private String surname;
    
    private String initial;
    
    /** Creates a new instance of Person */
    public PersonBO() {
    }
    
    /**
     * Getter for property firstName.
     * @return Value of property firstName.
     */
    public java.lang.String getFirstName() {
        return firstName;
    }
    
    /**
     * Setter for property firstName.
     * @param firstName New value of property firstName.
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public java.lang.String getId() {
        return id;
    }
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }
    
    /**
     * Getter for property initial.
     * @return Value of property initial.
     */
    public java.lang.String getInitial() {
        return initial;
    }
    
    /**
     * Setter for property initial.
     * @param initial New value of property initial.
     */
    public void setInitial(java.lang.String initial) {
        this.initial = initial;
    }
    
    /**
     * Getter for property surname.
     * @return Value of property surname.
     */
    public java.lang.String getSurname() {
        return surname;
    }
    
    /**
     * Setter for property surname.
     * @param surname New value of property surname.
     */
    public void setSurname(java.lang.String surname) {
        this.surname = surname;
    }
    
}
