/*
 * PSIMessage.java
 *
 * Created on den 19 januari 2005, 14:48
 */

package se.mpab.psi.bo;

/**
 *
 * @author Janne Berg
 * @description This is a wrapper for the messagedata
 */
public class PSIMessage {
    
    private int id ;
    private String address;
    private String type;
    private java.util.Date sent;
    private String status;
    private String country;
    private se.mpab.psi.mo.EntreMessage entreMessage;
    
    
    /** Creates a new instance of PSIMessage */
    public PSIMessage() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public java.util.Date getSent() {
        return sent;
    }

    public void setSent(java.util.Date sent) {
        this.sent = sent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
    
}
