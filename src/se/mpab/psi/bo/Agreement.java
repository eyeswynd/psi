/*
 * Agreement.java
 *
 * Created on den 3 september 2004, 13:39
 */

package se.mpab.psi.bo;

import java.util.Vector;
import java.util.Date;
import se.mpab.psi.mo.*;
import se.mpab.psi.*;
import se.mpab.psi.db.*;
import se.mpab.util.common.PropertiesSetup;
import org.apache.log4j.LogManager;
import org.apache.log4j.Level;
import java.util.Properties;
import se.mpab.util.common.PropertiesSetup;
import se.mpab.util.common.PropertiesSetupException;

/**
 *
 * @author  Hans Olsberg
 */
public class Agreement {
    
    private Customer customer;
    private String UNID;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(Agreement.class);
    private String stock;
    private String PartnerCondition;
    private String PartnerName;
    private String PartnerAddress;
    private String PartnerZipcode;
    private String PartnerCoName;
    private String PartnerCity;
    private String SpecificationHeadline;
    private String Specification;
    private String SpecialConditions;
    private String AgreementCondition;         
    private String AppointSupplierEnabled;
    private String CalcRowsSpecification;
    private String AppointSupplierName;
    private String AppointSupplierOrgNo;
    private boolean esignEnabled;
    
       /*  
     *TODO
     * Since the model in Entre is so full of double data  wee use this variables for now, we need to make
     *a better solution in the future. this should be taken care of the DAO objects
    */ 
    private int duration;
    private long dealNo;
    private int version;
    
        /** Creates a new instance of Agreement */
    public Agreement() {
    }
    
    /** Creates a new instance of Agreement */
    public Agreement(long dealNo, int version) throws Exception {
        setDealNo(dealNo);
        this.setVersion(version);
    }
  
      /**
     * Getter for property partner.
     * @return Value of property partnercondition.
     */
    public java.lang.String getPartnerCondition() {
        return PartnerCondition;
    }

    public void setPartnerCondition(java.lang.String PartnerCondition) {
        this.PartnerCondition = PartnerCondition;
    }
     /**
     * Getter for property partner.
     * @return Value of property partnername.
     */ 
    public java.lang.String getPartnername() {
        return PartnerName;
    }

    public void setPartnername(java.lang.String PartnerName) {
        this.PartnerName = PartnerName;
    }
     /**
     * Getter for property partner.
     * @return Value of property partneraddress.
     */ 
    public java.lang.String getPartnerAddress() {
        return PartnerAddress;
    }
    public void setPartnerAddress(java.lang.String PartnerAddress) {
        this.PartnerAddress = PartnerAddress;
    }
      /**
     * Getter for property PartnerZipcoder.
     * @return Value of property PartnerZipcode.
     */
     public java.lang.String getPartnerZipcode() {
        return PartnerZipcode;
    }
    public void setPartnerZipCode(java.lang.String partnerzipcode) {
        this.PartnerZipcode = partnerzipcode;
    }
      /**
     * Getter for property PartnerCoName.
     * @return Value of property PartnerCoName.
     */ 
    public java.lang.String getPartnerCoName() {
        return PartnerCoName;
    }
    public void setPartnerCoName(java.lang.String PartnerCoName) {
        this.PartnerCoName = PartnerCoName;
    }
      /**
     * Getter for property PartnerCity.
     * @return Value of property PartnerCity.
     */
    public java.lang.String getPartnerCity () {
        return PartnerCity;    
    }
    public void setPartnerCity(java.lang.String PartnerCity) {
        this.PartnerCity = PartnerCity;
    }
    
     /**
     * Getter for property Specification.
     * @return Value of property Specification.
     */
    
    public java.lang.String getSpecification () {
        return Specification;    
    }
    public void setSpecification(java.lang.String Specification) {
        this.Specification = Specification;
    }
    
     /**
     * Getter for property SpecificationHeadline.
     * @return Value of property SpecificationHeadline.
     */
    
    
   public java.lang.String getSpecificationHeadline () {
        return SpecificationHeadline; 
   }
    public void setSpecificationHeadline(java.lang.String SpecificationHeadline) {
        this.SpecificationHeadline = SpecificationHeadline;
    }
    
      /**
     * Getter for property AppointSupplierenabled.
     * @return Value of property AppointSupplierenabled.
     */
    
    
   public java.lang.String getAppointSupplierEnabled () {
        return AppointSupplierEnabled; 
   }
    public void setAppointSupplierEnabled(java.lang.String AppointSupplierEnabled) {
        this.AppointSupplierEnabled = AppointSupplierEnabled;
    }
      
    /**
     * Getter for property CalcRowsSpecification.
     * @return Value of property CalcRowsSpecification.
     */   
     
   public java.lang.String getCalcRowsSpecification () {
        return CalcRowsSpecification; 
   }
    public void setCalcRowsSpecification(java.lang.String CalcRowsSpecification) {
        this.CalcRowsSpecification = CalcRowsSpecification;
    }
    
    /**
     * Getter for property AppointSuppliername.
     * @return Value of property AppointSuppliername.
     */  
    
    public java.lang.String getAppointSupplierName() {
        return AppointSupplierName; 
   }
    public void setAppointSupplierName(java.lang.String AppointSupplierName) {
        this.AppointSupplierName = AppointSupplierName;
    } 
       
     /**
     * Getter for property partner.
     * @return Value of property specialcondtiotions.
     */
    public java.lang.String getSpecialConditions () {
        return SpecialConditions;    
    }
    public void setSpecialConditions(java.lang.String SpecialConditions) {
        this.SpecialConditions = SpecialConditions;
    }
     /**
     * Getter for property agreementcondtiotion.
     * @return Value of property agreementcondtiotion.
     */
    public java.lang.String getAgreementCondition () {
        return AgreementCondition;    
    }
    public void setAgreementCondition(java.lang.String AgreementCondition) {
        this.AgreementCondition = AgreementCondition;
    }
    
        /**
     * Getter for property AppointSupplierOrgno.
     * @return Value of property AppointSupplierOrgno.
     */
    
    public java.lang.String getAppointSupplierOrgno () {
        return AppointSupplierOrgNo;    
    }
    public void setAppointSupplierOrgno(java.lang.String AppointSupplierOrgNo) {
        this.AppointSupplierOrgNo = AppointSupplierOrgNo;
    }    
    

    /**
     * Getter for property customer.
     * @return Value of property customer.
     */
    public se.mpab.psi.bo.Customer getCustomer() {
        return customer;
    }
    

    
    /**
     * Getter for property dealNo.
     * @return Value of property dealNo.
     */
    public long getDealNo() {
        return dealNo;
    }
    
    /**
     * Setter for property dealNo.
     * @param dealNo New value of property dealNo.
     */
    public void setDealNo(long dealNo) {
        this.dealNo = dealNo;
    }
    
    /**
     * Getter for property duration.
     * @return Value of property duration.
     */
    public int getDuration() {
        return duration;
    }
    
    /**
     * Setter for property duration.
     * @param duration New value of property duration.
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }
      
    public java.lang.String getUNID() {
        return UNID;
    }
    
    /**
     * Setter for property UNID.
     * @param UNID New value of property UNID.
     */
    public void setUNID(java.lang.String UNID) {
        this.UNID = UNID;
    }
    

    public int getVersion() {
        return version;
    }
    
    public void setVersion(int version) {
        this.version = version;
    }
    

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
    
    public boolean getEsignEnabled() {
        return this.esignEnabled;
    }

    public void setEsignEnabled(boolean enabled) {
        this.esignEnabled = enabled;
    }
}
