// Copyright Marquardt & Partners
/*
 * omer.java
 *
 * Created on den 3 september 2004, 14:03
 */

package se.mpab.psi.bo;

import se.mpab.psi.mo.*;
import org.apache.log4j.LogManager;
/**
 *
 * @author  janne.berg
 */
public class Customer {
    
    private String type;
    private String orgNo;
    private String name;
    private String coName;
    private String phone;
    private String address;
    private String zipCode;
    private String city;
    private String contactName;
    private String contactPhone;
    private String contactEmail;
    private String contactFax;
    private String billCOName;
    private String instAddress;
    private String billAddress;
    private String instCity;
    private String billZipCode;
    private String billCity;
    private String instReference;
    private boolean verified;
    private String riskPercent;
    private String UNID;
    private String seat;
    private String externalReference;
    
    /** Creates a new instance of omer */
    private static org.apache.log4j.Logger logger = LogManager.getLogger(Customer.class);
    public Customer(EntreMessage message) {
        this.setName("EPIS generic Customer");
        this.setOrgNo(message.getBody().getCreditApplication().getClient().getIdentificationNumber());
        
    }
    public Customer(){
        this.setName("EPIS generic Customer");
    }
    
    /**
     * Getter for property address.
     * @return Value of property address.
     */
    public java.lang.String getAddress() {
        return address;
    }
    
    /**
     * Setter for property address.
     * @param address New value of property address.
     */
    public void setAddress(java.lang.String address) {
        this.address = address;
    }
    
    /**
     * Getter for property billAddress.
     * @return Value of property billAddress.
     */
    public java.lang.String getBillAddress() {
        return billAddress;
    }
    
    /**
     * Setter for property billAddress.
     * @param billAddress New value of property billAddress.
     */
    public void setBillAddress(java.lang.String billAddress) {
        this.billAddress = billAddress;
    }
    
    /**
     * Getter for property billCity.
     * @return Value of property billCity.
     */
    public java.lang.String getBillCity() {
        return billCity;
    }
    
    /**
     * Setter for property billCity.
     * @param billCity New value of property billCity.
     */
    public void setBillCity(java.lang.String billCity) {
        this.billCity = billCity;
    }
    
    /**
     * Getter for property billCOName.
     * @return Value of property billCOName.
     */
    public java.lang.String getBillCOName() {
        return billCOName;
    }
    
    /**
     * Setter for property billCOName.
     * @param billCOName New value of property billCOName.
     */
    public void setBillCOName(java.lang.String billCOName) {
        this.billCOName = billCOName;
    }
    
    /**
     * Getter for property billZipCode.
     * @return Value of property billZipCode.
     */
    public java.lang.String getBillZipCode() {
        return billZipCode;
    }
    
    /**
     * Setter for property billZipCode.
     * @param billZipCode New value of property billZipCode.
     */
    public void setBillZipCode(java.lang.String billZipCode) {
        this.billZipCode = billZipCode;
    }
    
    /**
     * Getter for property city.
     * @return Value of property city.
     */
    public java.lang.String getCity() {
        return city;
    }
    
    /**
     * Setter for property city.
     * @param city New value of property city.
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }
    
    /**
     * Getter for property coName.
     * @return Value of property coName.
     */
    public java.lang.String getCoName() {
        return coName;
    }
    
    /**
     * Setter for property coName.
     * @param coName New value of property coName.
     */
    public void setCoName(java.lang.String coName) {
        this.coName = coName;
    }
    
    /**
     * Getter for property contactEmail.
     * @return Value of property contactEmail.
     */
    public java.lang.String getContactEmail() {
        return contactEmail;
    }
    
    /**
     * Setter for property contactEmail.
     * @param contactEmail New value of property contactEmail.
     */
    public void setContactEmail(java.lang.String contactEmail) {
        this.contactEmail = contactEmail;
    }
    
    /**
     * Getter for property contactFax.
     * @return Value of property contactFax.
     */
    public java.lang.String getContactFax() {
        return contactFax;
    }
    
    /**
     * Setter for property contactFax.
     * @param contactFax New value of property contactFax.
     */
    public void setContactFax(java.lang.String contactFax) {
        this.contactFax = contactFax;
    }
    
    /**
     * Getter for property contactName.
     * @return Value of property contactName.
     */
    public java.lang.String getContactName() {
        return contactName;
    }
    
    /**
     * Setter for property contactName.
     * @param contactName New value of property contactName.
     */
    public void setContactName(java.lang.String contactName) {
        this.contactName = contactName;
    }
    
    /**
     * Getter for property contactPhone.
     * @return Value of property contactPhone.
     */
    public java.lang.String getContactPhone() {
        return contactPhone;
    }
    
    /**
     * Setter for property contactPhone.
     * @param contactPhone New value of property contactPhone.
     */
    public void setContactPhone(java.lang.String contactPhone) {
        this.contactPhone = contactPhone;
    }
    
    /**
     * Getter for property instAddress.
     * @return Value of property instAddress.
     */
    public java.lang.String getInstAddress() {
        return instAddress;
    }
    
    /**
     * Setter for property instAddress.
     * @param instAddress New value of property instAddress.
     */
    public void setInstAddress(java.lang.String instAddress) {
        this.instAddress = instAddress;
    }
    
    /**
     * Getter for property instCity.
     * @return Value of property instCity.
     */
    public java.lang.String getInstCity() {
        return instCity;
    }
    
    /**
     * Setter for property instCity.
     * @param instCity New value of property instCity.
     */
    public void setInstCity(java.lang.String instCity) {
        this.instCity = instCity;
    }
    
    /**
     * Getter for property instReference.
     * @return Value of property instReference.
     */
    public java.lang.String getInstReference() {
        return instReference;
    }
    
    /**
     * Setter for property instReference.
     * @param instReference New value of property instReference.
     */
    public void setInstReference(java.lang.String instReference) {
        this.instReference = instReference;
    }
    
    /**
     * Getter for property name.
     * @return Value of property name.
     */
    public java.lang.String getName() {
        return name;
    }
    
    /**
     * Setter for property name.
     * @param name New value of property name.
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }
    
    /**
     * Getter for property orgNo.
     * @return Value of property orgNo.
     */
    public java.lang.String getOrgNo() {
        return orgNo;
    }
    
    /**
     * Setter for property orgNo.
     * @param orgNo New value of property orgNo.
     */
    public void setOrgNo(java.lang.String orgNo) {
        this.orgNo = orgNo;
    }
    
    /**
     * Getter for property phone.
     * @return Value of property phone.
     */
    public java.lang.String getPhone() {
        return phone;
    }
    
    /**
     * Setter for property phone.
     * @param phone New value of property phone.
     */
    public void setPhone(java.lang.String phone) {
        this.phone = phone;
    }
    
    /**
     * Getter for property riskPercent.
     * @return Value of property riskPercent.
     */
    public java.lang.String getRiskPercent() {
        return riskPercent;
    }
    
    /**
     * Setter for property riskPercent.
     * @param riskPercent New value of property riskPercent.
     */
    public void setRiskPercent(java.lang.String riskPercent) {
        this.riskPercent = riskPercent;
    }
    
    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public java.lang.String getType() {
        return type;
    }
    
    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }
    
    /**
     * Getter for property verified.
     * @return Value of property verified.
     */
    public boolean isVerified() {
        return verified;
    }
    
    /**
     * Setter for property verified.
     * @param verified New value of property verified.
     */
    public void setVerified(boolean verified) {
        this.verified = verified;
    }
    
    /**
     * Getter for property zipCode.
     * @return Value of property zipCode.
     */
    public java.lang.String getZipCode() {
        return zipCode;
    }
    
    /**
     * Setter for property zipCode.
     * @param zipCode New value of property zipCode.
     */
    public void setZipCode(java.lang.String zipCode) {
        this.zipCode = zipCode;
    }
    
    /**
     * Getter for property UNID.
     * @return Value of property UNID.
     */
    public java.lang.String getUNID() {
        return UNID;
    }
    
    /**
     * Setter for property UNID.
     * @param UNID New value of property UNID.
     */
    public void setUNID(java.lang.String UNID) {
        this.UNID = UNID;
    }
    
    /**
     * Getter for property seat.
     * @return Value of property seat.
     */
    public java.lang.String getSeat() {
        return seat;
    }
    
    /**
     * Setter for property seat.
     * @param seat New value of property seat.
     */
    public void setSeat(java.lang.String seat) {
        this.seat = seat;
    }
    
        /**
     * Getter for property externalReference.
     * @return Value of property externalReference.
     */
    public java.lang.String getExternalReference() {
        return externalReference;
    }
    
    /**
     * Setter for property externalReference.
     * @param externalReference New value of property externalReference.
     */
    public void setExternalReference(java.lang.String externalReference) {
        this.externalReference = externalReference;
    }
    
}
