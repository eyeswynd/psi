/*
 * Credit.java
 *
 * Created on den 3 september 2004, 13:39
 */
package se.mpab.psi.bo;

import java.util.Vector;
import java.util.Date;
import se.mpab.psi.mo.*;
import se.mpab.psi.*;
import se.mpab.psi.db.*;
import se.mpab.util.common.PropertiesSetup;
import org.apache.log4j.LogManager;
import org.apache.log4j.Level;
import java.util.Properties;
import se.mpab.util.common.PropertiesSetup;
import se.mpab.util.common.PropertiesSetupException;

/**
 *
 * @author  janne.berg
 */
public class Credit {

    private double addToFirstPayment;
    private Date autoDecisionTime;
    private String autoDecision;
    private String autoDecisionSource;
    private String autoRiskPrognosisCode;
    private String autoReason;
    private String creditTemplate;
    private String creditQueue;
    private String comments;
    private int custVerified;
    private String conditionCode;
    private String conditionDescription;
    private String history;
    private String internalComments;
    private String manualDecision;
    private String manualReason;
    private String manualDecisionBy;
    private Date manualDecisionTime;
    private Customer customer;
    private String UNID;
    private Vector guarantors;
    private double downPayment;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(Credit.class);
    private String stock;
    /*
     *TODO
     * Since the model in Entre is so full of double data  wee use this variables for now, we need to make
     *a better solution in the future. this should be taken care of the DAO objects
     */
    private double financedAmount;
    private int duration;
    private long dealNo;
    private int version;
    private int validDays;

    /**
     * Get the value of validDays
     *
     * @return the value of validDays
     */
    public int getValidDays() {
        return validDays;
    }

    /**
     * Set the value of validDays
     *
     * @param validDays new value of validDays
     */
    public void setValidDays(int validDays) {
        this.validDays = validDays;
    }

    /** Creates a new instance of Credit */
    public Credit() {
    }

    /** Creates a new instance of Credit */
    public Credit(long dealNo, int version) throws Exception {
        setDealNo(dealNo);
        this.setVersion(version);
    }

    public Date getDecisionTime() {
        // Helper method to get either manual or auto decision time
        if (manualDecisionIsEmpty()) {
            // Use auto decision
            return this.getAutoDecisionTime();
        } else {
            // Use manual decision
            return this.getManualDecisionTime();
        }
    }

    public String getDecision() {
        //  logger.setLevel(Level.DEBUG);
        String decision = PSIProperties.PENDING_TXT;
        logger.debug("autoDecision: " + getAutoDecision() + " and manualDecision: " + getManualDecision());
        //Auto Approved or DENIED
        if ((getAutoDecision().equals(PSIProperties.APPROVED_TXT) ||
                getAutoDecision().equals(PSIProperties.DENIED_TXT)) && manualDecisionIsEmpty()) {
            decision = getAutoDecision();

        } //Manual Decision
        else { //if ( getAutoDecision().trim().equalsIgnoreCase(PSIProperties.MANUAL_DECISION_TXT)) {
            //Manual decison not made yet
            if (manualDecisionIsEmpty()) {
                logger.debug("manualDecision is empty ");
                decision = PSIProperties.PENDING_TXT;
            } //Manual Decision made
            else if (getManualDecision().equals(PSIProperties.APPROVED_TXT) ||
                    getManualDecision().equals(PSIProperties.DENIED_TXT) ||
                    getManualDecision().equals(PSIProperties.APPROVED_WITH_CONDITION_TXT)) {
                logger.debug("set decsion to manualDecision");
                decision = getManualDecision();
            } //Manual decision is consideration, taken away because of patch in CreditDAO 2005-09-09
            // This is temporary fix for NO
            else if (getManualDecision().equals(PSIProperties.POSTPONED_TXT)) {
                logger.debug("decision is set to pending req info");
                decision = PSIProperties.PENDING_INFOREQUIRED_TXT;
            } else {
                logger.debug("decision is set to pending text");
                decision = PSIProperties.PENDING_TXT;
            }

        }
        return decision;
    }

    public boolean manualDecisionIsEmpty() {
        if (manualDecision == null) {
            return true;
        }
        if (manualDecision.equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    public boolean conditionCodeIsEmpty() {
        if (conditionCode == null) {
            return true;
        }
        if (conditionCode.equals("")) {
            return true;
        }
        return false;
    }

    /**
     * Getter for property conditionCode.
     * @return Value of property conditionCode.
     */
    public java.lang.String getConditionCode() {
        if (conditionCodeIsEmpty()) {
            return PSIProperties.NO_CONDITION_TXT;
        } else {
            return conditionCode;
        }

    }

    /**
     * Getter for property conditionCode.
     * @return Value of property conditionCode.
     */
    //TODO what the hell is this
    public java.lang.String getConditionText() {
        try {

            if (conditionCodeIsEmpty()) {
                return PSIProperties.NO_CONDITION_TXT;
            } else {
                return this.conditionDescription;
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return getConditionCode();
        }

    }

    /**
     * Getter for property autoDecision.
     * @return Value of property autoDecision.
     */
    public java.lang.String getAutoDecision() {
        return autoDecision;
    }

    /**
     * Setter for property autoDecision.
     * @param autoDecision New value of property autoDecision.
     */
    public void setAutoDecision(java.lang.String autoDecision) {
        this.autoDecision = autoDecision;
    }

    /**
     * Getter for property autoDecisionSource.
     * @return Value of property autoDecisionSource.
     */
    public java.lang.String getAutoDecisionSource() {
        return autoDecisionSource;
    }

    /**
     * Setter for property autoDecisionSource.
     * @param autoDecisionSource New value of property autoDecisionSource.
     */
    public void setAutoDecisionSource(java.lang.String autoDecisionSource) {
        this.autoDecisionSource = autoDecisionSource;
    }

    /**
     * Getter for property autoDecisionTime.
     * @return Value of property autoDecisionTime.
     */
    public java.util.Date getAutoDecisionTime() {
        return autoDecisionTime;
    }

    /**
     * Setter for property autoDecisionTime.
     * @param autoDecisionTime New value of property autoDecisionTime.
     */
    public void setAutoDecisionTime(java.util.Date autoDecisionTime) {
        this.autoDecisionTime = autoDecisionTime;
    }

    /**
     * Getter for property autoReason.
     * @return Value of property autoReason.
     */
    public java.lang.String getAutoReason() {
        return autoReason;
    }

    /**
     * Setter for property autoReason.
     * @param autoReason New value of property autoReason.
     */
    public void setAutoReason(java.lang.String autoReason) {
        this.autoReason = autoReason;
    }

    /**
     * Getter for property autoRiskPrognosisCode.
     * @return Value of property autoRiskPrognosisCode.
     */
    public java.lang.String getAutoRiskPrognosisCode() {
        return autoRiskPrognosisCode;
    }

    /**
     * Setter for property autoRiskPrognosisCode.
     * @param autoRiskPrognosisCode New value of property autoRiskPrognosisCode.
     */
    public void setAutoRiskPrognosisCode(java.lang.String autoRiskPrognosisCode) {
        this.autoRiskPrognosisCode = autoRiskPrognosisCode;
    }

    /**
     * Getter for property comments.
     * @return Value of property comments.
     */
    public java.lang.String getComments() {
        return comments;
    }

    /**
     * Setter for property comments.
     * @param comments New value of property comments.
     */
    public void setComments(java.lang.String comments) {
        this.comments = comments;
    }

    /**
     * Setter for property conditionCode.
     * @param conditionCode New value of property conditionCode.
     */
    public void setConditionCode(java.lang.String conditionCode) {
        this.conditionCode = conditionCode;
    }

    /**
     * Getter for property creditQueue.
     * @return Value of property creditQueue.
     */
    public java.lang.String getCreditQueue() {
        return creditQueue;
    }

    /**
     * Setter for property creditQueue.
     * @param creditQueue New value of property creditQueue.
     */
    public void setCreditQueue(java.lang.String creditQueue) {
        this.creditQueue = creditQueue;
    }

    /**
     * Getter for property creditTemplate.
     * @return Value of property creditTemplate.
     */
    public java.lang.String getCreditTemplate() {
        return creditTemplate;
    }

    /**
     * Setter for property creditTemplate.
     * @param creditTemplate New value of property creditTemplate.
     */
    public void setCreditTemplate(java.lang.String creditTemplate) {
        this.creditTemplate = creditTemplate;
    }

    /**
     * Getter for property customer.
     * @return Value of property customer.
     */
    public se.mpab.psi.bo.Customer getCustomer() {
        return customer;
    }

    /**
     * Setter for property customer.
     * @param customer New value of property customer.
     */
    public void setCustomer(se.mpab.psi.bo.Customer customer) {
        this.customer = customer;
    }

    /**
     * Getter for property custVerified.
     * @return Value of property custVerified.
     */
    public int getCustVerified() {
        return custVerified;
    }

    /**
     * Setter for property custVerified.
     * @param custVerified New value of property custVerified.
     */
    public void setCustVerified(int custVerified) {
        this.custVerified = custVerified;
    }

    /**
     * Getter for property dealNo.
     * @return Value of property dealNo.
     */
    public long getDealNo() {
        return dealNo;
    }

    /**
     * Setter for property dealNo.
     * @param dealNo New value of property dealNo.
     */
    public void setDealNo(long dealNo) {
        this.dealNo = dealNo;
    }

    /**
     * Getter for property duration.
     * @return Value of property duration.
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Setter for property duration.
     * @param duration New value of property duration.
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Getter for property financedAmount.
     * @return Value of property financedAmount.
     */
    public double getFinancedAmount() {
        return financedAmount;
    }

    /**
     * Setter for property financedAmount.
     * @param financedAmount New value of property financedAmount.
     */
    public void setFinancedAmount(double financedAmount) {
        this.financedAmount = financedAmount;
    }

    /**
     * Getter for property history.
     * @return Value of property history.
     */
    public java.lang.String getHistory() {
        return history;
    }

    /**
     * Setter for property history.
     * @param history New value of property history.
     */
    public void setHistory(java.lang.String history) {
        this.history = history;
    }

    /**
     * Getter for property internalComments.
     * @return Value of property internalComments.
     */
    public java.lang.String getInternalComments() {
        return internalComments;
    }

    /**
     * Setter for property internalComments.
     * @param internalComments New value of property internalComments.
     */
    public void setInternalComments(java.lang.String internalComments) {
        this.internalComments = internalComments;
    }

    /**
     * Getter for property manualDecision.
     * @return Value of property manualDecision.
     */
    public java.lang.String getManualDecision() {
        return manualDecision;
    }

    /**
     * Setter for property manualDecision.
     * @param manualDecision New value of property manualDecision.
     */
    public void setManualDecision(java.lang.String manualDecision) {
        this.manualDecision = manualDecision;
    }

    /**
     * Getter for property manualDecisionBy.
     * @return Value of property manualDecisionBy.
     */
    public java.lang.String getManualDecisionBy() {
        return manualDecisionBy;
    }

    /**
     * Setter for property manualDecisionBy.
     * @param manualDecisionBy New value of property manualDecisionBy.
     */
    public void setManualDecisionBy(java.lang.String manualDecisionBy) {
        this.manualDecisionBy = manualDecisionBy;
    }

    /**
     * Getter for property manualDecisionTime.
     * @return Value of property manualDecisionTime.
     */
    public java.util.Date getManualDecisionTime() {
        return manualDecisionTime;
    }

    /**
     * Setter for property manualDecisionTime.
     * @param manualDecisionTime New value of property manualDecisionTime.
     */
    public void setManualDecisionTime(java.util.Date manualDecisionTime) {
        this.manualDecisionTime = manualDecisionTime;
    }

    /**
     * Getter for property manualReason.
     * @return Value of property manualReason.
     */
    public java.lang.String getManualReason() {
        return manualReason;
    }

    /**
     * Setter for property manualReason.
     * @param manualReason New value of property manualReason.
     */
    public void setManualReason(java.lang.String manualReason) {
        this.manualReason = manualReason;
    }

    /**
     * Getter for property UNID.
     * @return Value of property UNID.
     */
    public java.lang.String getUNID() {
        return UNID;
    }

    /**
     * Setter for property UNID.
     * @param UNID New value of property UNID.
     */
    public void setUNID(java.lang.String UNID) {
        this.UNID = UNID;
    }

    /**
     * Getter for property version.
     * @return Value of property version.
     */
    public int getVersion() {
        return version;
    }

    /**
     * Setter for property version.
     * @param version New value of property version.
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Getter for property downPayment.
     * @return Value of property downPayment.
     */
    public double getDownPayment() {
        return downPayment;
    }

    /**
     * Setter for property downPayment.
     * @param downPayment New value of property downPayment.
     */
    public void setDownPayment(double downPayment) {
        this.downPayment = downPayment;
    }

    public String getConditionDescription() {
        return conditionDescription;
    }

    public void setConditionDescription(String conditionDescription) {
        this.conditionDescription = conditionDescription;
    }

    public Vector getGuarantors() {
        return guarantors;
    }

    public void setGuarantors(Vector guarantors) {
        this.guarantors = guarantors;
    }

    public double getAddToFirstPayment() {
        return addToFirstPayment;
    }

    public void setAddToFirstPayment(double addToFirstPayment) {
        this.addToFirstPayment = addToFirstPayment;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }
}
