/*
 * SaveToFile.java
 *
 * Created on den 13 januari 2005, 12:59
 */

package se.mpab.psi;

import se.mpab.psi.PSIProperties;
import se.mpab.util.common.PropertiesSetup;
import se.mpab.util.common.DateTimeHandler;
import java.util.Properties;

import java.io.StringReader;
import java.io.Serializable;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.LogManager;



/**
 *
 * @author  Viktor Norberg, Marquardt & Partners AB
 */

public class PSIMessageSave {
    
    private static PSIMessageSave psiMessageSave = null;
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(PSIMessageSave.class);
    
    private boolean incoming; // TRUE = incoming, FALSE = outgoing
    
    private String incomingDir;
    
    private String outgoingDir;
    
    private String stock;
    
    private String message;
    
    private String username;
    
    private String timestamp;
    
    /** Creates a new instance of SaveToFile */
    public PSIMessageSave() {
        // Create timestamp for identification
        
        DateTimeHandler dth = new DateTimeHandler();
        this.timestamp = dth.getTimeStamp(dth.YYYYMMDD) + "_" + dth.getTimeStamp(dth.HHMMSSCC);
        
        logger.debug("Timestamp for request: <" + this.timestamp +">");        
        try {
            Properties prop = PSIProperties.getProp();
            if (PropertiesSetup.propertyExists("incomingDir", prop)) {
                this.incomingDir = PropertiesSetup.getString("incomingDir", prop);
                //logger.debug("incoming dir: <" + this.incomingDir +">");
            }
            if (PropertiesSetup.propertyExists("outgoingDir", prop)) {
                this.outgoingDir= PropertiesSetup.getString("outgoingDir", prop);
                //logger.debug("outgoing dir: <" + this.outgoingDir +">");
            }
        } catch (Exception e) {
           logger.error(e.getMessage(),e);
        }

    }    
    
    public void save(boolean incoming, String message, String stock, String username) {        
        
        this.setIncoming(incoming);
        this.setMessage(message);        
        this.setStock(stock);
        this.setUsername(username);
        writeToFile();
        
    }
    
    public void writeToFile() {
        DateTimeHandler dth = new DateTimeHandler();
        java.util.Calendar cal = new java.util.GregorianCalendar();
        
        String datedirPath =  Integer.toString(cal.get(java.util.Calendar.YEAR)) + java.io.File.separatorChar  +
        Integer.toString(cal.get(java.util.Calendar.MONTH )+1)+java.io.File.separatorChar  +
        Integer.toString(cal.get(java.util.Calendar.DATE ));
        
        String fileName = username + " " + dth.getTimeStamp(dth.YYYYMMDD) + "_"
        + dth.getTimeStamp(dth.HHMMSSCC) + ".xml";
        
        java.io.File outgoingFileDir = new java.io.File(  outgoingDir + java.io.File.separatorChar +
        stock + java.io.File.separatorChar +
        datedirPath  );        
              
        try {
            boolean exists = outgoingFileDir.exists();
            if (!exists) {
                outgoingFileDir.mkdirs();
            }
            BufferedWriter out = new BufferedWriter(new FileWriter(outgoingFileDir.getAbsolutePath() +  java.io.File.separatorChar +  fileName));
            out.write(message);
            out.close();
        } catch (IOException e) {
            logger.warn(this.getClass().getName() + ", Error while saving request to file <"+ incomingDir + fileName+">",e);
            logger.error(e.getMessage(), e);
        }
    }
       

    /**
     * Getter for property outgoingDir.
     * @return Value of property outgoingDir.
     */
    public java.lang.String getOutgoingDir() {
        return outgoingDir;
    }
    
    /**
     * Setter for property outgoingDir.
     * @param outgoingDir New value of property outgoingDir.
     */
    public void setOutgoingDir(java.lang.String outgoingDir) {
        this.outgoingDir = outgoingDir;
    }

    /**
     * Getter for property incomingDir.
     * @return Value of property incomingDir.
     */
    public java.lang.String getIncomingDir() {
        return incomingDir;
    }
    
    /**
     * Setter for property incomingDir.
     * @param incomingDir New value of property incomingDir.
     */
    public void setIncomingDir(java.lang.String incomingDir) {
        this.incomingDir = incomingDir;
    }
  
    /**
     * Getter for property message.
     * @return Value of property message.
     */
    public String getMessage() {
        return message;
    }
    
    /**
     * Setter for property message.
     * @param message New value of property message.
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    /**
     * Getter for property stock.
     * @return Value of property stock.
     */
    public String getStock() {
        return stock;
    }
    
    /**
     * Setter for property stock.
     * @param stock New value of property stock.
     */
    public void setStock(String stock) {
        this.stock = stock;
    }
    
    /**
     * Getter for property username.
     * @return Value of property username.
     */
    public java.lang.String getUsername() {
        return username;
    }
    
    /**
     * Setter for property username.
     * @param username New value of property username.
     */
    public void setUsername(java.lang.String username) {
        this.username = username;
    }
    
    /**
     * Getter for property timestamp.
     * @return Value of property timestamp.
     */
    public java.lang.String getTimestamp() {
        return timestamp;
    }    
  
    /**
     * Setter for property timestamp.
     * @param timestamp New value of property timestamp.
     */
    public void setTimestamp(java.lang.String timestamp) {
        this.timestamp = timestamp;
    }
    
    /**
     * Getter for property direction.
     * @return Value of property direction.
     */
    public boolean isIncoming() {
        return incoming;
    }
    
    /**
     * Setter for property direction.
     * @param direction New value of property direction.
     */
    public void setIncoming(boolean incoming) {
        this.incoming = incoming;
    }
    
}
