// Copyright Marquardt & Partners
/*
 * RequestAuthentication.java
 *
 * Created on den 26 augusti 2004, 12:35
 */
package se.mpab.psi;

import java.io.*;
import java.util.List;
import java.util.Iterator;
import java.util.Properties;
import org.apache.log4j.LogManager;
import se.mpab.util.common.PropertiesSetup;
import se.mpab.psi.bo.User;
import se.mpab.psi.db.UserDAO;
import se.mpab.psi.mo.EntreMessage;

/**
 * Authentication of User, Password and IP-number
 * @author Viktor Norberg
 * @version 1
 */
public class RequestAuthentication {
    
    
    private String allowedIpFile = null;
    private String allowedUsersFile = null;
    
    private String ip = null;
    private EntreMessage message = null;
    
    private User user = null;
    
    // For properties functionality
    private Properties prop;
    
    
    private Status status = null;
    private static org.apache.log4j.Logger logger = LogManager.getLogger(RequestAuthentication.class);
    
    /** Creates a new instance of RequestAuthentication */
    public RequestAuthentication(String ip, EntreMessage message) {
        this.ip = ip;
        this.message = message;
        
        try {
            prop = PSIProperties.getProp();
            if (PropertiesSetup.propertyExists("EPISAllowedIP", prop)) {
                this.allowedIpFile  = PropertiesSetup.getString("EPISAllowedIP", prop);
                logger.debug("Allowed IP File: <"+ this.allowedIpFile +">");
            }
            if (PropertiesSetup.propertyExists("EPISAllowedUsers", prop)) {
                this.allowedUsersFile  = PropertiesSetup.getString("EPISAllowedUsers", prop);
                logger.debug("Allowed User File <" + this.allowedUsersFile+">");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
        
        
    }
    public boolean authenticateRequest() throws Exception {
        
        // Authenticate user in DB
        this.authUser();
        this.authIP();
        return true;
    }
    
    private boolean authUser() throws Exception {
        
        boolean returnBool = false;
        
        // First try to fetch user from DB
        UserDAO userDAO= new UserDAO(this.message.getBody().getCreditApplication().getClient().getSeat());
        this.user = userDAO.findByName(this.message.getHeader().getUser());
       
        logger.debug("Allowed IP file to use: <" + this.allowedIpFile +">");
        org.jdom.input.SAXBuilder builder = new org.jdom.input.SAXBuilder();
        File tmpFile = new File(this.allowedUsersFile);
        org.jdom.Document jdomDoc = builder.build(tmpFile);
        
        // Cort items to Elements
        logger.info("Fetch elements and analyse User");
        org.jdom.Element el = jdomDoc.getRootElement();
        List l = el.getChildren("user");
        
        // Loop though list of IPs and check if OK
        Iterator it = l.iterator();
        org.jdom.Element tmpEl;

        while (it.hasNext()) {
            tmpEl = (org.jdom.Element) it.next();
            if (tmpEl.getTextTrim().equals(this.user.getUNID())) {
                logger.info("user <" + this.user.getName() + "> is OK");
                returnBool = true;
            }
        }
        
        if (returnBool == false) {
            throw new AutenticationException(Status.STATUS_USER_NOT_FOUND);
        }
        
        
        return returnBool;
        
        
        
    }
    private boolean authIP() throws Exception {
        
        // Validate from allowedIpXML
        boolean returnBool = false;

        // Read the file
        logger.debug("Read and build xml doc " + this.allowedIpFile);
        org.jdom.input.SAXBuilder builder = new org.jdom.input.SAXBuilder();
        File tmpFile = new File(this.allowedIpFile);
        org.jdom.Document jdomDoc = builder.build(tmpFile);
        
        // Cort items to Elements
        logger.info("Fetch elements and analyse IP");
        org.jdom.Element el = jdomDoc.getRootElement();
        List l = el.getChildren("IP");
        
        // Loop though list of IPs and check if OK
        Iterator it = l.iterator();
        org.jdom.Element tmpEl;

        while (it.hasNext()) {
            tmpEl = (org.jdom.Element) it.next();
            if (tmpEl.getTextTrim().equals(this.ip)) {
                logger.info("IP <" + this.ip + "> is OK");
                returnBool = true;
            }
        }
        if (returnBool == false) {
            throw new AutenticationException(Status.STATUS_IP_NOT_ALLOWED);
        }
        
        return returnBool;
    }
    
}
