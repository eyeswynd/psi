/*
 * PSIDBUtils.java
 *
 * Created on den 22 september 2006, 14:43
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package se.mpab.psi.util;

import de.bea.domingo.DDocument;
import java.util.Calendar;
import org.apache.log4j.LogManager;
import se.mpab.psi.db.CreditDAO;

/**
 *
 * @author viktor.norberg
 */
public class PSIDBUtils {
    
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(CreditDAO.class);
    
    /** Creates a new instance of PSIDBUtils */
    public PSIDBUtils() {
    }
    
    public static Calendar getEntreDecisionTime(DDocument credit) {
        Calendar decisionTime;
        if (credit.getItemValueString("CR_ManualDecision") == null ||
                credit.getItemValueString("CR_ManualDecision").equals("")) {
            // Use AutoDecisionDate
            decisionTime = credit.getItemValueDate("CR_AutoDecisionTime");
        } else {
            // Use ManualDecisionDate
            decisionTime = credit.getItemValueDate("CR_ManualDecisionTime");
        }
        
        return decisionTime;
    }
    
}
