/*
 * PSIUtils.java
 *
 * Created on den 22 september 2006, 11:02
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package se.mpab.psi.util;

import de.bea.domingo.DDocument;
import de.bea.domingo.DItem;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import se.mpab.psi.bo.Customer;
import se.mpab.psi.db.DataException;
import se.mpab.psi.mo.Duration;
import se.mpab.psi.mo.Equipment;

/**
 * Utility functions thats PSI specific. Mostly its for creating ocr fetching data in a Domino structured
 * world
 * @author Janne Berg , Marquardt & Partners
 */
public class PSIUtils {
    
    private static org.apache.log4j.Logger logger = LogManager.getLogger(PSIUtils.class);
    
    public PSIUtils() {
    }
    
    
    public static Integer getPeriodLength(Duration duration){
        Integer i ;
        if( duration.getUnit().equalsIgnoreCase("MONTH") ) {
            i = new Integer(1);
        }else if( duration.getUnit().equalsIgnoreCase("QUARTER") ){
            i = new Integer(3);
        }else{
            //TODO error handling
            i = new Integer(3);
        }
        return i;
    }
    public static Integer getDurationInMonths(Duration duration)throws DataException{
        Integer i ;
        if( duration.getUnit().equalsIgnoreCase("MONTH") ) {
            i = new Integer(1*duration.getValue());
        }else if( duration.getUnit().equalsIgnoreCase("QUARTER") ){
            i = new Integer(3*duration.getValue());
        }else{
            //TODO error handling
            throw new DataException("Error ,illegal duration type");
            //i = new Integer(3*duration.getValue());
        }
        return i;
    }
    
    
    
    public static String getCurrency(Vector equipment){
        String currency = "";
        for (Iterator it=equipment.iterator(); it.hasNext(); ) {
            Equipment eqRow =(Equipment) it.next();
            //TODO must check i there is different currencies on different rows
            currency = eqRow.getAmount().getCurrency();
        }
        
        return currency;
    }
    public static String getStringFromMultivalue(List textField){
        String text = "";
        for (Iterator it=textField.iterator(); it.hasNext(); ) {
            String row =(String) it.next();
            text +=row + "\n";
        }
        return text;
        
    }
    
    
    public static String formatOrgNo(Customer customer){
        String localizedOrgNo = customer.getOrgNo();
        if (customer.getSeat().equals("SE")  ){
            if (localizedOrgNo.indexOf("-")< 0){
                StringBuffer buf = new StringBuffer(localizedOrgNo);
                buf = buf.insert(6,"-");
                localizedOrgNo = buf.toString();
            }
        }
        return localizedOrgNo;
    }
    
    
    public static void logAllFields(DDocument doc){
        logger.setLevel(Level.INFO);
        Level level = logger.getLevel();
        Iterator items = doc.getItems();
        while (items.hasNext()) {
            //   logger.info("******");
            DItem item = (DItem)items.next();
            //  logger.info("------ [" +  item.getName() + "] -- [" +  item.getText() + "] -------");
            logger.info("------ " +  item.getName() + " -- [" +  item.getValueString() + "] -------");
        }
        logger.setLevel(level);
    }
    
        /* Todo implement this if we need to make EUR deal through PSI later on
    private Vector getCurrencyOptions(String country){
        Properties prop = PSIProperties.getProp();
        Vector options = new Vector();
        String currencyOptions = "";
        if (PropertiesSetup.propertyExists(country.toUpperCase() + "CurrencyOptions", prop)) {
            templateDirectory = PropertiesSetup.getString(country.toUpperCase() + "CurrencyOptions", prop);
        } else{
            logger.warn("Cant find document template directory using default");
            templateDirectory = "e:/PSI/templates";
        }
        return options;
         
    }
         */
    
    
}
