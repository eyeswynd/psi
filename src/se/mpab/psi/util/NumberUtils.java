/*
 * NumberUtils.java
 *
 * Created on den 15 september 2006, 10:06
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package se.mpab.psi.util;

/**
 *
 * @author janne.berg
 */
public class NumberUtils {
    
    /** Creates a new instance of NumberUtils */
    public NumberUtils() {
    }
    
    public static String getFractionFreeString(double doublevalue){
        return Long.toString((long)doublevalue);
    }
    
}
