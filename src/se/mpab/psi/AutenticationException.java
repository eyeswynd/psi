/*
 * AutenticationException.java
 *
 * Created on den 24 september 2004, 10:55
 */

package se.mpab.psi;

/**
 *
 * @author  vino42
 */

public class AutenticationException extends Exception {
    private String message;
    private int code ;
    
    /** Creates a new instance of AutenticationException */
    public AutenticationException(int status) {
      this(Status.getNameByStatus(status),status) ;
    }
    public AutenticationException(String message, int code){
        this.message = message;
        this.code = code;
    }
    public String getMessage() {
        return this.message;
    }
    
    public int getCode() {
        return this.code;        
    }
}
