/*
 * PSIValidator.java
 *
 * Created on den 4 november 2005, 09:09
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package se.mpab.psi;

import se.mpab.psi.exceptions.DataException;
//import se.mpab.epis.Status;

/**
 *
 * @author viktor.norberg
 */
public class PSIValidator {

    /** Creates a new instance of PSIValidator */
    public PSIValidator() {
    }

    public boolean validateIdentificationNumber(String country,
            String identificationNumber) throws DataException {

        if (identificationNumber != null) {
            if (identificationNumber.equals("")) {
                throw new DataException(Status.getNameByStatus(Status.STATUS_IDENTIFICATION_NUMBER_ERROR));
            } else {
                if (!country.equalsIgnoreCase("SE")) {
                    if (identificationNumber.indexOf("-") != -1) {
                        throw new DataException(Status.getNameByStatus(Status.STATUS_IDENTIFICATION_NUMBER_ERROR));
                    }
                }
            }
        } else {
            throw new DataException(Status.getNameByStatus(Status.STATUS_IDENTIFICATION_NUMBER_ERROR));
        }

        return true;
    }
}
